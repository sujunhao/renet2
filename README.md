# RENET2 -  Accurate Full-text Gene-Disease Relation Extraction with Efficient Iterative Data Expansion

---
![RENET2 logo](/data/image/RENET2_logo.png "RENET2 logo")



## Introduction

Relation extraction is a fundamental task for extracting gene-disease associations from biomedical text. Existing tools have limited capacity, as they can only extract gene-disease associations from single sentences or abstract texts. In this work, we propose RENET2, a deep learning-based relation extraction method, which implements section filtering and ambiguous relations modeling to extract gene-disease associations from full-text articles. RENET2 adopted a novel iterative data expansion strategy to build an annotated full-text dataset to resolve the scarcity of labels on full-text articles.


## installation

```bash
git clone https://sujunhao@bitbucket.org/sujunhao/renet2.git
cd renet2

conda create -n renet2 python=3.7
conda activate renet2

conda install -c conda-forge ruby scikit-learn=0.22.2.post1 pandas=1.0.1 numpy=1.18.1 tqdm=4.42.1
conda install pytorch==1.2.0 cudatoolkit=10.0 -c pytorch

#install genia sentence splitter from: http://www.nactem.ac.uk/y-matsu/geniass/
cd tools
# wget http://www.nactem.ac.uk/y-matsu/geniass/geniass-1.00.tar.gz
tar -xf geniass-1.00.tar.gz
cd geniass
make
cd ../../
```


## running RENET2 model

```
## using --help to check more information
## using --no_cuda if you do not want to use GPUs

cd renet2

# training 10 RENET2 models (optional, trained model already in the models dir)
python train_renet2_ft.py --raw_data_dir ../data/ft_data/ --annotation_info_dir ../data/ft_info --model_dir ../models/ft_models/ --pretrained_model_p ../models/Bst_abs_10  --epochs 10 --models_number 10 --batch_size 60 --have_SiDa ../data/ft_info/ft_base/ft_base --gda_fn_d ../data/ft_gda_train/

# using the trained RENET2 models to predict GDA (using --is_sensitive_mode to enable RENET2-Sensitive mode)
python predict_renet2_ft.py --raw_data_dir ../data/ft_data/ --model_dir ../models/ft_models/ --models_number 10 --batch_size 60 --gda_fn_d ../data/ft_gda/ 

# check predicted GDA
less ../data/ft_gda/gda_rst.tsv

# using 5-folds cross-validataion to test RENET2 performance
python evaluate_renet2_ft_cv.py --epochs 10 --raw_data_dir ../data/ft_data/  --rst_file_prefix ft_base --have_SiDa ../data/ft_info/ft_base/ft_base
```


## Understand Output File
There are 7 columns in the gda_rst.tsv:  


| 1 | 2 | 3 | 4 | 5 | 6 | 7 |
| --- | --- | --- | --- | --- | --- | --- | 
| pmid    | geneId  | diseaseId   |    g_name  | d_name  | prob_avg     |   prob_X |

where 'pmid' is Article PubMed Id, 'geneId' is the Entrez Gene ID (Entrez), 'diseaseId' is the Disease Id (MESH), 'g_name' is the gene name (a ID with multiple names will be seperated by '|'), 'd_name' is the disease name (a ID with multiple names is seperated by '|'), 'prob_avg' is the predicted mean GDP (gene-disease probability) of all 10 models, 
'prob_X' is the predicted GDP of each models.




## Dataset

```
# Abstract dataset
# 		1st round
./data/abs_data/1st
# 		2nd round
./data/abs_data/2nd
# 		dataset obtained from RENET 
./data/abs_data/ori
./data/abs_data/ori_tet

# Full-text dataset
./data/ft_data
```

## Annotated Gene-disease associations based on iterative data expansion strategy
```
# annotated abstract GDA (fisrt round)
less ./data/ann_table/ann_1st.tsv
# annotated abstract GDA (fisrt round)
less ./data/ann_table/ann_2nd.tsv

# annotated full-text GDA (fisrt and second round)
less ./data/ft_info/ft_500_n.tsv
```


## Found gene-disease associations from PMC & LitCovid
```
cd data
tar -xf renet2_gda_rst.tar.gz

# PMC
less pmc/pmd_gda.tsv    
# LitCovid
less litcovid/gda_rst.tsv
```

## benchmark

### run BeFree
```
pip install pymongo
pip install regex
cd benchmark/BeFree
git clone git@bitbucket.org:ibi_group/befree.git
#run Generate_BeFree_Input.ipynb on python jypyter notebook to genrate BeFree input
sh benchmark_befree.sh
```

### run DTMiner
```
cd benchmark/DTMiner
#run Generate_DTMiner_Input.ipynb on python jypyter notebook to genrate BeFree input
sh benchmark_DTMiner.sh 
```

### run BioBERT
```
cd benchmark/BioBERT
git clone https://github.com/dmis-lab/biobert
cd biobert; pip install -r requirements.txt
./download.sh

# generate BioBERT input 
run Generate_BioBERT_Input.ipynb on python jypyter notebook

# run BioBERT
sh run_bert.sh
```

### benchmarking (using RENET2 cross-validation to evalutate RENET2/BeFree/DTMiner/BioBERT results)
```

python evaluate_renet2_ft_cv.py --epochs 10 --raw_data_dir ../data/ft_data/  --rst_file_prefix ft_base --have_SiDa ../data/ft_info/ft_base/ft_base

# benchmarking RENET2 on abstract data
run ./renet2/renet2/exp_abs.ipynb on jypyter notebook
```

## (optional) Data preparing, download/parse data from PubMed/PubTator Central
please provide your list at RENET2/data dir, examples list can be found at RENET2/data/test_download_pmid_list.csv (for abstract) or RENET2/data/test_download_pmcid_list.csv(for full-text)

```
cd renet2

# downloading data
# testing download abstracts data 
python download_data.py --process_n 3 --id_f ../test/test_download_pmcid_list.csv --type abs --dir ../data/raw_data/abs/ 
# testing download full-text data
python download_data.py --process_n 3 --id_f ../test/test_download_pmcid_list.csv --type ft --dir ../data/raw_data/ft/

# parsing data
python parse_data.py --id_f ../test/test_download_pmcid_list.csv --type 'ft' --in_abs_dir ../data/raw_data/abs/  --in_ft_dir ../data/raw_data/ft/ --out_dir ../data/test_data/
# (optional) normalize annotated ID
python normalize_ann.py --in_f ../data/test_data/anns.txt --out_f ../data/test_data/anns_n.txt
```          

## (optional) found and visualze a pair of gene-disease annotation obtrained from Pubtar Central
```
run ./renet2/renet2/vis_text.ipynb on jupyter notebook
```
![RENET2 vis](/data/image/RENET2_vis.png "RENET2 vis")
