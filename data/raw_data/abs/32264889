32264889|t|HPV E6 and E7 oncoproteins cooperatively alter the expression of Disc Large 1 polarity protein in epithelial cells.
32264889|a|BACKGROUND: Persistent infection with high-risk Human Papillomavirus (HPVs) is associated with the development of cervical cancer. The transforming capacity of these viruses relies on the cooperative action of the E6 and E7 viral oncoproteins. Among the oncogenic activities of E6, the interaction and interference with cell polarity PDZ proteins have been well established. One of the most characterized PDZ targets of HPV E6 is human Disc large 1 (DLG1), a scaffolding protein involved in the control of cell polarity and proliferation. Interestingly, in cervical squamous intraepithelial lesions, alterations in DLG1 expression were observed in association to tumour progression. Moreover, the expression of both HPV E6 and E7 proteins may be responsible for the changes in DLG1 abundance and cell localization observed in the HPV-associated lesions. METHODS: Due to the relevance of DLG1 deregulation in tumour development, we have performed an in-depth investigation of the expression of DLG1 in the presence of the HPV oncoproteins in epithelial cultured cells. The effects of HPV E6 and E7 proteins on DLG1 abundance and subcellular localization were assessed by western blot and confocal fluorescence microscopy, respectively. RESULTS: We demonstrated that the relative abundance of HPV-18 E6 and DLG1 is a key factor that contributes to defining the expression abundance of both proteins. We also show here that a high expression level of DLG1 may negatively affect HPV-18 E6 nuclear expression. Moreover, the co-expression of HPV-18 E6 and E7 produces a striking effect on DLG1 subcellular localization and a co-distribution in the cytoplasmic region. Interestingly, HPV-18 E7 is also able to increase DLG1 levels, likely by rescuing it from the E6-mediated proteasomal degradation. CONCLUSIONS: In general, the data suggest that HPV-18 E6 and E7 may have opposing activities in regards to the regulation of DLG1 levels and may cooperatively contribute to its subcellular redistribution in the HPV context. These findings constitute a step forward in understanding the differential expression of DLG1 during tumour progression in an HPV-associated model.
32264889	65	77	Disc Large 1	Gene	1739
32264889	139	148	infection	Disease	MESH:D007239
32264889	230	245	cervical cancer	Disease	MESH:D002583
32264889	552	564	Disc large 1	Gene	1739
32264889	566	570	DLG1	Gene	1739
32264889	673	714	cervical squamous intraepithelial lesions	Disease	MESH:D000081483
32264889	731	735	DLG1	Gene	1739
32264889	779	785	tumour	Disease	MESH:D009369
32264889	893	897	DLG1	Gene	1739
32264889	1003	1007	DLG1	Gene	1739
32264889	1024	1030	tumour	Disease	MESH:D009369
32264889	1109	1113	DLG1	Gene	1739
32264889	1225	1229	DLG1	Gene	1739
32264889	1421	1425	DLG1	Gene	1739
32264889	1564	1568	DLG1	Gene	1739
32264889	1699	1703	DLG1	Gene	1739
32264889	1828	1832	DLG1	Gene	1739
32264889	2034	2038	DLG1	Gene	1739
32264889	2222	2226	DLG1	Gene	1739
32264889	2234	2240	tumour	Disease	MESH:D009369

