This is the implementation of the DTMiner, a reliable and efficient framework that takes large biomedical literature repositories as inputs, extracts credible relationships between diseases and genes from them, and presents the results in the form of possible genes related to a given disease. 

The framework consists of evidence extraction, which finds pairs of co-occurring genes and diseases within the scope of a sentence, evidence evaluation whereby we extract and evaluate features at the semantic and syntactic level and ranking algorithms that display how closely genes are related with a given disease. 

The web service is available through http://gdr-web.rwebox.com/public_html/index.php.