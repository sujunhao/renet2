package rdi.cn.source;

import java.util.List;

public interface LiteratureParser <T>{
	public final int XML = 1;
	public final int HTML = 2;
	
	
	List<T> parser();
	int getFormat();

}
