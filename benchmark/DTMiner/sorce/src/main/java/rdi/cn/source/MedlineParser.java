package rdi.cn.source;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import rdi.cn.misc.XMLUtil;
import cn.edu.sjtu.gdr.config.ConfigManager;

import com.sun.org.apache.xpath.internal.XPathAPI;

public class MedlineParser implements LiteratureParser<MedlineItem> {
	public static final String AUTHOR_ALL_TXT = "author_all.txt";
	public static final String META_ALL_TXT = "meta_all.txt";
	private List<MedlineItem> items;
	private InputStream inputStream;
	private Document content;
	private static final String TITLE_EXP = "Article/ArticleTitle/text()";
	private static final String TEXT_EXP = "Article/Abstract/AbstractText";
	private static final String AUT_EXP = "Article/AuthorList/Author";
	// private static final String AFF_EXP =
	// "Article/AuthorList/Author/Affiliation";
	private static final String JOURNAL_EXP = "Article/Journal";
	private static final String BACKGROUND = "BACKGROUND";
	private static final String METHODS = "METHODS";
	private static final String RESULTS = "RESULTS";
	private static final String CONCLUSIONS = "CONCLUSIONS";
	private static final String OBJECTIVE = "OBJECTIVE";

	private static XPath xPath;
	private static XPathExpression titleXPATH;
	private static XPathExpression textXPATH;
	private static XPathExpression authXPATH;
	private static XPathExpression jounalXPATH;
	private String sourceName;
	private Pattern yearMonthPattern = Pattern
			.compile("^[1-2][9|0][0-9]{2}?\\w{3}?");

	public InputStream getInputStream() {
		return inputStream;
	}

	public void setSourceFileName(String sourceName) {
		this.sourceName = sourceName;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public List<MedlineItem> getItems() {
		return items;
	}

	public void setItems(List<MedlineItem> items) {
		this.items = items;
	}

	public void init() {
		if (xPath == null) {
			xPath = XPathFactory.newInstance().newXPath();
			try {
				titleXPATH = xPath.compile(TITLE_EXP);
				textXPATH = xPath.compile(TEXT_EXP);
				authXPATH = xPath.compile(AUT_EXP);
				jounalXPATH = xPath.compile(JOURNAL_EXP);
			} catch (XPathExpressionException e) {
				e.printStackTrace();
			}
		}
	}

	public Itr iterator() {
		try {
			init();
			content = XMLUtil.getDocument(inputStream);
			NodeList citations = XPathAPI.selectNodeList(content,
					"/MedlineCitationSet/MedlineCitation");
			return new Itr(citations, sourceName);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
		System.exit(1);
		return null;
	}

	public List<MedlineItem> parser() {
		List<MedlineItem> items = new ArrayList<MedlineItem>();
		try {
			init();
			content = XMLUtil.getDocument(inputStream);
			NodeList citations = XPathAPI.selectNodeList(content,
					"/MedlineCitationSet/MedlineCitation");
			for (int i = 0; i < citations.getLength(); i++) {
				Node node = citations.item(i);
				Node clone = node.cloneNode(true);
				MedlineItem item = parseMedlineItem(clone, sourceName);
//				MedlineItem item = getMedlineItemMeta(clone);
//				clone.getParentNode().removeChild(clone);
//				disposeNode(clone);
				items.add(item);
			}

		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.items = items;
		return items;

	}

	// private void disposeNode(Node node) throws ParserConfigurationException {
	// Document document = XMLUtil.getDocument();
	// document.adoptNode(node);
	//
	// }

    /*
	private MedlineItem getMedlineItemMeta(Node node)
			throws XPathExpressionException, TransformerException {
		MedlineItem item = new MedlineItem();
		item.setSourceFile(sourceName);
		Node pmid = XPathAPI.selectSingleNode(node, "PMID");
		// System.out.println(pmid.getTextContent());
		item.setPmid(pmid.getTextContent());// XPathAPI.selectSingleNode(node,
											// "Article/ArticleTitle").getTextContent()//
		item.setMetadata(parseJournal((Node) jounalXPATH.evaluate(node,
				XPathConstants.NODE)));
		item.setAuthors(parseAuthor((NodeList) authXPATH.evaluate(node,
				XPathConstants.NODESET)));
		return item;
	}
	*/

	private List<AuthorMetadata> parseAuthor(NodeList nodes) {
		List<AuthorMetadata> authors = new ArrayList<AuthorMetadata>();
		for (int i = 0; i < nodes.getLength(); i++) {
			AuthorMetadata author = new AuthorMetadata();
			authors.add(author);
			NodeList authorAttrs = nodes.item(i).getChildNodes();
			for (int j = 0; j < authorAttrs.getLength(); j++) {
				Node authorAttr = authorAttrs.item(j);
				String name = authorAttr.getNodeName();
				if ("LastName".equals(name)) {
					author.setLastName(authorAttr.getTextContent());
				} else if ("ForeName".equals(name)) {
					author.setForeName(authorAttr.getTextContent());
				} else if ("Initials".equals(name)) {
					author.setInitials(authorAttr.getTextContent());
				} else if ("Affiliation".equals(name)) {
					author.setAffiliation(authorAttr.getTextContent());
				} else if ("CollectiveName".equals(name)) {
					author.setCollectiveName(authorAttr.getTextContent());
				}
			}

		}
		return authors;

	}

	private PaperMetadata parseJournal(Node node) {
		PaperMetadata metaData = new PaperMetadata();
		PublishType ptype = new PublishType();
		if (node != null) {
			NodeList nodes = node.getChildNodes();
			for (int i = 0; i < nodes.getLength(); i++) {
				Node n = nodes.item(i);
				String name = n.getNodeName();
				if ("ISSN".equals(name)) {
					ptype.setIssn(n.getTextContent());
				} else if ("JournalIssue".equals(name)) {
					parseIssue(n, ptype);
				} else if ("Title".equals(name)) {
					ptype.setJournal(n.getTextContent());
				}
			}
		}
		metaData.setJournal(ptype);
		return metaData;
	}

	private void parseIssue(Node node, PublishType ptype) {
		if (node != null) {
			NodeList nodes = node.getChildNodes();
			for (int i = 0; i < nodes.getLength(); i++) {
				Node n = nodes.item(i);
				String name = n.getNodeName();
				if ("Volume".equals(name)) {
					ptype.setVolume(n.getTextContent());
				} else if ("Issue".equals(name)) {
					ptype.setIssue(n.getTextContent());
				} else if ("PubDate".equals(name)) {
					parseDate(n, ptype);
				}
			}

		}

	}

	private void parseDate(Node node, PublishType ptype) {
		int year = 0, month = 0, day = 0;
		if (node != null) {
			NodeList nodes = node.getChildNodes();
			for (int i = 0; i < nodes.getLength(); i++) {
				Node n = nodes.item(i);
				String name = n.getNodeName();
				if ("Year".equals(name)) {
					year = Integer.parseInt(n.getTextContent());
				} else if ("Month".equals(name)) {
					month = parseMonth(n.getTextContent());
				} else if ("Day".equals(name)) {
					day = Integer.parseInt(n.getTextContent());
					;
				} else if ("Season".equals(name)) {
					month = parseSeason(n.getTextContent());
				} else if ("MedlineDate".equals(name)) {
					String medlineDate = node.getTextContent();
					// if(medlineDate.contains(", ")){
					// medlineDate = medlineDate.trim().replaceAll(", ",",");
					// }
					String[] yearMonth = medlineDate.split(" ");
					if (yearMonth.length > 0) {
						if (yearMonth[0].contains("-")) {
							String[] years = yearMonth[0].split("-");
							int startYear = Integer.parseInt(years[0].trim());
							int endYear = Integer.parseInt(years[1].trim());
							year = (startYear + endYear) / 2;
						} else if (yearMonthPattern
								.matcher(yearMonth[0].trim()).find()) {
							year = Integer.parseInt(yearMonth[0].trim()
									.substring(0, 4));
							month = parseMonth(yearMonth[0].trim().substring(4)
									.trim());
						} else if (yearMonth[0].contains(",")) {
							String[] years = yearMonth[0].split(",");
							int startYear = Integer.parseInt(years[0].trim());
							int endYear = Integer.parseInt(years[1].trim());
							year = (startYear + endYear) / 2;
						} else
							year = Integer.parseInt(yearMonth[0].trim());
						if (yearMonth.length > 1) {
							String[] months = yearMonth[1].split("-");
							int[] numMonth = new int[months.length];
							parseMonth(months, numMonth);
							int start = numMonth[0];
							int end = numMonth[numMonth.length - 1];
							month = (int) ((start + end) / 2);
							if ((start + end) % 2 == 1) {
								month++;
								day = 15;
							}

						}
					}
				}
			}
			Calendar calendar = Calendar.getInstance();
			calendar.set(year, month - 1, day);
			ptype.setPubDate(calendar.getTime());
		}
	}

	private int parseSeason(String season) {
		if ("Spring".equalsIgnoreCase(season)) {
			return 2;
		} else if ("Summer".equalsIgnoreCase(season)) {
			return 5;
		} else if ("Autumn".equalsIgnoreCase(season)
				|| "Fall".equalsIgnoreCase(season)) {
			return 8;
		} else if ("Winter".equalsIgnoreCase(season)) {
			return 11;
		}
		System.out.println("Season: " + season);
		return 5;
	}

	private int parseMonth(String month) {
		if ("Jan".equalsIgnoreCase(month)) {
			return 1;
		} else if ("Feb".equalsIgnoreCase(month)) {
			return 2;
		} else if ("Mar".equalsIgnoreCase(month)) {
			return 3;
		} else if ("Apr".equalsIgnoreCase(month)) {
			return 4;
		} else if ("May".equalsIgnoreCase(month)) {
			return 5;
		} else if ("Jun".equalsIgnoreCase(month)) {
			return 6;
		} else if ("Jul".equalsIgnoreCase(month)) {
			return 7;
		} else if ("Aug".equalsIgnoreCase(month)) {
			return 8;
		} else if ("Sep".equalsIgnoreCase(month)) {
			return 9;
		} else if ("Oct".equalsIgnoreCase(month)) {
			return 10;
		} else if ("Nov".equalsIgnoreCase(month)) {
			return 11;
		} else if ("Dec".equalsIgnoreCase(month)) {
			return 12;
		}
		return 0;
	}

	private void parseMonth(String[] months, int[] numMonth) {
		for (int i = 0; i < months.length; i++) {
			String month = months[i];
			if ("Jan".equalsIgnoreCase(month)) {
				numMonth[i] = 1;
			} else if ("Feb".equalsIgnoreCase(month)) {
				numMonth[i] = 2;
			} else if ("Mar".equalsIgnoreCase(month)) {
				numMonth[i] = 3;
			} else if ("Apr".equalsIgnoreCase(month)) {
				numMonth[i] = 4;
			} else if ("May".equalsIgnoreCase(month)) {
				numMonth[i] = 5;
			} else if ("Jun".equalsIgnoreCase(month)) {
				numMonth[i] = 6;
			} else if ("Jul".equalsIgnoreCase(month)) {
				numMonth[i] = 7;
			} else if ("Aug".equalsIgnoreCase(month)) {
				numMonth[i] = 8;
			} else if ("Sep".equalsIgnoreCase(month)) {
				numMonth[i] = 9;
			} else if ("Oct".equalsIgnoreCase(month)) {
				numMonth[i] = 10;
			} else if ("Nov".equalsIgnoreCase(month)) {
				numMonth[i] = 11;
			} else if ("Dec".equalsIgnoreCase(month)) {
				numMonth[i] = 12;
			}
		}

	}

	private static MedlineItem parseMedlineItem(Node node, String sourceName)
			throws XPathExpressionException, TransformerException {
		MedlineItem item = new MedlineItem();
		item.setSourceFile(sourceName);
		Node pmid = XPathAPI.selectSingleNode(node, "PMID");
		// System.out.println(pmid.getTextContent());
		item.setPmid(pmid.getTextContent());// XPathAPI.selectSingleNode(node,
											// "Article/ArticleTitle").getTextContent()//
		String title = (String) titleXPATH
				.evaluate(node, XPathConstants.STRING);
		item.setTitle(title);
		NodeList texts = (NodeList) textXPATH.evaluate(node,
				XPathConstants.NODESET);
		StringBuilder textBuilder = new StringBuilder();
		StringBuilder backgroundContent = new StringBuilder();
		StringBuilder methodContent = new StringBuilder();
		StringBuilder resultContent = new StringBuilder();
		StringBuilder conclusionContent = new StringBuilder();
		StringBuilder objectiveContent = new StringBuilder();
		for (int i = 0; i < texts.getLength(); i++) {
			Node textNode = texts.item(i);
			if (textNode instanceof Element) {
				Element textElement = (Element) textNode;
				String textContent = textElement.getTextContent();
				textBuilder.append(textContent).append("\n");
				String category = textElement.getAttribute("NlmCategory");
				if (BACKGROUND.equalsIgnoreCase(category)) {
					backgroundContent.append(textContent).append("\n");
				} else if (METHODS.equalsIgnoreCase(category)) {
					methodContent.append(textContent).append("\n");
				} else if (RESULTS.equalsIgnoreCase(category)) {
					resultContent.append(textContent).append("\n");
				} else if (CONCLUSIONS.equalsIgnoreCase(category)) {
					conclusionContent.append(textContent).append("\n");
				} else if (OBJECTIVE.equalsIgnoreCase(category)) {
					objectiveContent.append(textContent).append("\n");
				}

			}
			item.setText(textBuilder.toString().trim());
			item.setBackground(backgroundContent.toString().trim());
			item.setResults(resultContent.toString().trim());
			item.setMethods(methodContent.toString().trim());
			item.setConclusions(conclusionContent.toString().trim());
			item.setObjective(objectiveContent.toString().trim());
		}
		// TODO get author
		// Element authorList = (Element) authXPATH.evaluate(node,
		// XPathConstants.NODE);
		// NodeList authors = authorList.getElementsByTagName("Author");
		// for(int i=0;i<authors.getLength();i++){
		// Element author = (Element) authors.item(i);
		// NodeList affs = author.getElementsByTagName("Affiliation");
		// if(affs.getLength()>0){
		// item.setAuthors(authors)
		// }
		// }
		return item;
	}

	public int getFormat() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void clear() {
		items.clear();

	}

	public static void main(String[] args) throws IOException {
		MedlineParser mp = new MedlineParser();
		mp.init();
		File filedir = new File(ConfigManager.getConfig(
				ConfigManager.MAIN_CONFIG, "doc-dir", "MEDLINE_DIR"));
		extractMetadata(mp, filedir);

		System.out.println("Parse finished!");
		System.out.println("Writer Closed");

	}

	private static void extractMetadata(MedlineParser mp, File filedir)
			throws IOException, FileNotFoundException {
		long sysdate = System.currentTimeMillis();
		File[] files = filedir.listFiles(new FileFilter() {
			public boolean accept(File pathname) {
				return pathname.getName().endsWith(".gz");
			}
		});
		Arrays.sort(files, new Comparator<File>() {

			public int compare(File o1, File o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		List<MedlineItem> items = new ArrayList<MedlineItem>();
		BufferedWriter bw = new BufferedWriter(new FileWriter(new File(filedir,
				META_ALL_TXT)));
		BufferedWriter bw2 = new BufferedWriter(new FileWriter(new File(
				filedir, AUTHOR_ALL_TXT)));
		for (File file : files) {
			GZIPInputStream inputStream = new GZIPInputStream(
					new FileInputStream(file));
			mp.setInputStream(inputStream);
			System.out.println("Parsing ... " + file.getName());

			mp.setSourceFileName(file.getName());
			items.addAll(mp.parser());
			writeItems(bw, bw2, items);
			items.clear();
			mp.clear();
			inputStream.close();

			System.out.println("Parsed ... " + file.getName() + "\t"
					+ (System.currentTimeMillis() - sysdate));

		}
		bw.close();
		bw2.close();
	}

	private static void writeItems(BufferedWriter bw, BufferedWriter bw2,
			List<MedlineItem> items) throws IOException {
		for (MedlineItem item : items) {

			PaperMetadata articleMeta = item.getMetadata();
			if (articleMeta != null) {
				bw.write(item.getPmid());
				bw.write("\t");
				// if(articleMeta.getPubModel()!=null)
				// bw.write(articleMeta.getPubModel());
				// bw.write("\t");
				// if(articleMeta.getDateCreated()!=null)
				// bw.write(DateFormatUtils.format(articleMeta.getDateCreated(),"yyyy-mm-dd"));
				// bw.write("\t");
				// if(articleMeta.getDateRevised()!=null)
				// bw.write(DateFormatUtils.format(articleMeta.getDateRevised(),"yyyy-mm-dd"));
				// bw.write("\t");
				// if(articleMeta.getDateCompleted()!=null)
				// bw.write(DateFormatUtils.format(articleMeta.getDateCompleted(),"yyyy-mm-dd"));
				// bw.write("\t");
				if (articleMeta.getJournal() != null
						&& articleMeta.getJournal().getPubDate() != null)
					bw.write(DateFormatUtils.format(articleMeta.getJournal()
							.getPubDate(), "yyyy-MM-dd"));
				bw.write("\t");
				if (articleMeta.getJournal() != null
						&& articleMeta.getJournal().getJournal() != null)
					bw.write(articleMeta.getJournal().getJournal());
				bw.write("\n");
			}

            /*
			List<AuthorMetadata> authors = item.getAuthors();
			if (authors != null && authors.size() > 0) {
				for (AuthorMetadata author : authors) {
					bw2.write(item.getPmid());
					bw2.write("\t");
					if (author.getAffiliation() != null)
						bw2.write(author.getAffiliation());
					bw2.write("\t");
					if (author.getIdentifier() != null)
						bw.write(author.getIdentifier());
					bw2.write("\t");
					if (author.getForeName() != null)
						bw2.write(author.getForeName());
					bw2.write("\t");
					if (author.getLastName() != null)
						bw2.write(author.getLastName());
					bw2.write("\t");
					if (author.getInitials() != null)
						bw2.write(author.getInitials());
					bw2.write("\t");
					if (author.getCollectiveName() != null)
						bw2.write(author.getCollectiveName());
				}
				bw2.write("\n");
			}
			*/

		}
		bw.flush();
		bw2.flush();

	}

	private class Itr implements Iterator<MedlineItem> {
		private NodeList citations_;
		private int index_, length_;
		private String sourceName_;

		Itr(NodeList citations, String sourceName) {
			citations_ = citations;
			index_ = 0;
			sourceName_ = sourceName;
			length_ = citations_.getLength();
		}

		public boolean hasNext() {
			return index_ != length_;
		}

		public MedlineItem next() {
			try {
				if (!hasNext()) {
					throw new RuntimeException("no more items");
				}
				Node node = citations_.item(index_++);
				Node clone = node.cloneNode(true);
				MedlineItem item = MedlineParser.parseMedlineItem(clone, sourceName_);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (TransformerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		private MedlineItem getMedlineItemMeta(Node node) {
			MedlineItem item = new MedlineItem();
			item.setSourceFile(sourceName);
			Node pmid;
			try {
				pmid = XPathAPI.selectSingleNode(node, "PMID");
				// System.out.println(pmid.getTextContent());
				item.setPmid(pmid.getTextContent());// XPathAPI.selectSingleNode(node,
				// "Article/ArticleTitle").getTextContent()//
				item.setMetadata(parseJournal((Node) jounalXPATH.evaluate(node,
						XPathConstants.NODE)));
				//item.setAuthors(parseAuthor((NodeList) authXPATH.evaluate(node,
				//		XPathConstants.NODESET)));
			} catch (TransformerException e) {
				e.printStackTrace();
				System.exit(1);
			} catch (XPathExpressionException e) {
				e.printStackTrace();
				System.exit(1);
			}
			return item;
		}
	}

}
