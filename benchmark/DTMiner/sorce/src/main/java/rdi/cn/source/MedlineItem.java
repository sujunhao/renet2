package rdi.cn.source;

import java.util.List;

public class MedlineItem {
	private String title;
	private String text;
	private String background;
	private String methods;
	private String results;
	private String conclusions;
	private String objective;
	private String department;
	private String pmid;
	private String sourceFile;
	// private PaperMetadata articleMeta;

	//private List<AuthorMetadata> authors;
    private List<String> authors;

	private List<String> keywords;

	private PaperMetadata metadata;

	public String getSourceFile() {
		return sourceFile;
	}

	public void setSourceFile(String sourceFile) {
		this.sourceFile = sourceFile;
	}

	//public List<AuthorMetadata> getAuthors() {
	//	return authors;
	//}
    public List<String> getAuthors(){ return authors;}

	//public void setAuthors(List<AuthorMetadata> authors) {
	//	this.authors = authors;
	//}
    public void setAuthors(List<String> authors) {this.authors = authors;}

	public String getBackground() {
		return background;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getPmid() {
		return pmid;
	}

	public void setPmid(String pmid) {
		this.pmid = pmid;
	}

	public void setBackground(String background) {
		this.background = background;
	}

	public String getMethods() {
		return methods;
	}

	public void setMethods(String methods) {
		this.methods = methods;
	}

	public String getResults() {
		return results;
	}

	public void setResults(String results) {
		this.results = results;
	}

	public String getConclusions() {
		return conclusions;
	}

	public void setConclusions(String conclusions) {
		this.conclusions = conclusions;
	}

	public String getObjective() {
		return objective;
	}

	public void setObjective(String objective) {
		this.objective = objective;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public List<String> getKeywords() {
		return keywords;
	}

	public void setKeywords(List<String> keywords) {
		this.keywords = keywords;
	}

	public PaperMetadata getMetadata() {
		return metadata;
	}

	public void setMetadata(PaperMetadata metadata) {
		this.metadata = metadata;
	}

	@Override
	public String toString() {
		return this.pmid;
	}
}
