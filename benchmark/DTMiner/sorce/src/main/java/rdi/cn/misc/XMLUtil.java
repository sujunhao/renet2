package rdi.cn.misc;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.Locale;
import java.util.Properties;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl;
import com.sun.org.apache.xerces.internal.util.XMLChar;
import com.sun.org.apache.xpath.internal.XPathAPI;

/**
 * Static utility methods for XML handling.
 */
public class XMLUtil {

	public static final String DEFAULT_ENCODING = "UTF-8";

	private static DocumentBuilder getDocumentBuilder() throws ParserConfigurationException {
//		DocumentBuilderFactory factory = new DocumentBuilderFactoryImpl();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		factory.setValidating(false);
		factory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
		factory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);

		factory.setNamespaceAware(true);
		factory.setValidating(false);
        return factory.newDocumentBuilder();
	}

    public static Document getDocument() throws ParserConfigurationException {
        DocumentBuilder builder = getDocumentBuilder();
        return builder.newDocument();
    }

    public static String Document2String(Node d) throws IOException, UnsupportedEncodingException {
        try {
            ByteArrayOutputStream output = new ByteArrayOutputStream();

            write(d, output);

            String s = output.toString(DEFAULT_ENCODING);

            return s;
        } catch (Exception e) {
            e.printStackTrace();
        }
 
        return null;
    }

    public static Document getDocument(InputStream is) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilder db = getDocumentBuilder();
        Document doc = db.parse(is);
        return doc;
    }
    
    public static Document getDocument(Reader reader) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilder db = getDocumentBuilder();
    	InputSource is = new InputSource(reader);
        Document doc = db.parse(is);
        return doc;
    }

    public static Document getDocument(String contents) throws ParserConfigurationException, SAXException, IOException {
    	StringReader reader = new StringReader(contents);
    	return getDocument(reader);
    }

    /**
     * @deprecated Use getDocument instead
     */
    public static Document String2Document(String s) throws ParserConfigurationException, SAXException, IOException {
        return getDocument(s);
    }

    public static String createXMLStringWithQuotes(String str) {
        StringBuffer sb = new StringBuffer();

        sb.append("\"");
        xmlString(str, sb);
        sb.append("\"");

        return sb.toString();
    }

    public static String createXMLStringWithoutQuotes(String str) {
        StringBuffer sb = new StringBuffer();

        xmlString(str, sb);

        return sb.toString();
    }

    public static void xmlString(String str, StringBuffer sb) {
        // Special case for empty string converted to one space string
        if (str.trim().length() == 0) {
            sb.append(" ");

            return;
        }

        // -- then dump the data
        final int MAX_SIZE = 250; //
        final String forbiddenCharacters = " \"'&<>";
        final String[] replacements = new String[] { "&#32;", "&quot;", "&apos;", "&amp;", "&lt;", "&gt;" };

        int size = 0;

        for (int i = 0; i < str.length(); i++) {
            int code = str.charAt(i);

            if (str.charAt(i) == '_') {
                sb.append(str.charAt(i));
            } else if (((code >= 91) && (code <= 96)) || (code >= 123)) {
                sb.append("&#");
                sb.append(code);
                sb.append(";");
            } else {
                int index = forbiddenCharacters.indexOf(str.charAt(i));

                if (index == -1) {
                    if ((size + 1) >= MAX_SIZE) {
                        break;
                    }

                    sb.append(str.charAt(i));
                    size++;
                } else {
                    if ((size + replacements[index].length()) >= MAX_SIZE) {
                        break;
                    }

                    sb.append(replacements[index]);
                    size += replacements[index].length();
                }
            }
        }
    }

    public static void write(Node d, OutputStream s) throws TransformerException, TransformerConfigurationException {
        write(d, s, true);
    }

    public static void write(Node d, OutputStream s, boolean omit_xml_decl) throws TransformerException, TransformerConfigurationException {
        TransformerFactory tfactory = TransformerFactory.newInstance();
        String omit_xml_decl_string = (omit_xml_decl) ? "yes" : "no";
        String indent = (!omit_xml_decl) ? "yes" : "no";

        // This creates a transformer that does a simple identity transform,
        // and thus can be used for all intents and purposes as a serializer.
        Transformer serializer = tfactory.newTransformer();

        Properties oprops = new Properties();

        oprops.put(OutputKeys.METHOD, "xml");
        oprops.put(OutputKeys.INDENT, indent);
        oprops.put(OutputKeys.OMIT_XML_DECLARATION, omit_xml_decl_string);
		oprops.put(OutputKeys.ENCODING, DEFAULT_ENCODING);

        try {
            String ss = checkNullAtt(d);

            if (ss != null) {
                System.err.println("Found null att: \n" + ss);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        serializer.setOutputProperties(oprops);
        serializer.transform(new DOMSource(d), new StreamResult(s));
    }

    /**
     * Check for null attributes or invalid xml attribute characters within a Node.
     * Invalid characters are replaced by this method.
     * @param e
     * @return An error string for any invalid characters, or null if OK.
     */    
    public static String checkNullAtt(Node e) {
        NamedNodeMap nm = e.getAttributes();

        for (int i = 0; (nm != null) && (i < nm.getLength()); i++) {
            Attr att = (Attr) nm.item(i);
            String val = att.getValue();
            if (val == null) {
                return " Node (" + e.getNodeName() + ") attribute (" + att.getName() + ") value is null ";
            }
            for (int c = 0; c < val.length(); c++) {
            	if (XMLChar.isInvalid(val.charAt(c))) {
            		att.setValue("");
                    return " Node (" + e.getNodeName() + ") attribute (" + att.getName() + ") value was invalid ";
            	}
            }
        }

        NodeList list = e.getChildNodes();

        for (int i = 0; (list != null) && (i < list.getLength()); i++) {
            Node n = list.item(i);
            String s = checkNullAtt(n);

            if (s != null) {
                return "Node (" + n.getNodeName() + ") " + s;
            }
        }

        return null;
    }

    public static String checkNullAtt(Document doc) {
        Element list = doc.getDocumentElement();

        return checkNullAtt(list);
    }

    static public Object getFirstImmediateChildrenByElementName(Node node, String element_name) {
        Vector<Node> v = getImmediateChildrenByElementName(node, element_name);

        if ((v == null) || (v.size() == 0)) {
            return null;
        }

        return v.get(0);
    }

    static public Vector<Node> getImmediateChildrenByElementName(Node node, String element_name) {
        Vector<Node> matching_elements = new Vector<Node>();
        NodeList children = node.getChildNodes();
        int num_children = children.getLength();

        for (int i = 0; i < num_children; i++) {
            Node child = children.item(i);

            if (child instanceof Element) {
                Element e = (Element) child;
                String e_name = e.getTagName();

                if (e_name.equals(element_name)) {
                    matching_elements.add(child);
                }
            }
        }

        return matching_elements;
    }

    /**
     * Calling System.out.println() will try to convert the Unicode to native
     * encoding. This will generally be okay unless your current font does not
     * support Unicode. So avoid using that if your purpose is to examine the
     * actual Unicode values. The following code will help you escape a Java
     * string in \\uXXXX format to generated 7-bit ASCII strings.
     *
     * @param str The Unicode string
     * @param escapeAscii true to perform escaping.
     * @return The escaped String
     */
    public static String escapeUnicodeString(String str, boolean escapeAscii) {
        String ostr = new String();

        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);

            if (!escapeAscii && ((ch >= 0x0020) && (ch <= 0x007e))) {
                ostr += ch;
            } else {
                ostr += "\\u";

                String hex = Integer.toHexString(str.charAt(i) & 0xFFFF);

                if (hex.length() == 2) {
                    ostr += "00";
                }

                ostr += hex.toUpperCase(Locale.ENGLISH);
            }
        }

        return (ostr);
    }

    public static Node selectSingleNode(Reader reader, String xpath) {
        try {
	        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	        DocumentBuilder db = dbf.newDocumentBuilder();
	        InputSource input_source = new InputSource(reader);
	        Document doc = db.parse(input_source);
	        return XPathAPI.selectSingleNode(doc, xpath);
        } catch (Exception e) {
            return null;
        }
    }

    public static NodeList selectNodeList(Reader reader, String xpath) {
        try {
	        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	        DocumentBuilder db = dbf.newDocumentBuilder();
	        InputSource input_source = new InputSource(reader);
	        Document doc = db.parse(input_source);
	        return XPathAPI.selectNodeList(doc, xpath);
        } catch (Exception e) {
            return null;
        }
    }

	public static void setTextContent(Document doc, Element element, String value) {
		try{
			element.setTextContent(value);
		}catch(Exception e){
			Node tNode = doc.createTextNode(value);
			element.appendChild(tNode);
		}catch(Error e){
			Node tNode = doc.createTextNode(value);
			element.appendChild(tNode);
		}
		
	}
}