package rdi.cn.misc;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

public class ExtractANSI {
	private List<AnsiItem> items = new ArrayList<AnsiItem>();
	private AnsiItem startItem;
	private Map<String, AnsiItem> itemMap = new HashMap<String, AnsiItem>();
	private Map<String, String> existingPair = new LinkedHashMap<String, String>();
	
	public static void main(String [] args) throws IOException{
		ExtractANSI extractor = new ExtractANSI();
		extractor.readFile("C:\\Users\\kvnx632.RD\\Desktop\\New folder (2)\\uGene.ansi", "ABCDEFG12345");
		
		extractor.writeFile("C:\\Users\\kvnx632.RD\\Desktop\\New folder (2)\\GeneZCS.txt");

	}
	
	public void readFile(String filename, String startItem) throws IOException{
		File file = new File(filename);
		int max = 33;
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line;
		
		AnsiItem currentItem = null;
		while((line=br.readLine())!=null){
			String itemName = line.trim();
			if(!line.startsWith(" ")){
				if(line.startsWith("SID")){
					continue;
				}else{
					if(currentItem!=null){
						items.add(currentItem);
					}
					currentItem = new AnsiItem(itemName);
					itemMap.put(itemName, currentItem);
					if(startItem.equalsIgnoreCase(itemName)){
						this.startItem = currentItem;
					}
				}
			}else if(currentItem!=null){
				line = itemName;
				if(line.startsWith("PT")){
					currentItem.setPrimaryTermline(line);
					if(StringUtils.countMatches(line, " ")>max){
						max=StringUtils.countMatches(line, " ");
						System.out.println(line);
					}
				}else if(line.startsWith("SYN#ZCS")){
					currentItem.addZCS(line);
				}else if(line.startsWith("SYN")){
					currentItem.addSynonymline(line);
				}else if(line.startsWith("NT")){
					currentItem.addNarrowline(line);
				}
			}
		}
		br.close();
		System.out.println(max);
		
	}
	
	public void writeFile(String outfile) throws IOException{
		BufferedWriter bw = new BufferedWriter(new FileWriter(outfile));
		if(this.startItem==null)
			printZCS(items, bw);
		else
			printFromStart(startItem, bw);
		bw.close();
	}

	private void printFromStart(AnsiItem startItem, Writer bw) throws IOException {
		Set<AnsiItem> oItem = new HashSet<AnsiItem>(getDescendant(startItem));
		oItem.add(startItem);
		printWithKey(oItem, bw);
		//quickPrintItems(items, bw);
		
	}
	
	private Set<AnsiItem> getDescendant(AnsiItem parent){
		Set<AnsiItem> items = new HashSet<AnsiItem>();
		for(String itemName:parent.getNarrawLine()){
			AnsiItem item = itemMap.get(itemName);
			if(item==null){
				System.err.println("No item:"+itemName);
			}
			items.add(item);
			items.addAll(getDescendant(item));
		}
		return items;
	}

	private void quickPrintItems(Collection<AnsiItem> items, Writer wr) throws IOException {
		for(AnsiItem item:items){
			if(item.getPrimaryTerm()==null)continue;
			wr.write(item.toMultiLineWithKeyString());
			wr.write("\n");
			
		}
	}
	
	private void quickPrintLong(Collection<AnsiItem> items, Writer wr) throws IOException {
		for(AnsiItem item:items){
			if(item.getPrimaryTerm()==null)continue;
			wr.write(item.toMultiLineString());
			wr.write("\n");
		}
		
	}
	
	private void printZCS(Collection<AnsiItem> items, Writer wr) throws IOException{
		for(AnsiItem item:items){
			if(item.getPrimaryTerm()==null)continue;
			List<String> zcss = item.getZCS();
			for(String zcs:zcss){
				wr.write(zcs);
				wr.write("\n");
			}
		}
	}
	
	
	private void printWithKey(Collection<AnsiItem> items, Writer wr) throws IOException{
		
		for(AnsiItem item:items){
			if(item.getPrimaryTerm()==null)continue;
//			Set<String> keys = new HashSet<String>(existingPair.keySet());
			Map<String, String> newMap = item.getMap();
//			keys.retainAll(newMap.keySet());
//			for(String key:keys){
//				wr.write(key);
//				wr.write("\t");
//				wr.write(existingPair.get(key));
//				wr.write("\n");
//				wr.write(key);
//				wr.write("\t");
//				wr.write(newMap.get(key));
//				wr.write("\n");
//				
//			}
			
			existingPair.putAll(newMap);
		}
		for(String key:existingPair.keySet()){
			wr.write(key);
			wr.write("\t");
			wr.write(existingPair.get(key));
			wr.write("\n");
		}
//		for(AnsiItem item:items){
//			if(item.getPrimaryTerm()==null)continue;
//			wr.write(item.getPrimaryTerm());
//			wr.write("\t");
//			wr.write(item.getName());
//			wr.write("\n");
//		}
	}

}
