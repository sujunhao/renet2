package rdi.cn.source;

import java.util.Date;

public class PaperMetadata {
	private Date dateCreated;
	private Date dateCompleted;
	private Date dateRevised;
	private String pubModel;
	private PublishType journal;
	
	public Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	public Date getDateCompleted() {
		return dateCompleted;
	}
	public void setDateCompleted(Date dateCompleted) {
		this.dateCompleted = dateCompleted;
	}
	public Date getDateRevised() {
		return dateRevised;
	}
	public void setDateRevised(Date dateRevised) {
		this.dateRevised = dateRevised;
	}
	public String getPubModel() {
		return pubModel;
	}
	public void setPubModel(String pubModel) {
		this.pubModel = pubModel;
	}
	public PublishType getJournal() {
		return journal;
	}
	public void setJournal(PublishType journal) {
		this.journal = journal;
	}

}
