package rdi.cn.source;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

public class MatchFileList {

	public static void main(String[] args) throws IOException {
		List<String> nameList = readList("H:\\temp\\list2.processed");
		List<String> fileList = readZipFiles("H:\\temp\\tempWHO\\30k", 8000);
		Set<String> fileSet = new HashSet<String>(fileList);
		System.out.println(nameList.size());
		System.out.println(fileSet.size());
		nameList.removeAll(fileSet);
		// int allSize = nameList.size();
		// for (int i = 0; i < 3; i++) {
		// File newFile = new File("H:\\temp\\tempWHO", "part"+(i+201)+".sh");
		// BufferedWriter bw = new BufferedWriter(new FileWriter(newFile));
		// bw.write("#!/bin/bash\n");
		// for (int j=0;j<2000&&j+i*2000<allSize;j++) {
		// String filename = nameList.get(i*2000+j);
		// bw.write("wget --user=\"trialuser\" --password=\\!trialu5er  \"http://apps.who.int/trialsearch/trial.aspx?trialid=");
		// bw.write(filename);
		// bw.write("\" -O ");
		// bw.write(filename.replaceAll("/", ""));
		// bw.write(".html");
		// bw.write("\n");
		// }
		// bw.close();
		// }
		System.out.println(fileList.size());
		System.out.println(nameList.size());

	}

	private static List<String> readZipFiles(String dirStr, int size)
			throws ZipException, IOException {
		File dir = new File(dirStr);
		List<String> nameList = new ArrayList<String>();
		File[] files = dir.listFiles(new FilenameFilter() {

			public boolean accept(File dir, String name) {
				return name.endsWith(".zip");
			}

		});
			for (File singleFile : files) {
				ZipFile zipfile = new ZipFile(singleFile);
				Enumeration<? extends ZipEntry> entries = zipfile.entries();
				for (; entries.hasMoreElements();) {
					ZipEntry whoFile = entries.nextElement();
					if (!whoFile.isDirectory() && whoFile.getSize() > size) {
						String name = whoFile.getName();
						//System.out.println(name);
						int startIndex = name.indexOf("/");
						if (name.endsWith(".html")) {
							nameList.add(name.substring(startIndex+1, name.length() - 5));
						} else{
							nameList.add(name.substring(startIndex+1, name.length()));
						}
					}
				}
				zipfile.close();
			}


		return nameList;
	}

	private static List<String> readFiles(String dirStr, int size) {
		File dir = new File(dirStr);
		List<String> nameList = new ArrayList<String>();
		File[] files = dir.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				File[] childFiles = file.listFiles();
				for (File whoFile : childFiles) {
					if (whoFile.isFile() && whoFile.length() > size) {
						if (whoFile.getName().endsWith(".html")) {
							nameList.add(whoFile.getName().substring(0,
									whoFile.getName().length() - 5));
						} else
							nameList.add(whoFile.getName());
					}
				}
			}
		}

		return nameList;
	}

	private static List<String> readList(String filename) throws IOException {
		List<String> nameList = new ArrayList<String>();
		BufferedReader br = new BufferedReader(new FileReader(filename));
		String line = null;
		while ((line = br.readLine()) != null) {
			if (line.trim().length() == 0)
				continue;
			nameList.add(line.trim());
		}
		br.close();
		return nameList;
	}

}
