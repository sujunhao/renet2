package rdi.cn.misc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class AnsiItem {
	private String name;
	private String primaryTerm;
	private List<String> synonyms = new ArrayList<String>();
	private List<String> narrawLine = new ArrayList<String>();
	private List<String> zcss = new ArrayList<String>();
	private String[] skipItems = new String[]{"Homo_sapiens", "Mmu_OTHER", "Rno_OTHER", "uDisease"};
	
	
	public List<String> getNarrawLine() {
		return narrawLine;
	}

	public void setNarrawLine(List<String> narrawLine) {
		this.narrawLine = narrawLine;
	}

	public AnsiItem(String name){
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPrimaryTerm() {
		return primaryTerm;
	}

	public void setPrimaryTerm(String primaryTerm) {
		this.primaryTerm = primaryTerm;
	}

	public List<String> getSynonyms() {
		return synonyms;
	}

	public void setSynonyms(List<String> synonyms) {
		this.synonyms = synonyms;
	}

	public void setPrimaryTermline(String line) {
		this.primaryTerm = line.substring(3).trim();
		
	}

	public void addSynonymline(String line) {
		this.synonyms.add(line.substring(4).trim());
		
	}

	public void addNarrowline(String line) {
		String child = line.substring(3).trim();
		narrawLine.add(child);
	}

	public void addZCS(String line) {
		this.synonyms.add(line.substring(8).trim());
		this.zcss.add(line.substring(8).trim());
	}

	public List<String> getZCS() {
		return zcss;
	}

	public String toLineString() {
		Set<String> names = new HashSet<String>();
		names.add(primaryTerm.toLowerCase());
		for(String synonym:synonyms){
			names.add(synonym.toLowerCase());
		}
		names.remove(primaryTerm.toLowerCase());
		StringBuilder sb = new StringBuilder(primaryTerm.toLowerCase());
		for(String name:names){
			sb.append("^").append(name);
		}
		
		return sb.toString();
	}
	
	
	public String toMultiLineString() {
		Set<String> names = new HashSet<String>();
		names.add(primaryTerm.toLowerCase());
		for(String synonym:synonyms){
			names.add(synonym.toLowerCase());
		}
		names.remove(primaryTerm.toLowerCase());
		StringBuilder sb = new StringBuilder(primaryTerm.toLowerCase());
		for(String name:names){
			sb.append("\n").append(name);
		}
		
		return sb.toString();
	}

	public String toMultiLineWithKeyString() {
		Set<String> names = new HashSet<String>();
		names.add(primaryTerm.toLowerCase());
		for(String synonym:synonyms){
			names.add(synonym.toLowerCase());
		}
		names.remove(primaryTerm.toLowerCase());
		StringBuilder sb = new StringBuilder(primaryTerm.toLowerCase());
		sb.append("\t");
		sb.append(primaryTerm);
		for(String name:names){
			sb.append("\n").append(name).append("\t").append(primaryTerm);
		}
		
		return sb.toString();
	}

	public Map<String, String> getMap() {
		Map<String, String> map = new HashMap<String,String>();
		if(Arrays.binarySearch(skipItems, name)>=0)
			return map;
		Set<String> names = new HashSet<String>();
		names.add(primaryTerm.toLowerCase());
		for(String synonym:synonyms){
			names.add(synonym.toLowerCase());
		}
		names.remove(primaryTerm.toLowerCase());
		
		map.put(primaryTerm.toLowerCase(), primaryTerm);
		for(String name:names){
			map.put(name.toLowerCase(), primaryTerm);
		}
		
		return map;
	}

}
