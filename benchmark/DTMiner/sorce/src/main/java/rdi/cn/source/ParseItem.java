package rdi.cn.source;

import java.util.List;

/**
 * Created by Xie Yanping on 2015/9/19.
 */
public class ParseItem {
    private String pmid;
    private String content;
    private int sen;
    private String text;
    private List<String> abst;

    public String getContent() {
        return content;
    }

    public void setContent(String text) {
        this.content = text;
    }

    public String getPmid() {
        return pmid;
    }

    public void setPmid(String pmid) {
        this.pmid = pmid;
    }

    public void setText(String text) { this.text = text; }

    public String getText() { return text; }

    public void setAbst(List<String> abst){this.abst = abst;}
    public List<String> getAbst(){return abst;}
    @Override
    public String toString() {
        return this.pmid;
    }
}
