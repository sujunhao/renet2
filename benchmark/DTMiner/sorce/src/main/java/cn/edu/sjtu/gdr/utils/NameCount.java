package cn.edu.sjtu.gdr.utils;

public class NameCount<T> {
	private T name;
	private int count;

	public NameCount(T name, int count) {
		this.name = name;
		this.count = count;
	}

	public NameCount(T name) {
		this(name, 0);
	}

	public T getName() {
		return name;
	}

	public void inc() {
		++count;
	}

	public void inc(int step) {
		count += step;
	}

	public int getCount() {
		return count;
	}

	public String toString() {
		return name + " " + count;
	}
}
