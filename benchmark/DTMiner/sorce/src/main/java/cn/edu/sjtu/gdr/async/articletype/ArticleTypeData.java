package cn.edu.sjtu.gdr.async.articletype;

import java.util.ArrayList;
import java.util.List;

public class ArticleTypeData {
	int pmid;
	List<Boolean> tag = new ArrayList<Boolean>();

	public boolean isOthers() {
		for (Boolean b : tag) {
			if (b)
				return false;
		}
		
		return true;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(pmid);
		for (Boolean b : tag) {
			sb.append(" ");
			if (b) {
				sb.append(1);
			} else {
				sb.append(0);
			}
		}
		
		return sb.toString();
	}
}
