package cn.edu.sjtu.gdr.medline.json;

import com.fasterxml.jackson.databind.JsonNode;

public class MedlineCitationJson {

	private JsonNode json;

	public MedlineCitationJson(JsonNode json) {
		this.json = json;
	}

	public JsonNode get(int index) {
		return json.get(index);
	}

	public JsonNode get(String fieldName) {
		return json.get(fieldName);
	}

	public JsonNode getPath(String[] path) {
		JsonNode cur = json;
		for (String key : path) {
			cur = cur.path(key);
		}
		return cur;
	}

	public String getPathOrDefaultString(String[] path, String defaultValue) {
		JsonNode cur = getPath(path);
		return cur.isMissingNode() ? defaultValue : cur.asText(defaultValue);
	}

	public int getPathOrDefaultInt(String[] path, int defaultValue) {
		JsonNode cur = getPath(path);
		return cur.isMissingNode() ? defaultValue : cur.asInt(defaultValue);
	}

	public JsonNode getJson() {
		return json;
	}

	public String toString() {
		return json.toString();
	}
}
