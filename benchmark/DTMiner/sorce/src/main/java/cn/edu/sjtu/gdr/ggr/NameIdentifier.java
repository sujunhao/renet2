package cn.edu.sjtu.gdr.ggr;

/**
 * Use to represent a name with a identifier
 * 
 * @author Jack Sun(jacksunwei@gmail.com)
 *
 */
public class NameIdentifier {
	public final String name;
	public final String identifier;

	public NameIdentifier(String name, String identifier) {
		this.name = name;
		this.identifier = identifier;
	}

	public String getIdentifier() {
		return identifier;
	}

	public String getName() {
		return name;
	}

	public String toString() {
		return "{" + name + " -> " + identifier + "}";
	}
}
