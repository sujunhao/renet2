package cn.edu.sjtu.gdr.ggr;

import java.util.ArrayList;
import java.util.List;

public class NameHolder {
	
	public static final String SPLITTER = "$$$";
	public static final String INNER_SPLITTER = "##";
	
	public String name;
    //public List<String> norms;
	// the begin/end character offset of the name
	public int beginPos, endPos;

	// the begin/end word offset of the name
	public int beginIndex, endIndex;

	public NameHolder() {
		name = "";
		beginPos = endPos = 0;
        //norms = new ArrayList<>();
		beginIndex = endIndex = 0;
	}

	public String serialize() {
		StringBuilder sb = new StringBuilder();
		sb.append(name)
                .append(INNER_SPLITTER).append(beginIndex)
                .append(INNER_SPLITTER).append(endIndex)
                .append(INNER_SPLITTER).append(beginPos)
                .append(INNER_SPLITTER).append(endPos);
       /* for(String tmp : norms){
            sb.append(INNER_SPLITTER).append(tmp);
        }
        sb.append(INNER_SPLITTER).append(norms.size());*/
		return sb.toString();
	}

	public NameHolder deserialize(String record) {
		int si = record.length(), ei = si;
		/*si = record.lastIndexOf(INNER_SPLITTER, si - 1);
        int normSize = Integer.parseInt(record.substring(si + 2, ei));
        for(int i=0; i<normSize; i++){
            ei = si;
            si = record.lastIndexOf(INNER_SPLITTER, si - 1);
            norms.add(record.substring(si + 2, ei));
        }
        ei = si;*/
        /*si = record.lastIndexOf(INNER_SPLITTER, si - 1);
        endPos = Integer.parseInt(record.substring(si + 2, ei));
        ei = si;
        si = record.lastIndexOf(INNER_SPLITTER, si - 1);
        beginPos = Integer.parseInt(record.substring(si + 2, ei));
        ei = si;*/
        si = record.lastIndexOf(INNER_SPLITTER, si - 1);
		endIndex = Integer.parseInt(record.substring(si + 2, ei));
		ei = si;
		si = record.lastIndexOf(INNER_SPLITTER, si - 1);
		beginIndex = Integer.parseInt(record.substring(si + 2, ei));
		name = record.substring(0, si);

		return this;
	}
}