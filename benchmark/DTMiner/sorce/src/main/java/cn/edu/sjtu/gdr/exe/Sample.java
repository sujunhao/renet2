package cn.edu.sjtu.gdr.exe;

import java.io.*;

/**
 * Created by Xie Yanping on 2015/12/12.
 */
public class Sample {
    private static int ratio = 10000;
    private static void process(String inPath, String outNum) throws IOException{
        BufferedReader br = new BufferedReader(new FileReader(inPath));
        BufferedWriter bw = new BufferedWriter(new FileWriter("res/data/prog-output/sample"+outNum+".txt"));
        int count = 0, num;
        String line;
        while ((line = br.readLine())!=null){
            num = Integer.parseInt(line.substring(line.lastIndexOf(" ")+1));
            for(int i=0; i<num; ++i){
                ++count;
                if(count%ratio == 0) {
                    bw.write(br.readLine());
                    bw.newLine();
                    bw.write(br.readLine());
                    bw.newLine();
                    bw.write(br.readLine());
                    bw.newLine();
                    br.readLine();
                    br.readLine();
                }else {
                    br.readLine();
                    br.readLine();
                    br.readLine();
                    br.readLine();
                    br.readLine();
                }
            }
            bw.flush();
        }
        br.close();
        bw.close();
    }
    public static void main(String[] args) throws IOException{
        process(args[0],args[1]);
    }
}
