package cn.edu.sjtu.gdr.data.feature;

import cn.edu.sjtu.gdr.data.label.LabeledData;
import cn.edu.sjtu.gdr.data.label.LabeledRelationData;
import cn.edu.sjtu.gdr.data.utils.NLPToolsForFeatures;
import cn.edu.sjtu.gdr.ggr.NamePosition;
import java.util.List;

/**
 * Created by Xie Yanping on 2015/7/29.
 */
public class WindowBetweenWalkFeature {
    public static List<String> extractRelationFeature(LabeledRelationData data) {
        return extractFeature(data.getGeneIndex(), data.getDiseaseIndex(),
                data.getSentenceLabeledData());
    }

    public static List<String> extractFeature(NamePosition gene, NamePosition disease,
                                        LabeledData data) {
        int fromIndex = NLPToolsForFeatures.getRootOfWindow(gene, data.getDepTree());
        int toIndex = NLPToolsForFeatures.getRootOfWindow(disease, data.getDepTree());

        if(fromIndex>toIndex){
            int tmp = fromIndex;
            fromIndex = toIndex;
            toIndex = tmp;
        }

        List<String> features = NLPToolsForFeatures.getWalkfeature(fromIndex, toIndex, data.getDepTree());

        return features;
    }
}
