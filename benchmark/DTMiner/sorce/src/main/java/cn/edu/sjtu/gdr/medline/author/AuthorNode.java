package cn.edu.sjtu.gdr.medline.author;

import java.util.ArrayList;
import java.util.List;

public class AuthorNode {
	private String pmid;
	// author ids
	private List<Integer> authors;
	
	AuthorNode(String pmid) {
		authors = new ArrayList<Integer>();
	}

	public String getPmid() {
		return pmid;
	}

	public List<Integer> getAuthors() {
		return authors;
	}
	
	public void addAuthor(int authorId) {
		authors.add(authorId);
	}
}
