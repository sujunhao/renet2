package cn.edu.sjtu.gdr.data.label;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import cn.edu.sjtu.gdr.ggr.NamePosition;

public class LabeledDataParser implements Iterable<LabeledData> {

	private List<LabeledData> allData;

	public LabeledDataParser(String pathYN, String pathF, String path) {
		allData = new ArrayList<LabeledData>();
		//readAll(path);
        if(!pathYN.equals("")) {
            readGAD_YN(pathYN);
            System.out.println("finish reading YN");
        }
        if(!pathF.equals("")) {
            readGAD_F(pathF);
            System.out.println("finish reading F");
        }
        if(!path.equals("")) {
            read(path);
            System.out.println("finish reading our own data");
        }
	}

	private void read(String path) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));

			String str = null;
			LabeledData data = null;

			while ((str = br.readLine()) != null) {
				if (str.startsWith("pmid:")) {
					if (data != null)
						allData.add(data);
					int pos1 = str.indexOf(' ');
					int pos2 = str.lastIndexOf(' ');
					data = new LabeledData(str.substring(pos1 + 1, pos2),
							Integer.parseInt(str.substring(pos2 + 1)));
					data.setSentence(br.readLine()); // set sentence
					br.readLine(); // skip marker line
				} else {
					Scanner scanner = new Scanner(str);
					scanner.next(); // skip the type seriel number
					String reln = scanner.next();

					if (reln.equals("gene")) {
						data.addGeneLabel(new NamePosition(scanner.nextInt(),
								scanner.nextInt()), getLabel(str));
					} else if (reln.equals("disease")) {
						data.addDiseaseLabel(new NamePosition(
								scanner.nextInt(), scanner.nextInt()),
								getLabel(str));
					} else if (reln.equals("relation")) {
						scanner.next(); // skip "gene"
						int genePos1 = scanner.nextInt();
						int genePos2 = scanner.nextInt();
						scanner.next(); // skip disease
						int diseasePos1 = scanner.nextInt();
						int diseasePos2 = scanner.nextInt();
						data.addRelationLabel(new NamePosition(genePos1,
								genePos2), new NamePosition(diseasePos1,
								diseasePos2), getLabel(str));
					}
					scanner.close();
				}
			}
			if (data != null)
				allData.add(data);

			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

    private void readGAD_YN(String path) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(path));

            String str = null;
            LabeledData data = null;
            br.readLine();//pass first line
            while ((str = br.readLine()) != null) {
                if (data != null)
                    allData.add(data);
                int idx = str.indexOf("\t");
                data = new LabeledData(str.substring(0,idx), 0);
                String tmpStr;
                tmpStr = str.substring(idx + 2, str.indexOf("\t", idx + 1)-1);
                int relnLabel = tmpStr.equals("Y") ? 1 : 0;
                //int relnLabel = 1;
                for(int i=0; i<5; ++i) {
                    idx = str.indexOf("\t",idx+1);
                }
                tmpStr = str.substring(idx+2,str.indexOf("\t",idx+1)-1);
                //char offset
                int fromIdxG = Integer.parseInt(tmpStr.substring(0, tmpStr.indexOf("#")));
                int endIdxG = Integer.parseInt(tmpStr.substring(tmpStr.indexOf("#")+1,tmpStr.length()));
                data.addGeneLabel(new NamePosition(fromIdxG,endIdxG), 1);
                for(int i=0; i<3; ++i) {
                    idx = str.indexOf("\t",idx+1);
                }
                tmpStr = str.substring(idx+2,str.indexOf("\t",idx+1)-1);
                int fromIdxD = Integer.parseInt(tmpStr.substring(0, tmpStr.indexOf("#")));
                int endIdxD = Integer.parseInt(tmpStr.substring(tmpStr.indexOf("#")+1,tmpStr.length()));
                data.addDiseaseLabel(new NamePosition(fromIdxD,endIdxD), 1);
                idx = str.indexOf("\t",idx+1);
                tmpStr = str.substring(idx + 2, str.length()-1);
                data.setSentence(tmpStr); // set sentence
                data.addGADRelationLabel(new NamePosition(fromIdxG, endIdxG),
                        new NamePosition(fromIdxD, endIdxD), relnLabel);
            }

            if (data != null)
                allData.add(data);

            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readGAD_F(String path) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(path));

            String str;
            LabeledData data = null;
            br.readLine();//pass first line
            while ((str = br.readLine()) != null) {
                if (data != null)
                    allData.add(data);
                int idx = str.indexOf("\t");
                data = new LabeledData(str.substring(0,idx), 0);
                String tmpStr;
                tmpStr = str.substring(idx+2,idx+3);
                if(tmpStr.equals("F"))
                    continue;
                if(tmpStr.equals("Y"))
                    continue;
                int label = tmpStr.equals("Y")? 1 : 0;
                for(int i=0; i<5; ++i) {
                    idx = str.indexOf("\t",idx+1);
                }
                tmpStr = str.substring(idx+2,str.indexOf("\t",idx+1)-1);
                //char offset
                int fromIdxG = Integer.parseInt(tmpStr.substring(0, tmpStr.indexOf("#")));
                int endIdxG = Integer.parseInt(tmpStr.substring(tmpStr.indexOf("#")+1,tmpStr.length()));
                data.addGeneLabel(new NamePosition(fromIdxG,endIdxG), 1);
                for(int i=0; i<3; ++i) {
                    idx = str.indexOf("\t",idx+1);
                }
                tmpStr = str.substring(idx+2,str.indexOf("\t",idx+1)-1);
                int fromIdxD = Integer.parseInt(tmpStr.substring(0, tmpStr.indexOf("#")));
                int endIdxD = Integer.parseInt(tmpStr.substring(tmpStr.indexOf("#")+1,tmpStr.length()));
                data.addDiseaseLabel(new NamePosition(fromIdxD,endIdxD), 1);
                idx = str.indexOf("\t",idx+1);
                tmpStr = str.substring(idx + 2, str.length()-1);
                data.setSentence(tmpStr); // set sentence
                data.addGADRelationLabel(new NamePosition(fromIdxG, endIdxG),
                        new NamePosition(fromIdxD, endIdxD), label);
            }

            if (data != null)
                allData.add(data);

            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

	private int getLabel(String line) {
		try {
			line = line.trim();
			return Integer.parseInt(line.substring(line.length() - 1));
		} catch (Exception e) {
			System.err.println("line: " + line);
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public Iterator<LabeledData> iterator() {
		return allData.iterator();
	}

	public List<LabeledData> allData() {
		return allData;
	}

	public static void main(String[] argv) {
		LabeledDataParser parser = new LabeledDataParser(
                //"res/GAD_Corpus_IBIgroup/GAD_Y_N.csv",
                //"","res/GAD_Corpus_IBIgroup/GAD_F.csv","");
				"","","res/experiment/labeled-exp.txt");
		System.out.println(parser.allData());
	}
}
