package cn.edu.sjtu.gdr.graph;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class DirectedGraph extends Graph<DirectedGraphNode> {

	private Map<Integer, DirectedGraphNode> nodes;

	public DirectedGraph() {
		// use TreeMap because when graph goes large, rehash will be very
		// consuming
		nodes = new TreeMap<Integer, DirectedGraphNode>();
	}

	@Override
	public Iterator<DirectedGraphNode> iterator() {
		return nodes.values().iterator();
	}

	@Override
	public void addNode(DirectedGraphNode gn) {
		nodes.put(gn.getId(), gn);
	}

	@Override
	public DirectedGraphNode getNode(int id) {
		return nodes.get(id);
	}

	@Override
	public DirectedGraphNode ensureAndGetNode(int id) {
		if (getNode(id) == null) {
			addNode(new DirectedGraphNode(id));
		}
		return getNode(id);
	}

	@Override
	public int size() {
		return nodes.size();
	}

}
