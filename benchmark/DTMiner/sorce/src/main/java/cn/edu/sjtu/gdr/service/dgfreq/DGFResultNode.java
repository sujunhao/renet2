package cn.edu.sjtu.gdr.service.dgfreq;

/**
 * Disease Gene Frequency result node
 * 
 * @author Jack Sun(jacksunwei@gmail.com)
 *
 */
public class DGFResultNode {

	public String geneName;
	public int frequency;

	public DGFResultNode(String geneName, int frequency) {
		this.geneName = geneName;
		this.frequency = frequency;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(geneName).append(" ").append(frequency);
		return sb.toString();
	}
}
