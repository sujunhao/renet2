package cn.edu.sjtu.gdr.exe;

import cn.edu.sjtu.gdr.config.ConfigManager;

import java.io.*;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;

/**
 * Created by Xie Yanping on 2015/12/16.
 */
public class SplitFile {
    private static Map<String, String> init() throws IOException{
        String dirPar = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
                "exe", "MEDLINE_ABSTRACT_PAR_COMBINE");
        File dirFilePar = new File(dirPar);
        File[] filePar = dirFilePar.listFiles(new FileFilter() {
            public boolean accept(File pathname) {
                return pathname.getName().endsWith(".gz");
            }
        });
        Arrays.sort(filePar, new Comparator<File>() {
            public int compare(File o1, File o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        Map<String, String> pmidMap = new HashMap<>();
        for(File f : filePar){
            BufferedReader br = new BufferedReader(new InputStreamReader(new GZIPInputStream(
                    new FileInputStream(f))));
            String line = br.readLine();
            String pmid = line.substring(6,line.lastIndexOf(" "));
            pmidMap.put(pmid,f.getName().substring(0,14)+".txt");
        }
        return pmidMap;
    }
    private static void process(Map<String,String> pmidMap, int size) throws IOException{
        String inPath = "res/data/prog-output/gene-network.txt";
        String outDir = "res/data/prog-output/split/";
        BufferedReader br = new BufferedReader(new FileReader(inPath));
        String line,pmid,fileName;
        int num;
        line = br.readLine();
        pmid = line.substring(6,line.lastIndexOf(" "));
        num = Integer.parseInt(line.substring(line.lastIndexOf(" ")+1));
        for (int i=0; i<size; ++i){
            fileName = pmidMap.get(pmid);
            System.out.println(fileName+" starts.");
            BufferedWriter bw = new BufferedWriter(new FileWriter(outDir+fileName));
            bw.write(line);
            bw.newLine();
            for(int j=0; j<num; ++j){
                for (int k=0; k<5; ++k) {
                    bw.write(br.readLine());
                    bw.newLine();
                }
            }
            while ((line = br.readLine())!=null){
                pmid = line.substring(6,line.lastIndexOf(" "));
                num = Integer.parseInt(line.substring(line.lastIndexOf(" ")+1));
                if(pmidMap.containsKey(pmid))
                    break;
                bw.write(line);
                bw.newLine();
                for(int j=0; j<num; ++j){
                    for (int k=0; k<5; ++k) {
                        bw.write(br.readLine());
                        bw.newLine();
                    }
                }
            }
            bw.flush();
            bw.close();
            System.out.println(fileName+" ends.");
        }
        br.close();
    }
    public static void main(String[] args) throws IOException{
        Map<String, String> pmidMap = init();
        process(pmidMap, pmidMap.size());
    }
}
