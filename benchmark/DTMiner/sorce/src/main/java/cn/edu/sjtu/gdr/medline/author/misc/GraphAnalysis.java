package cn.edu.sjtu.gdr.medline.author.misc;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import cn.edu.sjtu.gdr.utils.GgrLogger;

public class GraphAnalysis {

	public static void main(String[] args) throws IOException {
		GgrLogger.log("starting...");

		String inpath = args[0];
		String outpath = args[1];

		BufferedReader br = new BufferedReader(new FileReader(inpath));
		BufferedWriter bw = new BufferedWriter(new FileWriter(outpath));

		String str = null;
		int stepCount = 0, count = 0, num = 0;

		while ((str = br.readLine()) != null) {
			br.readLine(); // skip in edges
			str = br.readLine(); // out edges
			stepCount += str.length();

			++count;
			if ((count & 0xffff) == 0) {
				bw.write((++num) + "\t" + stepCount + "");
				stepCount = 0;
				bw.newLine();
				GgrLogger.log((count >> 10) + "K nodes scanned...");
			}
		}
		bw.write(stepCount + "");
		bw.newLine();

		br.close();
		bw.flush();
		bw.close();

		GgrLogger.log("finishing...");
	}

}
