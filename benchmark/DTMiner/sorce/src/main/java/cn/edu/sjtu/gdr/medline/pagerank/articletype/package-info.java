/**
 * Article type utils for page rank sorting
 */
/**
 * @author Jack Sun(jacksunwei@gmail.com)
 *
 */
package cn.edu.sjtu.gdr.medline.pagerank.articletype;