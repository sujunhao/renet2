package cn.edu.sjtu.gdr.medline.rank;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.utils.GgrLogger;

public class CoAuthorScore implements Score {

	// pmid -> author list
	protected Map<Integer, List<Integer>> authorMeta;
	// author id -> pr
	protected Map<Integer, Double> pr = new HashMap<Integer, Double>();

	public CoAuthorScore() {
		String authorMetaPath = ConfigManager.getConfig("author",
				"AUTHOR_META_ID_PATH");
		authorMeta = readAuthorMeta(authorMetaPath);
		String path = ConfigManager.getConfig("pagerank-results",
				"COAUTHOR_PAGERANK");
		pr = readCoAutherPagerank(path);
	}
	
	public CoAuthorScore(Map<Integer, List<Integer>> authorMeta, Map<Integer, Double> pr ) {
		this.authorMeta = authorMeta;
		this.pr = pr;
	}

	@Override
	public double score(Set<Integer> pmids) {
		double score = 0;
		for( Integer pmid: pmids ) {
			List<Integer> authors = authorMeta.getOrDefault(pmid, new ArrayList<Integer>());
			for( Integer author: authors) {
				score += getPageRankScore(author);
			}
		}
		return score;
	}
	
	protected double getPageRankScore(Integer author) {
		return pr.getOrDefault(author, 0.0);
	}
	
	public static Map<Integer, Double> readCoAutherPagerank(String path) {
		GgrLogger.log("start reading author score page rank from: " + path);
		Map<Integer, Double> pr = new HashMap<Integer, Double>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));

			String str = null, pair[] = new String[2];
			int pos = 0, count = 0;

			while ((str = br.readLine()) != null) {
				pos = str.indexOf('\t');
				pair[0] = str.substring(0, pos);
				pair[1] = str.substring(pos + 1);
				pr.put(Integer.parseInt(pair[0]), Double.parseDouble(pair[1]));

				++count;
				if ((count & 0xfffff) == 0) {
					GgrLogger.log((count >> 20) + "M line processed...");
				}
			}

			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		GgrLogger.log("finish reading author score page rank...");
		return pr;
	}

	public static Map<Integer, List<Integer>> readAuthorMeta(String path) {
		Map<Integer, List<Integer>> authorMeta = new HashMap<Integer, List<Integer>>();
		GgrLogger.log("start reading author meta...");
		try {

			BufferedReader br = new BufferedReader(new FileReader(path));

			String str = null, authors[];
			List<Integer> authorList = null;

			br.readLine(); // skip empty line
			while ((str = br.readLine()) != null) {
				int pmid = 0;
				try {
					pmid = Integer.parseInt(str.substring("pmid: ".length()));
				} catch (NumberFormatException e) {
					br.readLine(); // skip next line
					continue;
				}

				str = br.readLine();
				if (str.equals(""))
					continue;

				authors = str.split("\t");
				authorList = new ArrayList<Integer>(authors.length);
				for (String authorId : authors)
					authorList.add(Integer.parseInt(authorId));
				authorMeta.put(pmid, authorList);
			}

			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		GgrLogger.log("finish reading author meta..");
		return authorMeta;
	}
}
