package cn.edu.sjtu.gdr.data.feature;

import java.util.List;

import cn.edu.sjtu.gdr.data.label.LabeledData;
import cn.edu.sjtu.gdr.data.label.LabeledRelationData;
import cn.edu.sjtu.gdr.data.utils.NLPToolsForFeatures;
import cn.edu.sjtu.gdr.ggr.NamePosition;
import cn.edu.sjtu.gdr.utils.GgrLogger;

public class WindowRootLCAFeature {

	public static final double LCA_IS_VB = 1.0;
	public static final double LCA_IS_NOT_VB = 0.0;

	public static final double LCA_IS_PREP = 1.0;
	public static final double LCA_IS_NOT_PREP = 0.0;

	public static final double LCA_IS_NN = 1.0;
	public static final double LCA_IS_NOT_NN = 0.0;

	public static double extractRelationGeneDiseaseVbFeature(
			LabeledRelationData data) {
		return extractFeature(data.getGeneIndex(), data.getDiseaseIndex(),
				data.getSentenceLabeledData(), "VB", LCA_IS_VB, LCA_IS_NOT_VB);
	}

	public static double extractRelationGeneDiseasePrepFeature(
			LabeledRelationData data) {
		return extractFeature(data.getGeneIndex(), data.getDiseaseIndex(),
				data.getSentenceLabeledData(), "IN", LCA_IS_PREP,
				LCA_IS_NOT_PREP);
	}

	public static double extractRelationGeneDiseaseNNFeature(
			LabeledRelationData data) {
		return extractFeature(data.getGeneIndex(), data.getDiseaseIndex(),
				data.getSentenceLabeledData(), "NN", LCA_IS_NN, LCA_IS_NOT_NN);
	}

	public static double extractFeature(NamePosition np1, NamePosition np2,
			LabeledData data, String tag, double trueValue, double falseValue) {
		int root1 = NLPToolsForFeatures.getRootOfWindow(np1,
				data.getDepTree());
		List<Integer> pathWn1 = data.getDepTree().getNode(root1 + 1)
				.getPathFromRoot();
		int root2 = NLPToolsForFeatures.getRootOfWindow(np2, data.getDepTree());
		List<Integer> pathWn2 = data.getDepTree().getNode(root2 + 1)
				.getPathFromRoot();

		int lcaIndex = 0;
		for (int i = 0; i < Math.min(pathWn1.size(), pathWn2.size()); ++i) {
            int a = pathWn1.get(i);
            int b = pathWn2.get(i);
			if (pathWn1.get(i).equals(pathWn2.get(i))) {
				lcaIndex = pathWn1.get(i);
			} else {
				break;
			}
		}

		if (lcaIndex == 0) {
			GgrLogger.warn("root error in "
					+ WindowRootLCAFeature.class.toGenericString()
					+ " for data " + data.getPmid() + "1:" + pathWn1 + "2:" + pathWn2);
			return falseValue;
		}
		return data.getTagged().get(lcaIndex - 1).tag().startsWith(tag) ? trueValue
				: falseValue;
	}

	public static double extractNoneFeature(NamePosition np1, NamePosition np2,
			LabeledData data) {
		int root1 = NLPToolsForFeatures.getRootOfWindow(np1,
				data.getDepTree());
		List<Integer> pathWn1 = data.getDepTree().getNode(root1 + 1)
				.getPathFromRoot();
		int root2 = NLPToolsForFeatures.getRootOfWindow(np2, data.getDepTree());
		List<Integer> pathWn2 = data.getDepTree().getNode(root2 + 1)
				.getPathFromRoot();

		int lcaIndex = 0;
		for (int i = 0; i < Math.min(pathWn1.size(), pathWn2.size()); ++i) {
			if (pathWn1.get(i).equals(pathWn2.get(i))) {
				lcaIndex = pathWn1.get(i);
			} else {
				break;
			}
		}

        if (lcaIndex == 0) {
            GgrLogger.warn("root error in "
                    + WindowRootLCAFeature.class.toGenericString()
                    + " for data " + data.getPmid());
            return 0.0;
        }

		String tag = data.getTagged().get(lcaIndex - 1).tag();

		if (tag.startsWith("IN") || tag.startsWith("NN")
				|| tag.startsWith("VB")) {
			return 0.0;
		} else {
			return 1.0;
		}
	}

	public static double extractRelationGeneDiseaseNoneFeature(
			LabeledRelationData data) {
		return extractNoneFeature(data.getGeneIndex(), data.getDiseaseIndex(),
				data.getSentenceLabeledData());
	}

}
