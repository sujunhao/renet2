package cn.edu.sjtu.gdr.evaluation;

import cn.edu.sjtu.gdr.obo.EscapeSynonymNormalizer;
import cn.edu.sjtu.gdr.obo.Normalizer;
import cn.edu.sjtu.gdr.obo.SynonymNormalizer;

import java.io.*;
import java.util.List;

/**
 * Created by Xie Yanping on 2015/10/2.
 */
public class CoPubNormalizer {
    private static Normalizer geneSyn = EscapeSynonymNormalizer.getGeneNormalizer();
    private static String inpath = "res/data/copub";
    public static void main(String[] args) throws IOException{
        File[] files = new File(inpath).listFiles();
        for(File file : files) {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String outpath = "res/data/copub/"+file.getName()+".norm";
            BufferedWriter bw = new BufferedWriter(new FileWriter(outpath));
            br.readLine();
            String line;
            while((line=br.readLine())!=null){
                int score = Integer.valueOf(line.substring(line.lastIndexOf("\t")+1));
                String tmp = line.substring(0,line.indexOf("entrezgene=")-1);
                String geneName = tmp.substring(0,tmp.lastIndexOf("\t"));
                List<String> geneNorms = geneSyn.normalize(geneName);
                for(String norm : geneNorms){
                    bw.write(norm+"\t"+Integer.toString(score)+"\n");
                }
            }
            br.close();
            bw.flush();
            bw.close();
        }

    }
}
