package cn.edu.sjtu.gdr.exe;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

import cn.edu.sjtu.gdr.config.ConfigManager;

public class GenerateGeneId {

	public static void main(String[] args) throws IOException {
		String inpath = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"obo", "GENE_SYN");
		String outpath = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"exe", "GENE_ID");
		Set<String> genes = new HashSet<String>();
		BufferedReader br = new BufferedReader(new FileReader(inpath));
		BufferedWriter bw = new BufferedWriter(new FileWriter(outpath));

		String str = null, geneNames[] = null;
		while ((str = br.readLine()) != null) {
			geneNames = str.split("###");
			for (String gene : geneNames) {
				genes.add(gene);
			}
		}

		geneNames = genes.toArray(geneNames);
		Arrays.sort(geneNames, new Comparator<String>() {
			public int compare(String o1, String o2) {
				return o1.compareTo(o2);
			}
		});
		
		int id = 0;
		bw.write("total: " + geneNames.length);
		bw.newLine();
		for (String gene : geneNames) {
			bw.write(++id + "\t" + gene);
			bw.newLine();
		}
		bw.flush();
		bw.close();
		br.close();
	}

}
