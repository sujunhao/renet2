package cn.edu.sjtu.gdr.ggr;

import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.util.Triple;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;

/**
 * Created by Xie Yanping on 2015/11/27.
 */
public class NER {
    private AbstractSequenceClassifier classifier;
    NER(){
        String serializedClassifier = "gad-model.ser.gz";
        classifier = CRFClassifier.getClassifierNoExceptions(serializedClassifier);
    }
    public List<Triple<String, Integer, Integer>> process(String sen){
        //List<Triple<String, Integer, Integer>> test = classifier.classifyToCharacterOffsets(sen);
        return classifier.classifyToCharacterOffsets(sen);
    }
    public static void main(String[] args) throws IOException {
		System.setOut(new PrintStream("gad.out"));
		
		BufferedReader sentenceFile = new BufferedReader(new FileReader(
				"res/GAD_Corpus_IBIgroup/test.tsv"));
		String line = sentenceFile.readLine();
        NER ner = new NER();
        int i = 1;
		while (line != null){
			System.out.println(line);
			List<Triple<String, Integer, Integer>> nerRes = ner.process(line);
			for(Triple<String, Integer, Integer> res : nerRes){
	            if(res.first.equals("G")){
	            	System.out.println(line.substring(res.second, res.third) + " G");
				} else if(res.first.equals("D")){
					System.out.println(line.substring(res.second, res.third) + " D");
				}
			}
			line = sentenceFile.readLine();
			++i;
		}

    }
}
