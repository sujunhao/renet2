package cn.edu.sjtu.gdr.evaluation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import kangqi.util.struct.MapIntHelper;
import fig.basic.LogInfo;

public class RankingAggregation {
	
	public static Map<String, Integer> BordaScore = new HashMap<String, Integer>();
	
	public static ArrayList<String> elements = new ArrayList<String>();
	
	public static HashMap<String, Integer> pairs = new HashMap<String, Integer>();
	
	public static boolean verbose = true;

	public static void main(String[] args) {
		
		/*
		String[] s1 = {"2", "7", "5", "6", "8", "1", "11",};
		String[] s2 = {"2", "1", "3", "4", "6", "5"};
		String[] s3 = {"4", "2", "3", "6", "1"};
		ArrayList<Double> weight = new ArrayList<Double>();
		weight.add(0.4); weight.add(0.3); weight.add(0.3);
				
		ArrayList<String> ex1 = new ArrayList<String>(Arrays.asList(s1));
		ArrayList<String> ex2 = new ArrayList<String>(Arrays.asList(s2));
		ArrayList<String> ex3 = new ArrayList<String>(Arrays.asList(s3));
		
		ArrayList<ArrayList<String>> ex_l = new ArrayList<ArrayList<String>>();
		ex_l.add(ex1);
		ex_l.add(ex2);
		ex_l.add(ex3);
		*/
		
		//---------------------------------------------------------------------
		
		ArrayList<String> standard = new ArrayList<String>();
		Random rand = new Random();
		for (int i=0; i<100; i++) {
			int tmp = rand.nextInt(100);
			while (standard.contains(String.valueOf(tmp)))
				tmp = rand.nextInt(100);
			standard.add(String.valueOf(tmp));
		}
		
		ArrayList<ArrayList<String>> ex_l = new ArrayList<ArrayList<String>> ();
		
		ex_l.add(mutate(mutate(standard)));
		
		for (int i=0; i<50; i++) {
			int tmp = rand.nextInt(200);
			while (standard.contains(String.valueOf(tmp)))
				tmp = rand.nextInt(200);
			standard.add(String.valueOf(tmp));
		}
		
		ex_l.add(mutate(mutate(mutate(mutate(standard)))));
		
		
		for (int i=0; i<ex_l.get(0).size(); i++) {
			String id = ex_l.get(0).get(i);
			System.out.print(id + " ");
		}
		System.out.println();
		
		for (int i=0; i<ex_l.get(1).size(); i++) {
			String id = ex_l.get(1).get(i);
			System.out.print(id + " ");
		}
		System.out.println();
		
		ArrayList<String> ex_r = rankingAggreg(ex_l);
		for (int i=0; i<ex_r.size(); i++) {
			String id = ex_r.get(i);
			System.out.print(id + " ");
		}
		System.out.println();
		
		for (int i=0; i<standard.size(); i++) {
			String id = standard.get(i);
			System.out.print(id + " ");
		}
		System.out.println("\n" + getScore(standard));
	}
	
	public static ArrayList<String> rankingAggreg(ArrayList<ArrayList<String>> lists, ArrayList<Double> weightList) {
		ArrayList<ArrayList<String>> tmp = new ArrayList<ArrayList<String>>();
		int len = lists.size();
		for (int i=0; i<len; i++) {
			int cnt = (int) Math.round(weightList.get(i)*10);
			for (int j=0; j<cnt; j++)
				tmp.add(lists.get(i));
		}
		/*
		ArrayList<Map.Entry<String, Integer>> ranks =  ranking_v1(tmp);
		ArrayList<String> ret = new ArrayList<String>();
		for (Map.Entry<String, Integer> entry: ranks) {
			ret.add(entry.getKey());
		}
		*/
		ArrayList<String> ret = ranking_v2(tmp);
		return ret;
	}
	
	public static ArrayList<String> rankingAggreg(ArrayList<ArrayList<String>> lists) {
		ArrayList<String> ret = ranking_v2(lists);
		return ret;
	}

	public static ArrayList<Map.Entry<String, Integer>> ranking_v1(ArrayList<ArrayList<String>> lists) {	
		for (ArrayList<String> list : lists) {
			int len = list.size();
			for (int i=0; i<len; i++) {
				if (! BordaScore.containsKey(list.get(i))) 
					BordaScore.put(list.get(i), len-i-1);
				else {
					int tmp = BordaScore.get(list.get(i));
					BordaScore.put(list.get(i), tmp+len-i-1);
				}
			}
		}	
		
		ArrayList<Map.Entry<String, Integer>> ret =
			    new ArrayList<Map.Entry<String, Integer>>(BordaScore.entrySet());	
		
		Collections.sort(ret, new Comparator<Map.Entry<String, Integer>>() {   
		    public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {      
		        return (o2.getValue() - o1.getValue()); 
		        //return (o1.getKey()).toString().compareTo(o2.getKey());
		    }
		}); 
		
		return ret;
	}
	
	public static ArrayList<String> ranking_v2(ArrayList<ArrayList<String>> lists) {
		getElements(lists);
		int len =elements.size();
        if(len == 0) return new ArrayList<String>();
		int numOfMutate = len*(len-1)/2 > 100? 100: len*(len-1)/2;
		//int numOfMutate = len*(len-1)/2;
		int numOfGeneration = numOfMutate > 20? 20 : numOfMutate;
		getPairs(lists);
		ArrayList<ArrayList<String>> generation = new ArrayList<ArrayList<String>> ();
		HashMap<ArrayList<String>, Integer> pmap = new HashMap<ArrayList<String>, Integer>();
		ArrayList<ArrayList<String>> self = new ArrayList<ArrayList<String>>();
		
		if (verbose) System.out.print("the ancestor: \n  " + getScore(elements) + ": ");
		if (verbose) printList(elements);
		
		if (verbose) System.out.println("initial scores: ");
		generation.add(elements);
		
		for (int i=0; i<numOfMutate; i++) {
			ArrayList<String> tmp = mutate(elements);
			while (generation.contains(tmp))
				tmp = mutate(elements);
			generation.add(tmp);
			if (verbose) System.out.printf("  %d: ", getScore(tmp));
			if (verbose) printList(tmp);
		}
		if (verbose) System.out.printf("the size of generation: %d\n", generation.size());
		
		int flag = 0, round = 0, prev = 0;
		while (flag < 4) {
			if (verbose) LogInfo.begin_track("round %d...", round);
			pmap.clear();
			for (ArrayList<String> list: generation) {
				pmap.put(list, getScore(list));
				//System.out.printf("  %d: ", getScore(list));
				//printList(list);
				
				self.clear();
				for (int i=0; i<numOfMutate; i++) {
					ArrayList<String> tmp = mutate(list);
					while (self.contains(tmp))
						tmp = mutate(list);
					self.add(tmp);
				}
				
				for (ArrayList<String> tmp : self) 
					if (!pmap.containsKey(tmp)) {
						int score = getScore(tmp);
						//System.out.printf("  %d: ", score);
						//printList(tmp);
						pmap.put(tmp, score);
					}
			}
			
			ArrayList<Map.Entry<ArrayList<String>, Integer>> pool = 
					new MapIntHelper<ArrayList<String>>().sort(pmap);
			if (verbose) LogInfo.logs("the size of pool: %d", pool.size());
            if(pool.size() == 0 || numOfGeneration == 0) break; ////
			
			generation.clear();
			for(int i=0; i<numOfGeneration; i++) {
				Map.Entry<ArrayList<String>, Integer> tmp = pool.get(i);
				generation.add(tmp.getKey());
				if (verbose) System.out.print("  " + tmp.getValue());	
			}
			if (verbose) System.out.println();
			if (verbose) LogInfo.end_track();
			round ++;
			if (pool.get(0).getValue() == prev) flag ++;
			else flag = 0;
			prev = pool.get(0).getValue();
		}
		
		return generation.get(0);
		
	}
	
	public static void getElements(ArrayList<ArrayList<String>> lists) {
        elements.clear();
		for (ArrayList<String> list: lists) 
			for (String elem: list) 
				if (!elements.contains(elem)) 
					elements.add(elem);
	}
	
	public static void getPairs(ArrayList<ArrayList<String>> lists) {
		for (ArrayList<String> list: lists) 
			for (int i=0; i<list.size()-1; i++) 
				for(int j=i+1; j<list.size(); j++) {
					String pair = list.get(i) + "\t" + list.get(j);
					if (pairs.containsKey(pair)) {
						int cnt = pairs.get(pair);
						pairs.put(pair, cnt+1);
					}
					else
						pairs.put(pair, 1);
				}
	}
	
	public static Integer getScore(ArrayList<String> list) {
		int score = 0;
		for (int i=0; i<list.size()-1; i++)
			for (int j=i+1; j<list.size(); j++) {
				String pair = list.get(i) + "\t" + list.get(j);
				if (pairs.containsKey(pair)) 
					score += pairs.get(pair);
			}
		return score;
	}
	
	public static ArrayList<String> mutate(ArrayList<String> list) {
		ArrayList<String> ret = new ArrayList<String>(list);
		Random rand = new Random();
		int len = list.size();
		int pos_1 = rand.nextInt(len), pos_2 = rand.nextInt(len);
		while(pos_1 == pos_2) pos_2 = rand.nextInt(len);
		//LogInfo.logs("%d %d", pos_1, pos_2);
		ret.set(pos_1, list.get(pos_2));
		ret.set(pos_2, list.get(pos_1));
		return ret;
	}
	
	public static void printList(ArrayList<String> tmp) {
		for (int j=0; j<tmp.size(); j++)
			System.out.print("  " + tmp.get(j));
		System.out.println();
	}
	
}
