package cn.edu.sjtu.gdr.medline.abst;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.zip.GZIPInputStream;

import cn.edu.sjtu.gdr.medline.xml.ParserResult;
import cn.edu.sjtu.gdr.medline.xml.XmlParser;
import rdi.cn.source.MedlineItem;

public class MedlineAbstPumper implements Iterator<MedlineItem> {

	private BufferedReader br;
	private List<MedlineItem> items;
	private Iterator<MedlineItem> inner;

	public MedlineAbstPumper(File file) throws FileNotFoundException,
			IOException {
		br = new BufferedReader(new InputStreamReader(new GZIPInputStream(
				new FileInputStream(file))));
		items = new ArrayList<MedlineItem>();
		readAll();
		inner = items.iterator();
	}

	public MedlineAbstPumper(String path) throws FileNotFoundException,
			IOException {
		this(new File(path));
	}

	public boolean hasNext() {
		return inner.hasNext();
	}

	public MedlineItem next() {
		return inner.next();
	}

	private void readAll() throws IOException {
		String str = null;
		StringBuilder sb = new StringBuilder();
		while ((str = br.readLine()) != null) {
			if (str.startsWith("<MedlineCitation ")) {
				sb.setLength(0);
			} else if (str.startsWith("</MedlineCitation>")) {
				items.add(extract(sb.toString()));
			} else {
				sb.append(str).append("\n");
			}
		}
	}

	private MedlineItem extract(String xml) {
		MedlineItem item = new MedlineItem();

		String pmid = XmlParser.getFirstInner(xml, "PMID", 0).result;
		item.setPmid(pmid);
        List<String> authors = XmlParser.getAuthors(xml);
        item.setAuthors(authors);

        ParserResult t = XmlParser.getFirstInner(xml,"ArticleTitle",0);
        String title;
        if(t == null){
            //no title, orz
            item.setTitle("");
            System.out.println(pmid+" no title");
        }else{
            title = t.result;
            if(title.lastIndexOf(".") != title.length()-1
                    && title.lastIndexOf("?") != title.length()-1){
                title += ".";
            }
            item.setTitle(title);
        }
		ParserResult r = XmlParser.getFirstInner(xml, "Abstract", 0);
		if (r == null) {
			// you mother fucker, what is this shit? the data is bullshit!
			item.setText("");
			return item;
		}
		
		String xmlAbst = r.result;

		if (xmlAbst.indexOf("<AbstractText") == -1) {
			item.setText(xmlAbst.replaceAll("\r*\n", " "));
		} else {
			StringBuilder abst = new StringBuilder();
			int endpos = 0;
			ParserResult res = null;
			while ((res = XmlParser.getFirstInner(xmlAbst, "AbstractText",
					endpos)) != null) {
				abst.append(res.result).append(" ");
				endpos = res.endPos;
			}
			item.setText(abst.toString().replaceAll("\r*\n", " "));
		}
		return item;
	}
}
