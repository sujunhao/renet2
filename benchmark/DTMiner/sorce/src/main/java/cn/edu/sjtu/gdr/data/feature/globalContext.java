package cn.edu.sjtu.gdr.data.feature;

import cn.edu.sjtu.gdr.data.label.LabeledData;
import cn.edu.sjtu.gdr.data.label.LabeledRelationData;
import cn.edu.sjtu.gdr.ggr.NamePosition;
import cn.edu.sjtu.gdr.utils.GgrLogger;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.process.Morphology;

import java.util.*;

/**
 * Created by Xie Yanping on 2015/10/25.
 */
public class globalContext {
    public static Map<String,Map<String,Integer>> extract(LabeledRelationData data) {
        return extractFeature(data.getGeneIndex(), data.getDiseaseIndex(),
                data.getSentenceLabeledData());
    }

    public static Map<String,Map<String,Integer>> extractFeature(NamePosition np1, NamePosition np2,
                                        LabeledData data) {
        Map<String,Map<String,Integer>> res = new HashMap<>();
        Map<String,Integer> tempMap;
        String tempString;
        Integer tempInt;
        if (np1.getStart() > np2.getStart()) {
            NamePosition temp = np1;
            np1 = np2;
            np2 = temp;
        }

        List<TaggedWord> tagged = data.getTagged();
        List<String> lemmaList = data.getLemma();

        for(int n=1; n<4; n++) {
            //F
            if((tempMap = res.get("F")) == null) {
                res.put("F", new HashMap<String,Integer>());
                tempMap = res.get("F");
            }
            for (int i = 0; i < np1.getStart(); ++i) {
                if(i+n<=np1.getStart()) {
                    tempString = "";
                    for (int j = 0; j < n; ++j) {
                        //tempString += tagged.get(i + j).word();
                        tempString += lemmaList.get(i + j);
                    }
                    if ((tempInt = tempMap.get(tempString)) == null) {
                        tempMap.put(tempString, 1);
                    } else {
                        tempMap.put(tempString, ++tempInt);
                    }
                }
            }

            //B
            if((tempMap = res.get("B")) == null) {
                res.put("B", new HashMap<String,Integer>());
                tempMap = res.get("B");
            }
            for (int i = np1.getEnd(); i < np2.getStart(); ++i) {
                if(i+n<=np2.getStart()) {
                    tempString = "";
                    for (int j = 0; j < n; ++j) {
                        //tempString += tagged.get(i + j).word();
                        tempString += lemmaList.get(i + j);
                    }
                    if ((tempInt = tempMap.get(tempString)) == null) {
                        tempMap.put(tempString, 1);
                    } else {
                        tempMap.put(tempString, ++tempInt);
                    }
                }
            }

            //A
            if((tempMap = res.get("A")) == null) {
                res.put("A", new HashMap<String,Integer>());
                tempMap = res.get("A");
            }
            for (int i = np2.getEnd(); i < tagged.size(); ++i) {
                if(i+n<=tagged.size()) {
                    tempString = "";
                    for (int j = 0; j < n; ++j) {
                        //tempString += tagged.get(i + j).word();
                        tempString += lemmaList.get(i + j);
                    }
                    if ((tempInt = tempMap.get(tempString)) == null) {
                        tempMap.put(tempString, 1);
                    } else {
                        tempMap.put(tempString, ++tempInt);
                    }
                }
            }
        }

        return res;
    }
}
