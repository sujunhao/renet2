package cn.edu.sjtu.gdr.medline.articletype;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.data.utils.NLPToolsForFeatures;
import cn.edu.sjtu.gdr.medline.json.MedlineCitationJson;
import cn.edu.sjtu.gdr.medline.json.MedlineCitationJsonUtils;
import cn.edu.sjtu.gdr.utils.GgrLogger;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.process.Morphology;

public class VitroTester extends ArticleTypeTester {

	private static Set<String> keywords;

	@Override
	public Boolean apply(MedlineCitationJson mc) {
		String title = MedlineCitationJsonUtils.findTitle(mc);
		if (testSentece(title))
			return true;
		
		List<String> abstracts = MedlineCitationJsonUtils.findAbstract(mc);
		for (String sen : abstracts) {
			if (testSentece(sen))
				return true;
		}

		return false;
	}

	private boolean testSentece(String sentence) {
		DocumentPreprocessor dp = new DocumentPreprocessor(new StringReader(
				sentence));
		for (List<HasWord> sen : dp)
			if (testSentence(sen))
				return true;

		return false;
	}

	private boolean testSentence(List<HasWord> sentence) {
		List<TaggedWord> tagged = NLPToolsForFeatures.tagger().tagSentence(
				sentence);
		StringBuffer sb = new StringBuffer();
		for (TaggedWord tw : tagged) {
			sb.append(Morphology.stemStatic(tw.word(), tw.tag()).word()).append(" ");
		}

		String str = sb.toString();
		for (String keyword : getKeywords()) {
			if (str.indexOf(keyword) != -1) {
				return true;
			}
		}

		return false;
	}

	private static Set<String> getKeywords() {
		if (keywords == null) {
			synchronized (VitroTester.class) {
				if (keywords == null) {
					keywords = new HashSet<String>();
					String path = ConfigManager.getConfig("artical-type",
							"VITRO_KEYWORDS_PATH");

					try {
						GgrLogger.log("start loading vitro keywords...");
						BufferedReader br = new BufferedReader(new FileReader(
								path));
						String str = null;

						while ((str = br.readLine()) != null) {
							keywords.add(str);
						}

						br.close();
						GgrLogger.log("finish loading vitro keywords...");
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}

		return keywords;
	}

	@Override
	public String name() {
		return "Vitro";
	}

}
