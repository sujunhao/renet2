/**
 * Service used for page rank as score of gene rank
 */
/**
 * @author Jack Sun(jacksunwei@gmail.com)
 *
 */
package cn.edu.sjtu.gdr.service.pagerank;