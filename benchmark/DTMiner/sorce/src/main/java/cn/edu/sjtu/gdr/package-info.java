/**
 * gdr - Gene-Disease Relation
 * 
 * This project is from adapt lab. Using literature
 * mining based methodology to identify the relation
 * between gene and disease. 
 * 
 * The Project is a collaborate project with AZ. 
 * 
 */
package cn.edu.sjtu.gdr;