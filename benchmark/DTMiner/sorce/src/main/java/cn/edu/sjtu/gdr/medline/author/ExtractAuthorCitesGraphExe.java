package cn.edu.sjtu.gdr.medline.author;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.graph.DirectedGraph;
import cn.edu.sjtu.gdr.graph.DirectedGraphNode;
import cn.edu.sjtu.gdr.graph.DirectedGraphOutputter;
import cn.edu.sjtu.gdr.graph.DirectedGraphReader;
import cn.edu.sjtu.gdr.utils.GgrLogger;

public class ExtractAuthorCitesGraphExe {

	private DirectedGraph authorGraph, paperGraph;

	private String authorMetaPath, paperGraphPath, outPath;

	// pmid -> [author id list]
	private Map<Integer, List<Integer>> authorMeta;

	public ExtractAuthorCitesGraphExe(String authorMetaPath,
			String paperGraphPath, String outPath) {
		this.authorMetaPath = authorMetaPath;
		this.paperGraphPath = paperGraphPath;
		this.outPath = outPath;
		init();
	}

	public void run() {
		int writePmid = 0, count = 0;
		List<Integer> writeAuthors = null, citeAuthors = null;
		DirectedGraphNode writeNode = null;

		for (Map.Entry<Integer, List<Integer>> meta : authorMeta.entrySet()) {
			writePmid = meta.getKey();
			writeAuthors = meta.getValue();

			if ((writeNode = paperGraph.getNode(writePmid)) != null) {
				for (Integer citePmid : writeNode.getOutEdges()) {
					if ((citeAuthors = authorMeta.get(citePmid)) != null) {
						for (Integer writeAuthor : writeAuthors) {
							for (Integer citeAuthor : citeAuthors) {
								// add edge: writeAuthor --> citeAuthor
								authorGraph.addEdge(writeAuthor, citeAuthor);
							}
						}
					}
				}
			}

			++count;
			if ((count & 0xfffff) == 0) {
				GgrLogger.log((count >> 20) + "M papers processed...");
			}
		}

		GgrLogger.log("outputing author cites graph...");
		new DirectedGraphOutputter(authorGraph, outPath).output();
		GgrLogger.log("finish outputing author cites graph...");
	}

	private void init() {
		GgrLogger.log("reading author meta...");
		readAuthorMeta(authorMetaPath);
		GgrLogger.log("finish reading author meta");

		GgrLogger.log("loading paper citing graph...");
		paperGraph = new DirectedGraphReader(paperGraphPath).read();
		GgrLogger.log("finish loading paper citing graph...");

		authorGraph = new DirectedGraph();
	}

	private void readAuthorMeta(String path) {
		try {
			authorMeta = new HashMap<Integer, List<Integer>>();

			BufferedReader br = new BufferedReader(new FileReader(path));

			String str = null, authors[];
			List<Integer> authorList = null;

			br.readLine(); // skip empty line
			while ((str = br.readLine()) != null) {
				int pmid = 0;
				try {
					pmid = Integer.parseInt(str.substring("pmid: ".length()));
				} catch (NumberFormatException e) {
					br.readLine(); // skip next line
					continue;
				}

				str = br.readLine();
				if (str.equals(""))
					continue;

				authors = str.split("\t");
				authorList = new ArrayList<Integer>();
				for (String authorId : authors)
					authorList.add(Integer.parseInt(authorId));
				authorMeta.put(pmid, authorList);
			}

			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		String authorMetaPath = ConfigManager.getConfig(
				ConfigManager.MAIN_CONFIG, "author", "AUTHOR_META_ID_PATH");
		String paperGraphPath = ConfigManager.getConfig(
				ConfigManager.MAIN_CONFIG, "cites", "PAPER_CITES_GRAPH_PATH");
		String outPath = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"author", "AUTHOR_CITE_GRAPH_PATH");

		new ExtractAuthorCitesGraphExe(authorMetaPath, paperGraphPath, outPath)
				.run();
	}

}
