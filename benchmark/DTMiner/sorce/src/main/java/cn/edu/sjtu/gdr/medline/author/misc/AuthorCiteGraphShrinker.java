package cn.edu.sjtu.gdr.medline.author.misc;

import java.util.function.Predicate;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.graph.DirectedGraphIdShrinker;

public class AuthorCiteGraphShrinker {

	public static void main(String[] args) {
		Predicate<Integer> pred = new Predicate<Integer>() {

			@Override
			public boolean test(Integer t) {
				return t >= Integer.parseInt(ConfigManager.getConfig(
						"author-misc", "MIN_PAPER_COUNT"));
			}
		};
		String selectorPath = ConfigManager.getConfig("author",
				"AUTHOR_PAPER_COUNT");
		String graphPath = ConfigManager.getConfig("author-misc",
				"ORIGINAL_GRAPH_PATH");
		String outPath = ConfigManager.getConfig("author-misc",
				"SHRINK_GRAPH_PATH");

		AuthorIdSelector selector = new AuthorIdSelector(selectorPath, pred);
		new DirectedGraphIdShrinker(graphPath, selector.getSelectedSet(),
				outPath).shrink();
	}

}
