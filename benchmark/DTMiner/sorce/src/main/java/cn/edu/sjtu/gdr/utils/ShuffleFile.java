package cn.edu.sjtu.gdr.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ShuffleFile {

	List<String> lines, tagLines;

	public ShuffleFile(String inpath, String outpath) {
		lines = new ArrayList<String>();
		tagLines = new ArrayList<String>();
		readAll(inpath);
		output(outpath);
	}

	private void output(String outpath) {
		Random rand = new Random();
		for (int i = 0; i < lines.size(); ++i) {
			int j = i + rand.nextInt(lines.size() - i);
			swap(i, j, lines);
			swap(i, j, tagLines);
		}

		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(outpath));
			BufferedWriter bwTag = new BufferedWriter(new FileWriter(outpath + ".tag"));

			for (int i = 0; i < lines.size(); ++i) {
				bw.write(lines.get(i));
				bw.newLine();
				bwTag.write(tagLines.get(i));
				bwTag.newLine();
			}

			bw.flush();
			bwTag.flush();
			bw.close();
			bwTag.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void swap(int i, int j, List<String> lines) {
		if (i != j) {
			String str = lines.get(i);
			lines.set(i, lines.get(j));
			lines.set(j, str);
		}
	}

	private void readAll(String path) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			BufferedReader brtag = new BufferedReader(new FileReader(path + ".tag"));
			String str = null;

			while ((str = br.readLine()) != null) {
				lines.add(str);
				tagLines.add(brtag.readLine());
			}

			br.close();
			brtag.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		new ShuffleFile("res/experiment/svmdata",
				"res/experiment/svmdata.train");
	}
}
