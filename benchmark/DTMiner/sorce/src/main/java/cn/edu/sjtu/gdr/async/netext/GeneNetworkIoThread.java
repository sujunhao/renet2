package cn.edu.sjtu.gdr.async.netext;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import cn.edu.sjtu.gdr.async.LoggableThread;
import cn.edu.sjtu.gdr.config.ConfigManager;

public class GeneNetworkIoThread extends LoggableThread {

	private ControlData control;

	public GeneNetworkIoThread(ControlData control) {
		this.control = control;
	}

	public void run() {
		String path = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG, "exe",
				"GENE_NETWORK_FILE");
		BufferedWriter bw = null;
		int count = 0;
		
		try {
			bw = new BufferedWriter(new FileWriter(path));

			List<NetworkOutputData> output = null;
			while (true) {
				try {
					output = control.outputBuffer.pop();
					if (output == null) {
						if (!control.isRunning() && control.threadNum.get() == 0) {
							log("no more buffer, io thread about to exit");
							break;
						}
						Thread.sleep(10);
						continue;
					}
					
					++count;
					log("output new buffer");
					for (NetworkOutputData data : output) {
						bw.write(data.serialize());
						bw.newLine();
					}
					log("output " + count + " buffers");

				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			bw.flush();
			bw.close();
			log("io flush finish, io thread exit!");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
}
