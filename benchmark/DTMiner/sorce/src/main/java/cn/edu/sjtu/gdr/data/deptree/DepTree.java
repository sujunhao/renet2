package cn.edu.sjtu.gdr.data.deptree;

import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.TypedDependency;

import java.util.List;

public class DepTree {

	DepTreeNode[] sentence;
    //private final int pa=0;

    public DepTree(List<TaggedWord> tag, GrammaticalStructure gs) {
		sentence = new DepTreeNode[tag.size() + 1];

		sentence[0] = new DepTreeNode(0, "ROOT", "-");
		for (int i = 0; i < tag.size(); ++i) {
			sentence[i + 1] = new DepTreeNode(i + 1, tag.get(i).word(), tag.get(i)
					.tag());
		}

		for (TypedDependency td : gs.allTypedDependencies()) {
			int head = td.gov().index();
			int son = td.dep().index();
			sentence[son].setHead(sentence[head], td.reln().getShortName());
		}
	}
	
	public DepTreeNode getNode(int i) {
		return sentence[i];
	}

    public int getSize() { return  sentence.length;}
	
	public String toString() {
		return sentence.toString();
	}
}
