package cn.edu.sjtu.gdr.graph;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class UndirectedGraphNode extends GraphNode {
	// private Set<Integer> edges;
	// map edge -> weight
	private Map<Integer, Integer> edges;

	public UndirectedGraphNode(int id, Map<Integer, Integer> edges) {
		super(id);
		this.edges = edges;
	}

	public UndirectedGraphNode(int id) {
		this(id, new TreeMap<Integer, Integer>());
	}

	public void addOutEdge(int id, int weight) {
		if (!edges.containsKey(id)) {
			edges.put(id, 0);
		}
		edges.put(id, edges.get(id) + weight);
	}

	public void removeOutEdge(int id) {
		edges.remove(id);
	}


	/**
	 * Get the edge-weight of the the node to amother node
	 * 
	 * @param nodeId
	 * @return -1 if no such edge
	 */
	public int getOutEdgeWeight(int nodeId) {
		if (!edges.containsKey(nodeId)) {
			return -1;
		}
		return edges.get(nodeId);
	}

	public int getInEdgeWeight(int nodeId) {
		return getOutEdgeWeight(nodeId);
	}

	public Iterator<Integer> edgesIterator() {
		return edges.keySet().iterator();
	}

	public Collection<Integer> getOutEdges() {
		return edges.keySet();
	}

	public Collection<Integer> getInEdges() {
		return getOutEdges();
	}

	public int getOutDegree() {
		return edges.size();
	}
	
	public int getInDegree() {
		return getOutDegree();
	}

	public boolean equals(Object gn) {
		return gn instanceof UndirectedGraphNode
				&& this.id == ((UndirectedGraphNode) gn).id;
	}

	@Override
	public void addInEdge(int id, int weight) {
		addOutEdge(id, weight);
	}

	@Override
	public void removeInEdge(int id) {
		removeOutEdge(id);
	}
}
