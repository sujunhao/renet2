package cn.edu.sjtu.gdr.obo;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

import cn.edu.sjtu.gdr.config.ConfigManager;

public class GeneDiseaseSynonymGeneratorExe {

	static void work(String inpath, String outpath) throws IOException {
		OboTermParser parser = new OboTermParser(inpath);
		BufferedWriter bw = new BufferedWriter(new FileWriter(outpath));
		for (Iterator<OboTerm> itr = parser.iterator(); itr.hasNext();) {
			bw.write(itr.next().synString());
			bw.newLine();
		}
		bw.flush();
		bw.close();
	}

	public static void main(String[] args) throws IOException {
		work(ConfigManager.getConfig(ConfigManager.MAIN_CONFIG, "obo",
				"GENE_OBO"), ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"obo", "GENE_SYN"));
		work(ConfigManager.getConfig(ConfigManager.MAIN_CONFIG, "obo",
				"DISEASE_OBO"), ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"obo", "DISEASE_SYN"));
		
	}
}
