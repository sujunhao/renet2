package cn.edu.sjtu.gdr.medline.rank;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import bottomLevel.function.DirectedGraphReaderForPageRank;
import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.utils.GgrLogger;

public class PaperCiteSuppress extends PaperCiteScore {

	private Map<Integer, Map<Integer, Integer>> graph;
	private static double alpha = 0.5, beta = 1.0;
	
	public PaperCiteSuppress() {
		super();
		String path = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"graph", "PAPER_CITE_GRAPH");
		GgrLogger.log("start reading graph...");
		graph = new DirectedGraphReaderForPageRank(
				path).read();
		GgrLogger.log("finish reading graph...");
	}
	
	public PaperCiteSuppress(Map<Integer, Double> pr) {
		super(pr);
		String path = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"graph", "PAPER_CITE_GRAPH");
		GgrLogger.log("start reading graph...");
		graph = new DirectedGraphReaderForPageRank(
				path).read();
		GgrLogger.log("finish reading graph...");
	}
	
	@Override
	public double score(Set<Integer> pmids) {
		double score = 0;
		for( Integer x: pmids )
			for( Integer y: pmids ) {
				Map<Integer, Integer> cites1 = graph.getOrDefault(x, new HashMap<Integer, Integer>());
				Map<Integer, Integer> cites2 = graph.getOrDefault(y, new HashMap<Integer, Integer>());
				
				if( cites1.containsKey(y) )
					score += alpha * getPageRankScore(x) + beta * getPageRankScore(y);
				else if( cites2.containsKey(x))
					score += alpha * getPageRankScore(y) + beta * getPageRankScore(x);
				else
					score += getPageRankScore(y) + getPageRankScore(x);
			}
		return score / (2 * pmids.size());
	}

}
