package cn.edu.sjtu.gdr.service.dgfreq;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.obo.EscapeSynonymNormalizer;
import cn.edu.sjtu.gdr.obo.Normalizer;
import cn.edu.sjtu.gdr.obo.SynonymNormalizer;

public class DiseaseGeneFrequency {

	private static Normalizer diseaseNorm = EscapeSynonymNormalizer
			.getDiseaseNormalizer();
	// disease name -> gene name -> pmids
	private Map<String, Map<String, Set<Integer>>> map;

	public DiseaseGeneFrequency(String path) {
		map = new HashMap<String, Map<String, Set<Integer>>>();
		readAll(path);
	}

	private static final int MAX_RESULT = 150;
	private static final int MIN_VAL = 5;

	/**
	 * Get top MAX_RESULT freq genes
	 * 
	 * @param diseaseName
	 * @return
	 */
	public List<DGFResultNode> getResult(String diseaseName, boolean reverse) {
		List<DGFResultNode> res = new ArrayList<DGFResultNode>();
		List<String> norms = diseaseNorm.normalize(diseaseName.toLowerCase());
		if (norms.size() == 0) {
			return res;
		}
		Map<String, Set<Integer>> geneMap = new HashMap<String, Set<Integer>>();
		for (String norm : norms) {
			Map<String, Set<Integer>> tmp = map.get(norm);
			for (String gene : tmp.keySet()) {
				if (!geneMap.containsKey(gene))
					geneMap.put(gene, new HashSet<Integer>());
				geneMap.get(gene).addAll(tmp.get(gene));
			}
		}

		List<Map.Entry<String, Set<Integer>>> list = new ArrayList<Map.Entry<String, Set<Integer>>>(
				geneMap.entrySet());
		Collections.sort(list,
				new Comparator<Map.Entry<String, Set<Integer>>>() {
					@Override
					public int compare(Entry<String, Set<Integer>> o1,
							Entry<String, Set<Integer>> o2) {
						return o2.getValue().size() - o1.getValue().size();
					}
				});

		if( reverse )
			Collections.reverse(list);
		
		int count = 0;
		for (int i = 0; i < list.size() && count < MAX_RESULT; ++i) {
			Map.Entry<String, Set<Integer>> entry = list.get(i);
			if( entry.getValue().size() >= MIN_VAL) {
				res.add(new DGFResultNode(entry.getKey(), entry.getValue().size()));
				++ count;
			}
		}
		return res;
	}

	public Set<Integer> getPmids(String disease, String gene) {
		Set<Integer> res = new HashSet<Integer>();
		List<String> norms = diseaseNorm.normalize(disease.toLowerCase());
		if (norms.size() > 0) {
			for (String norm : norms) {
				Map<String, Set<Integer>> tmp = map.get(norm);
				res.addAll(tmp.getOrDefault(gene, new HashSet<Integer>()));
			}
		}
		return res;
	}

	private volatile static DiseaseGeneFrequency dgf = null;

	public static DiseaseGeneFrequency getInstance() {
		if (dgf == null) {
			synchronized (DiseaseGeneFrequency.class) {
				if (dgf == null) {
					dgf = new DiseaseGeneFrequency(ConfigManager.getConfig(
							ConfigManager.MAIN_CONFIG, "exe",
							"DISEASE_GENE_FREQ"));
				}
			}
		}
		return dgf;
	}

	private void readAll(String path) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			String str = null, tmp[] = null;
			String disease, gene;
			int num = 0, lastSpace;
			Map<String, Set<Integer>> geneMap;

			while ((str = br.readLine()) != null) {
				lastSpace = str.lastIndexOf(" ");
				disease = str.substring(9, lastSpace);
				geneMap = new HashMap<String, Set<Integer>>();
				map.put(disease, geneMap);
				num = Integer.parseInt(str.substring(lastSpace + 1));
				for (int i = 0; i < num; ++i) {
					str = br.readLine();
					gene = str.substring(6);
					tmp = br.readLine().split("\t");
					Set<Integer> pmids = new HashSet<Integer>();
					for (int j = 0; j < tmp.length; ++j)
						pmids.add(Integer.parseInt(tmp[j]));
					geneMap.put(gene, pmids);
				}
			}

			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
