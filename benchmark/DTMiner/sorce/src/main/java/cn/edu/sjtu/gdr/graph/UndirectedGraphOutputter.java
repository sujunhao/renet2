package cn.edu.sjtu.gdr.graph;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class UndirectedGraphOutputter {

	public static final String nodePrefix = "node: ";
	public static final String WEIGHT_SPLITTER = "$", EDGE_SPLITTER = "\t";

	private UndirectedGraph graph;
	private File file;

	public UndirectedGraphOutputter(UndirectedGraph graph, File file) {
		this.graph = graph;
		this.file = file;
	}
	
	public UndirectedGraphOutputter(UndirectedGraph graph, String path) {
		this(graph, new File(path));
	}
	
	public UndirectedGraphOutputter(UndirectedGraph graph, String parent, String filename) {
		this(graph, new File(parent, filename));
	}

	public void output() {
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(file));

			for (UndirectedGraphNode node : graph) {
				bw.write(nodePrefix + node.getId());
				bw.newLine();
				for (Integer edgeId : node.getOutEdges()) {
					bw.write(edgeId + WEIGHT_SPLITTER + node.getOutEdgeWeight(edgeId)
							+ EDGE_SPLITTER);
				}
				bw.newLine();
			}

			bw.flush();
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
