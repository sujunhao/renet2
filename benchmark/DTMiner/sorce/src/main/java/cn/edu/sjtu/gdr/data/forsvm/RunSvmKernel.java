package cn.edu.sjtu.gdr.data.forsvm;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import cn.edu.sjtu.gdr.data.feature.TrainTestGenerate;
import svmJavaKernel.kernel.*;
import svmJavaKernel.HybridReader;
import svmJavaKernel.libsvm.ex.Instance;
import svmJavaKernel.libsvm.ex.SVMPredictor;
import svmJavaKernel.libsvm.ex.SVMTrainer;
import svmJavaKernel.libsvm.svm_model;
import svmJavaKernel.libsvm.svm_parameter;

/**
 * Created by Xie Yanping on 2015/7/28.
 */
public class RunSvmKernel {
    public RunSvmKernel(){
        KernelManager.setCustomKernel(new HybridKernel());
    }
    public static void testLinearKernel(String[] args) throws IOException, ClassNotFoundException {
        String dataFileName = "res/experiment/svmdata.hybrid"+args[0];
        String trainFileName = dataFileName+".train";
        String testFileName = dataFileName+".test";
        String outputFileName = dataFileName+".output";

        /*//Generate train and test set
        String[] generateArgs = {dataFileName};
        TrainTestGenerate.main(generateArgs);
        //Read training file
        Instance[] trainingInstances = HybridReader.readFeature(trainFileName,true);*/
        Instance[] instances = HybridReader.readFeature(dataFileName,true);

        //Register kernel function
        KernelManager.setCustomKernel(new HybridKernel());

        //Setup parameters
        svm_parameter param = new svm_parameter();
        //param.probability = 1;

        //Train the model
        /*System.out.println("Training started...");
        long startTime = System.currentTimeMillis();
        svm_model model = SVMTrainer.train(trainingInstances, param);
        long finishTime = System.currentTimeMillis();
        System.out.println("time elapsed: " + (finishTime - startTime) / (1000));
        System.out.println("Training completed.");*/

        //Save the trained model
        /*if(args.length>=1)
            SVMTrainer.saveModel(model, "svm/hybrid.model"+args[0]);
        else
            SVMTrainer.saveModel(model, "svm/hybrid.model");*/
        //svm_model model = SVMPredictor.loadModel("svm/"+walkOrHybrid+".model.b");

        //Read test file
        /*Instance[] testingInstances = HybridReader.readFeature(testFileName, true);
        //Predict results
        //SVMPredictor.predictProbability(testingInstances,model,true);
        double[] predictions = SVMPredictor.predict(testingInstances, model, true);
        writeOutputs(outputFileName, predictions,testingInstances);*/
        SVMTrainer.doCrossValidation(instances, param, 10, true);
        //SVMTrainer.doInOrderCrossValidation(trainingInstances, param, 10, true);
    }

    public static double runKernel(String featureStr, svm_model model) throws IOException, ClassNotFoundException {
        boolean isHybrid = true;
        String walkOrHybrid = isHybrid?"hybrid":"walk";

        //Register kernel function
        //KernelManager.setCustomKernel(new HybridKernel());

        //Setup parameters
        //svm_parameter param = new svm_parameter();

        //svm_model model = SVMPredictor.loadModel("svm/"+walkOrHybrid+".model");

        //Read test file;
        Instance testingInstances = HybridReader.readFeature(featureStr);
        //Predict results
        double prediction = SVMPredictor.predictProbability(testingInstances, model, false);
        return prediction;
    }

    private static void writeOutputs(String outputFileName, double[] predictions, Instance[] instances) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(outputFileName));
        for (int i=0 ; i<predictions.length; ++i) {
            double p = predictions[i];
            writer.write(String.format("%.0f ", p));
            writer.write(instances[i].toString());
            writer.newLine();
        }
        writer.close();
    }

    private static void showUsage() {
        System.out.println("Demo training-file testing-file output-file");
    }

    private static boolean checkArgument(String[] args) {
        return args.length == 3;
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        testLinearKernel(args);
    }
}
