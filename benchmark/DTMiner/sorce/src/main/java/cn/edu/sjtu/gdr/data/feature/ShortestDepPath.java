package cn.edu.sjtu.gdr.data.feature;

import cn.edu.sjtu.gdr.data.deptree.DepTree;
import cn.edu.sjtu.gdr.data.label.LabeledData;
import cn.edu.sjtu.gdr.data.label.LabeledRelationData;
import cn.edu.sjtu.gdr.data.utils.NLPToolsForFeatures;
import cn.edu.sjtu.gdr.ggr.NamePosition;
import com.sun.org.apache.bcel.internal.generic.LADD;
import edu.stanford.nlp.ling.TaggedWord;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Xie Yanping on 2015/12/18.
 */
public class ShortestDepPath {
    private static int ngram = 4; // max 3
    public static List<String> extractRelationFeature(LabeledRelationData data, int n) {
        ngram = n;
        return extractFeature(data.getGeneIndex(), data.getDiseaseIndex(),
                data.getSentenceLabeledData());
    }

    public static List<String> extractFeature(NamePosition gene, NamePosition disease,
                                              LabeledData data) {
        int fromIndex = NLPToolsForFeatures.getRootOfWindow(gene, data.getDepTree());
        int toIndex = NLPToolsForFeatures.getRootOfWindow(disease, data.getDepTree());

        /*if(fromIndex>toIndex){
            int tmp = fromIndex;
            fromIndex = toIndex;
            toIndex = tmp;
        }*/
        DepTree parse = data.getDepTree();
        List<Integer> shortestDepPath = NLPToolsForFeatures.getDependencyPath(fromIndex,toIndex,parse);
        int LCA = shortestDepPath.get(shortestDepPath.size()-1);
        shortestDepPath.remove(shortestDepPath.size()-1);
        int LCAIdx = shortestDepPath.indexOf(LCA);
        List<String> lemmaList = data.getLemma();
        lemmaList.add("ROOT");
        if(LCA == -1)
            shortestDepPath.set(LCAIdx,lemmaList.size()-1);
        for (int i=gene.getStart(); i<gene.getEnd(); ++i)
            lemmaList.set(i,"GENE#");
        for (int i=disease.getStart(); i<disease.getEnd(); ++i)
            lemmaList.set(i,"DISEASE#");
        List<TaggedWord> taggedWordList = data.getTagged();
        taggedWordList.add(new TaggedWord("ROOT","-"));
        List<String> features = new ArrayList<>();
        Map<String,Integer> res = new HashMap<>();
        String tempString;
        Integer tempInt;
        /*for(int n=1; n<ngram; n++) {//ngram n = 1,2,3
            for (int i = 0; i <= shortestDepPath.size(); ++i) {
                if (i + n <= shortestDepPath.size()) {
                    tempString = "";
                    for (int j = 0; j < n; ++j) {
                        tempString += lemmaList.get(shortestDepPath.get(i + j));
                    }
                    if ((tempInt = res.get(tempString)) == null) {
                        res.put(tempString, 1);
                    } else {
                        res.put(tempString, ++tempInt);
                    }
                }
            }
        }*/
        for(int n=1; n<ngram; n++) {//ngram n = 1,2,3
            for (int i = LCAIdx; i >= 0; --i) {
                if(i == LCAIdx && n == 1)
                    continue;
                if (i - n + 1 >= 0) {
                    tempString = "*";
                    for (int j = 0; j < n; ++j) {
                        tempString += lemmaList.get(shortestDepPath.get(i - j));
                    }
                    if(n==1 && (tempString.equals("*GENE#")||tempString.equals("*DISEASE#")))
                        continue;
                    if ((tempInt = res.get(tempString)) == null) {
                        res.put(tempString, 1);
                    } else {
                        res.put(tempString, ++tempInt);
                    }
                }
            }
            for (int i = LCAIdx; i < shortestDepPath.size(); ++i) {
                if (i + n <= shortestDepPath.size()) {
                    tempString = "*";
                    for (int j = 0; j < n; ++j) {
                        tempString += lemmaList.get(shortestDepPath.get(i + j));
                    }
                    if(n==1 && (tempString.equals("*GENE#")||tempString.equals("*DISEASE#")))
                        continue;
                    if ((tempInt = res.get(tempString)) == null) {
                        res.put(tempString, 1);
                    } else {
                        res.put(tempString, ++tempInt);
                    }
                }
            }
        }
        if(LCAIdx>0 && LCAIdx<shortestDepPath.size()-1){
            if(lemmaList.get(shortestDepPath.get(LCAIdx-1)).compareTo(lemmaList.get(shortestDepPath.get(LCAIdx+1)))<=0)
                tempString = lemmaList.get(shortestDepPath.get(LCAIdx-1))+"*"+
                        lemmaList.get(shortestDepPath.get(LCAIdx))+
                        lemmaList.get(shortestDepPath.get(LCAIdx+1));
            else
                tempString = lemmaList.get(shortestDepPath.get(LCAIdx+1))+"*"+
                        lemmaList.get(shortestDepPath.get(LCAIdx))+
                        lemmaList.get(shortestDepPath.get(LCAIdx-1));
            if ((tempInt = res.get(tempString)) == null) {
                res.put(tempString, 1);
            } else {
                res.put(tempString, ++tempInt);
            }
        }
        for(int k = 0; k<shortestDepPath.size(); ++k){
            int idx = shortestDepPath.get(k);
            if(taggedWordList.get(idx).tag().startsWith("VB")){
                int parent = parse.getNode(idx+1).getHeadIndex()-1;
                int children = -1;
                if(k<=LCAIdx && k>0)
                    children = shortestDepPath.get(k-1);
                else if(k>LCAIdx && k<shortestDepPath.size()-1)
                    children = shortestDepPath.get(k+1);
                List<Integer> vbMods = NLPToolsForFeatures.getVbMod(idx,taggedWordList.size()-1,parse);
                if (vbMods.size()==0)
                    continue;
                for(int n=1; n<ngram; n++) {
                    if(n == 1) {
                        for (Integer modIdx : vbMods) {
                            tempString = "*"+lemmaList.get(modIdx);
                            if ((tempInt = res.get(tempString)) == null) {
                                res.put(tempString, 1);
                            } else {
                                res.put(tempString, ++tempInt);
                            }
                        }
                    }
                    if(n == 2){
                        for (Integer modIdx : vbMods) {
                            tempString = "*"+lemmaList.get(idx)+lemmaList.get(modIdx);
                            /*if(modIdx<idx)
                                tempString = lemmaList.get(modIdx) + lemmaList.get(idx);
                            else
                                tempString = lemmaList.get(idx) + lemmaList.get(modIdx);*/
                            if ((tempInt = res.get(tempString)) == null) {
                                res.put(tempString, 1);
                            } else {
                                res.put(tempString, ++tempInt);
                            }
                        }
                    }
                    if(n == 3) {
                        for (int i = 0; i < vbMods.size(); ++i) {
                            for (int j = i + 1; j < vbMods.size(); ++j) {
                                if (lemmaList.get(vbMods.get(i)).compareTo(lemmaList.get(vbMods.get(j))) <= 0)
                                    tempString = lemmaList.get(vbMods.get(i)) + "*" + lemmaList.get(idx) + lemmaList.get(vbMods.get(j));
                                else
                                    tempString = lemmaList.get(vbMods.get(j)) + "*" + lemmaList.get(idx) + lemmaList.get(vbMods.get(i));
                                /*if(vbMods.get(j) < idx)
                                    //tempString = lemmaList.get(vbMods.get(i))+lemmaList.get(vbMods.get(j))+lemmaList.get(idx);
                                    continue;
                                else if(vbMods.get(i) > idx)
                                    //tempString = lemmaList.get(idx)+lemmaList.get(vbMods.get(i))+lemmaList.get(vbMods.get(j));
                                    continue;
                                else
                                    tempString = lemmaList.get(vbMods.get(i))+lemmaList.get(idx)+lemmaList.get(vbMods.get(j));*/
                                if ((tempInt = res.get(tempString)) == null) {
                                    res.put(tempString, 1);
                                } else {
                                    res.put(tempString, ++tempInt);
                                }
                            }
                        }
                        if (shortestDepPath.contains(parent)) {
                            //if(parent>=0){
                            for (Integer modIdx : vbMods) {
                                tempString = "*" + lemmaList.get(parent) + lemmaList.get(idx) + lemmaList.get(modIdx);
                                /*if (modIdx < idx && idx < parent)
                                    tempString = lemmaList.get(modIdx) + lemmaList.get(idx) + lemmaList.get(parent);
                                else if (modIdx < idx && modIdx < parent)
                                    tempString = lemmaList.get(modIdx) + lemmaList.get(parent)+lemmaList.get(idx);
                                else if(modIdx < idx && parent<modIdx)
                                    tempString = lemmaList.get(parent) + lemmaList.get(modIdx)+lemmaList.get(idx);
                                else if(modIdx>idx && parent<idx)
                                    tempString = lemmaList.get(parent) + lemmaList.get(idx)+lemmaList.get(modIdx);
                                else if(modIdx>idx && parent<modIdx)
                                    tempString = lemmaList.get(idx) + lemmaList.get(parent)+lemmaList.get(modIdx);
                                else
                                    tempString = lemmaList.get(idx) + lemmaList.get(modIdx)+lemmaList.get(parent);*/
                                if ((tempInt = res.get(tempString)) == null) {
                                    res.put(tempString, 1);
                                } else {
                                    res.put(tempString, ++tempInt);
                                }
                            }
                        }
                        if (children >= 0){
                            for (Integer modIdx : vbMods) {
                                /*if (modIdx < idx && idx < children)
                                    tempString = lemmaList.get(modIdx) + lemmaList.get(idx) + lemmaList.get(children);
                                else if (modIdx < idx && modIdx < children)
                                    tempString = lemmaList.get(modIdx) + lemmaList.get(children)+lemmaList.get(idx);
                                else if(modIdx < idx && children<modIdx)
                                    tempString = lemmaList.get(children) + lemmaList.get(modIdx)+lemmaList.get(idx);
                                else if(modIdx>idx && children<idx)
                                    tempString = lemmaList.get(children) + lemmaList.get(idx)+lemmaList.get(modIdx);
                                else if(modIdx>idx && children<modIdx)
                                    tempString = lemmaList.get(idx) + lemmaList.get(children)+lemmaList.get(modIdx);
                                else
                                    tempString = lemmaList.get(idx) + lemmaList.get(modIdx)+lemmaList.get(children);*/
                                if (lemmaList.get(modIdx).compareTo(lemmaList.get(children)) <= 0)
                                    tempString = lemmaList.get(modIdx) + "*" + lemmaList.get(idx) + lemmaList.get(children);
                                else
                                    tempString = lemmaList.get(children) + "*" + lemmaList.get(idx) + lemmaList.get(modIdx);
                                if ((tempInt = res.get(tempString)) == null) {
                                    res.put(tempString, 1);
                                } else {
                                    res.put(tempString, ++tempInt);
                                }
                            }
                        }
                        if(k==LCAIdx && k<shortestDepPath.size()-1) {
                            children = shortestDepPath.get(k + 1);
                            for (Integer modIdx : vbMods) {
                                /*if (modIdx < idx && idx < children)
                                    tempString = lemmaList.get(modIdx) + lemmaList.get(idx) + lemmaList.get(children);
                                else if (modIdx < idx && modIdx < children)
                                    tempString = lemmaList.get(modIdx) + lemmaList.get(children)+lemmaList.get(idx);
                                else if(modIdx < idx && children<modIdx)
                                    tempString = lemmaList.get(children) + lemmaList.get(modIdx)+lemmaList.get(idx);
                                else if(modIdx>idx && children<idx)
                                    tempString = lemmaList.get(children) + lemmaList.get(idx)+lemmaList.get(modIdx);
                                else if(modIdx>idx && children<modIdx)
                                    tempString = lemmaList.get(idx) + lemmaList.get(children)+lemmaList.get(modIdx);
                                else
                                    tempString = lemmaList.get(idx) + lemmaList.get(modIdx)+lemmaList.get(children);*/
                                if (lemmaList.get(modIdx).compareTo(lemmaList.get(children)) <= 0)
                                    tempString = lemmaList.get(modIdx) + "*" + lemmaList.get(idx) + lemmaList.get(children);
                                else
                                    tempString = lemmaList.get(children) + "*" + lemmaList.get(idx) + lemmaList.get(modIdx);
                                if ((tempInt = res.get(tempString)) == null) {
                                    res.put(tempString, 1);
                                } else {
                                    res.put(tempString, ++tempInt);
                                }
                            }
                        }
                    }
                }
            }
            if(taggedWordList.get(idx).tag().startsWith("NN")) {
                int parent = parse.getNode(idx + 1).getHeadIndex() - 1;
                int children = -1;
                if (k <= LCAIdx && k > 0)
                    children = shortestDepPath.get(k - 1);
                else if (k > LCAIdx && k < shortestDepPath.size() - 1)
                    children = shortestDepPath.get(k + 1);
                List<Integer> nnMods = NLPToolsForFeatures.getNnMod(idx, taggedWordList.size() - 1, parse);
                if (nnMods.size() == 0)
                    continue;
                for (int n = 1; n < ngram; n++) {
                    if (n == 1) {
                        for (Integer modIdx : nnMods) {
                            tempString = "*" + lemmaList.get(modIdx);
                            if ((tempInt = res.get(tempString)) == null) {
                                res.put(tempString, 1);
                            } else {
                                res.put(tempString, ++tempInt);
                            }
                        }
                    }
                    if (n == 2) {
                        for (Integer modIdx : nnMods) {
                            tempString = "*" + lemmaList.get(idx) + lemmaList.get(modIdx);
                            if ((tempInt = res.get(tempString)) == null) {
                                res.put(tempString, 1);
                            } else {
                                res.put(tempString, ++tempInt);
                            }
                        }
                    }
                    if (n == 3) {
                        for (int i = 0; i < nnMods.size(); ++i) {
                            for (int j = i + 1; j < nnMods.size(); ++j) {
                                if (lemmaList.get(nnMods.get(i)).compareTo(lemmaList.get(nnMods.get(j))) <= 0)
                                    tempString = lemmaList.get(nnMods.get(i)) + "*" + lemmaList.get(idx) + lemmaList.get(nnMods.get(j));
                                else
                                    tempString = lemmaList.get(nnMods.get(j)) + "*" + lemmaList.get(idx) + lemmaList.get(nnMods.get(i));
                                if ((tempInt = res.get(tempString)) == null) {
                                    res.put(tempString, 1);
                                } else {
                                    res.put(tempString, ++tempInt);
                                }
                            }
                        }
                        if (shortestDepPath.contains(parent)) {
                            //if(parent>=0){
                            for (Integer modIdx : nnMods) {
                                tempString = "*" + lemmaList.get(parent) + lemmaList.get(idx) + lemmaList.get(modIdx);
                                if ((tempInt = res.get(tempString)) == null) {
                                    res.put(tempString, 1);
                                } else {
                                    res.put(tempString, ++tempInt);
                                }
                            }
                        }
                        if (children >= 0) {
                            for (Integer modIdx : nnMods) {
                                if (lemmaList.get(modIdx).compareTo(lemmaList.get(children)) <= 0)
                                    tempString = lemmaList.get(modIdx) + "*" + lemmaList.get(idx) + lemmaList.get(children);
                                else
                                    tempString = lemmaList.get(children) + "*" + lemmaList.get(idx) + lemmaList.get(modIdx);
                                if ((tempInt = res.get(tempString)) == null) {
                                    res.put(tempString, 1);
                                } else {
                                    res.put(tempString, ++tempInt);
                                }
                            }
                        }
                        if (k == LCAIdx && k < shortestDepPath.size() - 1) {
                            children = shortestDepPath.get(k + 1);
                            for (Integer modIdx : nnMods) {
                                if (lemmaList.get(modIdx).compareTo(lemmaList.get(children)) <= 0)
                                    tempString = lemmaList.get(modIdx) + "*" + lemmaList.get(idx) + lemmaList.get(children);
                                else
                                    tempString = lemmaList.get(children) + "*" + lemmaList.get(idx) + lemmaList.get(modIdx);
                                if ((tempInt = res.get(tempString)) == null) {
                                    res.put(tempString, 1);
                                } else {
                                    res.put(tempString, ++tempInt);
                                }
                            }
                        }
                    }
                }
            }
        }
        lemmaList.remove(lemmaList.size()-1);
        taggedWordList.remove(taggedWordList.size()-1);
        for(String gram : res.keySet()){
            features.add(gram+"\t"+res.get(gram));
        }
        return features;
    }
}
