package cn.edu.sjtu.gdr.exe;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.graph.UndirectedGraph;
import cn.edu.sjtu.gdr.graph.UndirectedGraphNode;
import cn.edu.sjtu.gdr.graph.UndirectedGraphOutputter;
import cn.edu.sjtu.gdr.utils.GgrLogger;
import cn.edu.sjtu.gdr.utils.NameID;

public class GeneGraphGenerator {

	private UndirectedGraph geneGraph;
	private String inpath;
	private String outpath;
	private NameID geneId;
	
	public static final int MIN_EDGE_WEIGHT = 10;

	public GeneGraphGenerator(String networkPath, String outputPath)
			throws FileNotFoundException {
		this.inpath = networkPath;
		this.outpath = outputPath;
		init();
	}

	void init() {
		geneGraph = new UndirectedGraph();
		geneId = NameID.getGeneIdInstance();
	}

	public void generate() throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(this.inpath));
		int id = 0, num = 0;
		long count = 0;
		String str = null, context[] = null;
		Set<Integer> cooccurSet = new HashSet<Integer>();

		GgrLogger.log("starting to generate");
		// first line: pmid + num of sentence
		// second line: gene co-occur
		// third line: disease co-occur
		while ((str = br.readLine()) != null) {
			num = Integer.parseInt(str.substring(str.lastIndexOf(' ') + 1));
			for (int sen = 0; sen < num; ++sen) {
				str = br.readLine(); // read in gene co-occur
				context = str.split(Pattern.quote("$$$"));
				cooccurSet.clear();
				for (int i = 0; i < context.length; i += 2) {
					// first is identifier and second is the actual name in
					// literature
					id = geneId.getId(context[i]); // get the id of identifier
					if (id != 0) {
						cooccurSet.add(id);
					} 
				}
				addToGraph(cooccurSet);
				br.readLine(); // skip disease co-occur
			}
			++count;
			if ((count & 0xfffff) == 0) {
				GgrLogger.log((count >> 20) + "M papers scanned..");
			}
		}
		GgrLogger.log("finish loading: " + count + " papers loaded");
		br.close();

		GgrLogger.log("start filter weak edges...");
		filterEdge();
		GgrLogger.log("finish filter weak edges...");
		
		GgrLogger.log("start outputing nodes...");
		GgrLogger.log(geneGraph.size() + " nodes to output... ");
		new UndirectedGraphOutputter(geneGraph, outpath).output();
		GgrLogger.log("finish outputing nodes...");
	}

	private void addToGraph(Set<Integer> cooccurSet) {
		for (Integer id1 : cooccurSet) {
			for (Integer id2 : cooccurSet) {
				if (id1 != id2) {
					addEdge(id1, id2);
				}
			}
		}
	}
	
	private void filterEdge() {
		for (UndirectedGraphNode gn : geneGraph) {
			List<Integer> removeIds = new ArrayList<Integer>();
			for (int edgeId : gn.getOutEdges()) {
				if (gn.getOutEdgeWeight(edgeId) < MIN_EDGE_WEIGHT) {
					removeIds.add(edgeId);
				}
			}
			gn.removeOutEdges(removeIds);
		}
	}

	private void addEdge(int id1, int id2) {
		geneGraph.ensureAndGetNode(id1).addOutEdge(id2);
	}

	public static void main(String[] args) throws FileNotFoundException,
			IOException {
		// TODO GENE_NETWORK_FILE file format modified, update needed
		String inpath = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"exe", "GENE_NETWORK_FILE");
		String outpath = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"exe", "GENE_GRAPH");
		new GeneGraphGenerator(inpath, outpath).generate();
	}

}
