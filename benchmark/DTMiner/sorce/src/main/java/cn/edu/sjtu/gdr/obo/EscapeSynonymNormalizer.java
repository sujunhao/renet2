package cn.edu.sjtu.gdr.obo;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cn.edu.sjtu.gdr.config.ConfigManager;

/**
 * Match ignoring case
 * 
 * @author Jack Sun(jacksunwei@gmail.com)
 *
 */
public class EscapeSynonymNormalizer implements Normalizer {

	// escaped-name -> normalized names
    private static final int MIN_RECOGNIZE_LENGTH = 3;
	private Map<String, Set<String>> map = new HashMap<String, Set<String>>();
    private String[] punctuation = {",","(",")","-",":",".","/"};
    //private String[] splitter = {"-",".","/"};
	public EscapeSynonymNormalizer(String path) {
		init(path);
	}

	@Override
	public boolean canNormalize(String name) {
		if (name == null)
			return false;
        name = name.toLowerCase();
		if(map.containsKey(name) && map.get(name).size() > 0)
            return true;
        return false;
	}

    @Override
    public boolean canNormalize(Set<String> names) {
        if (names.size() == 0)
            return false;
        for(String name : names) {
            if(name == null)
                return false;
            name = name.toLowerCase();
            if (map.containsKey(name) && map.get(name).size() > 0)
                return true;
        }
        return false;
    }

    @Override
    public List<String> normalize(String name){
        name = name.toLowerCase();
        List<String> res = new ArrayList<String>();
        if (name == null) {
            return res;
        }

        Set<String> norms = map.get(name);
        String tmp = name;
        if(norms == null || norms.size() == 0){
            tmp= clean(name);
            norms = map.get(tmp);
        }
        if(norms == null || norms.size() == 0){
            norms = new HashSet<>();
            Set<String> splits = split(name,tmp);
            for(String str : splits){
                if(map.get(str)!=null) {
                    norms.addAll(map.get(str));
                    break;
                }
            }
        }
        res.addAll(norms);
        return res;
    }

	@Override
	public List<String> normalize(String name,String cleanName, Set<String> splitList) {//head of res is the name
		List<String> res = new ArrayList<String>();
        //res.add(name);
		if (name == null) {
			return res;
		}
        //String tmp = clean(name, false);
        //res.set(0,tmp);
		Set<String> norms = map.get(name.toLowerCase());
        if(norms == null || norms.size() == 0)
            norms = map.get(cleanName.toLowerCase());
        /*if(norms == null || norms.size() == 0)
            norms = map.get(supCleanName.toLowerCase());*/
        if(norms == null || norms.size() == 0){
            norms = new HashSet<>();
            //Set<String> splits = split(name);
            for(String str : splitList){
                if(map.containsKey(str.toLowerCase())) {
                    norms.addAll(map.get(str.toLowerCase()));
                    //res.set(0,str);
                    //break;
                }
            }
        }
		res.addAll(norms);
		return res;
	}

	private static Normalizer geneNorm, diseaseNorm;

	public static Normalizer getGeneNormalizer() {
		if (geneNorm == null) {
			synchronized (EscapeSynonymNormalizer.class) {
				if (geneNorm == null) {
					geneNorm = new EscapeSynonymNormalizer(
							ConfigManager.getConfig("obo", "GENE_SYN"));
				}
			}
		}

		return geneNorm;
	}

	public static Normalizer getDiseaseNormalizer() {
		if (diseaseNorm == null) {
			synchronized (EscapeSynonymNormalizer.class) {
				if (diseaseNorm == null) {
					diseaseNorm = new EscapeSynonymNormalizer(
							ConfigManager.getConfig("obo", "DISEASE_SYN"));
				}
			}
		}

		return diseaseNorm;
	}

	private void init(String path) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));

			String str = null, temp[] = null, norm = null;
			while ((str = br.readLine()) != null) {
				temp = str.split(OboTerm.SPLITTER);
				norm = temp[0];
                norm = norm.toLowerCase();
				for (String name : temp) {
                    name = name.toLowerCase();
                    if (!map.containsKey(name)) {
                        map.put(name, new HashSet<String>());
                    }
                    map.get(name).add(norm);
                    String tmp = clean(name);
                    if (!map.containsKey(tmp)) {
                        map.put(tmp, new HashSet<String>());
                    }
                    map.get(tmp).add(norm);
                    tmp = suppressClean(name, true);
                    if (!map.containsKey(tmp)) {
                        map.put(tmp, new HashSet<String>());
                    }
                    map.get(tmp).add(norm);
				}
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

    private String clean(String input){
        //input = input.toLowerCase();
        input = input.replaceAll(",|\\(|\\)|-|:|\\.|/", " ");
        return input;
    }

    private String suppressClean(String input, boolean isInit){
        input = input.toLowerCase();
        //for(String tmp : punctuation){
            input = input.replaceAll(",|\\(|\\)|-|:|\\.|/", "");
        //}
        input = input.replaceAll("\\s"," ");
        //input = input.replaceAll("\\s","");
        if(input.endsWith(" "))
            input = input.substring(0,input.length()-1);
        //if(!isInit && input.length()<MIN_RECOGNIZE_LENGTH)
          //  input += "+++";//make it not matching
        return input;
    }

    private Set<String> split(String input, String cleanInput){
        Set<String> output = new HashSet<>();
        String tmpString;
        int idx, base;
        for(String tmp : punctuation){
            tmpString = input;
            base = 0;
            while(tmpString.contains(tmp)){
                idx = input.indexOf(tmp,base);
                base = idx+1;
                tmpString = input.substring(base);
                if(idx > 0) {
                    output.add(input.substring(0, idx));
                    output.add(cleanInput.substring(0, idx));
                }
                if(idx < input.length() - 1) {
                    output.add(input.substring(idx + 1));
                    output.add(cleanInput.substring(idx + 1));
                }
            }
        }
        return output;
    }
}
