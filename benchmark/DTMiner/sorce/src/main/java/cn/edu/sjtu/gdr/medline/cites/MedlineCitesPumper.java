package cn.edu.sjtu.gdr.medline.cites;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.zip.GZIPInputStream;

import cn.edu.sjtu.gdr.utils.GgrLogger;

/**
 * Medline citation pumper
 * 
 * @author Jack Sun(jacksunwei@gmail.com)
 *
 */
public class MedlineCitesPumper implements Iterator<MedlineCites> {

	private MedlineCites current, next;
	BufferedReader br;

	public MedlineCitesPumper(File gzFile) {
		try {
			br = new BufferedReader(new InputStreamReader(new GZIPInputStream(
					new FileInputStream(gzFile))));
			init();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	protected void finalize() {
		try {
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public MedlineCitesPumper(String path) {
		this(new File(path));
	}

	public MedlineCitesPumper(String parent, String filename) {
		this(new File(parent, filename));
	}

	private void init() {
		readNext();
		current = next;
		next = null;
		readNext();
	}

	@Override
	public boolean hasNext() {
		return current != null;
	}

	@Override
	public MedlineCites next() {
		if (current == null) {
			throw new RuntimeException("no more element!");
		}
		MedlineCites res = current;
		current = next;
		next = null;
		readNext();
		return res;
	}

	private static enum STATE {
		STATE_1, STATE_2, STATE_3, STATE_4
	};

	private void readNext() {
		if (next != null) {
			throw new RuntimeException("fuck you forget to clear next!");
		}
		try {
			String str = null;
			STATE state = STATE.STATE_1;

			while ((str = br.readLine()) != null) {
				switch (state) {
				case STATE_1:
					if (str.startsWith("<MedlineCitation ")) {
						next = new MedlineCites();
						next.setPmid(extractPmid(br.readLine()));
						state = STATE.STATE_2;
					}
					break;
				case STATE_2:
					if (str.startsWith("<CommentsCorrectionsList>")) {
						state = STATE.STATE_3;
					} else if (str.startsWith("</MedlineCitation>")) {
						// jump out
						return;
					}
					break;
				case STATE_3:
					if (str.startsWith("<CommentsCorrections RefType=\"Cites\">")) {
						state = STATE.STATE_4;
					} else if (str.startsWith("</CommentsCorrectionsList>")) {
						state = STATE.STATE_2;
					}
					break;
				case STATE_4:
					if (str.startsWith("<PMID ")) {
						next.addCites(extractPmid(str));
					} else if (str.startsWith("</CommentsCorrections>")) {
						state = STATE.STATE_3;
					}
					break;
				default:
					break;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String extractPmid(String pmidLine) {
		int start, end;
		do {
			if ((start = pmidLine.indexOf('>') + 1) == 0)
				break;
			if ((end = pmidLine.indexOf('<', start)) == 0)
				break;
			return pmidLine.substring(start, end);
		} while (false);
		GgrLogger.warn("fail to parse pmid, return empty string");
		return "";
	}
	

	public static void main(String[] argv) throws IOException {
		MedlineCitesPumper pumper = new MedlineCitesPumper(
				"D:\\workspace\\research\\medline-docs\\medline14n0001.xml.gz");
		BufferedWriter bw = new BufferedWriter(new FileWriter("d:\\test.txt"));
		while (pumper.hasNext()) {
			bw.write(pumper.next().toString());
			bw.newLine();
		}
		bw.flush();
		bw.close();
	}
}
