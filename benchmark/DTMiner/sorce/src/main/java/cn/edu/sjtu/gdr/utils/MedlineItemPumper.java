package cn.edu.sjtu.gdr.utils;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.zip.GZIPInputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import rdi.cn.source.MedlineItem;

public class MedlineItemPumper implements Iterator<MedlineItem> {

	private File[] fileList_;
	private int fileIndex_;
	private MedlineParser parser_;

	public MedlineItemPumper(String dir) {
		init(dir);
	}

	public MedlineItem next() {
		return parser_.next();
	}

	public boolean hasNext() {
		while (parser_ == null || !parser_.hasNext()) {
			if (!primeNext()) return false;
		}
		return true;
	}

	/**
	 * prime to next file
	 * 
	 * @return true if indeed prime to next; false if nothing can do
	 */
	private boolean primeNext() {
		if (fileIndex_ == fileList_.length) {
			return false;
		}
		File file = fileList_[fileIndex_++];
		GZIPInputStream is = null;
		try {
			is = new GZIPInputStream(new FileInputStream(file));
			parser_ = new MedlineParser(file.getName(), is);
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
		System.exit(1);
		return false;
	}

	private void init(String dir) {
		fileIndex_ = 0;
		File fileDir = new File(dir);
		this.fileList_ = fileDir.listFiles(new FileFilter() {
			public boolean accept(File pathname) {
				return pathname.getName().endsWith(".gz");
			}
		});
		Arrays.sort(this.fileList_, new Comparator<File>() {
			public int compare(File o1, File o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
	}
}
