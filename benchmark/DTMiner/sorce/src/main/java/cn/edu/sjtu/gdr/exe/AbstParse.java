package cn.edu.sjtu.gdr.exe;

import java.io.*;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.trees.GrammaticalRelation;
import edu.stanford.nlp.trees.TreeGraphNode;
import edu.stanford.nlp.trees.TypedDependency;
import org.apache.commons.lang3.StringEscapeUtils;

import cn.edu.sjtu.gdr.config.ConfigManager;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.parser.nndep.DependencyParser;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import edu.stanford.nlp.trees.GrammaticalStructure;

public class AbstParse extends Thread {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		String abst = "res/data/prog-output/input.txt";
        BufferedWriter bw = new BufferedWriter(new FileWriter("res/data/prog-output/tmp.txt"));
        DocumentPreprocessor dp = new DocumentPreprocessor(
                abst);
        List<List<HasWord>> sens = new ArrayList<List<HasWord>>();
        for (List<HasWord> sen : dp) {
            sens.add(sen);
        }

        for (List<HasWord> sen : sens) {
            List<TaggedWord> tagged = tagger.tagSentence(sen);
            GrammaticalStructure gs = parser.predict(tagged);

            bw.write(tagged.toString());
            bw.newLine();
            bw.write(gs.toString());

            bw.newLine();
        }
        bw.flush();
        bw.close();

	}

    private File infile;
    private String outdir;
    
    public AbstParse(File infile, String outdir) {
    	this.infile = infile;
    	this.outdir = outdir;
    }
    
    public void run() {
    	try {
			processFile(infile, outdir);
			System.out.println("[" + new Date() + "]\t"+ infile.getName() + "\t\tDone.");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    private static String modelPath = DependencyParser.DEFAULT_MODEL;
    private static String taggerPath = "edu/stanford/nlp/models/pos-tagger/english-left3words/english-left3words-distsim.tagger";
    
    private static MaxentTagger tagger = new MaxentTagger(taggerPath);
    private static DependencyParser parser = DependencyParser.loadFromModelFile(modelPath);
    
	static void processFile(File infile, String outdir)
			throws FileNotFoundException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(
				new GZIPInputStream(new FileInputStream(infile))));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
				new GZIPOutputStream(new FileOutputStream(new File(outdir,
						infile.getName())))));
		String str = null, abst = null;

		while ((str = br.readLine()) != null) {
			abst = StringEscapeUtils.unescapeXml(br.readLine().trim());
			DocumentPreprocessor dp = new DocumentPreprocessor(
					new StringReader(abst));
			List<List<HasWord>> sens = new ArrayList<List<HasWord>>();
			for (List<HasWord> sen : dp) {
				sens.add(sen);
			}

            if(sens.size()!=0) {
                bw.write(str);
                bw.write(" ");
                bw.write("" + sens.size());
                bw.newLine();
            }

			for (List<HasWord> sen : sens) {
				int begin = ((CoreLabel) sen.get(0)).beginPosition();
				int end = ((CoreLabel) sen.get(sen.size() - 1)).endPosition();
				bw.write(abst.substring(begin, end));
				bw.newLine();
				
				List<TaggedWord> tagged = tagger.tagSentence(sen);
				GrammaticalStructure gs = parser.predict(tagged);

                bw.write(tagged.toString());
                bw.newLine();
				bw.write(gs.toString());
                //bw.newLine();

                //bw.write(tempGs.toString());
				bw.newLine();
			}
		}

		br.close();
		bw.flush();
		bw.close();
	}

}
