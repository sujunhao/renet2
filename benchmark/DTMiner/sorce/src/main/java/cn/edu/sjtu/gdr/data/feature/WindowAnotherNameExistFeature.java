package cn.edu.sjtu.gdr.data.feature;

import cn.edu.sjtu.gdr.data.label.LabeledData;
import cn.edu.sjtu.gdr.data.label.LabeledDiseaseData;
import cn.edu.sjtu.gdr.data.label.LabeledGeneData;
import cn.edu.sjtu.gdr.data.label.LabeledRelationData;
import cn.edu.sjtu.gdr.ggr.NamePosition;

/**
 * See Feature 14
 * <p>
 * whether there exist another name between a gene and disease relation within
 * the sentence
 * 
 * @author Jack Sun(jacksunwei@gmail.com)
 *
 */
public class WindowAnotherNameExistFeature {

	public static final double EXIST = 1.0;
	public static final double NON_EXIST = 0.0;

	public static double extractRelationFeature(LabeledRelationData data) {
		return extractFeature(data.getGeneIndex(), data.getDiseaseIndex(),
				data.getSentenceLabeledData());
	}

	public static double extractFeature(NamePosition np1, NamePosition np2,
			LabeledData data) {

		if (np1.getStart() > np2.getStart()) {
			NamePosition t = np1;
			np1 = np2;
			np2 = t;
		}

		NamePosition np;

		for (LabeledGeneData geneData : data.getGeneLabels()) {
			np = geneData.getGeneIndex();
			if (np.getStart() > np1.getStart()
					&& np.getStart() < np2.getStart()) {
				return EXIST;
			}
		}

		for (LabeledDiseaseData diseaseData : data.getDiseaseLabels()) {
			np = diseaseData.getDiseaseIndex();
			if (np.getStart() > np1.getStart()
					&& np.getStart() < np2.getStart()) {
				return EXIST;
			}
		}

		return NON_EXIST;
	}

}
