package cn.edu.sjtu.gdr.data.deptree;

import edu.stanford.nlp.process.Morphology;

import java.util.ArrayList;
import java.util.List;

public class DepTreeNode {
	int index;
	DepTreeNode head;
	String reln;

	String postag;
	String word;
    String lemma;

	public DepTreeNode(int index, String word, String postag) {
		this.index = index;
		this.word = word;
		this.postag = postag;
        Morphology morph = new Morphology();
        lemma = morph.lemma(word,postag);
	}

	public void setHead(DepTreeNode head, String reln) {
		this.head = head;
		this.reln = reln;
	}

	public int getHeadIndex() {
		return head.index;
	}

	/**
	 * Get index of the path from root to this node
	 * 
	 * @return
	 */
	public List<Integer> getPathFromRoot() {
		List<Integer> res = null;
		if (head == null) {
			res = new ArrayList<Integer>();
		} else {
			res = head.getPathFromRoot();
		}
		
		res.add(index);
		return res;
	}

    public List<String> getVfeatureFromRoot() {
        List<String> res = null;
        if (head == null) {
            res = new ArrayList<String>();
            res.add(postag);
            return res;
        } else {
            res = head.getVfeatureFromRoot();
        }

        res.add(head.getPostag()+reln+postag);
        return res;
    }

    public List<String> getEfeatureFromRoot() {
        List<String> res = null;
        if (head.getHead() == null) {
            res = new ArrayList<String>();
            res.add(reln+head.getPostag()+"NULL");
            return res;
        } else {
            res = head.getEfeatureFromRoot();
        }

        res.add(reln+head.getPostag()+head.reln);
        return res;
    }

    public List<String> getWalkfeatureFromRoot() {
        List<String> res = null;
        if (head == null) {
            res = new ArrayList<String>();
            res.add(postag);
            //res.add(postag+lemma);
            return res;
        } else {
            res = head.getWalkfeatureFromRoot();
        }

        res.add(reln);
        res.add(postag);
        //res.add(postag+lemma);
        return res;
    }

	public int getIndex() {
		return index;
	}

	public DepTreeNode getHead() {
		return head;
	}

	public String getReln() {
		return reln;
	}

	public String getPostag() {
		return postag;
	}

	public String getWord() {
		return word;
	}

	public String toString() {
		return word + "/" + postag + "-" + index;
	}

}
