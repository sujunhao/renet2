package cn.edu.sjtu.gdr.graph.ranker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import cn.edu.sjtu.gdr.graph.UndirectedGraph;
import cn.edu.sjtu.gdr.graph.UndirectedGraphNode;

public class DegreeRanker extends Ranker {

	public DegreeRanker(UndirectedGraph graph) {
		super(graph);
	}

	@Override
	public int[] rank(final boolean asc) {
		List<UndirectedGraphNode> nodes = new ArrayList<UndirectedGraphNode>();
		for (Iterator<UndirectedGraphNode> itr = graph.iterator(); itr.hasNext();) {
			nodes.add(itr.next());
		}
		Collections.sort(nodes, new Comparator<UndirectedGraphNode>() {
			public int compare(UndirectedGraphNode o1, UndirectedGraphNode o2) {
				int v = o1.getOutDegree() - o2.getOutDegree();
				return asc ? v : -v;
			}
		});
		int[] ids = new int[nodes.size()];
		for (int i = 0; i < ids.length; ++i) {
			ids[i] = nodes.get(i).getId();
		}
		return ids;
	}

}
