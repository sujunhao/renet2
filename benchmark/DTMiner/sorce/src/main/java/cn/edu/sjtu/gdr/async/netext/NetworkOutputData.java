package cn.edu.sjtu.gdr.async.netext;

import java.util.ArrayList;
import java.util.List;

import cn.edu.sjtu.gdr.ggr.NameHolder;
import cn.edu.sjtu.gdr.utils.IoObject;

public class NetworkOutputData implements IoObject {

	private String pmid;
    private List<String> sentenceList;
    private List<String> parsedList;
    private List<String> taggedList;


	private List<List<NameHolder>> genesList;
	private List<List<NameHolder>> diseasesList;

	public NetworkOutputData(String pmid) {
		this.pmid = pmid;
		genesList = new ArrayList<List<NameHolder>>();
		diseasesList = new ArrayList<List<NameHolder>>();
        sentenceList = new ArrayList<>();
        parsedList = new ArrayList<>();
        taggedList = new ArrayList<>();
	}

	// add data of one sentence each time.
	public void addDataParse(List<NameHolder> genes, List<NameHolder> diseases, String sentence, String parsed, String tagged) {
		genesList.add(genes);
		diseasesList.add(diseases);
        sentenceList.add(sentence);
        parsedList.add(parsed);
        taggedList.add(tagged);
	}

    public void addData(List<NameHolder> genes, List<NameHolder> diseases, String sentence) {
        genesList.add(genes);
        diseasesList.add(diseases);
        sentenceList.add(sentence);
    }

	public String serializeParse() {
		StringBuilder sb = new StringBuilder();
		sb.append("pmid: ").append(pmid).append(" ").append(genesList.size());
		for (int i = 0; i < genesList.size(); ++i) {
			sb.append("\n");
			appendList(sb, genesList.get(i));
			sb.append("\n");
			appendList(sb, diseasesList.get(i));
            sb.append("\n");
            sb.append(sentenceList.get(i));
            sb.append("\n");
            sb.append(parsedList.get(i));
            sb.append("\n");
            sb.append(taggedList.get(i));
		}
		return sb.toString();
	}

    public String serialize() {
        StringBuilder sb = new StringBuilder();
        sb.append("pmid: ").append(pmid).append(" ").append(genesList.size());
        for (int i = 0; i < genesList.size(); ++i) {
            sb.append("\n");
            appendList(sb, genesList.get(i));
            sb.append("\n");
            appendList(sb, diseasesList.get(i));
            sb.append("\n");
            sb.append(sentenceList.get(i));
            sb.append("\n");
            sb.append(parsedList.get(i));
            sb.append("\n");
            sb.append(taggedList.get(i));
        }
        return sb.toString();
    }

	/**
	 * append list to the StringBuilder: an NameIdentifier will be output in the
	 * format of [identifier]$$$[name]
	 * 
	 * @param sb
	 * @param lst
	 */
	private void appendList(StringBuilder sb, List<NameHolder> lst) {
		if (lst.size() > 0) {
			NameHolder nh = null;

			nh = lst.get(0);
			sb.append(nh.serialize());
			for (int i = 1; i < lst.size(); ++i) {
				nh = lst.get(i);
				sb.append(NameHolder.SPLITTER).append(nh.serialize());
			}
		}
	}

}
