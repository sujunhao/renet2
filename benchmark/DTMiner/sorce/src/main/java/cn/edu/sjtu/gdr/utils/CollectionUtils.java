package cn.edu.sjtu.gdr.utils;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Map;
import java.util.Map.Entry;

public class CollectionUtils {

	public static <T> boolean exists(Collection<T> collection, T value) {
		for (T t : collection) {
			if (t.equals(value)) {
				return true;
			}
		}
		return false;
	}

	public static <T, V extends Comparable<V>> Entry<T, V>[] sortMap(
			Map<T, V> map) {
		return sortMap(map, false);
	}

	public static <T, V extends Comparable<V>> Entry<T, V>[] sortMap(
			Map<T, V> map, boolean asc) {
		@SuppressWarnings("unchecked")
		Map.Entry<T, V>[] res = new Map.Entry[0];
		res = map.entrySet().toArray(res);
		Arrays.sort(res, new Comparator<Map.Entry<T, V>>() {

			@Override
			public int compare(Entry<T, V> o1, Entry<T, V> o2) {
				if (asc)
					return o1.getValue().compareTo(o2.getValue());
				else
					return o2.getValue().compareTo(o1.getValue());
			}
		});
		
		return res;
	}
}
