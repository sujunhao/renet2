package cn.edu.sjtu.gdr.exe;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import cn.edu.sjtu.gdr.data.forsvm.RunSvmKernel;
import cn.edu.sjtu.gdr.data.label.LabeledData;
import cn.edu.sjtu.gdr.data.label.LabeledRelationData;
import cn.edu.sjtu.gdr.ggr.NamePosition;
import cn.edu.sjtu.gdr.obo.Normalizer;
import org.apache.commons.lang3.StringUtils;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.ggr.NameHolder;
import cn.edu.sjtu.gdr.obo.EscapeSynonymNormalizer;
import cn.edu.sjtu.gdr.utils.GgrLogger;
import cn.edu.sjtu.gdr.data.feature.RelationFeatureSet1;
import cn.edu.sjtu.gdr.data.forsvm.RunSvmKernel;
import svmJavaKernel.libsvm.ex.SVMPredictor;
import svmJavaKernel.libsvm.svm_model;

public class DiseaseGeneFrequencyGenerator {

	// map from [disease normalized name] -> [gene normalized] -> [Set of pmid]
	private Map<String, Map<String, Set<Integer>>> map;
	private String inpath, outpath;

	private Normalizer geneSyn = EscapeSynonymNormalizer.getGeneNormalizer();
	private Normalizer disSyn = EscapeSynonymNormalizer.getDiseaseNormalizer();

    private svm_model model;

	public DiseaseGeneFrequencyGenerator(String inpath, String outpath) throws IOException, ClassNotFoundException{
		this.inpath = inpath;
		this.outpath = outpath;
		map = new HashMap<String, Map<String, Set<Integer>>>();
        model = SVMPredictor.loadModel("svm/hybrid.model");
	}

	public void generate() throws IOException, ClassNotFoundException {
		GgrLogger.log("start reading gene-network...");

		BufferedReader br = new BufferedReader(new FileReader(inpath));
		String str = null;
		int num = 0, pmid, count = 0;

		while ((str = br.readLine()) != null) {
			pmid = Integer.parseInt(str.substring(6, str.lastIndexOf(" ")));
            GgrLogger.log("pmid: "+Integer.toString(pmid));
			num = Integer.parseInt(str.substring(str.lastIndexOf(' ') + 1));
			for (int sen = 0; sen < num; ++sen) {
				List<NameHolder> genes = line2NameList(br.readLine());
				List<NameHolder> diseases = line2NameList(br.readLine());
                String sentence = br.readLine();
				record(genes, diseases, pmid, sentence, sen);
			}

			++count;
			if ((count & 0xfffff) == 0) {
				GgrLogger.log((count >> 20) + "M papers scanned..");
			}
		}

		br.close();
		GgrLogger.log("finished reading, starting to output");
		output();
	}

	private List<NameHolder> line2NameList(String line) {
		List<NameHolder> res = new ArrayList<NameHolder>();
		if (line.trim().equals("")) {
			return res;
		}

		String tmp[] = line.split("\\$\\$\\$");
		for (int i = 0; i < tmp.length; i += 2) {
			res.add(new NameHolder().deserialize(tmp[i]));
		}

		return res;
	}

	private void record(List<NameHolder> genes, List<NameHolder> diseases,
			int pmid, String sentence, int num) throws IOException, ClassNotFoundException{
		// record disease gene co-occurance
		if (diseases.size() == 0) {
			return;
		}
		for (NameHolder disease : diseases) {
			for (NameHolder gene : genes) {
				List<String> disNorms = disSyn.normalize(disease.name);
				List<String> geneNorms = geneSyn.normalize(gene.name);
                //List<String> disNorms = disease.norms;
                //List<String> geneNorms = gene.norms;
				for (String disNorm : disNorms)
					for (String geneNorm : geneNorms) {
                        String feature = null;
                        LabeledData data = new LabeledData(Integer.toString(pmid),
                                num);
                        data.setSentence(sentence);
                        data.addRelationLabel(new NamePosition(gene.beginIndex,
                                gene.endIndex), new NamePosition(disease.beginIndex,
                                disease.endIndex), 0);
                        RelationFeatureSet1 rfs1 = new RelationFeatureSet1();
                        for (LabeledRelationData relData : data.getRelationLabels()) {
                            feature = rfs1.getHybridFeature(relData,4);
                        }
                        double prediction = RunSvmKernel.runKernel(feature,model);
                        if (prediction > 0.5) {
                            if (!map.containsKey(disNorm))
                                map.put(disNorm,
                                        new HashMap<String, Set<Integer>>());
                            Map<String, Set<Integer>> geneMap = map.get(disNorm);
                            if (!geneMap.containsKey(geneNorm))
                                geneMap.put(geneNorm, new HashSet<Integer>());
                            geneMap.get(geneNorm).add(pmid);
                        }
					}
			}
		}
	}

	private void output() throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(outpath));
		Set<String> diseases = map.keySet();
        /*
        for (String disease : diseases) {
            Map<String, Set<Integer>> geneMap = map.get(disease);
            for (String gn : geneMap.keySet()) {
                bw.write(disease + "$$$" + gn);
                Set<Integer> pmids = geneMap.get(gn);
                for( Integer p: pmids) {
                    bw.write("$$$" + p);
                }
                bw.newLine();
            }
        }
        */

		for (String disease : diseases) {
            Map<String, Set<Integer>> geneMap = map.get(disease);
			bw.write("disease: " + disease + " " + geneMap.size());
			bw.newLine();

			List<Map.Entry<String, Set<Integer>>> genes = new ArrayList<Map.Entry<String, Set<Integer>>>(
					geneMap.entrySet());
			Collections.sort(genes, new Comparator<Map.Entry<String, Set<Integer>>>() {
				public int compare(Entry<String, Set<Integer>> o1,
						Entry<String, Set<Integer>> o2) {
					return o2.getValue().size() - o1.getValue().size();
				}
			});
			for(Map.Entry<String, Set<Integer>> entry: genes) {
				bw.write("gene: " + entry.getKey());
				bw.newLine();
				bw.write(StringUtils.join(entry.getValue(), "\t"));
				bw.newLine();
			}
		}

		bw.flush();
		bw.close();

		GgrLogger.log("finish outputting...");
	}

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		String inpath = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"exe", "GENE_NETWORK_FILE_CLEAN");
		String outpath = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"exe", "DISEASE_GENE_FREQ");
		new DiseaseGeneFrequencyGenerator(inpath, outpath).generate();
	}

}
