package cn.edu.sjtu.gdr.exe;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.lang3.StringEscapeUtils;

import cn.edu.sjtu.gdr.config.ConfigManager;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.process.DocumentPreprocessor;

public class AbstractSentenceSegmenterExe {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		String indir = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"exe", "MEDLINE_ABSTRACT");
		String outdir = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"exe", "MEDLINE_ABSTRACT_SEG");
		File[] files = new File(indir).listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				return pathname.getName().endsWith("gz");
			}
		});
		Arrays.sort(files, new Comparator<File>() {
			@Override
			public int compare(File o1, File o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});

		for (File infile : files) {
			processFile(infile, outdir);
		}
	}

	static void processFile(File infile, String outdir)
			throws FileNotFoundException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(
				new GZIPInputStream(new FileInputStream(infile))));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
				new GZIPOutputStream(new FileOutputStream(new File(outdir,
						infile.getName())))));
		String str = null, abst = null;

		while ((str = br.readLine()) != null) {
			abst = StringEscapeUtils.unescapeXml(br.readLine().trim());
			DocumentPreprocessor dp = new DocumentPreprocessor(
					new StringReader(abst));
			List<List<HasWord>> sens = new ArrayList<List<HasWord>>();
			for (List<HasWord> sen : dp) {
				sens.add(sen);
			}

			bw.write(str);
			bw.write(" ");
			bw.write("" + sens.size());
			bw.newLine();

			for (List<HasWord> sen : sens) {
				int begin = ((CoreLabel) sen.get(0)).beginPosition();
				int end = ((CoreLabel) sen.get(sen.size() - 1)).endPosition();
				bw.write(abst.substring(begin, end));
				bw.newLine();
			}
		}

		br.close();
		bw.flush();
		bw.close();
	}
}
