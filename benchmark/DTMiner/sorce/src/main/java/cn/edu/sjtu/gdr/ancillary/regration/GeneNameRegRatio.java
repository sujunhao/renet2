package cn.edu.sjtu.gdr.ancillary.regration;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.ggr.NameIdentifier;
import cn.edu.sjtu.gdr.utils.GgrLogger;
import cn.edu.sjtu.gdr.utils.NameCount;

public class GeneNameRegRatio {

	Map<String, Map<String, NameCount<String>>> geneMap;
	Map<String, NameCount<String>[]> buffer;

	public GeneNameRegRatio(String path) {
		geneMap = new HashMap<String, Map<String, NameCount<String>>>();
		buffer = new HashMap<String, NameCount<String>[]>();
		readAll(path);
	}

	@SuppressWarnings("unchecked")
	public NameCount<String>[] analyse(String geneIdent) {
		geneIdent = geneIdent.toLowerCase();
		NameCount<String>[] res = buffer.get(geneIdent);
		if (res != null) {
			return res;
		}

		Map<String, NameCount<String>> map = geneMap.get(geneIdent);
		if (map == null) {
			return null;
		}

		map.remove(geneIdent);
		res = new NameCount[0];
		res = map.values().toArray(res);
		Arrays.sort(res, new Comparator<NameCount<String>>() {
			@Override
			public int compare(NameCount<String> o1, NameCount<String> o2) {
				return o1.getCount() - o2.getCount();
			}
		});

		buffer.put(geneIdent, res);
		return res;
	}

	private void readAll(String path) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			String str = null;
			int count = 0;

			while ((str = br.readLine()) != null) {
				int num = Integer
						.parseInt(str.substring(str.lastIndexOf(' ') + 1));
				for (int i = 0; i < num; ++i) {
					List<NameIdentifier> res = line2NameList(br.readLine());
					for (NameIdentifier ni : res) {
						incName(ni.identifier, ni.name);
					}
					br.readLine(); // skip disease
				}
				
				++count;
				if ((count & 0xfffff) == 0) {
					GgrLogger.log((count >> 20) + "M papers scanned..");
				}
			}

			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void incName(String geneIdent, String geneName) {
		geneIdent = geneIdent.toLowerCase();
		Map<String, NameCount<String>> identMap = geneMap.get(geneIdent);
		if (identMap == null) {
			identMap = new HashMap<String, NameCount<String>>();
			geneMap.put(geneIdent, identMap);
		}
		NameCount<String> count = identMap.get(geneName);
		if (count == null) {
			count = new NameCount<String>(geneName, 0);
			identMap.put(geneName, count);
		}
		count.inc();
	}

	private List<NameIdentifier> line2NameList(String line) {
		List<NameIdentifier> res = new ArrayList<NameIdentifier>();
		if (line.trim().equals("")) {
			return res;
		}
		String[] tmp = line.split("\\$\\$\\$");
		for (int i = 0; i < tmp.length; i += 2) {
			res.add(new NameIdentifier(tmp[i + 1], tmp[i]));
		}
		return res;
	}

	public static void main(String[] args) throws IOException {
		// TODO GENE_NETWORK_FILE file format modified, update needed
		String path = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG, "exe",
				"GENE_NETWORK_FILE");
		GgrLogger.log("Loading...");
		GeneNameRegRatio gnrr = new GeneNameRegRatio(path);
		GgrLogger.log("loading finished..");

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String str = null;
		NameCount<String>[] res = null;
		while (true) {
			System.out.print("please input geneIdent: ");
			str = br.readLine().trim();
			res = gnrr.analyse(str);
			if (res == null) {
				System.out.println("unfound gene id");
			} else {
				for (NameCount<String> nc : res) {
					System.out.println(nc.getName() + " " + nc.getCount());
				}
			}
		}
	}

}
