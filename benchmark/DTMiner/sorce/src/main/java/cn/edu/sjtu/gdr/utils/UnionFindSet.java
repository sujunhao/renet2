package cn.edu.sjtu.gdr.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * Union-Find Set
 * 
 * @author Jack Sun(jacksunwei@gmail.com)
 *
 *         used for gene normalization
 */
public class UnionFindSet<V> {

	private Map<V, SetNode<V>> map_;
	
	public UnionFindSet() {
		map_ = new HashMap<V, SetNode<V>>();
	}

	public V find(V value) {
		SetNode<V> node = map_.get(value);
		if (node == null) {
			return null;
		}
		return findNode(node).getValue();
	}

	public void union(V v1, V v2) {
		add(v1);
		add(v2);
		SetNode<V> n1 = map_.get(v1), n2 = map_.get(v2);
		findNode(n1).setParent(findNode(n2));
	}

	public void add(V value) {
		if (map_.containsKey(value)) {
			return;
		}
		map_.put(value, new SetNode<V>(value));
	}
	
	private SetNode<V> findNode(SetNode<V> node) {
		SetNode<V> parent = node.getParent();
		if (parent != node) {
			node.setParent(findNode(parent));
			return node.getParent();
		}
		return node;
	}

	private static class SetNode<V> {
		private V value_;
		private SetNode<V> parent_;

		SetNode(V value) {
			this.value_ = value;
			parent_ = this;
		}

		void setParent(SetNode<V> parent) {
			this.parent_ = parent;
		}

		V getValue() {
			return value_;
		}

		SetNode<V> getParent() {
			return parent_;
		}
	}
}
