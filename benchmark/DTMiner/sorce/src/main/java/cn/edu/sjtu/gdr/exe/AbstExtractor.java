package cn.edu.sjtu.gdr.exe;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.zip.GZIPOutputStream;

import rdi.cn.source.MedlineItem;
import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.medline.abst.MedlineAbstPumper;

public class AbstExtractor {

	private String indir;
	private String outdir;
	private File[] files;

	public AbstExtractor(String indir, String outdir) {
		this.indir = indir;
		this.outdir = outdir;
		init();
	}

	public void run() throws FileNotFoundException, IOException {
		MedlineAbstPumper pumper = null;
		MedlineItem item = null;
		for (File gzfile : files) {
			pumper = new MedlineAbstPumper(gzfile);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
					new GZIPOutputStream(new FileOutputStream(new File(outdir,
							filename(gzfile.getName()))))));
			while (pumper.hasNext()) {
				item = pumper.next();
				bw.write("pmid: ");
				bw.write(item.getPmid());
				bw.newLine();
				//bw.write(item.getText());
                bw.write(item.getTitle());
				bw.newLine();
			}
			System.out.println(gzfile.getName() + " finished");
			bw.flush();
			bw.close();
		}
	}

	private String filename(String infile) {
		return new String(infile.substring(0, infile.length() - 7) + ".gz");
	}

	private void init() {
		File dir = new File(indir);
		files = dir.listFiles(new FileFilter() {
			public boolean accept(File pathname) {
				return pathname.getName().endsWith(".gz");
			}
		});
		Arrays.sort(files, new Comparator<File>() {
			public int compare(File o1, File o2) {
				// fuck you, reverse order
				return -o1.getName().compareTo(o2.getName());
			}
		});
	}

	public static void main(String[] args) throws FileNotFoundException,
			IOException {
		String path = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"doc-dir", "MEDLINE_DIR");
		//String out = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG, "exe",
		//		"MEDLINE_ABSTRACT");
        String out = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG, "exe",
                "MEDLINE_ABSTRACT_TITLE");
		new AbstExtractor(path, out).run();
	}

}
