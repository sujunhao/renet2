package cn.edu.sjtu.gdr.graph;

import cn.edu.sjtu.gdr.config.ConfigManager;

public class GeneGraph {
	private volatile static UndirectedGraph geneGraph = null;

	public static UndirectedGraph getInstance() {
		if (geneGraph == null) {
			synchronized (GeneGraph.class) {
				if (geneGraph == null) {
					// readGeneGraph();
					geneGraph = new UndirectedGraphReader(ConfigManager.getConfig(
							ConfigManager.MAIN_CONFIG, "exe", "GENE_GRAPH"))
							.read();
				}
			}
		}
		return geneGraph;
	}

}
