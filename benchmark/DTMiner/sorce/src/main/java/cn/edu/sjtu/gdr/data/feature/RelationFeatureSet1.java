package cn.edu.sjtu.gdr.data.feature;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cn.edu.sjtu.gdr.data.label.LabeledData;
import cn.edu.sjtu.gdr.data.label.LabeledDataParser;
import cn.edu.sjtu.gdr.data.label.LabeledDiseaseData;
import cn.edu.sjtu.gdr.data.label.LabeledGeneData;
import cn.edu.sjtu.gdr.data.label.LabeledRelationData;
import cn.edu.sjtu.gdr.utils.GgrLogger;
import edu.stanford.nlp.util.StringUtils;

/**
 * 参见OneNote Feature of Sentence
 * 
 * @author Jack Sun(jacksunwei@gmail.com)
 *
 */
public class RelationFeatureSet1 {

    public String getHybridFeature(LabeledRelationData data, int n) {
        List<String> res = new ArrayList<String>();

        res.add(Integer.toString(data.getLabel())); // label

        /*
        // 1
        res.add(res.size()
                + ":$:"
                + Double.toString(WindowWnRootFeature
                .extractRelationGeneFeature(data)));

        // 2
        res.add(res.size()
                + ":$:"
                + Double.toString(WindowWnRootFeature
                .extractRelationDiseaseFeature(data)));
        */

        /*
        // 3
        res.add(res.size()
                + ":"
                + Double.toString(WindowWnTagNNFeature
                .extractRelationGeneFeature(data)));

        // 4
        res.add(res.size()
                + ":"
                + Double.toString(WindowWnTagNNFeature
                .extractRelationDiseaseFeature(data)));
        */

        /*// 5
        res.add(res.size()
                + ":$:"
                + Double.toString(WindowWordCountFeature
                .extractRelationGeneFeature(data)));

        // 6
        res.add(res.size()
                + ":$:"
                + Double.toString(WindowWordCountFeature
                .extractRelationDiseaseFeature(data)));

        // 7
        res.add(res.size()
                + ":$:"
                + Double.toString(WindowWordCountFeature
                .extractRelationGeneDiseaseDistanceFeature(data)));

        // 8
        res.add(res.size()
                + ":$:"
                + Double.toString(WindowRootLCAFeature
                .extractRelationGeneDiseaseVbFeature(data)));

        // 9
        res.add(res.size()
                + ":$:"
                + Double.toString(WindowRootLCAFeature
                .extractRelationGeneDiseasePrepFeature(data)));

        // 10
        res.add(res.size()
                + ":$:"
                + Double.toString(WindowRootLCAFeature
                .extractRelationGeneDiseaseNNFeature(data)));

        // 11
        res.add(res.size()
                + ":$:"
                + Double.toString(WindowRootLCAFeature
                .extractRelationGeneDiseaseNoneFeature(data)));

        // 12
        res.add(res.size()
                + ":$:"
                + Double.toString(WindowBetweenWordsFeature
                .extractRelationFeature(data)));

        // 13
        res.add(res.size()
                + ":$:"
                + Double.toString(WindowDependencyPathLengthFeature
                .extractRelationGeneFeature(data)));

        // 14
        res.add(res.size()
                + ":$:"
                + Double.toString(WindowAnotherNameExistFeature
                .extractRelationFeature(data)));
*/
        /*
        // 15
        res.add(res.size()
                + ":"
                + Double.toString(WindowRootLengthFeature
                .extractRelationGeneFeature(data)));

        // 16
        res.add(res.size()
                + ":"
                + Double.toString(WindowRootLengthFeature
                .extractRelationDiseaseFeature(data)));
        */
        /*Map<String,Map<String,Integer>> gcFeature = globalContext.extract(data);
        Map<String,Integer> tempMap;
        tempMap = gcFeature.get("F");
        for(String tempString : tempMap.keySet()){
            res.add(res.size()+":"+tempString+"\t"+tempMap.get(tempString));
        }
        res.add(res.size()+":"+"F|");

        tempMap = gcFeature.get("B");
        for(String tempString : tempMap.keySet()){
            res.add(res.size()+":"+tempString+"\t"+tempMap.get(tempString));
        }
        res.add(res.size()+":"+"B|");

        tempMap = gcFeature.get("A");
        for(String tempString : tempMap.keySet()){
            res.add(res.size()+":"+tempString+"\t"+tempMap.get(tempString));
        }
        res.add(res.size()+":"+"A|");*/

        List<String> lcFeature = localContext.extract(data);
        for(String tempString : lcFeature){
            res.add(res.size()+":"+tempString);
        }
        List<String> shortestPathFeatures = ShortestDepPath.extractRelationFeature(data,n);
        for(String shortestPathFeature : shortestPathFeatures){
            res.add(res.size()+":"+shortestPathFeature);
        }
        res.add(res.size()+":"+"S|");
        List<String> rootToLCAFeatures = RootToLCA.extractRelationFeature(data,n);
        for(String rootToLCAFeature : rootToLCAFeatures){
            res.add(res.size()+":"+rootToLCAFeature);
        }
        /*List<String> walkFeatures = WindowBetweenWalkFeature
                .extractRelationFeature(data);
        for(String walkFeature : walkFeatures){
            res.add(res.size()
                    + ":"
                    + walkFeature);
        }*/

        return StringUtils.join(res, "\t\t");
    }


	public static void main(String[] argv) throws IOException {
		BufferedWriter bwTrain = new BufferedWriter(new FileWriter(
				"res/experiment/svmdata.hybrid"+argv[3]));

		GgrLogger.log("start reading labeled data...");
        String pathYN = "res/GAD_Corpus_IBIgroup/GAD_Y_N.csv";
        String pathF = "res/GAD_Corpus_IBIgroup/GAD_F.csv";
        String pathO = "res/experiment/labeled-more-exp.txt";
        int n = 4;
        if(argv.length>=1 && argv[0].equals("0")){
            pathYN = "";
        }
        if(argv.length>=2 && argv[1].equals("0")){
            pathF = "";
        }
        if(argv.length>=3 && argv[2].equals("0")){
            pathO = "";
        }
        /*if(argv.length>=4){
            n = Integer.parseInt(argv[3]);
        }*/
		LabeledDataParser parser = new LabeledDataParser(pathYN,pathF,pathO);
		GgrLogger.log("finish reading labeled data...");

		RelationFeatureSet1 rfs1 = new RelationFeatureSet1();
		for (LabeledData data : parser) {
			for (LabeledRelationData relData : data.getRelationLabels()) {
                //if(isValidGene(data,relData)&&isValidDisease(data,relData)) {
                    bwTrain.write(rfs1.getHybridFeature(relData,n));
                    bwTrain.newLine();
                //}
			}
		}

		bwTrain.flush();
		bwTrain.close();
	}

    public  static boolean isValidGene(LabeledData data, LabeledRelationData relData){
        for(LabeledGeneData geneData : data.getGeneLabels()){
            if(relData.getGeneIndex().equals(geneData.getGeneIndex()))
                if(geneData.getLabel()==LabeledGeneData.IS_GENE)
                    return true;
        }
        return false;
    }

    public  static boolean isValidDisease(LabeledData data, LabeledRelationData relData){
        for(LabeledDiseaseData diseaseData : data.getDiseaseLabels()){
            if(relData.getDiseaseIndex().equals(diseaseData.getDiseaseIndex()))
                if(diseaseData.getLabel()==LabeledDiseaseData.IS_DISEASE)
                    return true;
        }
        return false;
    }

	public static boolean isAllNameRight(LabeledData data) {
		for (LabeledGeneData geneData : data.getGeneLabels()) {
			if (geneData.getLabel() != LabeledGeneData.IS_GENE) {
				return false;
			}
		}

		for (LabeledDiseaseData diseaseData : data.getDiseaseLabels()) {
			if (diseaseData.getLabel() != LabeledDiseaseData.IS_DISEASE) {
				return false;
			}
		}

		return true;
	}
}
