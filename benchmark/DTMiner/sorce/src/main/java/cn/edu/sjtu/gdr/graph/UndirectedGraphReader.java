package cn.edu.sjtu.gdr.graph;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Pattern;

public class UndirectedGraphReader {

	private String inpath;

	public UndirectedGraphReader(String path) {
		this.inpath = path;
	}

	public UndirectedGraph read() {
		UndirectedGraph graph = null;

		try {
			BufferedReader br = new BufferedReader(new FileReader(inpath));
			graph = new UndirectedGraph();
			
			UndirectedGraphNode gn = null;
			String str = null, edges[] = null, pair[] = null;

			while ((str = br.readLine()) != null) {
				int id = Integer.parseInt(str.substring(UndirectedGraphOutputter.nodePrefix.length()));
				gn = graph.ensureAndGetNode(id);
				
				str = br.readLine().trim();
				if (str.length() == 0) continue;
				edges = str.split(UndirectedGraphOutputter.EDGE_SPLITTER);
				for (String edge : edges) {
					pair = edge.split(Pattern.quote(UndirectedGraphOutputter.WEIGHT_SPLITTER));
					gn.addOutEdge(Integer.parseInt(pair[0]), Integer.parseInt(pair[1]));
				}
			}

			br.close();
			return graph;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}
}
