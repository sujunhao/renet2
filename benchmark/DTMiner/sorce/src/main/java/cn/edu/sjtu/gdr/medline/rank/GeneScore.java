package cn.edu.sjtu.gdr.medline.rank;

public class GeneScore implements Comparable<GeneScore>{
	
	protected String name;
	protected double score;
	
	public GeneScore(String name, double score) {
		this.name = name;
		this.score = score;
	}

	public double getScore() {
		return score;
	}
	public String getName() {
		return name;
	}
	
	@Override
	public int compareTo(GeneScore o) {
		if (score < o.score) {
			return -1;
		} else if (score > o.score) {
			return 1;
		} else {
			return 0;
		}
	}
}
