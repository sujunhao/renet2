package cn.edu.sjtu.gdr.service.dgfreq;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;

import cn.edu.sjtu.gdr.service.RequestWrapper;
import cn.edu.sjtu.gdr.service.ServiceThread;

public class DiseaseGeneFreqThread extends ServiceThread {
	public static final String SERVICE_NAME = "diseaseGeneFreq";

	public DiseaseGeneFreqThread() {
		super();
	}

	@Override
	public void run() {
		while (true) {
			try {
				RequestWrapper request = buffer.pop();
				if (request == null) {
					synchronized (this) {
						wait();
					}
				} else {
					BufferedWriter bw = new BufferedWriter(
							new OutputStreamWriter(
									request.client.getOutputStream()));
					String strs[] = request.body.trim().split("###");
					String disease = strs[0];
					boolean reverse = Boolean.valueOf(strs[1]);
					System.out.println(disease);
					DiseaseGeneFrequency dgf = DiseaseGeneFrequency
							.getInstance();
					List<DGFResultNode> res = dgf.getResult(disease, reverse);
					bw.write("" + res.size());
					bw.newLine();
					for (DGFResultNode node : res) {
						bw.write(node.toString());
						bw.newLine();
					}
					bw.flush();
					bw.close();
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
