package cn.edu.sjtu.gdr.medline.articletype;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import cn.edu.sjtu.gdr.medline.json.MedlineCitationJson;

public class ClinicalTrialTester extends ArticleTypeTester{

	private static final String[] TYPE_PATH = new String[] { "medline",
			"MedlineCitation", "Article", "PublicationTypeList",
			"PublicationType" };

	private static final String TEXT = "Clinical Trial";

	@Override
	public Boolean apply(MedlineCitationJson mc) {
		JsonNode node = mc.getPath(TYPE_PATH);
		if (node.isArray()) {
			ArrayNode arr = (ArrayNode) node;
			for (int i = 0; i < arr.size(); ++i) {
				if (arr.path(i).asText().equals(TEXT))
					return true;
			}
		}
		return false;
	}

	@Override
	public String name() {
		return "ClinicalTrial";
	}

}
