package cn.edu.sjtu.gdr.data.label;

import java.lang.ref.WeakReference;

import cn.edu.sjtu.gdr.ggr.NamePosition;

public class LabeledGeneData {

	public static final int IS_GENE = 1;
	public static final int NOT_GENE = 0;
	
	int count;
	WeakReference<LabeledData> labeledData;
	NamePosition geneIndex;
	int label;
	
	public LabeledGeneData(LabeledData data, int count, NamePosition geneIndex, int label) {
		labeledData = new WeakReference<LabeledData>(data);
		this.count = count;
		this.geneIndex = geneIndex;
		this.label = label;
	}

	public int getCount() {
		return count;
	}

	public WeakReference<LabeledData> getLabeledData() {
		return labeledData;
	}

	public NamePosition getGeneIndex() {
		return geneIndex;
	}

	public int getLabel() {
		return label;
	}
}
