package cn.edu.sjtu.gdr.evaluation;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cn.edu.sjtu.gdr.medline.rank.GeneScore;

public class Evaluator {
 
	public static int CUT_OFF = 100;
	private static DisGeNet curatedAsso = null;
	public static String[] test = {"Kidney Failure, Chronic", "Kidney Neoplasms",
			"Acute Kidney Injury", "Cardiovascular Diseases", "Lung Neoplasms"};
	
	public static void evaluation(String disease, List<GeneScore> genes) {
		
		if( curatedAsso == null )
			curatedAsso = DisGeNet.getCuratedAsso();
		
		Map<String, Double> disGenesMap = curatedAsso.getGeneList(disease);
		Set<String> disGenes = disGenesMap.keySet();
		
		System.out.println(String.format("Ground Truth: %d\tGene List Size: %d", disGenes.size(), genes.size()));
		int c = 0;
//		int min = genes.size();
		int min = CUT_OFF < genes.size() ? CUT_OFF : genes.size();
		for( int i = 0; i < min; ++ i )
			if( disGenes.contains(genes.get(i).getName())) {
				System.out.println(genes.get(i).getName());
				++ c;
			}
		double precision = 1.0 * c / min,
				recall = 1.0 * c / disGenes.size();
		double f1 = 2 * precision * recall / (precision + recall);
		System.out.println(String.format("Precision: %.3f\tRecall: %.3f", precision, recall));
		System.out.println(String.format("Hit: %d\tF-score: %.3f", c, f1));
	}
	
	public static List<String> readList(String path) {
		List<String> result = new ArrayList<String>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			String line;
			while( (line = br.readLine()) != null ) {
				result.add(line.split("\t")[1]);
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static List<String> readComList(String path) {
		List<String> result = new ArrayList<String>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			String line;
			while( (line = br.readLine()) != null ) {
				result.add(line);
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public static List<String> aggregation(String path1, String path2, int top) {
		ArrayList<ArrayList<String>> lists = new ArrayList<ArrayList<String>>();
		ArrayList<String> list1 = new ArrayList<String>(readList(path1));
		ArrayList<String> list2 = new ArrayList<String>(readList(path2));
		if( top > 0 && top < list1.size())
			lists.add(new ArrayList<String>(list1.subList(0, top)));
		else
			lists.add(list1);
		if( top > 0 && top < list2.size())
			lists.add(new ArrayList<String>(list2.subList(0, top)));
		else
			lists.add(list2);
		return RankingAggregation.rankingAggreg(lists);
	}

    public static List<String> aggregation1(ArrayList<String> list1, ArrayList<String> list2, ArrayList<String> list3, int top) { //freq, coauthor sup, paper cite sup
        ArrayList<ArrayList<String>> lists = new ArrayList<ArrayList<String>>();
        if( top > 0 && top < list1.size())
            lists.add(new ArrayList<String>(list1.subList(0, top)));
        else
            lists.add(list1);
        if( top > 0 && top < list2.size())
            lists.add(new ArrayList<String>(list2.subList(0, top)));
        else
            lists.add(list2);
        if( top > 0 && top < list3.size())
            lists.add(new ArrayList<String>(list3.subList(0, top)));
        else
            lists.add(list3);
        RankingAggregation aggr = new RankingAggregation();
        return aggr.rankingAggreg(lists);
    }

    public static List<String> aggregation2(ArrayList<String> list1, ArrayList<String> list2, int top) { //coauthor sup, paper cite sup
        ArrayList<ArrayList<String>> lists = new ArrayList<ArrayList<String>>();
        if( top > 0 && top < list1.size())
            lists.add(new ArrayList<String>(list1.subList(0, top)));
        else
            lists.add(list1);
        if( top > 0 && top < list2.size())
            lists.add(new ArrayList<String>(list2.subList(0, top)));
        else
            lists.add(list2);
        RankingAggregation aggr = new RankingAggregation();
        return aggr.rankingAggreg(lists);
    }

    public static void main(String[] args) throws IOException{
        String path1 = "res/data/prog-output/score.CoAuthorSuppress.DF";
        String path2 = "res/data/prog-output/score.FreqGeneScore.DF";
        String path3 = "res/data/prog-output/score.PaperCiteSuppress.DF";
        String outpath = "res/data/prog-output/rank.aggr-2-200";
        BufferedReader br1 = new BufferedReader(new FileReader(path1));
        BufferedReader br2 = new BufferedReader(new FileReader(path2));
        BufferedReader br3 = new BufferedReader(new FileReader(path3));
        BufferedWriter bw = new BufferedWriter(new FileWriter(outpath));
        String line1, line2, line3;
        ArrayList<String> list1 = new ArrayList<>();
        ArrayList<String> list2 = new ArrayList<>();
        ArrayList<String> list3 = new ArrayList<>();
        List<String> res;
        String disease;
        int len1, len2, len3;
        int k;
        while((line1 = br1.readLine())!=null && (line2 = br2.readLine())!=null && (line3 = br3.readLine())!=null){
            list1.clear();
            list2.clear();
            list3.clear();
            disease = line1.substring(0,line1.lastIndexOf(" "));
            len1 = Integer.valueOf(line1.substring(line1.lastIndexOf(" ")+1));
            len2 = Integer.valueOf(line2.substring(line2.lastIndexOf(" ")+1));
            len3 = Integer.valueOf(line3.substring(line3.lastIndexOf(" ")+1));
            for(int i=0; i<len1; i++){
                line1 = br1.readLine();
                list1.add(line1.split("\t")[1]);
            }
            for(int i=0; i<len2; i++){
                line2 = br2.readLine();
                list2.add(line2.split("\t")[1]);
            }
            for(int i=0; i<len3; i++){
                line3 = br3.readLine();
                list3.add(line3.split("\t")[1]);
            }
            System.out.println("disease: "+disease);
            //res = aggregation1(list1,list2,list3,100);
            res = aggregation2(list1,list3,200);
            bw.write(disease+"\t"+Integer.toString(res.size())+"\n");
            k = 1;
            for(String item: res){
                bw.write(Integer.toString(k)+"\t"+item);
                bw.newLine();
                k++;
            }
            bw.flush();
        }
        br1.close();
        br2.close();
        br3.close();
        bw.close();
    }
}
