package cn.edu.sjtu.gdr.data.feature;

import java.util.List;

import cn.edu.sjtu.gdr.data.label.LabeledData;
import cn.edu.sjtu.gdr.data.label.LabeledRelationData;
import cn.edu.sjtu.gdr.ggr.NamePosition;
import edu.stanford.nlp.ling.TaggedWord;

/**
 * Gene wn的postag是否为nn （需要tag sentence） • 是nn为1，否则为0
 * 
 * Disease wn的postag是否为nn （需要tag sentence） • 是nn为1，否则为0
 * 
 * @author Jack Sun(jacksunwei@gmail.com)
 *
 */
public class WindowWnTagNNFeature {

	public static final double WN_IS_NN = 1.0;
	public static final double WN_IS_NOT_NN = 0.0;
	
	public static double extractRelationGeneFeature(LabeledRelationData data) {
		return extractFeature(data.getGeneIndex(), data.getSentenceLabeledData());
	}
	
	public static double extractRelationDiseaseFeature(LabeledRelationData data) {
		return extractFeature(data.getDiseaseIndex(), data.getSentenceLabeledData());
	}
	
	public static double extractFeature(NamePosition np, LabeledData data) {
		int wnIndex = np.getEnd() - 1;
		List<TaggedWord> tagged = data.getTagged();
		if (tagged.get(wnIndex).tag().startsWith("NN")) {
			return WN_IS_NN;
		} else {
			return WN_IS_NOT_NN;
		}
	}
}
