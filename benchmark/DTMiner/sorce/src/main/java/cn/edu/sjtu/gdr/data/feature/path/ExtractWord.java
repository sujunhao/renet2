package cn.edu.sjtu.gdr.data.feature.path;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import cn.edu.sjtu.gdr.data.label.LabeledData;
import cn.edu.sjtu.gdr.data.label.LabeledDataParser;
import cn.edu.sjtu.gdr.data.label.LabeledRelationData;
import cn.edu.sjtu.gdr.ggr.NamePosition;
import cn.edu.sjtu.gdr.utils.CollectionUtils;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.process.Morphology;

public class ExtractWord {

	private static final String[] concernedTag = new String[] { "VB", // vb
			"NN", // noun
			"IN" // prep
	};
	private static Morphology morph = new Morphology();
	private static final String positiveOutpath = "res/ancillary-exp/path-words.pos.txt";
	private static final String negtiveOutpath = "res/ancillary-exp/path-words.neg.txt";
	private static final String labeledDataPath = "res/experiment/labeled-more-exp.txt";

	private static final String featureWordPathFormat = "res/ancillary-exp/feature-words.%s.txt";

	private Map<Boolean, Map<String, Map<String, Integer>>> wordsBag;

	public ExtractWord() {
		wordsBag = new HashMap<Boolean, Map<String, Map<String, Integer>>>();
		wordsBag.put(true, oneMap());
		wordsBag.put(false, oneMap());
	}

	private Map<String, Map<String, Integer>> oneMap() {
		Map<String, Map<String, Integer>> res = new HashMap<String, Map<String, Integer>>();
		for (String tag : concernedTag) {
			res.put(tag, new HashMap<String, Integer>());
		}
		return res;
	}

	public void run() {
		LabeledDataParser parser = new LabeledDataParser(labeledDataPath,"","");

		for (LabeledData data : parser) {
			for (LabeledRelationData reln : data.getRelationLabels()) {
				handleRelation(reln);
			}
		}

		output();
	}

	private void output() {
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(
					positiveOutpath));
			outputWithTag(wordsBag.get(true), bw);
			bw.flush();
			bw.close();

			bw = new BufferedWriter(new FileWriter(negtiveOutpath));
			outputWithTag(wordsBag.get(false), bw);
			bw.flush();
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void outputWithTag(Map<String, Map<String, Integer>> map,
			BufferedWriter bw) throws IOException {
		for (String tag : concernedTag) {
			Map<String, Integer> tagMap = map.get(tag);
			Map.Entry<String, Integer>[] entries = CollectionUtils
					.sortMap(tagMap);

			bw.write("[" + tag + "]");
			bw.newLine();

			for (Entry<String, Integer> entry : entries) {
				bw.write(entry.getValue() + " " + entry.getKey());
				bw.newLine();
			}
		}
	}

	private void handleRelation(LabeledRelationData reln) {
		NamePosition geneIndex = reln.getGeneIndex();
		NamePosition diseaseIndex = reln.getDiseaseIndex();
		List<TaggedWord> words = middleWords(geneIndex, diseaseIndex, reln
				.getSentenceLabeledData().getTagged());

		Map<String, Map<String, Integer>> bag = null;
		if (reln.getLabel() == LabeledRelationData.HAS_RELATION) {
			bag = wordsBag.get(true);
		} else {
			bag = wordsBag.get(false);
		}

		for (TaggedWord tw : words) {
			for (String tag : concernedTag) {
				if (tw.tag().startsWith(tag)) {
					incMapEntry(bag.get(tag), morph.lemma(tw.word(), tw.tag()));
				}
			}
		}
	}

	private void incMapEntry(Map<String, Integer> map, String entry) {
		if (map.get(entry) == null) {
			map.put(entry, 1);
		} else {
			map.put(entry, map.get(entry) + 1);
		}
	}

	private List<TaggedWord> middleWords(NamePosition np1, NamePosition np2,
			List<TaggedWord> sentence) {
		List<TaggedWord> res = new ArrayList<TaggedWord>();
		if (np1.getStart() > np2.getStart()) {
			NamePosition temp = np1;
			np1 = np2;
			np2 = temp;
		}
		for (int i = np1.getEnd(); i < np2.getStart(); ++i) {
			res.add(sentence.get(i));
		}
		return res;
	}//what for?

	public void analyse() {
		for (String tag : concernedTag) {
			Map<String, Integer> trueWords = new HashMap<String, Integer>();
			Map<String, Double> middleWords = new HashMap<String, Double>();

			for (Map.Entry<String, Integer> entry : wordsBag.get(true).get(tag)
					.entrySet()) {
				if (wordsBag.get(false).get(tag).containsKey(entry.getKey())) {
					middleWords.put(entry.getKey(), 1.0 * entry.getValue()
							/ wordsBag.get(false).get(tag).get(entry.getKey()));
				} else {
					trueWords.put(entry.getKey(), entry.getValue());
				}
			}

			try {
				BufferedWriter bwTrue = new BufferedWriter(new FileWriter(
						String.format(featureWordPathFormat, tag + ".TRUE")));
				Map.Entry<String, Integer>[] res = CollectionUtils
						.sortMap(trueWords);
				for (Map.Entry<String, Integer> ent : res) {
					bwTrue.write(ent.getKey() + "\t" + ent.getValue());
					bwTrue.newLine();
				}
				bwTrue.flush();
				bwTrue.close();

				BufferedWriter bwMiddle = new BufferedWriter(new FileWriter(
						String.format(featureWordPathFormat, tag + ".MIDDLE")));
				for (String word : middleWords.keySet()) {
					bwMiddle.write(word + "\t"
							+ wordsBag.get(true).get(tag).get(word) + "\t"
							+ wordsBag.get(false).get(tag).get(word));
					bwMiddle.newLine();
				}
				bwMiddle.flush();
				bwMiddle.close();

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		ExtractWord ew = new ExtractWord();
		ew.run();
		ew.analyse();
	}

}
