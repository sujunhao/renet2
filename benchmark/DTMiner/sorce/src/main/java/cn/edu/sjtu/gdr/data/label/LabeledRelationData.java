package cn.edu.sjtu.gdr.data.label;

import java.lang.ref.WeakReference;

import cn.edu.sjtu.gdr.ggr.NamePosition;

public class LabeledRelationData implements Labeled {

	public static final int HAS_RELATION = 1;
	public static final int NO_RELATION = 0;

	NamePosition geneIndex, diseaseIndex;

	/** num of relation labels */
	int count;
	WeakReference<LabeledData> labeledData;
	int label;

	// sentence, dependency parsing result
	public LabeledRelationData(LabeledData data, int count,
			NamePosition geneIndex, NamePosition diseaseIndex, int label) {
		labeledData = new WeakReference<LabeledData>(data);
		this.count = count;
		this.geneIndex = geneIndex;
		this.diseaseIndex = diseaseIndex;
		this.label = label;
	}

	@Override
	public int getLabel() {
		return this.label;
	}

	@Override
	public void setLabel(int label) {
		this.label = label;
	}

	public NamePosition getGeneIndex() {
		return geneIndex;
	}

	public NamePosition getDiseaseIndex() {
		return diseaseIndex;
	}

	public LabeledData getSentenceLabeledData() {
		return labeledData.get();
	}

	public String getIdentifier() {
		return labeledData.get().getIdentifier() + "-reln-" + count;
	}
}
