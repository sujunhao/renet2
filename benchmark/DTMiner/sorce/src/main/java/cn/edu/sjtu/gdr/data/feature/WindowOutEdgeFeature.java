package cn.edu.sjtu.gdr.data.feature;

import cn.edu.sjtu.gdr.data.deptree.DepTree;
import cn.edu.sjtu.gdr.data.label.LabeledData;
import cn.edu.sjtu.gdr.data.label.LabeledRelationData;
import cn.edu.sjtu.gdr.ggr.NamePosition;

/**
 * Disease window内，时候wn是否是唯一outing edge <br>
 * Outing edge: 除wn外，其他node的head都在window内
 * 
 * @author Jack Sun(jacksunwei@gmail.com)
 *
 */
public class WindowOutEdgeFeature {

	public static final double WN_IS_ONLY_OUT_EDGE = 1.0;
	public static final double WN_IS_NOT_ONLY_OUT_EDGE = 0.0;
	
	public static double extractRelationGeneFeature(LabeledRelationData data) {
		return extractFeature(data.getGeneIndex(), data.getSentenceLabeledData());
	}
	
	public static double extractRelationDiseaseFeature(LabeledRelationData data) {
		return extractFeature(data.getDiseaseIndex(), data.getSentenceLabeledData());
	}

	public static double extractFeature(NamePosition np, LabeledData data) {
		int start = np.getStart() + 1, end = np.getEnd() + 1;
		DepTree tree = data.getDepTree();
		
		for (int i = start; i < end - 1; ++i) {
			int head = tree.getNode(i).getHeadIndex();
			if (head < start || head >= end) {
				return WN_IS_NOT_ONLY_OUT_EDGE;
			}
		}
		
		return WN_IS_ONLY_OUT_EDGE;
	}
}
