package cn.edu.sjtu.gdr.medline.abst;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import rdi.cn.source.MedlineItem;

public class ArticleTitlePumper implements Iterator<MedlineItem> {

	private BufferedReader br = null;
	private String line;
	
	public ArticleTitlePumper(String path) {
		try {
			br = new BufferedReader(new FileReader(path));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	@Override
	public boolean hasNext() {
		try {
			line = br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return line != null;
	}

	@Override
	public MedlineItem next() {
		MedlineItem item = new MedlineItem();
		item.setPmid(line);
		try {
			item.setTitle(br.readLine());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return item;
	}

}
