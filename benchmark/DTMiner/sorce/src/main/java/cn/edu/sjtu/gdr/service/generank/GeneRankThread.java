package cn.edu.sjtu.gdr.service.generank;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import cn.edu.sjtu.gdr.service.RequestWrapper;
import cn.edu.sjtu.gdr.service.ServiceThread;
import cn.edu.sjtu.gdr.utils.GgrLogger;

public class GeneRankThread extends ServiceThread {
	public static final String SERVICE_NAME = "geneRank";
	
	private static final int MAX_OUTPUT = 20;

	private static final String SPLLITER = "###";

	public GeneRankThread() {
		super();
	}

	public void run() {
		while (true) {
			try {
				RequestWrapper request = buffer.pop();
				if (request == null) {
					synchronized (this) {
						wait();
					}
				} else {
					BufferedWriter bw = new BufferedWriter(
							new OutputStreamWriter(
									request.client.getOutputStream()));
					List<String> genes = new ArrayList<String>();
					for (String gene : request.body.split(SPLLITER)) {
						genes.add(gene);
					}

					GeneGraphResult res = RequestProcesser.geneGraph(genes);

					bw.write("used genes: ");
					bw.newLine();
					bw.write(StringUtils.join(res.usedGenes, SPLLITER));
					bw.newLine();

					bw.write("used gene ident: ");
					bw.newLine();
					bw.write(StringUtils.join(res.usedGeneIdents, SPLLITER));
					bw.newLine();

					bw.write("unfound genes:");
					bw.newLine();
					bw.write(StringUtils.join(res.unfoundGenes, SPLLITER));
					bw.newLine();

					bw.write("degree-rank result: " + Math.min(MAX_OUTPUT, res.degreeResult.size()));
					bw.newLine();
					for (int i = 0; i < Math.min(MAX_OUTPUT, res.degreeResult.size()); ++i) {
						bw.write(res.degreeResult.get(i).netString());
						bw.newLine();
					}
					
					bw.write("between-rank result: " + Math.min(MAX_OUTPUT, res.betweenResult.size()));
					bw.newLine();
					for (int i = 0; i < Math.min(MAX_OUTPUT, res.betweenResult.size()); ++i) {
						bw.write(res.betweenResult.get(i).netString());
						bw.newLine();
					}
					
					bw.write("closeness-rank result: " + Math.min(MAX_OUTPUT, res.closenessResult.size()));
					bw.newLine();
					for (int i = 0; i < Math.min(MAX_OUTPUT, res.closenessResult.size()); ++i) {
						bw.write(res.closenessResult.get(i).netString());
						bw.newLine();
					}
					
					bw.write("eigen-rank result: " + Math.min(MAX_OUTPUT, res.eigenResult.size()));
					bw.newLine();
					for (int i = 0; i < Math.min(MAX_OUTPUT, res.eigenResult.size()); ++i) {
						bw.write(res.betweenResult.get(i).netString());
						bw.newLine();
					}

					bw.flush();
					bw.close();
					
					GgrLogger.log("GeneRankThread: one request finished..");

					// request.client.close();
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
