package cn.edu.sjtu.gdr.medline.pagerank;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class Rank1stMain {
	public static void main(String[] args) throws IOException {
		String disease = null;
		DiseasePageRankScoreRanker worker = new DiseasePageRankScoreRanker();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		List<RankedNode1st> res = null;

		BufferedWriter bw = null;

		while (true) {
			System.out.print("Enter Disease: ");
			disease = br.readLine().trim();
			res = worker.rank1st(disease);

			bw = new BufferedWriter(new FileWriter(disease + ".dump.csv"));

			for (RankedNode1st rn : res) {
				System.out.print(rn.name + "\t" + rn.score);
				bw.write(rn.name + "," + rn.score);
				for (String uuid : rn.uuids) {
					bw.write(",");
					bw.write(uuid);
				}
				bw.newLine();
				System.out.println();
			}

			bw.flush();
			bw.close();
		}
	}
}
