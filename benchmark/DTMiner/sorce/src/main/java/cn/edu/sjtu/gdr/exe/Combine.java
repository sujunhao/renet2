package cn.edu.sjtu.gdr.exe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.*;
import java.lang.*;
import java.io.*;
import java.net.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import cn.edu.sjtu.gdr.obo.EscapeSynonymNormalizer;
import cn.edu.sjtu.gdr.obo.Normalizer;
import cn.edu.sjtu.gdr.utils.CommonStopWord;
import cn.edu.sjtu.gdr.async.Pair;
import edu.stanford.nlp.process.Morphology;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.process.DocumentPreprocessor;

import edu.stanford.nlp.util.Triple;

public class Combine {
    private static final int MIN_ACRONYM_LENGTH = 3;//acronym length 
    private static final int MIN_RECOGNIZE_LENGTH = 3;
    private Morphology morph;
    private Normalizer geneNorm, diseaseNorm;
    private CommonStopWord csw;
	private HashMap<String, String> bingMapD;
	private HashMap<String, String> bingMapG;
	private Set<String> errorD;
	private Set<String> errorG;
    private static String taggerPath = "edu/stanford/nlp/models/pos-tagger/english-left3words/english-left3words-distsim.tagger";
    private static String spiderPath = "spider/spider.out";
	private static String filePath = "res/data/prog-output/combine/";
	private static String filePath2 = "/home/yanping/gdr/res/data/prog-output/split/";
	private static String outPath = "res/data/prog-output/final/";
	private BufferedReader br1;
	private BufferedReader br2;
	private BufferedWriter bw;
	private List<Pair<Integer, Integer>> p1, p2, pans;
	private List<String> n1, n2, nans;
	private Set<String> longTerm;
    private List<String> tagList;
	private List<String> wordList;
	Recognizer rcg;
	

    public Combine() {
        morph = new Morphology();
        csw = CommonStopWord.getInstance();
        geneNorm = EscapeSynonymNormalizer.getGeneNormalizer();
        diseaseNorm = EscapeSynonymNormalizer.getDiseaseNormalizer();
		bingMapD = new HashMap<String, String>();
		bingMapG = new HashMap<String, String>();
		errorD = new HashSet<String>();
		errorG = new HashSet<String>();
		rcg = new Recognizer();
    }


    private static String downloadPage(URL pageUrl) {
       try {
          // Open connection to URL for reading.
          BufferedReader reader =
            new BufferedReader(new InputStreamReader(pageUrl.openStream()));
          // Read page into buffer.
          String line;
          StringBuffer pageBuffer = new StringBuffer();
          while ((line = reader.readLine()) != null) {
            pageBuffer.append(line);
          }
          
          return pageBuffer.toString();
       } catch (Exception e) {

       }

       return null;
    }
	

	public String getName1(int x){
		String res = "medline15n0";
		if (x < 100) res = res + "0";
		if (x < 10)	res = res + "0";
		res = res + Integer.toString(x) + ".gz.txt";
		return res;
	}

	public String getName2(int x){
		String res = "medline15n0";
		if (x < 100) res = res + "0";
		if (x < 10)	res = res + "0";
		res = res + Integer.toString(x) + ".txt";
		return res;
	}

	boolean overlap(Pair<Integer, Integer> u, Pair<Integer, Integer> v){
		if (u.second <= v.first) return false;
		if (v.second <= u.first) return false;
		return true;
	}

	public boolean bingDisease(String buf) throws Exception{
System.out.println("Bing DISEASE " + buf);
		if (bingMapD.containsKey(buf)){
System.out.println("!!!");
			return true;
		}
		if (errorD.contains(buf)){
System.out.println("!!!");
			return false;
		}
		String pageContents = downloadPage(new URL("http://global.bing.com/search?q=" + buf + "+disease+wiki" + "&FORM=HPCNEN&setmkt=en-us&setlang=en-us"));
//		String pageContents = downloadPage(new URL("http://www.bing.com/search?q=" + buf + "+disease+wiki"));
		if (pageContents == null){
			errorD.add(buf);
			return false;
		}
		Pattern p = Pattern.compile(
        		"<a\\s+href\\s*=\\s*\"(.*?)\"\\s+h\\s*=\\s*\"(.*?)\">(.*?)/a>"
        		,Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(pageContents);
		while (m.find()){
			String sentence = m.group(3).trim();
			if (sentence.contains("Wikipedia")){
System.out.println(sentence);
				String result = rcg.recognize(sentence, false);
System.out.println(result);
				if (result != null){
					bingMapD.put(buf, result);
					return true;
				}	
				break;
			}
		}
		errorD.add(buf);
		return false;
	}

	public boolean bingGene(String buf) throws Exception{
System.out.println("Bing GENE " + buf);
		if (bingMapG.containsKey(buf)){
System.out.println("!!!");
			return true;
		}
		if (errorG.contains(buf)){
System.out.println("!!!");
			return false;
		}
		String pageContents = downloadPage(new URL("http://global.bing.com/search?q=" + buf + "+gene+wiki" + "&FORM=HPCNEN&setmkt=en-us&setlang=en-us"));
//		String pageContents = downloadPage(new URL("http://www.bing.com/search?q=" + buf + "+gene+wiki"));
		if (pageContents == null){
			errorG.add(buf);
			return false;
		}
		Pattern p = Pattern.compile(
        		"<a\\s+href\\s*=\\s*\"(.*?)\"\\s+h\\s*=\\s*\"(.*?)\">(.*?)/a>"
        		,Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(pageContents);
		while (m.find()){
			String sentence = m.group(3).trim();
			if (sentence.contains("Wikipedia")){
System.out.println(sentence);
				String result = rcg.recognize(sentence, true);
System.out.println(result);
				if (result != null){
					bingMapG.put(buf, result);
					return true;
				}
				break;
			}
		}
		errorG.add(buf);
		return false;
	}

	public void work(String t1, String t2, boolean isGene, boolean isTitle) throws Exception{
		String line1 = t1;
		String line2 = t2;
		p1 = new ArrayList<Pair<Integer, Integer>>();
		p2 = new ArrayList<Pair<Integer, Integer>>();
		pans = new ArrayList<Pair<Integer, Integer>>();
		n1 = new ArrayList<String>();
		n2 = new ArrayList<String>();
		nans = new ArrayList<String>();
		longTerm = new HashSet<String>();
        ArrayList<String> lemmas = new ArrayList<>();
        for (int i = 0; i < wordList.size(); i++) {
			lemmas.add(getLemma(tagList.get(i), i, wordList.get(i)));
        }


		if (line1.length() > 0){
			String tmp[] = line1.split("\\$\\$\\$");
			for (String element : tmp){
				String tt[] = element.split("##");
				n1.add(tt[0]);
				p1.add(new Pair<Integer, Integer>(Integer.parseInt(tt[1]), Integer.parseInt(tt[2])));
			}
		}
		if (line2.length() > 0){
			String tmp[] = line2.split("\\$\\$\\$");
			for (String element : tmp){
				String tt[] = element.split("##");
				n2.add(tt[0]);
				p2.add(new Pair<Integer, Integer>(Integer.parseInt(tt[1]), Integer.parseInt(tt[2])));
			}
		}

// dictionary

		while (p2.size() > 0){


// pick from long to short
			int k = 0, gap = 0;
			for (int i = 0; i < p2.size(); ++i){
				Pair<Integer, Integer> t = p2.get(i);
				if (t.second - t.first > gap){
					gap = t.second - t.first;
					k = i;
				}
			}

			String recRes;
            List<String> norms;
			boolean ok = true;
			if (isGene){
				norms = geneNorm.normalize(n2.get(k));
			} else {
				norms = diseaseNorm.normalize(n2.get(k));
			}

			if (norms.size() > 1){
				recRes = norms.get(0);
				norms.remove(0);
                boolean validFlag = hasLongTerm(norms, longTerm);
                if (!validFlag){
                    boolean checkFlag = checkLongTermOrNot(gap, recRes);
                    if(checkFlag) {
                        if (p1.contains(p2.get(k)))
                            validFlag = true;
                    } else{
                            validFlag = true;
                    }
				}
// found
                if (validFlag) {
					longTerm.addAll(norms);
					pans.add(p2.get(k));
					nans.add(n2.get(k));
                }
			} else {
                String lemma = lemmas.get(p2.get(k).second - 1);
                for (int i = p2.get(k).second - 2; i >= p2.get(k).first; --i)
                    lemma = wordList.get(i) + " " + lemma;
				
				if (isGene){
					norms = geneNorm.normalize(lemma);
				} else {
					norms = diseaseNorm.normalize(lemma);
				}

				if (norms.size() > 1){
					recRes = norms.get(0);
					norms.remove(0);
				    boolean validFlag = hasLongTerm(norms, longTerm);
	                if (!validFlag){
		                boolean checkFlag = checkLongTermOrNot(gap, recRes);
			            if(checkFlag) {
				            if (p1.contains(p2.get(k)))
					            validFlag = true;
		                } else{
			                    validFlag = true;
				        }
					}
// found
	                if (validFlag) {
						longTerm.addAll(norms);
						pans.add(p2.get(k));
						nans.add(n2.get(k));
					}
				}
			}
// remove used elements
			p2.remove(k);
			n2.remove(k);
		}		

// stanford ner & google
/*
		for (int i = 0; i < p1.size(); ++i){
			if (isTitle) break;
			boolean flag = false;
			for (Pair<Integer, Integer> element : pans){
				if (overlap(element, p1.get(i))){
					flag = true;
					break;
				}
			}
			if (flag){
				continue;
			}
			if (p1.get(i).second - p1.get(i).first > 1) continue;
			if (n1.get(i).length() > 3) continue;
			boolean ok = false;
			if (isGene){
				ok = bingGene(n1.get(i));
			} else {
				ok = bingDisease(n1.get(i));
			}
			if (ok){
				pans.add(p1.get(i));
				nans.add(n1.get(i));
			}
		}	
*/
// output

		for (int i = 0; i < pans.size(); ++i){
			Pair<Integer, Integer> t = pans.get(i);
			if (i > 0){
				bw.write("$$$");
			}
			bw.write(nans.get(i) + "##" + Integer.toString(t.first) + "##" + Integer.toString(t.second));
		}
		bw.newLine();
	}

	public void run() throws Exception{
		for (int caseNum = 1; caseNum < 780; ++caseNum){
			System.out.println("processing file number:" + Integer.toString(caseNum));
	        br1 = new BufferedReader(new FileReader(filePath + getName1(caseNum)));
	        br2 = new BufferedReader(new FileReader(filePath2 + getName2(caseNum)));
	        bw = new BufferedWriter(new FileWriter(outPath + getName2(caseNum)));
			String line = "", line2 = "";
			int count = 0;
			while ((line = br1.readLine()) != null){
				count += 1;
//				System.out.println(count);
				line2 = br2.readLine();
				bw.write(line);
				bw.newLine();
				String[] tmp = line.split(" ");
				int num = Integer.parseInt(tmp[2]);
				for (int ww = 0; ww < num; ++ww){
					String fuck1, fuck2, fuck3, fuck4;
					fuck1 = br1.readLine();
					fuck2 = br2.readLine();
					fuck3 = br1.readLine();
					fuck4 = br2.readLine();
					String[] fuck = new String[5];
					for (int zz = 0; zz < 3; ++zz){
						line = br1.readLine();
						line2 = br2.readLine();
						fuck[zz] = line;
					}
					tagList = new ArrayList<>();
			        wordList = new ArrayList<>();
				    parseTag(tagList, wordList, line);

					boolean flag = false;
					if (ww == 0) flag = true;
					work(fuck1, fuck2, true, flag);
					work(fuck3, fuck4, false, flag);
					for (int zz = 0; zz < 3; ++zz){
						bw.write(fuck[zz]);
						bw.newLine();
					}
//					System.out.println("step 2");
				}
			}
			br1.close();
			br2.close();
			bw.close();
		}
// output new dictionary
	    bw = new BufferedWriter(new FileWriter("dic/diseases.syn"));
		Iterator entries = bingMapD.entrySet().iterator();
		while (entries.hasNext()) {  
		    Map.Entry entry = (Map.Entry) entries.next();  
		    String key = (String)entry.getKey();  
		    String value = (String)entry.getValue();  
			bw.write("###" + value + "###" + key);
			bw.newLine();
		}  
		bw.close();

	    bw = new BufferedWriter(new FileWriter("dic/genes.syn"));
		entries = bingMapG.entrySet().iterator();
		while (entries.hasNext()) {  
		    Map.Entry entry = (Map.Entry) entries.next();  
		    String key = (String)entry.getKey();  
		    String value = (String)entry.getValue();  
			bw.write("###" + value + "###" + key);
			bw.newLine();
		}  
		bw.close();
	}

	public static void main(String args[]) throws Exception{
		new Combine().run();
	}

    private boolean checkLongTermOrNot(int windowSize, String phraseLiter) {
        if (windowSize == 1) {
            if ((csw.contains(morph
                    .stem(phraseLiter.toLowerCase()))) || csw.contains(phraseLiter.toLowerCase()))
                return true;
        }
        if (phraseLiter.length() <= MIN_ACRONYM_LENGTH) {
            return true;
        }
        return false;
    }

    private void parseTag(List<String> tagList, List<String> wordlist, String taggedIn) {
        String taggedStr = taggedIn.substring(1, taggedIn.length() - 1);
        String tags[] = taggedStr.split(", ");
        tagList.clear();
        wordlist.clear();
//		System.out.println(taggedIn);
//		System.out.println(Integer.toString(tags.length));
        for (int i = 0; i < tags.length; i++) {
            String temp = tags[i].substring(tags[i].lastIndexOf("/") + 1);
            String word = tags[i].substring(0, tags[i].lastIndexOf("/"));
            tagList.add(temp);
            wordlist.add(word);
        }
    }

    private String getLemma(String tag, int idx, String name) {
        String lemma = morph.lemma(name, tag);
        return lemma;
    }

    private boolean hasLongTerm(List<String> norms, Set<String> longTerm) {
        List<String> tmp = new ArrayList<>();
        for (String norm : norms) {
            if (longTerm.contains(norm)) {
                tmp.add(norm);
            }
        }
        if (tmp.size() != 0) {
            norms.clear();
            norms.addAll(tmp);
            return true;
        } else
            return false;
    }
}

