package cn.edu.sjtu.gdr.data.utils;

import java.util.ArrayList;
import java.util.List;

import cn.edu.sjtu.gdr.data.deptree.DepTree;
import cn.edu.sjtu.gdr.data.deptree.DepTreeNode;
import cn.edu.sjtu.gdr.ggr.NamePosition;
import cn.edu.sjtu.gdr.utils.GgrLogger;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.parser.nndep.DependencyParser;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.PennTreebankLanguagePack;
import edu.stanford.nlp.trees.Tree;
import javafx.util.Pair;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.tuple.Triple;

public class NLPToolsForFeatures {
	/*
	 * no need to be thread safe for tagger and parser, because they themselves
	 * are not thread safe. If necessary, make them thread local.
	 */

	private static MaxentTagger tagger;

	public static MaxentTagger tagger() {
		if (tagger == null) {
			tagger = new MaxentTagger(MaxentTagger.DEFAULT_JAR_PATH);
		}
		return tagger;
	}

	private static DependencyParser depParser;

	public static DependencyParser neuralDepParser() {
		if (depParser == null) {
			depParser = DependencyParser
					.loadFromModelFile(DependencyParser.DEFAULT_MODEL);
		}
		return depParser;
	}

	private static LexicalizedParser pcfgParser;

	public static LexicalizedParser pcfgParser() {
		if (pcfgParser == null) {
			pcfgParser = LexicalizedParser.loadModel();
		}
		return pcfgParser;
	}

	public static GrammaticalStructureFactory gsf;

	public static GrammaticalStructureFactory grammaticalStructureFactory() {
		if (gsf == null) {
			gsf = new PennTreebankLanguagePack().grammaticalStructureFactory();
		}
		return gsf;
	}

	public static GrammaticalStructure neuralDepParse(
			List<? extends HasWord> sentence) {
		return neuralDepParser().predict(sentence);
	}

	public static GrammaticalStructure pcfgDepParse(List<HasWord> sentence) {
		Tree tree = pcfgParser().apply(sentence);
		return grammaticalStructureFactory().newGrammaticalStructure(tree);
	}

	public static int getRootOfWindow(NamePosition window, DepTree parse) {
		for (int index = window.getEnd() - 1; index >= window.getStart(); --index) {
			int treeIndex = index + 1;
			DepTreeNode node = parse.getNode(treeIndex);
			if (node.getHead() == null) // for constituent parse
				continue;

			while (node.getIndex() > window.getStart()
					&& node.getIndex() <= window.getEnd()) {
				if (node.getHeadIndex() <= window.getStart()
						|| node.getHeadIndex() > window.getEnd())
					break;
				node = node.getHead();
			}

			return node.getIndex() - 1; // from treeIndex to tagged index
		}
		// unhappy path
		return window.getEnd() - 1;
	}

	public static List<Integer> getDependencyPath(int fromIndex, int toIndex, DepTree parse) {
		List<Integer> res = new ArrayList<>();

		int fromTreeIndex = fromIndex + 1;
		int toTreeIndex = toIndex + 1;
		List<Integer> fromNodePathFromRoot = parse.getNode(fromTreeIndex)
				.getPathFromRoot();
		List<Integer> toNodePathFromRoot = parse.getNode(toTreeIndex)
				.getPathFromRoot();

		if (fromNodePathFromRoot.get(0) != 0 || toNodePathFromRoot.get(0) != 0) {
			GgrLogger.warn(String.format(
					"no path from %d to %d in sentence", fromIndex,
					toIndex));
			return res;
		}

		int lcaTreeIndex = 0;
		for (int i = 0; i < Math.min(fromNodePathFromRoot.size(),
				toNodePathFromRoot.size()); ++i) {
			if (fromNodePathFromRoot.get(i) == toNodePathFromRoot.get(i)) {
				lcaTreeIndex = i;
			} else
				break;
		}

		for (int i = fromNodePathFromRoot.size() - 1; i > lcaTreeIndex; --i) {
			res.add(fromNodePathFromRoot.get(i) - 1);
		}
        //if (lcaTreeIndex != 0) {
            res.add(fromNodePathFromRoot.get(lcaTreeIndex) - 1);
        //}
		for (int i = lcaTreeIndex + 1; i < toNodePathFromRoot.size(); ++i) {
			res.add(toNodePathFromRoot.get(i) - 1);
		}
		res.add(fromNodePathFromRoot.get(lcaTreeIndex) - 1);//last element is LCA
		return res;
	}

    public static List<Integer> getVerbOnPath(int fromIndex, int toIndex, DepTree parse) {
        List<Integer> res = new ArrayList<>();

        int fromTreeIndex = fromIndex + 1;
        int toTreeIndex = toIndex + 1;
        List<Integer> fromNodePathFromRoot = parse.getNode(fromTreeIndex)
                .getPathFromRoot();
        List<Integer> toNodePathFromRoot = parse.getNode(toTreeIndex)
                .getPathFromRoot();

        if (fromNodePathFromRoot.get(0) != 0 || toNodePathFromRoot.get(0) != 0) {
            GgrLogger.warn(String.format(
                    "no path from %d to %d in sentence", fromIndex,
                    toIndex));
            return res;
        }

        int lcaTreeIndex = 0;
        for (int i = 0; i < Math.min(fromNodePathFromRoot.size(),
                toNodePathFromRoot.size()); ++i) {
            if (fromNodePathFromRoot.get(i) == toNodePathFromRoot.get(i)) {
                if(parse.getNode(fromNodePathFromRoot.get(i)).getPostag().startsWith("VB"))
                    res.add(fromNodePathFromRoot.get(i)-1);
                lcaTreeIndex = i;
            } else
                break;
        }

        for (int i = fromNodePathFromRoot.size() - 1; i > lcaTreeIndex; --i) {
            if(parse.getNode(fromNodePathFromRoot.get(i)).getPostag().startsWith("VB"))
                res.add(fromNodePathFromRoot.get(i)-1);
        }
        for (int i = lcaTreeIndex + 1; i < toNodePathFromRoot.size(); ++i) {
            if(parse.getNode(toNodePathFromRoot.get(i)).getPostag().startsWith("VB"))
                res.add(toNodePathFromRoot.get(i)-1);
        }
        return res;
    }

    public static List<Integer> getRootToLCA(int fromIndex, int toIndex, DepTree parse) {
        List<Integer> res = new ArrayList<>();

        int fromTreeIndex = fromIndex + 1;
        int toTreeIndex = toIndex + 1;
        List<Integer> fromNodePathFromRoot = parse.getNode(fromTreeIndex)
                .getPathFromRoot();
        List<Integer> toNodePathFromRoot = parse.getNode(toTreeIndex)
                .getPathFromRoot();

        if (fromNodePathFromRoot.get(0) != 0 || toNodePathFromRoot.get(0) != 0) {
            GgrLogger.warn(String.format(
                    "no path from %d to %d in sentence", fromIndex,
                    toIndex));
            return res;
        }

        for (int i = 1; i < Math.min(fromNodePathFromRoot.size(),
                toNodePathFromRoot.size()); ++i) {
            if (fromNodePathFromRoot.get(i) == toNodePathFromRoot.get(i)) {
                res.add(fromNodePathFromRoot.get(i)-1);
            } else
                break;
        }
        if(res.size()>0)
            res.remove(res.size()-1);
        return res;
    }
    public static List<Integer> getVbMod(int vbIndex, int size, DepTree parse) {
        List<Integer> res = new ArrayList<>();
        int vbTreeIndex = vbIndex + 1;

        for (int i = 0; i < size ; ++i) {
            if(parse.getNode(i+1).getHeadIndex() == vbIndex+1
                    && (parse.getNode(i+1).getReln().equals("neg")||
                    parse.getNode(i+1).getReln().equals("advmod")))
            res.add(i);
        }
        return res;
    }
    public static List<Integer> getNnMod(int nnIndex, int size, DepTree parse) {
        List<Integer> res = new ArrayList<>();
        int nnTreeIndex = nnIndex + 1;

        for (int i = 0; i < size ; ++i) {
            if(parse.getNode(i+1).getHeadIndex() == nnIndex+1
                    && parse.getNode(i+1).getReln().equals("det"))
                res.add(i);
        }
        return res;
    }

    public static List<String> getVfeature(int fromIndex, int toIndex,List<TaggedWord> tagged, DepTree parse) {
        List<String> vfeature = new ArrayList<>();

        int fromTreeIndex = fromIndex + 1;
        int toTreeIndex = toIndex + 1;
        List<String> fromNodeVfeatureFromRoot = parse.getNode(fromTreeIndex).getVfeatureFromRoot();
        List<String> toNodeVfeatureFromRoot = parse.getNode(toTreeIndex).getVfeatureFromRoot();

        if (!fromNodeVfeatureFromRoot.get(0).equals("-") || !toNodeVfeatureFromRoot.get(0).equals("-")) {
            GgrLogger.warn(String.format(
                    "no path from %d to %d in sentence [ %s ]", fromIndex,
                    toIndex, tagged.toString()));
            return vfeature;
        }

        int lcaTreeIndex = 0; ///////???
        for (int i = 0; i < Math.min(fromNodeVfeatureFromRoot.size(),
                toNodeVfeatureFromRoot.size()); ++i) {
            if (fromNodeVfeatureFromRoot.get(i).equals(toNodeVfeatureFromRoot.get(i))) {
                lcaTreeIndex = i;
            } else
                break;
        }

        for (int i = fromNodeVfeatureFromRoot.size() - 1; i > lcaTreeIndex; --i) {
            vfeature.add(fromNodeVfeatureFromRoot.get(i));
        }
        if (lcaTreeIndex != 0) {
            vfeature.add(fromNodeVfeatureFromRoot.get(lcaTreeIndex));
        }
        for (int i = lcaTreeIndex + 1; i < toNodeVfeatureFromRoot.size(); ++i) {
            vfeature.add(toNodeVfeatureFromRoot.get(i));
        }

        return vfeature;
    }

    public static List<String> getEfeature(int fromIndex, int toIndex,List<TaggedWord> tagged, DepTree parse) {
        List<String> efeature = new ArrayList<>();

        int fromTreeIndex = fromIndex + 1;
        int toTreeIndex = toIndex + 1;
        List<String> fromNodeEfeatureFromRoot = parse.getNode(fromTreeIndex).getEfeatureFromRoot();
        List<String> toNodeEfeatureFromRoot = parse.getNode(toTreeIndex).getEfeatureFromRoot();

        if (!fromNodeEfeatureFromRoot.get(0).startsWith("root") || !toNodeEfeatureFromRoot.get(0).startsWith("root")) {
            GgrLogger.warn(String.format(
                    "no path from %d to %d in sentence [ %s ]", fromIndex,
                    toIndex, tagged.toString()));
            return efeature;
        }

        int lcaTreeIndex = 0; ///////???
        for (int i = 0; i < Math.min(fromNodeEfeatureFromRoot.size(),
                toNodeEfeatureFromRoot.size()); ++i) {
            if (fromNodeEfeatureFromRoot.get(i).equals(toNodeEfeatureFromRoot.get(i))) {
                lcaTreeIndex = i;
            } else
                break;
        }

        for (int i = fromNodeEfeatureFromRoot.size() - 1; i > lcaTreeIndex; --i) {
            efeature.add(fromNodeEfeatureFromRoot.get(i));
        }
        if (lcaTreeIndex != 0) {
            efeature.add(fromNodeEfeatureFromRoot.get(lcaTreeIndex));
        }
        for (int i = lcaTreeIndex + 1; i < toNodeEfeatureFromRoot.size(); ++i) {
            efeature.add(toNodeEfeatureFromRoot.get(i));
        }

        return efeature;
    }

    public static List<String> getWalkfeature(int fromIndex, int toIndex, DepTree parse) {
        List<String> features = new ArrayList<>();

        int fromTreeIndex = fromIndex + 1;
        int toTreeIndex = toIndex + 1;
        List<String> fromNodeWalkfeatureFromRoot = parse.getNode(fromTreeIndex).getWalkfeatureFromRoot();
        List<String> toNodeWalkfeatureFromRoot = parse.getNode(toTreeIndex).getWalkfeatureFromRoot();

        //if (!fromNodeWalkfeatureFromRoot.get(0).equals("-root") || !toNodeWalkfeatureFromRoot.get(0).equals("-root")) {
        if (!fromNodeWalkfeatureFromRoot.get(0).equals("-") || !toNodeWalkfeatureFromRoot.get(0).equals("-")) {
            GgrLogger.warn(String.format(
                    "no path from %d to %d in sentence", fromIndex,
                    toIndex));
            return features;
        }

        int lcaTreeIndex = 0;
        for (int i = 0; i < Math.min(fromNodeWalkfeatureFromRoot.size(),
                toNodeWalkfeatureFromRoot.size()); ++i) {
            if (fromNodeWalkfeatureFromRoot.get(i).equals(toNodeWalkfeatureFromRoot.get(i))) {
                lcaTreeIndex = i;
            } else
                break;
        }

        if(lcaTreeIndex%2 == 1)
            lcaTreeIndex--;

        for (int i = fromNodeWalkfeatureFromRoot.size() - 1; i > lcaTreeIndex; --i) {
            if(i%2==0)
                features.add(fromNodeWalkfeatureFromRoot.get(i));
            else
                //features.add(fromNodeWalkfeatureFromRoot.get(i));
                features.add(fromNodeWalkfeatureFromRoot.get(i)+"left");
        }
        if (lcaTreeIndex != 0) {
            features.add(fromNodeWalkfeatureFromRoot.get(lcaTreeIndex));
        }
        for (int i = lcaTreeIndex + 1; i < toNodeWalkfeatureFromRoot.size(); ++i) {
            if(i%2==0)
                features.add(toNodeWalkfeatureFromRoot.get(i));
            else
                //features.add(toNodeWalkfeatureFromRoot.get(i));
                features.add(toNodeWalkfeatureFromRoot.get(i)+"right");
        }

        /*features.add("|");

        for(int i=0; i<parse.getSize(); i++){
            DepTreeNode tmp = parse.getNode(i);
            if(tmp.getHead() == null) continue;
            if(tmp.getReln().equals("amod")){
                DepTreeNode head = tmp.getHead();
                for(String feature : features){
                    if(feature.equals(head.getPostag()+head.getWord())) {
                        features.add(tmp.getPostag() + tmp.getWord());
                        features.add(feature);
                        break;
                    }
                }
            }
        }*/
        return features;
    }
}
