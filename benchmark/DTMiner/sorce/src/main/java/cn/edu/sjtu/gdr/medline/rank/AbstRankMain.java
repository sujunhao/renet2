package cn.edu.sjtu.gdr.medline.rank;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.evaluation.Evaluator;

public class AbstRankMain {

	public static String[] diseases = {"Kidney Failure, Chronic", "Kidney Neoplasms",
		"Acute Kidney Injury", "Cardiovascular Diseases", "Lung Neoplasms"};
	public static boolean dump = true;
	
	public static void main(String[] args) throws IOException {
		if (args.length >= 1 && args[0].equals("dump")) {
			dump = true;
		}
		String path = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"evidence", "GENE_RELATED_DISEASE_NUMBER");
		Map<String, Integer> DF = ComputeGeneDF.load(path);
		int total_num_of_disease = DF.get("");
		
		boolean comDF = false;
		if( "true".equals(ConfigManager.getConfig("disease-pagerank-score",
				"DF")))
			comDF = false;
		List<Score> scorers = new ArrayList<Score>();
//		scorers.add(new ArticleTypeScore());
//		scorers.add(new FreqGeneScore());
//		scorers.add(new PaperCiteScore());
//		scorers.add(new PaperCiteSuppress());
		scorers.add(new CoAuthorScore());
		scorers.add(new CoAuthorSuppress());

		for( String disease: diseases) {
			System.out.println(String.format("Start evaluating: %s", disease));
			Map<String, Set<Integer>> genePmids = AbstDisGene.getAbstEvidence(disease);
		
			for( Score scorer: scorers) {
				System.out.println(scorer.getClass().getSimpleName());
				List<GeneScore> genes = new ArrayList<GeneScore>();
				for(String gene: genePmids.keySet()) {
					double score = scorer.score(genePmids.get(gene));
					if( comDF )
						score *= Math.log(1.0 * total_num_of_disease / DF.get(gene));
					if( score > 0 )
						genes.add(new GeneScore(gene, score));
				}
				Collections.sort(genes, Collections.reverseOrder());
				Evaluator.evaluation(disease, genes);
				if( dump ) {
					String tag = scorer.getClass().getSimpleName();
					if( comDF )
						tag += ".DF";
					dump(disease, tag, genes);
				}
//				for( GeneScore gene: genes)
//					System.out.println(gene.getName() + "\t" + gene.getScore());
			}
		}
	}
	
	private static void dump(String disease, String tag,
			List<GeneScore> list) throws IOException {
		String path = String.format("dump/rank.%s.%s.dump", disease, tag);
		BufferedWriter bw = new BufferedWriter(new FileWriter(path));

		for (int i = 0; i < list.size(); ++ i) {
			GeneScore gs = list.get(i);
			bw.write(String.format("%d\t%s\t%s", i + 1, gs.getName(), gs.getScore()));
			bw.newLine();
		}

		bw.flush();
		bw.close();
	}
	
}
