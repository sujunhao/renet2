package cn.edu.sjtu.gdr.evaluation;

import cn.edu.sjtu.gdr.ggr.NameRecognizer;
import cn.edu.sjtu.gdr.ggr.NameRecognizerResult;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Xie Yanping on 2015/11/26.
 */
public class NerEvaluate {
    static String testPath = "res/GAD_Corpus_IBIgroup/test.tsv";
    static String outPath = "res/GAD_Corpus_IBIgroup/ourNER.txt";
    private static String taggerPath = "edu/stanford/nlp/models/pos-tagger/english-left3words/english-left3words-distsim.tagger";
    private static MaxentTagger tagger = new MaxentTagger(taggerPath);
    private static NameRecognizer recognizer = new NameRecognizer();
    private  static void process() throws IOException{

        BufferedReader br = new BufferedReader(new FileReader(testPath));
        BufferedWriter bw = new BufferedWriter(new FileWriter(outPath));
        String line;
        int count = 0;
        while((line = br.readLine())!=null) {
            DocumentPreprocessor dp = new DocumentPreprocessor(new StringReader(line));
            List<List<HasWord>> sens = new ArrayList<List<HasWord>>();
            for (List<HasWord> sen : dp) {
                sens.add(sen);
            }
            for (List<HasWord> sen : sens) {
                ++count;
                List<TaggedWord> tagged = tagger.tagSentence(sen);
                bw.write(line);
                bw.newLine();
                Set<String> longTerm = new HashSet<>();
                NameRecognizerResult recogRes = recognizer.recognize(line, tagged);
                for (String gene : recogRes.genes) {
                    bw.write(gene + "\t" + "G");
                    bw.newLine();
                }
                for (String disease : recogRes.diseases) {
                    bw.write(disease + "\t" + "D");
                    bw.newLine();
                }
                bw.flush();
            }
        }
        br.close();
        bw.close();
        System.out.println(count);
    }
    public static void main(String[] args) throws IOException{
        process();
    }
}
