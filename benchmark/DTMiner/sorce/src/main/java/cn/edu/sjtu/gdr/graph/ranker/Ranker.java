package cn.edu.sjtu.gdr.graph.ranker;

import cn.edu.sjtu.gdr.graph.UndirectedGraph;

/**
 * used for rank the nodes within one graph
 * 
 * @author Jack Sun(jacksunwei@gmail.com)
 *
 */
public abstract class Ranker {
	protected UndirectedGraph graph;

	public Ranker(UndirectedGraph graph) {
		this.graph = graph;
	}

	/**
	 * rank the node in a graph
	 * 
	 * @param asc
	 *            whether in ascending order
	 * @return an ordered list
	 */
	public abstract int[] rank(boolean asc);
}
