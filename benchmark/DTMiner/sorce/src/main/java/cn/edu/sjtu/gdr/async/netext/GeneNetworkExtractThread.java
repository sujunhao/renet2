package cn.edu.sjtu.gdr.async.netext;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cn.edu.sjtu.gdr.medline.abst.AbstParPumper;
import cn.edu.sjtu.gdr.async.LoggableThread;
import cn.edu.sjtu.gdr.ggr.NameHolder;
import cn.edu.sjtu.gdr.ggr.NamePosition;
import cn.edu.sjtu.gdr.ggr.NameRecognizer;
import cn.edu.sjtu.gdr.ggr.NameRecognizerResult;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import rdi.cn.source.ParseItem;

public class GeneNetworkExtractThread extends LoggableThread {

	private ControlData control;
	private NameRecognizer recognizer;
    private String taggerPath = "edu/stanford/nlp/models/pos-tagger/english-left3words/english-left3words-distsim.tagger";
    private MaxentTagger tagger;

	public GeneNetworkExtractThread(ControlData control) {
		this.control = control;
		recognizer = new NameRecognizer();
        tagger = new MaxentTagger(taggerPath);
	}

	public void run() {
		//TextAbstPumper pumper = null;
        AbstParPumper pumperPar = null;
		DocumentPreprocessor dp = null;
		//MedlineItem item = null;
        ParseItem itemPar = null;
		String pmid = null, parContent = null;
        List<String> abst;
        Set<String> longTerm = new HashSet<>();
		int paperCount = 0;

		while (control.isRunning()) {
            while ((pumperPar = control.parseBuffer.pop()) == null && control.isRunning()) {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                continue;
            }
            if(!control.isRunning()) break;
			log("starting a new round... processing file: " + pumperPar.fileName());
			List<NetworkOutputData> res = new ArrayList<NetworkOutputData>();
			paperCount = 0;
			while (pumperPar.hasNext()) {
                longTerm.clear();
				//item = pumper.next();
                itemPar = pumperPar.next();
				pmid = itemPar.getPmid();
				//abst = itemPar.getText();
                abst = itemPar.getAbst();
                //abst = abst.replaceAll("-"," ");
                parContent = itemPar.getContent();
                String[] temp = parContent.split("\\n");
                int i=0;
				NetworkOutputData data = new NetworkOutputData(new String(
						pmid.substring(6)));
                for(String sentence : abst){
                    dp = new DocumentPreprocessor(new StringReader(sentence));
                    List<HasWord> sent = dp.iterator().next();
                    String tag = temp[i++];
                    String gs = temp[i++];
                    List<TaggedWord> taggedWords = tagger.tagSentence(sent);
                    NameRecognizerResult recogRes = recognizer.recognize(sentence, taggedWords);
                    data.addDataParse(
                            translateResult(recogRes.genes, recogRes.genesPos, recogRes.genesSenIndex),
                            translateResult(recogRes.diseases,recogRes.diseasesPos, recogRes.diseasesSenIndex), sentence, gs, tag);
                }
				res.add(data);

				++paperCount;
				if ((paperCount & 0x3ff) == 0) {
					log((paperCount >> 10) + "K papers processed");
				}
			}
			log("end processing file: " + pumperPar.fileName());

			control.outputBuffer.push(res);
		}
		log("thread exit");
		control.threadNum.decrementAndGet();
	}
	private List<NameHolder> translateResult(List<String> names,  List<NamePosition> namePos, List<NamePosition> senIndex) {
		List<NameHolder> res = new ArrayList<NameHolder>();
		for (int i = 0; i < names.size(); ++i) {
			NameHolder nh = new NameHolder();
			nh.name = names.get(i);
			nh.beginPos = namePos.get(i).getStart();
			nh.endPos  = namePos.get(i).getEnd();
            //nh.norms = norms.get(i);
			nh.beginIndex = senIndex.get(i).getStart();
			nh.endIndex= senIndex.get(i).getEnd();
			res.add(nh);
		}
		return res;
	}
}
