package cn.edu.sjtu.gdr.service.generank;

import java.util.ArrayList;
import java.util.List;

import cn.edu.sjtu.gdr.graph.ranker.BetweennessRanker;
import cn.edu.sjtu.gdr.graph.ranker.ClosenessRanker;
import cn.edu.sjtu.gdr.graph.ranker.DegreeRanker;
import cn.edu.sjtu.gdr.graph.ranker.EigenRanker;

public class GeneGraphResult {
	public List<String> usedGenes;
	public List<String> usedGeneIdents;
	public List<String> unfoundGenes;

	/** Rank result of {@link DegreeRanker} */
	public List<GeneGraphRankNode> degreeResult;
	
	/** Rank result of {@link EigenRanker} */
	public List<GeneGraphRankNode> eigenResult;
	
	/** Rank result of {@link BetweennessRanker} */
	public List<GeneGraphRankNode> betweenResult;
	
	/** Rank result of {@link ClosenessRanker} */
	public List<GeneGraphRankNode> closenessResult;

	public GeneGraphResult() {
		usedGenes = new ArrayList<String>();
		usedGeneIdents = new ArrayList<String>();
		unfoundGenes = new ArrayList<String>();
		degreeResult = new ArrayList<GeneGraphRankNode>();
		eigenResult = new ArrayList<GeneGraphRankNode>();
		betweenResult = new ArrayList<GeneGraphRankNode>();
		closenessResult = new ArrayList<GeneGraphRankNode>();
	}
}
