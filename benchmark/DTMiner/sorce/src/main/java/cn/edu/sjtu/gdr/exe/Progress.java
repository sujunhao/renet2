package cn.edu.sjtu.gdr.exe;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Xie Yanping on 2015/11/5.
 */
public class Progress {
    private static void percent(String pmid, String idx) throws IOException{
        String inPath = "res/data/prog-output/gene-network-clean"+idx+".txt";
        BufferedReader br = new BufferedReader(new FileReader(inPath));
        String line;
        String tmpPmid;
        int num, count=0, loc = 0;
        while((line = br.readLine())!=null){
            tmpPmid = line.substring(6,line.lastIndexOf(" "));
            num = Integer.parseInt(line.substring(line.lastIndexOf(" ")+1));
            ++count;
            if ((count & 0xfffff) == 0) {
               System.out.println((count >> 20) + "M papers scanned..");
            }
            if(tmpPmid.equals(pmid))
                loc = count;
            for(int i=0; i<num; i++){
                br.readLine();
                br.readLine();
                br.readLine();
                br.readLine();
                br.readLine();
            }
        }
        if(loc!=0)
            System.out.println("progress: "+1.0*loc/count);
        else
            System.out.println(pmid+" not found");
    }
    public static void main(String[] argv)  throws IOException{
        percent(argv[1], argv[0]);
    }
}
