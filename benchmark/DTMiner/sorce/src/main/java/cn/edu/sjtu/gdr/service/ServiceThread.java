package cn.edu.sjtu.gdr.service;

import cn.edu.sjtu.gdr.async.LoggableThread;
import cn.edu.sjtu.gdr.async.SharedBuffer;

public abstract class ServiceThread extends LoggableThread {
	
	protected SharedBuffer<RequestWrapper> buffer;

	public ServiceThread() {
		buffer = new SharedBuffer<RequestWrapper>();
	}
	
	public SharedBuffer<RequestWrapper> getBuffer() {
		return buffer;
	}
}
