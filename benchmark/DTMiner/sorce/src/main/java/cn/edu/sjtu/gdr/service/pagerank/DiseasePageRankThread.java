package cn.edu.sjtu.gdr.service.pagerank;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

import cn.edu.sjtu.gdr.medline.pagerank.DiseasePageRankScoreRanker;
import cn.edu.sjtu.gdr.service.RequestWrapper;
import cn.edu.sjtu.gdr.service.ServiceThread;

public class DiseasePageRankThread extends ServiceThread {
	public static final String SERVICE_NAME = "diseasePageRank";

	private static DiseasePageRankScoreRanker ranker = null;

	@Override
	public void run() {
		while (true) {
			try {
				RequestWrapper request = buffer.pop();
				if (request == null) {
					synchronized (this) {
						wait();
					}
				} else {
					BufferedWriter bw = new BufferedWriter(
							new OutputStreamWriter(
									request.client.getOutputStream()));

					DiseasePageRankScoreRanker ranker = DiseasePageRankThread
							.getRanker();
					
					// TODO
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	private static DiseasePageRankScoreRanker getRanker() {
		if (ranker == null) {
			synchronized (DiseasePageRankThread.class) {
				if (ranker == null) {
					ranker = new DiseasePageRankScoreRanker();
				}
			}
		}
		return ranker;
	}

}
