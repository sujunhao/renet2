package cn.edu.sjtu.gdr.data.feature;

import cn.edu.sjtu.gdr.data.deptree.DepTree;
import cn.edu.sjtu.gdr.data.label.LabeledData;
import cn.edu.sjtu.gdr.data.label.LabeledRelationData;
import cn.edu.sjtu.gdr.data.utils.NLPToolsForFeatures;
import cn.edu.sjtu.gdr.ggr.NamePosition;
import edu.stanford.nlp.ling.TaggedWord;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Xie Yanping on 2015/12/19.
 */
public class VerbOnPath {
    private static int ngram = 4; // max 3
    public static List<String> extractRelationFeature(LabeledRelationData data, int n) {
        ngram = n;
        return extractFeature(data.getGeneIndex(), data.getDiseaseIndex(),
                data.getSentenceLabeledData());
    }

    public static List<String> extractFeature(NamePosition gene, NamePosition disease,
                                              LabeledData data) {
        int fromIndex = NLPToolsForFeatures.getRootOfWindow(gene, data.getDepTree());
        int toIndex = NLPToolsForFeatures.getRootOfWindow(disease, data.getDepTree());

        DepTree parse = data.getDepTree();
        List<Integer> verbOnPath = NLPToolsForFeatures.getVerbOnPath(fromIndex, toIndex, parse);
        List<String> lemmaList = data.getLemma();
        for (int i=gene.getStart(); i<gene.getEnd(); ++i)
            lemmaList.set(i,"GENE#");
        for (int i=disease.getStart(); i<disease.getEnd(); ++i)
            lemmaList.set(i,"DISEASE#");
        List<TaggedWord> taggedWordList = data.getTagged();
        List<String> features = new ArrayList<>();
        Map<String,Integer> res = new HashMap<>();
        String tempString;
        Integer tempInt;

        for(int k=0; k<verbOnPath.size(); ++k){
            int idx = verbOnPath.get(k);
                int parent = parse.getNode(idx+1).getHeadIndex()-1;
                List<Integer> vbMods = NLPToolsForFeatures.getVbMod(idx,taggedWordList.size(),parse);
                if (vbMods.size()==0)
                    continue;
                for(int n=1; n<ngram; n++) {
                    if(n == 1) {
                        for (Integer modIdx : vbMods) {
                            tempString = "*"+lemmaList.get(modIdx);
                            if ((tempInt = res.get(tempString)) == null) {
                                res.put(tempString, 1);
                            } else {
                                res.put(tempString, ++tempInt);
                            }
                        }
                    }
                    if(n == 2){
                        for (Integer modIdx : vbMods) {
                            tempString = "*" + lemmaList.get(idx) + lemmaList.get(modIdx);
                            /*if(modIdx<idx)
                                tempString = lemmaList.get(modIdx) + lemmaList.get(idx);
                            else
                                tempString = lemmaList.get(idx) + lemmaList.get(modIdx);*/
                            if ((tempInt = res.get(tempString)) == null) {
                                res.put(tempString, 1);
                            } else {
                                res.put(tempString, ++tempInt);
                            }
                        }
                    }
                    if(n == 3){
                        for(int i=0; i<vbMods.size(); ++i){
                            for (int j=i+1; j<vbMods.size(); ++j){
                                if(lemmaList.get(vbMods.get(i)).compareTo(lemmaList.get(vbMods.get(j)))<=0)
                                    tempString = lemmaList.get(vbMods.get(i))+"*"+lemmaList.get(idx)+lemmaList.get(vbMods.get(j));
                                else
                                    tempString = lemmaList.get(vbMods.get(j))+"*"+lemmaList.get(idx)+lemmaList.get(vbMods.get(i));
                                /*if(vbMods.get(j) < idx)
                                    tempString = lemmaList.get(vbMods.get(i))+lemmaList.get(vbMods.get(j))+lemmaList.get(idx);
                                else if(vbMods.get(i) > idx)
                                    tempString = lemmaList.get(idx)+lemmaList.get(vbMods.get(i))+lemmaList.get(vbMods.get(j));
                                else
                                    tempString = lemmaList.get(vbMods.get(i))+lemmaList.get(idx)+lemmaList.get(vbMods.get(j));*/
                                if ((tempInt = res.get(tempString)) == null) {
                                    res.put(tempString, 1);
                                } else {
                                    res.put(tempString, ++tempInt);
                                }
                            }
                        }
                        if(parent>=0) {
                            for (Integer modIdx : vbMods) {
                                tempString = "*"+lemmaList.get(parent)+lemmaList.get(idx)+lemmaList.get(modIdx);
                                /*if (modIdx < idx && idx < parent)
                                    tempString = lemmaList.get(modIdx) + lemmaList.get(idx) + lemmaList.get(parent);
                                else if (modIdx < idx && modIdx < parent)
                                    tempString = lemmaList.get(modIdx) + lemmaList.get(parent)+lemmaList.get(idx);
                                else if(modIdx < idx && parent<modIdx)
                                    tempString = lemmaList.get(parent) + lemmaList.get(modIdx)+lemmaList.get(idx);
                                else if(modIdx>idx && parent<idx)
                                    tempString = lemmaList.get(parent) + lemmaList.get(idx)+lemmaList.get(modIdx);
                                else if(modIdx>idx && parent<modIdx)
                                    tempString = lemmaList.get(idx) + lemmaList.get(parent)+lemmaList.get(modIdx);
                                else if(modIdx>idx && parent>modIdx)
                                    tempString = lemmaList.get(idx) + lemmaList.get(modIdx)+lemmaList.get(parent);*/
                                if ((tempInt = res.get(tempString)) == null) {
                                    res.put(tempString, 1);
                                } else {
                                    res.put(tempString, ++tempInt);
                                }
                            }
                        }
                    }
                }
            }

        for(String gram : res.keySet()){
            features.add(gram+"\t"+res.get(gram));
        }
        return features;
    }
}
