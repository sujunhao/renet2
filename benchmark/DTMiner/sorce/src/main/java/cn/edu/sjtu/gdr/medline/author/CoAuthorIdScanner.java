package cn.edu.sjtu.gdr.medline.author;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.utils.GgrLogger;
import cn.edu.sjtu.gdr.utils.NameIdGenerator;

public class CoAuthorIdScanner {

	private String inPath, idPath, outPath;

	public CoAuthorIdScanner(String inPath, String idPath, String outPath) {
		this.inPath = inPath;
		this.idPath = idPath;
		this.outPath = outPath;
	}

	public void generate() throws IOException {
		NameIdGenerator nameIdGen = new NameIdGenerator();

		BufferedReader br = new BufferedReader(new FileReader(inPath));
		BufferedWriter bwAuthor = new BufferedWriter(new FileWriter(outPath));

		String str = null, name = null;
		int count = 0;

		while ((str = br.readLine()) != null) {
			if (isPmid(str)) {
				bwAuthor.newLine();
				bwAuthor.write("pmid: " + str);
				bwAuthor.newLine();
			} else {
				int index = str.indexOf('\t');
				name = str.substring(0, index);
				bwAuthor.write(nameIdGen.getId(name) + "\t");
			}
			
			++count;
			if ((count & 0xfffff) == 0) {
				GgrLogger.log((count >> 20) + "M lines processed...");
			}
		}

		br.close();
		bwAuthor.flush();
		bwAuthor.close();

		GgrLogger.log("start outputing id...");
		nameIdGen.output(idPath);
		GgrLogger.log("finish outputing id...");
	}

	private boolean isPmid(String str) {
		char c = str.charAt(0);
		return c >= '0' && c <= '9';
	}

	public static void main(String[] args) throws IOException {
		String inPath = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG, "author", "AUTHOR_META_PATH");
		String idPath = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG, "author", "COAUTHOR_AUTHOR_ID");
		String outPath = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG, "author", "AUTHOR_META_ID_PATH");
		new CoAuthorIdScanner(inPath, idPath, outPath).generate();
	}
}
