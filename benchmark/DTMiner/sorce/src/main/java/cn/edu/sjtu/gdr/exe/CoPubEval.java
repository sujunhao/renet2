package cn.edu.sjtu.gdr.exe;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cn.edu.sjtu.gdr.evaluation.Evaluator;
import cn.edu.sjtu.gdr.medline.rank.GeneScore;
import cn.edu.sjtu.gdr.obo.EscapeSynonymNormalizer;
import cn.edu.sjtu.gdr.obo.Normalizer;
import cn.edu.sjtu.gdr.obo.SynonymNormalizer;

public class CoPubEval {

	private static String[] diseases = { "Kidney Failure, Chronic",
			"Kidney Neoplasms", "Acute Kidney Injury",
			"Cardiovascular Diseases", "Lung Neoplasms" };
	private static String[] files = {
			"res/data/copub/chronic_kidney_failure.txt",
			"res/data/copub/kidney_neoplasms.txt",
			"res/data/copub/acute_kidney_injury.txt",
			"res/data/copub/cardiovascular_diseases.txt",
			"res/data/copub/lung_neoplasms.txt" };

	public static void main(String[] args) throws IOException {
		Normalizer geneSyn = EscapeSynonymNormalizer.getGeneNormalizer();

		for (int i = 0; i < diseases.length; ++i) {
			System.out.println("Start evaluate " + diseases[i]);
		
			List<GeneScore> genes = new ArrayList<GeneScore>();
			
			BufferedReader br = new BufferedReader(new FileReader(files[i]));

			String line = br.readLine();
			while ((line = br.readLine()) != null) {
				String[] strs = line.split("\t");
				List<String> norms = null;
				if (strs[2].equals(""))
					norms = geneSyn.normalize(strs[1]);
				else
					norms = geneSyn.normalize(strs[2]);
				for(String norm: norms)
					genes.add(new GeneScore(norm, Double.parseDouble(strs[3])));
			}
			br.close();
			
			Collections.sort(genes, Collections.reverseOrder());
			Evaluator.evaluation(diseases[i], genes);
		}
	}
}
