package cn.edu.sjtu.gdr.obo;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import cn.edu.sjtu.gdr.config.ConfigManager;

public class OboTermMap {
	private Map<String, OboTerm> map;

	public OboTermMap(String path) {
		map = new HashMap<String, OboTerm>();
		init(path);
	}

	public OboTerm getTerm(String id) {
		return map.get(id.toLowerCase());
	}

	private static Object mutex = new Object();
	private static OboTermMap term;

	public static OboTermMap getGeneMap() {
		if (term == null) {
			synchronized (mutex) {
				if (term == null) {
					term = new OboTermMap(ConfigManager.getConfig(
							ConfigManager.MAIN_CONFIG, "obo", "GENE_OBO"));
				}
			}
		}
		return term;
	}

	private void init(String path) {
		OboTermParser parser = new OboTermParser(path);
		for (Iterator<OboTerm> itr = parser.iterator(); itr.hasNext();) {
			OboTerm term = itr.next();
			map.put(term.getId().toLowerCase(), term);
		}
	}
}
