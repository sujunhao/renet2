package cn.edu.sjtu.gdr.async.netext;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import cn.edu.sjtu.gdr.async.SharedBuffer;
import cn.edu.sjtu.gdr.medline.abst.AbstParPumper;
import cn.edu.sjtu.gdr.medline.abst.TextAbstPumper;

public class ControlData {
	// shared data buffer
	//public SharedBuffer<TextAbstPumper> inputBuffer;
    public SharedBuffer<AbstParPumper> parseBuffer;
	// shared output buffer
	public SharedBuffer<List<NetworkOutputData>> outputBuffer;

	public AtomicInteger threadNum;

	// control state: indicate whether more data exist
	private transient Boolean state;

	public ControlData(Boolean state, int threadNum, SharedBuffer<AbstParPumper> ParBuffer,
			SharedBuffer<List<NetworkOutputData>> outputBuffer) {
		this.state = state;
		//this.inputBuffer = inputBuffer;
        this.parseBuffer = ParBuffer;
		this.outputBuffer = outputBuffer;
		this.threadNum = new AtomicInteger(threadNum);
	}

	public ControlData(int threadNum,SharedBuffer<AbstParPumper> parBuffer,
			SharedBuffer<List<NetworkOutputData>> outputBuffer) {
		this(true, threadNum,parBuffer, outputBuffer);
	}

	public ControlData(int threadNum) {
		this(true, threadNum, new SharedBuffer<AbstParPumper>(),
				new SharedBuffer<List<NetworkOutputData>>());
	}

	public Boolean isRunning() {
		return state;
	}

	public void setState(Boolean state) {
		this.state = state;
	}
}
