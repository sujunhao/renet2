package cn.edu.sjtu.gdr.exe;

import cn.edu.sjtu.gdr.config.ConfigManager;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.parser.nndep.DependencyParser;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import org.apache.commons.lang3.StringEscapeUtils;

import java.io.*;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Created by Xie Yanping on 2015/9/12.
 */
public class AbstTag  extends Thread {

    public static void main(String[] args) throws FileNotFoundException, IOException {
        System.out.println("Start");
        String indir = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
                "exe", "MEDLINE_ABSTRACT");
        String outdir = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
                "exe", "MEDLINE_ABSTRACT_TAG");
        File[] files = new File(indir).listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.getName().endsWith("gz");
            }
        });
        Arrays.sort(files, new Comparator<File>() {
            @Override
            public int compare(File o1, File o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });

        Set<String> tagged = new HashSet<String>();
        for( File file: new File(outdir).listFiles())
            if( file.getName().endsWith("gz"))
                tagged.add(file.getName());

        System.out.println(tagged.size());
        System.out.println(files.length);
        int start = 0;
        if( args.length == 1)
            start = Integer.valueOf(args[0]);

        ExecutorService fixedThreadPool = Executors.newFixedThreadPool(8);

        for (int i = start; i < files.length; ++ i) {
            if( !tagged.contains(files[i].getName())) {
                System.out.println("[" + new Date() + "]\t"+ files[i].getName() + "\t\tStart.");
                Thread t = new AbstTag(files[i], outdir);
                fixedThreadPool.execute(t);
            }
        }
        System.out.println("Done.");
    }

    private File infile;
    private String outdir;

    public AbstTag(File infile, String outdir) {
        this.infile = infile;
        this.outdir = outdir;
    }

    public void run() {
        try {
            processFile(infile, outdir);
            System.out.println("[" + new Date() + "]\t"+ infile.getName() + "\t\tDone.");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private static String modelPath = DependencyParser.DEFAULT_MODEL;
    private static String taggerPath = "edu/stanford/nlp/models/pos-tagger/english-left3words/english-left3words-distsim.tagger";

    private static MaxentTagger tagger = new MaxentTagger(taggerPath);
    private static DependencyParser parser = DependencyParser.loadFromModelFile(modelPath);

    static void processFile(File infile, String outdir)
            throws FileNotFoundException, IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(
                new GZIPInputStream(new FileInputStream(infile))));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
                new GZIPOutputStream(new FileOutputStream(new File(outdir,
                        infile.getName())))));
        String str = null, abst = null;

        while ((str = br.readLine()) != null) {
            abst = StringEscapeUtils.unescapeXml(br.readLine().trim());
            DocumentPreprocessor dp = new DocumentPreprocessor(
                    new StringReader(abst));
            List<List<HasWord>> sens = new ArrayList<List<HasWord>>();
            for (List<HasWord> sen : dp) {
                sens.add(sen);
            }

            if(sens.size()!=0) {
                bw.write(str);
                bw.write(" ");
                bw.write("" + sens.size());
                bw.newLine();
            }
            for (List<HasWord> sen : sens) {
                int begin = ((CoreLabel) sen.get(0)).beginPosition();
                int end = ((CoreLabel) sen.get(sen.size() - 1)).endPosition();
                bw.write(abst.substring(begin, end));
                bw.newLine();

                List<TaggedWord> tagged = tagger.tagSentence(sen);
                bw.write(tagged.toString());
                bw.newLine();
            }
        }

        br.close();
        bw.flush();
        bw.close();
    }
}
