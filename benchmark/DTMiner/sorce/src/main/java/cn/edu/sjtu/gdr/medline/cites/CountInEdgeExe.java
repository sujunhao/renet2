package cn.edu.sjtu.gdr.medline.cites;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import cn.edu.sjtu.gdr.config.ConfigManager;

public class CountInEdgeExe {

	private static Map<Integer, Integer> map = new HashMap<Integer, Integer>();

	private static void readIn(String dir) throws IOException {
		File citeDir = new File(dir);
		File[] list = citeDir.listFiles();
		Arrays.sort(list, new Comparator<File>() {
			@Override
			public int compare(File o1, File o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		for (File f : list) {
			BufferedReader br = new BufferedReader(new FileReader(f));
			String str = null, tmp[];
			int to;

			while ((str = br.readLine()) != null) {
				// read in out edges
				str = br.readLine().trim();
				tmp = str.split("\t");
				for (String ss : tmp) {
					if (!ss.equals("")) {
						to = Integer.parseInt(ss);
						inc(to);
					}
				}
			}

			br.close();
		}
	}

	private static void output(String path) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(path));

		for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
			bw.write(entry.getKey() + " " + entry.getValue());
			bw.newLine();
		}

		bw.flush();
		bw.close();
	}

	private static void inc(int to) {
		int newVal = 1;
		if (map.containsKey(to)) {
			newVal += map.get(to);
		}
		map.put(to, newVal);
	}

	public void run() throws IOException {
		String dir = ConfigManager.getConfig("cites", "PAPER_CITES_DIR");
		String outpath = ConfigManager.getConfig("cites",
				"PAPER_CITE_IN_EDGE_COUNT_PATH");
		readIn(dir);
		output(outpath);
	}

	public static void main(String[] args) throws IOException {
		new CountInEdgeExe().run();
	}

}
