package cn.edu.sjtu.gdr.medline.abst;

import rdi.cn.source.ParseItem;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Xie Yanping on 2015/9/19.
 */
public class AbstParPumper {
        private String fileName;
        private BufferedReader br;
        private List<ParseItem> list;
        private Iterator<ParseItem> inner;

        public AbstParPumper(File file) throws FileNotFoundException, IOException {
            br = new BufferedReader(new InputStreamReader(new GZIPInputStream(
                    new FileInputStream(file))));
            fileName = file.getName();
            readAll();
        }

        public AbstParPumper(String path) throws FileNotFoundException, IOException {
            this(new File(path));
        }

        public String fileName() {
            return fileName;
        }

        public boolean hasNext() {
            return inner.hasNext();
        }

        public ParseItem next() {
            return inner.next();
        }

        private void readAll() throws IOException {
            list = new ArrayList<ParseItem>();
            String str = null, pmid = null, text = null;
            int sen;
            ParseItem item = null;
            while ((str = br.readLine()) != null) {
                pmid = str.substring(0, str.lastIndexOf(' '));
                item = new ParseItem();
                item.setPmid(pmid);
                sen = Integer.parseInt(str.substring(str.lastIndexOf(' ') + 1));
                str = "";
                //abst = "";
                List<String> abst = new ArrayList<>();
                for(int i=0; i<sen; i++){
                    text = br.readLine();
                    //abst += text + "\n";
                    abst.add(text);
                    text = br.readLine();
                    str = str + text + "\n";
                    br.readLine();
                    text = br.readLine();
                    str = str + text + "\n";
                }
                if(sen != 0) {
                    item.setContent(str);
                    //item.setText(abst);
                    item.setAbst(abst);
                    list.add(item);
                }
            }
            inner = list.iterator();
        }

}
