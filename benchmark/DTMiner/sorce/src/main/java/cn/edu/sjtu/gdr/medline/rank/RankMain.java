package cn.edu.sjtu.gdr.medline.rank;

import java.io.*;
import java.util.*;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.obo.EscapeSynonymNormalizer;
import cn.edu.sjtu.gdr.obo.Normalizer;

public class RankMain {

	public static String[] test = {"lung neoplasms", "kidney neoplasms", "kidney failure, acute",
            "kidney failure, chronic", "cardiovascular system disease"};
	public static boolean dump = true;
	private static Normalizer disSyn = EscapeSynonymNormalizer.getDiseaseNormalizer();
	public static void main(String[] args) throws IOException {
		if (args.length >= 1 && args[0].equals("dump")) {
			dump = true;
		}
        List<String> diseases = new ArrayList<>();
        String diseasePath = "res/data/prog-output/diseasename.txt";
        BufferedReader brDisease = new BufferedReader(new FileReader(diseasePath));
        String diseaseName = "";
        while((diseaseName = brDisease.readLine())!= null){
            diseases.add(diseaseName);
        }
		String path = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"evidence", "GENE_RELATED_DISEASE_NUMBER");
		Map<String, Integer> DF = ComputeGeneDF.load(path);
		int total_num_of_disease = DF.get("");
		DiseaseGene diseaseGene = new DiseaseGene(true);//svmOrNot
		
		boolean comDF = false;
		if( "true".equals(ConfigManager.getConfig("disease-pagerank-score",
				"DF")))
			comDF = true;
		List<Score> scorers = new ArrayList<Score>();
		//scorers.add(new ArticleTypeScore());
		scorers.add(new FreqGeneScore());
        //scorers.add(new PaperCitePagerankScore());
		//scorers.add(new PaperCiteScore());
		//scorers.add(new PaperCiteSuppress());
		//scorers.add(new CoAuthorScore());
		//scorers.add(new CoAuthorSuppress());

        //SynonymNormalizer disSyn = SynonymNormalizer.getDiseaseSynNorm();
		for( String disease: test) {
			System.out.println(String.format("Start evaluating: %s", disease));
            List<String> disNorms = disSyn.normalize(disease);
            disNorms.remove(0);
            Map<String, Set<Integer>> genePmids = new HashMap<>();
            for (String disNorm : disNorms)
                genePmids.putAll(diseaseGene.getEvidencePmids(disNorm));

			for( Score scorer: scorers) {
				System.out.println(scorer.getClass().getSimpleName());
				List<GeneScore> genes = new ArrayList<GeneScore>();
				for(String gene: genePmids.keySet()) {
					double score = scorer.score(genePmids.get(gene));
					if( comDF )
						score *= Math.log(1.0 * total_num_of_disease / DF.get(gene));
					//if( score > 0 )
					genes.add(new GeneScore(gene, score));
				}
				Collections.sort(genes, Collections.reverseOrder());
				//Evaluator.evaluation(disease, genes);
				if( dump ) {
					String tag = scorer.getClass().getSimpleName();
					if( comDF )
						tag += ".DF";
					dump(disease, tag, genes);
				}
//				for( GeneScore gene: genes)
//					System.out.println(gene.getName() + "\t" + gene.getScore());
			}
		}
	}
	
	private static void dump(String disease, String tag,
			List<GeneScore> list) throws IOException {
		//String path = String.format("dump/rank.%s.%s.dump", disease, tag);
        String path = "res/data/prog-output/score."+tag;
		BufferedWriter bw = new BufferedWriter(new FileWriter(path,true));
        bw.write(disease+" "+list.size());
        bw.newLine();

		for (int i = 0; i < list.size(); ++ i) {
			GeneScore gs = list.get(i);
			bw.write(String.format("%d\t%s\t%s", i + 1, gs.getName(), gs.getScore()));
			bw.newLine();
		}

		bw.flush();
		bw.close();
	}
	
}
