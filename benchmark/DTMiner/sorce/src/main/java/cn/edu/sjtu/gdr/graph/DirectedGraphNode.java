package cn.edu.sjtu.gdr.graph;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

public class DirectedGraphNode extends GraphNode {

	private Map<Integer, Integer> outEdges, inEdges;

	public DirectedGraphNode(int id) {
		this(id, new TreeMap<Integer, Integer>(),
				new TreeMap<Integer, Integer>());
	}

	public DirectedGraphNode(int id, Map<Integer, Integer> inEdge,
			Map<Integer, Integer> outEdge) {
		super(id);
		this.outEdges = outEdge;
		this.inEdges = inEdge;
	}

	@Override
	public void addOutEdge(int id, int weight) {
		outEdges.put(id, weight);
	}

	@Override
	public void addInEdge(int id, int weight) {
		inEdges.put(id, weight);
	}

	@Override
	public void removeOutEdge(int id) {
		outEdges.remove(id);
	}

	@Override
	public void removeInEdge(int id) {
		inEdges.remove(id);
	}

	@Override
	public int getOutEdgeWeight(int nodeId) {
		if (outEdges.containsKey(nodeId)) {
			return outEdges.get(nodeId);
		}
		return 0;
	}

	@Override
	public int getInEdgeWeight(int nodeId) {
		if (inEdges.containsKey(nodeId)) {
			return inEdges.get(nodeId);
		}
		return 0;
	}

	@Override
	public Collection<Integer> getOutEdges() {
		return outEdges.keySet();
	}

	@Override
	public Collection<Integer> getInEdges() {
		return inEdges.keySet();
	}

	@Override
	public int getOutDegree() {
		return outEdges.size();
	}

	@Override
	public int getInDegree() {
		return inEdges.size();
	}
}
