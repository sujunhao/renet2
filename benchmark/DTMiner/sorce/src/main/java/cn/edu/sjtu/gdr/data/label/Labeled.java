package cn.edu.sjtu.gdr.data.label;

public interface Labeled {

	public int getLabel();

	public void setLabel(int label);

}
