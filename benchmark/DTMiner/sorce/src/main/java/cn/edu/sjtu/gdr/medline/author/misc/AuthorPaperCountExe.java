package cn.edu.sjtu.gdr.medline.author.misc;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.utils.CollectionUtils;
import cn.edu.sjtu.gdr.utils.GgrLogger;

public class AuthorPaperCountExe {

	private String inpath, outpath, pmidPath;
	//private Set<String> pmidSet = new HashSet<String>();
	private Map<String, Integer> authorPaper = new TreeMap<String, Integer>();

	public AuthorPaperCountExe(String inpath, String pmidpath, String outpath) {
		this.inpath = inpath;
		this.pmidPath = pmidpath;
		this.outpath = outpath;
		//readPmid();
	}

	public void run() {
		GgrLogger.log("start reading count...");
		read();
		GgrLogger.log("finish reading count...");
		
		GgrLogger.log("start outputing...");
		output();
		GgrLogger.log("finish reading count...");
	}

	private void read() {
		try {
			BufferedReader br = new BufferedReader(new FileReader(inpath));
			String str = null, pmid = null, ids[] = null;
			int count = 0;

			//br.readLine(); // skip empty line
			while ((str = br.readLine()) != null) {
				pmid = str.substring("pmid: ".length());
				str = br.readLine();
				//if (!pmidSet.contains(pmid) || str.equals(""))
                if(str.equals(""))
					continue;
				ids = str.split("\t");
				for (String id : ids) {
					if (!authorPaper.containsKey(id))
						authorPaper.put(id, 0);
					authorPaper.put(id, authorPaper.get(id) + 1);
				}
				
				++count;
				if ((count & 0xfffff) == 0) {
					GgrLogger.log((count >> 20) + "M papers processed...");
				}
			}

			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void output() {
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(outpath));
			
			Map.Entry<String, Integer> entries[] = null;
			entries = CollectionUtils.sortMap(authorPaper, true);
			
			for (Map.Entry<String, Integer> entry : entries) {
				bw.write(entry.getKey() + "\t" + entry.getValue());
				bw.newLine();
			}
			
			bw.flush();
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	public static void main(String[] args) {
		String inpath = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG, "author", "AUTHOR_META_ID_PATH");
		String outpath = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG, "author", "AUTHOR_PAPER_COUNT");
		String pmidpath = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG, "cites", "PAPER_CITE_PMID_SET");
		new AuthorPaperCountExe(inpath, pmidpath, outpath).run();
	}

	/*private void readPmid() {
		GgrLogger.log("start reading pmids...");
		try {
			BufferedReader br = new BufferedReader(new FileReader(pmidPath));

			String str = null;

			while ((str = br.readLine()) != null) {
				pmidSet.add(str);
				br.readLine(); // skip date line
			}

			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		GgrLogger.log("finish reading pmids...");
	}*/
}
