package cn.edu.sjtu.gdr.medline.author;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.utils.GgrLogger;

public class ExtractAuthorCitesGraphEdges {

	private String authorMetaPath, outPath, pmidPath;

	// pmid -> [author id list]
	private Map<Integer, List<Integer>> authorMeta;
	private BufferedWriter bw;

	private Set<Integer> pmidSet;

	public ExtractAuthorCitesGraphEdges(String authorMetaPath, String pmidPath,
			String outPath) {
		this.authorMetaPath = authorMetaPath;
		this.pmidPath = pmidPath;
		this.outPath = outPath;
		init();
	}

	public void reduce(File file) throws IOException {
		GgrLogger.log("start processing file: " + file.getName());
		BufferedReader br = new BufferedReader(new FileReader(file));

		String str = null, citePmidsStr[] = null;
		int citePmid = 0;
		List<Integer> writeAuthors = null, citeAuthors = null;

		while ((str = br.readLine()) != null) {
			int writePmid = Integer.parseInt(str.substring("pmid: ".length()));

			if ((writeAuthors = authorMeta.get(writePmid)) != null) {
				if (!(str = br.readLine()).equals("")) {
					citePmidsStr = str.split("\t");
					for (String citePmidStr : citePmidsStr) {
						citePmid = Integer.parseInt(citePmidStr);
						if (!pmidSet.contains(writePmid)
								|| !pmidSet.contains(citePmid))
							continue;
						if ((citeAuthors = authorMeta.get(citePmid)) != null) {
							for (Integer writeAuthor : writeAuthors) {
								for (Integer citeAuthor : citeAuthors) {
									// add edge: writeAuthor --> citeAuthor
									bw.write(writeAuthor + "\t" + citeAuthor);
									bw.newLine();
								}
							}
						}

					}
				}
			} else { // error
				br.readLine(); // skip cite line
			}
		}

		br.close();
		GgrLogger.log("finish processing file: " + file.getName());
	}

	private void init() {
		GgrLogger.log("reading author meta...");
		readAuthorMeta(authorMetaPath);
		GgrLogger.log("finish reading author meta");

		GgrLogger.log("reading pmid...");
		readPmid();
		GgrLogger.log("finish reading pmid...");

		try {
			bw = new BufferedWriter(new FileWriter(outPath));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void readPmid() {
		pmidSet = new HashSet<Integer>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(pmidPath));

			String str = null;

			while ((str = br.readLine()) != null) {
				pmidSet.add(Integer.parseInt(str));
				br.readLine(); // skip date line
			}

			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void readAuthorMeta(String path) {
		try {
			authorMeta = new HashMap<Integer, List<Integer>>();

			BufferedReader br = new BufferedReader(new FileReader(path));

			String str = null, authors[];
			List<Integer> authorList = null;

			br.readLine(); // skip empty line
			while ((str = br.readLine()) != null) {
				int pmid = 0;
				try {
					pmid = Integer.parseInt(str.substring("pmid: ".length()));
				} catch (NumberFormatException e) {
					br.readLine(); // skip next line
					continue;
				}

				str = br.readLine();
				if (str.equals(""))
					continue;

				authors = str.split("\t");
				authorList = new ArrayList<Integer>(authors.length);
				for (String authorId : authors)
					authorList.add(Integer.parseInt(authorId));
				authorMeta.put(pmid, authorList);
			}

			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void flush() {
		try {
			bw.flush();
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] argv) throws IOException {
		String authorMetaPath = ConfigManager.getConfig(
				ConfigManager.MAIN_CONFIG, "author", "AUTHOR_META_ID_PATH");
		String pmidPath = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"cites", "PAPER_CITE_PMID_SET");
		String outPath = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"author", "AUTHOR_CITES_GRAPH_EDGES_PATH");
		String dirStr = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"cites", "PAPER_CITES_DIR");

		File[] files = new File(dirStr).listFiles();
		Arrays.sort(files, new Comparator<File>() {
			@Override
			public int compare(File o1, File o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});

		ExtractAuthorCitesGraphEdges executor = new ExtractAuthorCitesGraphEdges(
				authorMetaPath, pmidPath, outPath);
		for (File f : files) {
			executor.reduce(f);
		}

		executor.flush();
	}
}
