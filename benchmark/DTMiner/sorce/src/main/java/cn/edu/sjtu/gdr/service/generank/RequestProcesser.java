package cn.edu.sjtu.gdr.service.generank;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.graph.GeneGraph;
import cn.edu.sjtu.gdr.graph.UndirectedGraph;
import cn.edu.sjtu.gdr.graph.UndirectedGraphOutputter;
import cn.edu.sjtu.gdr.graph.ranker.DegreeRanker;
import cn.edu.sjtu.gdr.obo.EscapeSynonymNormalizer;
import cn.edu.sjtu.gdr.obo.Normalizer;
import cn.edu.sjtu.gdr.obo.SynonymNormalizer;
import cn.edu.sjtu.gdr.utils.GgrLogger;
import cn.edu.sjtu.gdr.utils.NameID;

public class RequestProcesser {

	public static GeneGraphResult geneGraph(List<String> genes) {
		UndirectedGraph geneGraph = GeneGraph.getInstance();
		Normalizer geneNorm = EscapeSynonymNormalizer.getGeneNormalizer();
		NameID geneId = NameID.getGeneIdInstance();

		GeneGraphResult result = new GeneGraphResult();
		Set<String> seedsSet = new HashSet<String>();

		// filter input genes
		for (String gene : genes) {
			List<String> idents = geneNorm.normalize(gene);
			if (idents.size() == 0) {
				result.unfoundGenes.add(gene);
			} else {
				for (String ident : idents) {
					result.usedGenes.add(gene);
					result.usedGeneIdents.add(ident);
					seedsSet.add(ident);
				}
			}
		}

		// find seeds id
		int[] seedIds = new int[result.usedGeneIdents.size()];
		for (int i = 0; i < result.usedGeneIdents.size(); ++i) {
			seedIds[i] = geneId.getId(result.usedGeneIdents.get(i));
		}

		// generate subgraph and build results
		UndirectedGraph subGraph = geneGraph.subGraph(seedIds);
		GgrLogger.log("GeneRank subGraph size: " + subGraph.size());

		int[] rank = new DegreeRanker(subGraph).rank(false);
		for (int i = 0; i < rank.length; ++i) {
			int id = rank[i];
			String geneName = geneId.getName(id);
			result.degreeResult.add(new GeneGraphRankNode(id, subGraph.getNode(
					id).getOutDegree(), geneName, seedsSet.contains(geneName)));
		}
		GgrLogger.log("degree rank finished.. ");

		// rank = new EigenRanker(subGraph).rank(false);
		// for (int i = 0; i < rank.length; ++i) {
		// int id = rank[i];
		// String geneName = geneId.getName(id);
		// result.eigenResult.add(new GeneGraphRankNode(
		// id,
		// subGraph.getEigenVectorValue(subGraph.getNode(id)),
		// geneName,
		// seedsSet.contains(geneName)));
		// }
		GgrLogger.log("degree rank finished.. ");
		//
		// rank = new ClosenessRanker(subGraph).rank(false);
		// for (int i = 0; i < rank.length; ++i) {
		// int id = rank[i];
		// String geneName = geneId.getName(id);
		// result.closenessResult.add(new GeneGraphRankNode(
		// id,
		// subGraph.getClosenessValue(subGraph.getNode(id)),
		// geneName,
		// seedsSet.contains(geneName)));
		// }
		GgrLogger.log("closeness rank finished.. ");

		// rank = new BetweennessRanker(subGraph).rank(false);
		// for (int i = 0; i < rank.length; ++i) {
		// int id = rank[i];
		// String geneName = geneId.getName(id);
		// result.betweenResult.add(new GeneGraphRankNode(
		// id,
		// subGraph.getBetweennessValue(subGraph.getNode(id)),
		// geneName,
		// seedsSet.contains(geneName)));
		// }
		GgrLogger.log("between rank finished.. ");

		new UndirectedGraphOutputter(subGraph, ConfigManager.getConfig(
				ConfigManager.MAIN_CONFIG, "service", "DUMP_DIR"),
				"gene-graph-" + System.currentTimeMillis()).output();
		;

		return result;
	}
}
