package cn.edu.sjtu.gdr.async;

import java.util.ArrayList;
import java.util.List;

public class FooThread extends Thread {
	
	private SharedBuffer<List<String>> buffer;
	
	public FooThread(SharedBuffer<List<String>> buffer) {
		this.buffer = buffer;
	}
	
	public void run() {
		List<String> list = null;
		while ((list = buffer.pop()) != null) {
			System.out.println(this.getId() + ": new round");
			for (String str : list) {
				System.out.println(this.getId() + ": " + str);
			}
			System.out.println(this.getId() + ": new round end!");
		}
		System.out.println(this.getId() + " exit!");
	}
	
	public static void main(String[] argv) throws InterruptedException {
		final int THREAD_COUNT = 6;
		final int BUFFER_COUNT = 10;

		SharedBuffer<List<String>> buffer = new SharedBuffer<List<String>>();
		for (int i = 0; i < BUFFER_COUNT; ++i) {
			List<String> list = new ArrayList<String>();
			for (int j = i; j < 20; ++j) {
				list.add(j + "");
			}
			buffer.push(list);
		}
		FooThread[] threads = new FooThread[THREAD_COUNT];
		for (int i = 0; i < THREAD_COUNT; ++i) {
			threads[i] = new FooThread(buffer);
		}
		for (int i = 0; i < THREAD_COUNT; ++i) {
			threads[i].start();
		}
		for (int i = 0; i < THREAD_COUNT; ++i) {
			threads[i].join();
		}
	}

}
