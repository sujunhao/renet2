package cn.edu.sjtu.gdr.utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import cn.edu.sjtu.gdr.config.ConfigManager;

public class CommonStopWord {
	private Set<String> word;

	private CommonStopWord(Set<String> word) {
		this.word = word;
	}

	public boolean contains(String str) {
		return str == null || word.contains(str.toLowerCase());
	}

	private volatile static CommonStopWord ins = null;

	public static CommonStopWord getInstance() {
		if (ins == null) {
			synchronized (CommonStopWord.class) {
				if (ins == null) {
					init();
				}
			}
		}
		return ins;
	}

	private static void init() {
		String path = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"doc-dir", "STOP_WORDS");
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			Set<String> set = new HashSet<String>();
			String str = null;

			while ((str = br.readLine()) != null) {
				set.add(str.toLowerCase());
			}

			ins = new CommonStopWord(set);
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
