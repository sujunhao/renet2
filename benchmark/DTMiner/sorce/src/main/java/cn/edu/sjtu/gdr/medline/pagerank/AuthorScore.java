package cn.edu.sjtu.gdr.medline.pagerank;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.edu.sjtu.gdr.utils.GgrLogger;

public class AuthorScore extends PageRankScore {
	// pmid -> author list
	private Map<Integer, List<Integer>> authorMeta;
	
	// author id -> pr
	private Map<Integer, Double> pr = new HashMap<Integer, Double>();

	private String path;

	public AuthorScore(String path, Map<Integer, List<Integer>> authorMeta) {
		this.path = path;
		this.authorMeta = authorMeta;
		GgrLogger.log("start reading author score page rank from: " + path);
		readall();
		GgrLogger.log("finish reading author score page rank...");
	}

	@Override
	public double score(int pmid) {
		double res = 0.0;
		List<Integer> authors = authorMeta.get(pmid);
		if (authors == null) {
			return res;
		}
		for (Integer author : authors) {
			res += pr.getOrDefault(author, 0.0);
		}
		return res;
	}

	private void readall() {
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));

			String str = null, pair[] = new String[2];
			int pos = 0, count = 0;

			while ((str = br.readLine()) != null) {
				pos = str.indexOf('\t');
				pair[0] = str.substring(0, pos);
				pair[1] = str.substring(pos + 1);
				pr.put(Integer.parseInt(pair[0]), Double.parseDouble(pair[1]));

				++count;
				if ((count & 0xfffff) == 0) {
					GgrLogger.log((count >> 20) + "M line processed...");
				}
			}

			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
