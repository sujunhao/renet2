package cn.edu.sjtu.gdr.ggr;

public class NamePosition {
	private int start, end;

	public NamePosition() {
		this(0, 0);
	}

	public NamePosition(int start, int end) {
		this.start = start;
		this.end = end;
	}

    public boolean equals(NamePosition x){
        if(start == x.getStart() && end == x.getEnd())
            return true;
        else return false;
    }

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getEnd() {
		return end;
	}

	public void setEnd(int end) {
		this.end = end;
	}

	public String toString() {
		return "{" + start + ", " + end + "}";
	}
}
