package cn.edu.sjtu.gdr.medline.cites;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import cn.edu.sjtu.gdr.config.ConfigManager;

public class CountInEdgeIdGenExe {

	private int threshold;

	public CountInEdgeIdGenExe(int threshold) {
		this.threshold = threshold;
	}

	public void run() throws IOException {
		String inpath = ConfigManager.getConfig("cites",
				"PAPER_CITE_IN_EDGE_COUNT_PATH");
		String outpath = String.format(
				ConfigManager.getConfig("cites", "PAPER_CITE_SHRINK_ID_PATH"),
				threshold);

		BufferedReader br = new BufferedReader(new FileReader(inpath));
		BufferedWriter bw = new BufferedWriter(new FileWriter(outpath));

		String str = null, pmid = null;
		int i, cnt;

		while ((str = br.readLine()) != null) {
			i = str.lastIndexOf(' ');
			pmid = str.substring(0, i);
			cnt = Integer.parseInt(str.substring(i + 1));
			if (cnt >= threshold) {
				bw.write(pmid);
				bw.newLine();
			}
		}

		br.close();
		bw.flush();
		bw.close();
	}

	public static void main(String[] args) throws IOException {
		int threshold = Integer.parseInt(ConfigManager.getConfig("cites", "PAPER_CITE_SHRINK_ID_THRESHOLD"));
		new CountInEdgeIdGenExe(threshold).run();
	}

}
