package cn.edu.sjtu.gdr.exe;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.zip.GZIPOutputStream;

import rdi.cn.source.MedlineItem;
import cn.edu.sjtu.gdr.async.LoggableThread;
import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.medline.abst.MedlineAbstPumper;

public class AbstExtractorThread extends LoggableThread {

	private String outdir;
	private AbstFiles files;

	public AbstExtractorThread(String outdir, AbstFiles files) {
		this.outdir = outdir;
		this.files = files;
	}

	public void run() {
		MedlineAbstPumper pumper = null;
		MedlineItem item = null;
		File gzfile = null;
		BufferedWriter bw = null;
		while ((gzfile = files.pop()) != null) {
			try {
				log("start processing " + gzfile.getName());
				pumper = new MedlineAbstPumper(gzfile);
				bw = new BufferedWriter(new OutputStreamWriter(
						new GZIPOutputStream(new FileOutputStream(new File(
								outdir, filename(gzfile.getName()))))));
				while (pumper.hasNext()) {
					item = pumper.next();
					bw.write("pmid: ");
					bw.write(item.getPmid());
					bw.newLine();
					bw.write(item.getText());
					bw.newLine();
				}
				log(gzfile.getName() + " finished");
				bw.flush();
				bw.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

	private String filename(String infile) {
		return new String(infile.substring(0, infile.length() - 7) + ".gz");
	}

	public static void main(String[] args) throws InterruptedException {
		final int THREAD_COUNT = Runtime.getRuntime().availableProcessors() * 3;
		String medlineDir = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"doc-dir", "MEDLINE_DIR");
		String outputDir = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"exe", "MEDLINE_ABSTRACT");
		AbstFiles files = new AbstFiles(medlineDir);
		AbstExtractorThread[] threads = new AbstExtractorThread[THREAD_COUNT];
		for (int i = 0; i < THREAD_COUNT; ++i) {
			threads[i] = new AbstExtractorThread(outputDir, files);
			threads[i].start();
		}
		
		for (int i = 0; i < THREAD_COUNT; ++i) {
			threads[i].join();
		}
	}

}

class AbstFiles {

	private String dir;
	private File[] files;
	AtomicInteger index;

	public AbstFiles(String dir) {
		this.dir = dir;
		index = new AtomicInteger(-1);
		init();
	}

	public File pop() {
		int i = index.incrementAndGet();
		return (i >= files.length) ? null : files[i];
	}

	private void init() {
		File dirfile = new File(dir);
		files = dirfile.listFiles(new FileFilter() {
			public boolean accept(File pathname) {
				return pathname.getName().endsWith(".gz");
			}
		});
		Arrays.sort(files, new Comparator<File>() {
			public int compare(File o1, File o2) {
				return -o1.getName().compareTo(o2.getName());
			}
		});
	}
}