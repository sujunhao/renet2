package cn.edu.sjtu.gdr.medline.rank;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.obo.EscapeSynonymNormalizer;
import cn.edu.sjtu.gdr.obo.Normalizer;
import cn.edu.sjtu.gdr.obo.SynonymNormalizer;
import cn.edu.sjtu.gdr.utils.GgrLogger;

/**
 * DF of Gene
 */
public class ComputeGeneDF {

	/**
	 * gene -> diseases
	 */
	private static Map<String, Map<String, List<Integer>>> compute(String path) {
		GgrLogger.log("start reading disease gene uuid map...");
		// gene -> disease -> pmids
		Map<String, Map<String, List<Integer>>> geneDiseaseMap = new HashMap<String, Map<String, List<Integer>>>();

		Normalizer disSyn = EscapeSynonymNormalizer.getDiseaseNormalizer();
		Normalizer geneSyn = EscapeSynonymNormalizer.getGeneNormalizer();
		Set<String> normDis = new HashSet<String>();

		try {
			BufferedReader br = new BufferedReader(new FileReader(path));

			String str = null, tmp[] = null;
			int pmid;
			while ((str = br.readLine()) != null) {
				tmp = str.split("\t");
				pmid = Integer
						.valueOf(tmp[2].substring(0, tmp[2].indexOf('-')));
				//List<String> genes = geneSyn.normalize(tmp[1]);
				//List<String> diseases = disSyn.normalize(tmp[0]);
				//for (String gene : genes) {
					if (!geneDiseaseMap.containsKey(tmp[1]))
						geneDiseaseMap.put(tmp[1],
								new HashMap<String, List<Integer>>());
					//for (String dis : diseases) {
						if (!geneDiseaseMap.get(tmp[1]).containsKey(tmp[0]))
							geneDiseaseMap.get(tmp[1]).put(tmp[0],
									new ArrayList<Integer>());
						geneDiseaseMap.get(tmp[1]).get(tmp[0]).add(pmid);
						normDis.add(tmp[0]);
			}

			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		GgrLogger.log("finish reading disease gene uuid map...");
		return geneDiseaseMap;
	}

	public static void write(String inpath, String outpath) {
		GgrLogger.log("start write number of gene-related diseases to file...");
		Map<String, Map<String, List<Integer>>> geneDiseaseMap = compute(inpath);
		Set<String> totalDis = new HashSet<String>();
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(outpath));
			for (String gene : geneDiseaseMap.keySet()) {
				totalDis.addAll(geneDiseaseMap.get(gene).keySet());
				bw.write(gene + "\t" + geneDiseaseMap.get(gene).size());
				bw.newLine();
			}
			bw.write("\t" + totalDis.size());
			bw.newLine();
			bw.flush();
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		GgrLogger.log("end write number of gene-related diseases to file...");
	}

	public static Map<String, Integer> load(String path) {
		GgrLogger.log("start load number of gene-related diseases to file...");
		Map<String, Integer> map = new HashMap<String, Integer>();

		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			String line;
			while ((line = br.readLine()) != null) {
				String[] strs = line.split("\t");
				map.put(strs[0], Integer.parseInt(strs[1]));
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		GgrLogger.log("end load number of gene-related diseases to file...");
		return map;
	}

	public static void main(String[] args) throws IOException {
		String inpath = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"evidence", "GENE_DISEASE_EVIDENCE_REDUCE");
        String outpath = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
                "evidence", "GENE_RELATED_DISEASE_NUMBER");
        write(inpath,outpath);
        /*
		Map<String, Map<String, List<Integer>>> map = compute(inpath);
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String gene = null;
		Map<String, List<Integer>> diseases = null;
		while (true) {
			gene = br.readLine();
			diseases = map.get(gene);
			if( diseases != null) {
				for(String dis: diseases.keySet()) {
					System.out.print(dis + ":");
					for(int pmid: diseases.get(dis))
					System.out.print("\t" + pmid);
					System.out.println();
				}
			}
		}
		*/
	}
}
