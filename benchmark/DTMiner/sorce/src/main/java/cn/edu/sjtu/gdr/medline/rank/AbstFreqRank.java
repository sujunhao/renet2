package cn.edu.sjtu.gdr.medline.rank;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.evaluation.Evaluator;
import cn.edu.sjtu.gdr.obo.EscapeSynonymNormalizer;
import cn.edu.sjtu.gdr.obo.Normalizer;
import cn.edu.sjtu.gdr.obo.SynonymNormalizer;

public class AbstFreqRank {
	public static String[] diseases = { "Kidney Failure, Chronic",
			"Kidney Neoplasms", "Acute Kidney Injury",
			"Cardiovascular Diseases", "Lung Neoplasms" };

	public static void main(String[] args) throws IOException {
		String path = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"evidence", "ABST_EVIDENCE");
		Map<String, Map<String, Integer>> map = load(path);
		Normalizer disSyn = EscapeSynonymNormalizer.getDiseaseNormalizer();

		for (String disease : diseases) {
			System.out.println(String.format("Start evaluating: %s", disease));
			List<String> disNorm = disSyn.normalize(disease);
			Map<String, Integer> tmp = new HashMap<String, Integer>();
			for (String dis : disNorm) {
				for (String gene : map.get(dis).keySet()) {
					if (!tmp.containsKey(gene)
							|| tmp.get(gene) < map.get(dis).get(gene))
						tmp.put(gene, map.get(dis).get(gene));
				}
			}
			System.out.println(tmp.size());
			List<GeneScore> genes = new ArrayList<GeneScore>();
			for (String gene : tmp.keySet())
				genes.add(new GeneScore(gene, tmp.get(gene)));
			Collections.sort(genes, Collections.reverseOrder());
			Evaluator.evaluation(disease, genes);
		}
	}

	public static Map<String, Map<String, Integer>> load(String path) {
		Map<String, Map<String, Integer>> map = new HashMap<String, Map<String, Integer>>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			String line;
			String[] strs = null;
			while ((line = br.readLine()) != null) {
				strs = line.split("\\$\\$\\$");
				if( !map.containsKey(strs[0]))
					map.put(strs[0], new HashMap<String, Integer>());
				map.get(strs[0]).put(strs[1], Integer.valueOf(strs[2]));
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return map;
	}
}
