package cn.edu.sjtu.gdr.medline.author;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.graph.DirectedGraph;
import cn.edu.sjtu.gdr.graph.DirectedGraphNode;
import cn.edu.sjtu.gdr.graph.DirectedGraphOutputter;
import cn.edu.sjtu.gdr.utils.GgrLogger;

public class ExtractAuthorCitesGraphPartial {

	public static final int STEP = Integer.parseInt(ConfigManager.getConfig(
			ConfigManager.MAIN_CONFIG, "cites-config", "PARTIAL_GRAPH_STEP"));
	public static final int START_ID = Integer.parseInt(ConfigManager.getConfig(
			ConfigManager.MAIN_CONFIG, "cites-config", "PARTIAL_GRAPH_START"));

	public void run() {
		File edgeFile = new File(ConfigManager.getConfig(
				ConfigManager.MAIN_CONFIG, "author",
				"AUTHOR_CITES_GRAPH_EDGES_PATH"));
		PartialDirectedGraphBuilder builder = null;
		String outPath = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"author", "AUTHOR_CITES_GRAPH_PATH_FORMAT");

		for (int startId = START_ID; true; startId += STEP) {
			int endId = startId + STEP;
			GgrLogger.log("start building graph for id: " + startId + " to "
					+ endId);
			builder = new PartialDirectedGraphBuilder(startId, endId, outPath);
			builder.build(edgeFile);
			GgrLogger.log("finish building graph for id: " + startId + " to "
					+ endId);
			if (!builder.isMoreAfter())
				break;
		}

	}

	public static void main(String[] args) {
		new ExtractAuthorCitesGraphPartial().run();
	}

}

class PartialDirectedGraphBuilder {

	private int startId, endId;
	private String outPathFormat;
	private DirectedGraph graph;

	private boolean moreBefore, moreAfter;

	public PartialDirectedGraphBuilder(int startId, int endId,
			String outPathFormat) {
		this.outPathFormat = outPathFormat;
		this.startId = startId;
		this.endId = endId;
		graph = new DirectedGraph();
		moreBefore = moreAfter = false;
	}

	public void build(File file) {
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(file));

			String str = null, ints[];
			int from = 0, to = 0, count = 0;
			DirectedGraphNode gn = null;

			while ((str = br.readLine()) != null) {
				ints = str.split("\t");
				from = Integer.parseInt(ints[0]);
				to = Integer.parseInt(ints[1]);

				if (isInPartial(from)) {
					gn = graph.ensureAndGetNode(from);
					gn.addOutEdge(to);
				}

				if (isInPartial(to)) {
					gn = graph.ensureAndGetNode(to);
					gn.addInEdge(from);
				}

				++count;
				if ((count & 0xfffff) == 0) {
					GgrLogger.log((count >> 20) + "M lines processed..");
					GgrLogger.log(graph.size() + " nodes collected..");
				}
			}

			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (graph.size() > 0) {
			String path = String.format(outPathFormat, "." + startId + "-"
					+ endId);
			new DirectedGraphOutputter(graph, path).output();
		}
	}

	public boolean isMoreBefore() {
		return moreBefore;
	}

	public boolean isMoreAfter() {
		return moreAfter;
	}

	private boolean isInPartial(int id) {
		if (id < startId) {
			moreBefore = true;
			return false;
		}
		if (id >= endId) {
			moreAfter = true;
			return false;
		}
		return true;
	}
}