package cn.edu.sjtu.gdr.medline.evidence;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.utils.GgrLogger;
import cn.edu.sjtu.gdr.obo.Hierarchy;

public class EvidenceReducer {

	// disease -> gene -> [list of uuid]
	private Map<String, Map<String, Set<String>>> evidences;

	private String inpath, outpath;
    private Hierarchy h = Hierarchy.getDisHierarchy();
    private int generationSize = 1;

	public EvidenceReducer(String evidencePath, String outpath) {
		evidences = new HashMap<String, Map<String, Set<String>>>();
		this.inpath = evidencePath;
		this.outpath = outpath;
	}

	public void run() {
		try {
			BufferedReader br = new BufferedReader(new FileReader(inpath));

			String str = null, triple[] = null;
			int count = 0;

			while ((str = br.readLine()) != null) {
				triple = str.split("\t");
                String tmp = triple[0].substring(0,triple[0].lastIndexOf("-"));
				record(tmp.substring(0,
                        tmp.lastIndexOf("-"))+"/"+triple[5],
                        triple[1], triple[2]);
				
				++count;
				if ((count & 0xfffff) == 0) {
					GgrLogger.log((count >> 20) + "M evidence loaded..");
				}
			}

			br.close();
			output();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void output() {
		GgrLogger.log("starting to output...");
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(outpath));

			for (Map.Entry<String, Map<String, Set<String>>> entry : evidences
					.entrySet()) {
				for (Map.Entry<String, Set<String>> e : entry.getValue()
						.entrySet()) {
					bw.write(entry.getKey()); // write disease
					bw.write("\t");
					bw.write(e.getKey()); // write gene
					for (String uuid : e.getValue()) {
						bw.write("\t");
						bw.write(uuid);
					}
					bw.newLine();
				}
			}

			bw.flush();
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		GgrLogger.log("finish to output...");
	}

	private void record(String uuid, String gene, String disease) {
		Map<String, Set<String>> map;
		Set<String> list;
        Set<String> ancestors = h.getAllAncestors(disease,generationSize);
		if ((map = evidences.get(disease)) == null) {
			map = new HashMap<String, Set<String>>();
			evidences.put(disease, map);
		}

		if ((list = map.get(gene)) == null){
			list = new HashSet<String>();
			map.put(gene, list);
		}
        list.add(uuid);

        for(String ancestor : ancestors){
            if ((map = evidences.get(ancestor)) == null) {
                map = new HashMap<String, Set<String>>();
                evidences.put(ancestor, map);
            }

            if ((list = map.get(gene)) == null) {
                list = new HashSet<String>();
                map.put(gene, list);
            }
            list.add(uuid);
        }

	}

	public static void main(String[] args) {
		String evidencePath = ConfigManager.getConfig(
				ConfigManager.MAIN_CONFIG, "evidence", "GENE_DISEASE_EVIDENCE")+".txt";
		String outpath = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"evidence", "GENE_DISEASE_EVIDENCE_REDUCE");
		new EvidenceReducer(evidencePath, outpath).run();
	}

}
