package cn.edu.sjtu.gdr.obo;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import cn.edu.sjtu.gdr.config.ConfigManager;

public class Hierarchy {

	static private Map<String, Set<String>> children = new HashMap<>();
	static private Map<String, Set<String>> parent = new HashMap<>();
    static private Map<String, Set<String>> map = new HashMap<>();
    static private Map<String, Set<String>> newMap = new HashMap<>();

	private Hierarchy(String path) {
		//children = new HashMap<String, Set<String>>();
		//parent = new HashMap<String, Set<String>>();
		load(path);
	}

	static private void load(String path) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			String line, id;
			while ((line = br.readLine()) != null) {
				if (line.equals("[Term]")) {
					line = br.readLine();
					id = line.substring(4).toLowerCase().replaceAll(" ","");
					if (!parent.containsKey(id))
						parent.put(id, new HashSet<String>());
					while ((line = br.readLine()) != null) {
						if (line.equals(""))
							break;
						if (line.startsWith("is_a:")) {
							String[] strs = line.substring(5).split("!");
							String tmp = strs[0].trim().toLowerCase().replaceAll(" ","");
							parent.get(id).add(tmp);
							if (!children.containsKey(tmp))
								children.put(tmp, new HashSet<String>());
							children.get(tmp).add(id);
						}
					}
				}
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

    static private void readDict(){
        try {
            String path = ConfigManager.getConfig("obo", "DISEASE_SYN");
            BufferedReader br = new BufferedReader(new FileReader(path));
            String str, temp[], norm;
            while ((str = br.readLine()) != null) {
                temp = str.split(OboTerm.SPLITTER);
                norm = temp[0];
                norm = norm.toLowerCase();
                if (!map.containsKey(norm)) {
                    map.put(norm, new HashSet<String>());
                    newMap.put(norm, new HashSet<String>());
                }
                for (int i=1; i<temp.length; ++i) {
                    String name = temp[i];
                    name = name.toLowerCase();
                    map.get(norm).add(name);
                    newMap.get(norm).add(name);
                }
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static private void print() throws IOException{
        readDict();
        for(String norm : map.keySet()){
            Set<String> ancestors = getAllAncestors(norm,1);
            for(String ancestor : ancestors){
                Set<String> synonyms = map.get(norm);
                newMap.get(ancestor).addAll(synonyms);
            }
        }
        BufferedWriter bw = new BufferedWriter(new FileWriter("res/data/withChildren.syn"));
        for(String norm : newMap.keySet()){
            Set<String> synonyms = newMap.get(norm);
            bw.write(norm);
            for(String syn : synonyms)
                bw.write("###"+syn);
            bw.newLine();
        }
        bw.flush();
        bw.close();
    }

	public Set<String> getAllDescendants(String id) {
		id = id.toLowerCase();
		Set<String> ret = new HashSet<String>();
		Set<String> s1 = new HashSet<String>(children.getOrDefault(id,
				new HashSet<String>()));
		Set<String> s2 = new HashSet<String>();
		while (s1.size() > 0) {
			for (String s : s1) {
				if (!ret.contains(s)) {
					ret.add(s);
					s2.addAll(children.getOrDefault(s, new HashSet<String>()));
				}
			}
			s1.clear();
			s1.addAll(s2);
			s2.clear();
		}
		return ret;
	}
    static public Set<String> getAllAncestors(String id, int size){
        id = id.toLowerCase();
        Set<String> ret = new HashSet<>();
        Set<String> s1 = new HashSet<String>(parent.getOrDefault(id,
                new HashSet<String>()));
        Set<String> s2 = new HashSet<String>();
        int i = 0;
        while (s1.size() > 0 && i<size) {
            ++i;
            for (String s : s1) {
                if (!ret.contains(s)) {
                    if(!s.equals("root")) ret.add(s);
                    s2.addAll(parent.getOrDefault(s, new HashSet<String>()));
                }
            }
            s1.clear();
            s1.addAll(s2);
            s2.clear();
        }
        return ret;
    }

	private static Hierarchy disHierarchy = null;
	public static Hierarchy getDisHierarchy() {
		if (disHierarchy == null) {
			String path = ConfigManager.getConfig("obo", "DISEASE_OBO");
			disHierarchy = new Hierarchy(path);
		}
		return disHierarchy;
	}

	public static void main(String[] args) throws IOException{
        load(ConfigManager.getConfig("obo", "DISEASE_OBO"));
        print();
	}
}
