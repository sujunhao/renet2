package cn.edu.sjtu.gdr.data.feature;

import java.util.List;

import cn.edu.sjtu.gdr.data.deptree.DepTree;
import cn.edu.sjtu.gdr.data.label.LabeledData;
import cn.edu.sjtu.gdr.data.label.LabeledRelationData;
import cn.edu.sjtu.gdr.ggr.NamePosition;
import cn.edu.sjtu.gdr.utils.CollectionUtils;

/**
 * Gene windows内是否所有词都直接或间接以wn为root <br>
 * Disease window内是否所有词都直接或间接以wn为root
 * 
 * @author Jack Sun(jacksunwei@gmail.com)
 *
 */
public class WindowWnRootFeature {

	public static final double WN_IS_ROOT = 1.0;
	public static final double WN_IS_NOT_ROOT = 0.0;

	public static double extractRelationGeneFeature(LabeledRelationData data) {
		return extractFeature(data.getGeneIndex(), data.getSentenceLabeledData());
	}

	public static double extractRelationDiseaseFeature(LabeledRelationData data) {
		return extractFeature(data.getDiseaseIndex(),
				data.getSentenceLabeledData());
	}

	public static double extractFeature(NamePosition np, LabeledData data) {
		int start = np.getStart() + 1, end = np.getEnd() + 1;
		DepTree tree = data.getDepTree();
		int wnIndex = tree.getNode(end - 1).getIndex();

		for (int i = start; i < end; ++i) {
			List<Integer> path = tree.getNode(i).getPathFromRoot();
			if (!CollectionUtils.exists(path, wnIndex)) {
				return WN_IS_NOT_ROOT;
			}
		}

		return WN_IS_ROOT;
	}

}
