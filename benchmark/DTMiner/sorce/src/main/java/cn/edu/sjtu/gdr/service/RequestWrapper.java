package cn.edu.sjtu.gdr.service;

import java.net.Socket;

public class RequestWrapper {
	public final Socket client;
	public final String body;

	public RequestWrapper(Socket client, String body) {
		this.client = client;
		this.body = body;
	}

}
