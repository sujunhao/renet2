package cn.edu.sjtu.gdr.exe;

import cn.edu.sjtu.gdr.config.ConfigManager;
import java.io.*;
import java.util.Arrays;
import java.util.Comparator;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Created by Xie Yanping on 2015/10/28.
 */
public class AbstCombine {
    private String indir1;
    private String indir2;
    private String outdir;
    private File[] files1;
    private File[] files2;

    public AbstCombine(String indir1, String indir2, String outdir) {
        this.indir1 = indir1;
        this.indir2 = indir2;
        this.outdir = outdir;
        init();
    }

    public void run() throws FileNotFoundException, IOException {
        for (int i=0; i<files1.length; ++i) {
            BufferedReader br1 = new BufferedReader(new InputStreamReader(new GZIPInputStream(
                    new FileInputStream(files1[i]))));
            BufferedReader br2 = new BufferedReader(new InputStreamReader(new GZIPInputStream(
                    new FileInputStream(files2[i]))));
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
                    new GZIPOutputStream(new FileOutputStream(new File(outdir,
                            files1[i].getName())))));
            String line1, line2;
            String pmid1, pmid2;
            int num1, num2;
            while ((line1 = br1.readLine()) != null) {
                line2 = br2.readLine();
                pmid1 = line1.substring(6,line1.lastIndexOf(" "));
                pmid2 = line2.substring(6,line2.lastIndexOf(" "));
                if(!pmid1.equals(pmid2))
                    System.out.println(pmid1+" unmatched");
                num1 = Integer.parseInt(line1.substring(line1.lastIndexOf(" ")+1));
                num2 = Integer.parseInt(line2.substring(line2.lastIndexOf(" ")+1));
                bw.write("pmid: "+pmid1+" "+Integer.toString(num1+num2));
                bw.newLine();
                for(int j=0; j<num2; ++j){
                    for(int k=0; k<4; ++k) {
                        bw.write(br2.readLine());
                        bw.newLine();
                    }
                }
                for(int j=0; j<num1; ++j){
                    for(int k=0; k<4; ++k) {
                        bw.write(br1.readLine());
                        bw.newLine();
                    }
                }
            }
            System.out.println(files1[i].getName() + " finished");
            bw.flush();
            bw.close();
        }
    }

    private void init() {
        File dir1 = new File(indir1);
        File dir2 = new File(indir2);
        files1 = dir1.listFiles(new FileFilter() {
            public boolean accept(File pathname) {
                return pathname.getName().endsWith(".gz");
            }
        });
        Arrays.sort(files1, new Comparator<File>() {
            public int compare(File o1, File o2) {
                // fuck you, reverse order
                return -o1.getName().compareTo(o2.getName());
            }
        });
        files2 = dir2.listFiles(new FileFilter() {
            public boolean accept(File pathname) {
                return pathname.getName().endsWith(".gz");
            }
        });
        Arrays.sort(files2, new Comparator<File>() {
            public int compare(File o1, File o2) {
                // fuck you, reverse order
                return -o1.getName().compareTo(o2.getName());
            }
        });
    }

    public static void main(String[] args) throws FileNotFoundException,
            IOException {
        String in1 = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
                "exe", "MEDLINE_ABSTRACT_PAR");
        String in2 = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
                "exe", "MEDLINE_ABSTRACT_PAR_TITLE");
        String out = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG, "exe",
                "MEDLINE_ABSTRACT_PAR_COMBINE");
        new AbstCombine(in1, in2, out).run();
    }
}
