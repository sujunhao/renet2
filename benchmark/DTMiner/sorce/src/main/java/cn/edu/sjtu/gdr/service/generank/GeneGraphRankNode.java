package cn.edu.sjtu.gdr.service.generank;

public class GeneGraphRankNode {
	public int id;
	public Object rankValue;
	public String geneName;
	public boolean isSeed;

	private GeneGraphRankNode(int id, String geneName, boolean isSeed) {
		this.id = id;
		this.geneName = geneName;
		this.isSeed = isSeed;
	}

	public GeneGraphRankNode(int id, double rankValue, String geneName,
			boolean isSeed) {
		this(id, geneName, isSeed);
		this.rankValue = rankValue;
	}

	public GeneGraphRankNode(int id, int rankValue, String geneName,
			boolean isSeed) {
		this(id, geneName, isSeed);
		this.rankValue = rankValue;
	}

	/**
	 * format: "[id] [rankValue] [y/n(isSeed)] [geneName]"
	 * 
	 * @return
	 */
	public String netString() {
		StringBuilder sb = new StringBuilder();
		sb.append(id).append(" ").append(rankValue).append(" ")
				.append((isSeed ? "y" : "n")).append(" ").append(geneName);
		return sb.toString();
	}
}
