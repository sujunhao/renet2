package cn.edu.sjtu.gdr.data.analysis;

import java.io.StringReader;
import java.util.List;

import cn.edu.sjtu.gdr.ggr.NamePosition;
import cn.edu.sjtu.gdr.ggr.NameRecognizer;
import cn.edu.sjtu.gdr.ggr.NameRecognizerResult;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.process.DocumentPreprocessor;

public class LabelData {
	/** format: gene startIndex endIndex {gene_name}: */
	private static final String GENE_RECOGNIZE_FORMAT = "gene %d %d {%s}: ";
	/** format: disease startIndex endIndex {disease_name}: */
	private static final String DISEASE_RECOGNIZE_FORMAT = "disease %d %d {%s}: ";
	/**
	 * format: relation gene startIndex endIndex disease startIndex endIndex
	 * ({gene_name} vs. {disease_name}):
	 */
	private static final String RELATION_FORMAT = "relation gene %d %d disease %d %d ({%s} vs. {%s}): ";

	private String pmid;
	private int num;
	private String liter;
	private String markerLine;
	private List<HasWord> sentence;
	private List<NamePosition> genesSenIndex;
	private List<NamePosition> diseasesSenIndex;

	/**
	 * Construct from the result of {@link NameRecognizer}
	 * 
	 * @param liter
	 * @param res
	 */
	public LabelData(String pmid, int num, String liter,
			NameRecognizerResult res) {
		this.pmid = pmid;
		this.num = num;
		this.liter = liter;
		this.markerLine = res.underlineMark(liter, 0);
		this.sentence = new DocumentPreprocessor(new StringReader(liter))
				.iterator().next();
		this.genesSenIndex = res.genesSenIndex;
		this.diseasesSenIndex = res.diseasesSenIndex;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder(1024);
		sb.append("pmid: ").append(pmid).append(" ").append(num).append("\n");
		sb.append(liter).append("\n");
		sb.append(markerLine);

		int count = 0;
		int start = 0, end = 0;
		for (NamePosition np : genesSenIndex) {
			sb.append("\n");
			start = np.getStart();
			end = np.getEnd();
			sb.append(count++)
					.append(" ")
					.append(String.format(GENE_RECOGNIZE_FORMAT, start, end,
							subStringInLiter(start, end)));
		}

		count = 0;
		for (NamePosition np : diseasesSenIndex) {
			sb.append("\n");
			start = np.getStart();
			end = np.getEnd();
			sb.append(count++)
					.append(" ")
					.append(String.format(DISEASE_RECOGNIZE_FORMAT, start, end,
							subStringInLiter(start, end)));
		}

		count = 0;
		for (NamePosition geneNp : genesSenIndex) {
			for (NamePosition diseaseNp : diseasesSenIndex) {
				sb.append("\n");
				sb.append(count++)
						.append(" ")
						.append(String.format(
								RELATION_FORMAT,
								geneNp.getStart(),
								geneNp.getEnd(),
								diseaseNp.getStart(),
								diseaseNp.getEnd(),
								subStringInLiter(geneNp.getStart(),
										geneNp.getEnd()),
								subStringInLiter(diseaseNp.getStart(),
										diseaseNp.getEnd())));
			}
		}
		return sb.toString();
	}

	private String subStringInLiter(int start, int end) {
		CoreLabel first = (CoreLabel) sentence.get(start);
		CoreLabel last = (CoreLabel) sentence.get(end - 1);
		return liter.substring(first.beginPosition(), last.endPosition());
	}

}
