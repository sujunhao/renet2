package cn.edu.sjtu.gdr.service.dgfreq;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Set;

import cn.edu.sjtu.gdr.service.RequestWrapper;
import cn.edu.sjtu.gdr.service.ServiceThread;

public class DiseaseGeneFreqDetailThread extends ServiceThread {
	public static final String SERVICE_NAME = "diseaseGeneFreqDetail";

	public DiseaseGeneFreqDetailThread() {
		super();
	}

	@Override
	public void run() {
		while (true) {
			try {
				RequestWrapper request = buffer.pop();
				if (request == null) {
					synchronized (this) {
						wait();
					}
				} else {
					BufferedWriter bw = new BufferedWriter(
							new OutputStreamWriter(
									request.client.getOutputStream()));
					String[] strs = request.body.trim().split("\t");
					String disease = strs[0];
					String gene = strs[1];
					System.out.println(disease + "\t" + gene);
					DiseaseGeneFrequency dgf = DiseaseGeneFrequency
							.getInstance();
					Set<Integer> pmids = dgf.getPmids(disease, gene);
					bw.write("" + pmids.size());
					bw.newLine();
					for (Integer pmid: pmids) {
						bw.write(pmid.toString());
						bw.newLine();
					}
					bw.flush();
					bw.close();
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
