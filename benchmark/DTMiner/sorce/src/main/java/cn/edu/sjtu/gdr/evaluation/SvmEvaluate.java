package cn.edu.sjtu.gdr.evaluation;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Xie Yanping on 2015/12/9.
 */
public class SvmEvaluate {
    static private String svmRes = "svm/hybrid.res";
    static private String testPath = "res/experiment/svmdata.hybrid.test";
    static private void compare() throws IOException{
        BufferedReader br1 = new BufferedReader(new FileReader(svmRes));
        BufferedReader br2 = new BufferedReader(new FileReader(testPath));
        br1.close();
        br2.close();
    }
    static public void main(String[] args){

    }
}
