package cn.edu.sjtu.gdr.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.utils.GgrLogger;

public class ReactorService {

	public static void main(String[] args) throws IOException {
		ServerSocket socket = null;
		try {
			DispatchHandler handler = new DispatchHandler();
			String host = ConfigManager.getConfig("service", "HOST");
			int port = Integer.parseInt(ConfigManager.getConfig("service",
					"PORT"));
			InetSocketAddress address = new InetSocketAddress(host, port);
			socket = new ServerSocket();
			socket.bind(address);

			while (true) {
				Socket client = socket.accept();
				BufferedReader br = new BufferedReader(new InputStreamReader(
						client.getInputStream()));
				String service = br.readLine();
				String body = br.readLine();
				// br.close();
				GgrLogger.log("accept from: " + client.getInetAddress()
						+ ", service: " + service);

				handler.handle(client, service, body);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			// socket.close();
		}
	}
}
