package cn.edu.sjtu.gdr.graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;
import java.util.TreeMap;

import org.jblas.ComplexDouble;
import org.jblas.ComplexDoubleMatrix;
import org.jblas.DoubleMatrix;
import org.jblas.Eigen;

public class UndirectedGraph extends Graph<UndirectedGraphNode> {
	private Map<Integer, UndirectedGraphNode> nodes;
	private Map<Integer, Double> eigenVector = null;
	private Map<Integer, Integer> closeness = null;
	private Map<Integer, Double> betweenness = null;

	public UndirectedGraph(Map<Integer, UndirectedGraphNode> nodes) {
		this.nodes = nodes;
	}

	public UndirectedGraph() {
		// use TreeMap because when graph goes large, rehash will be very
		// consuming
		this(new TreeMap<Integer, UndirectedGraphNode>());
	}

	public void addNode(UndirectedGraphNode gn) {
		this.nodes.put(gn.getId(), gn);
	}

	public UndirectedGraphNode getNode(int id) {
		return nodes.get(id);
	}

	public UndirectedGraphNode ensureAndGetNode(int id) {
		if (getNode(id) == null) {
			addNode(new UndirectedGraphNode(id));
		}
		return getNode(id);
	}

	public Iterator<UndirectedGraphNode> iterator() {
		return nodes.values().iterator();
	}

	public int size() {
		return nodes.size();
	}

	/**
	 * get the eigenvector centrality of a node
	 * 
	 * @param node
	 * @return
	 */
	public double getEigenVectorValue(UndirectedGraphNode node) {
		if (eigenVector == null)
			getEigenVector();
		if (eigenVector.containsKey(node.getId()))
			return eigenVector.get(node.getId());
		return 0;
	}

	/**
	 * get the closeness centrality of a node
	 * 
	 * @param node
	 * @return
	 */
	public int getClosenessValue(UndirectedGraphNode node) {
		if (closeness == null)
			getShortestPaths();
		if (closeness.containsKey(node.getId()))
			return closeness.get(node.getId());
		return 0;
	}

	/**
	 * get the betweenness centrality of a node
	 * 
	 * @param node
	 * @return
	 */
	public double getBetweennessValue(UndirectedGraphNode node) {
		if (betweenness == null)
			getShortestPaths();
		if (betweenness.containsKey(node.getId()))
			return betweenness.get(node.getId());
		return 0;
	}

	/**
	 * get the eigenvector of the graph
	 */
	private void getEigenVector() {
		// convert adjacency list to adjacency matrix
		int size = nodes.size();
		double[][] matrix = new double[size][size];
		for (int i = 0; i < size; i++)
			for (int j = 0; j < size; j++)
				matrix[i][j] = 0;

		Map<Integer, Integer> map = new HashMap<Integer, Integer>(); // nodeID
																		// -->
																		// row
		int i = 0;
		for (int nodeID : nodes.keySet())
			map.put(nodeID, i++);

		for (int nodeID : nodes.keySet()) {
			int row = map.get(nodeID);
			UndirectedGraphNode node = nodes.get(nodeID);
			for (Iterator<Integer> itr = node.edgesIterator(); itr.hasNext();) {
				int col = map.get(itr.next());
				matrix[row][col] = 1;
			}
		}

		// compute the principle eigenvector
		eigenVector = new HashMap<Integer, Double>();
		DoubleMatrix doubleMatrix = new DoubleMatrix(matrix);
		ComplexDouble[] eigenvalues = Eigen.eigenvalues(doubleMatrix).toArray();
		// get index of max eigenvalue
		int maxIndex = 0;
		for (int j = 0; j < eigenvalues.length; j++) {
			double newnumber = eigenvalues[j].real();
			if ((newnumber > eigenvalues[maxIndex].real())) {
				maxIndex = j;
			}
		}

		ComplexDoubleMatrix eigenVectors = Eigen.eigenvectors(doubleMatrix)[0];
		ComplexDouble[] column = eigenVectors.getColumn(maxIndex).toArray();

		for (int j = 0; j < column.length; j++) {
			for (int k : map.keySet())
				if (map.get(k) == j) {
					eigenVector.put(k, column[j].real());
				}
		}
	}

	public void init() {
		getEigenVector();
		getShortestPaths();
	}

	/**
	 * get all shortest paths in the graph
	 */
	private void getShortestPaths() {
		closeness = new HashMap<Integer, Integer>();
		betweenness = new HashMap<Integer, Double>();
		Set<Integer> vertexes = nodes.keySet();
		for (int v : vertexes)
			betweenness.put(v, 0.0);

		for (int s : vertexes) {
			// breadth first search
			Stack<Integer> stack = new Stack<Integer>();

			Queue<Integer> queue = new LinkedList<Integer>();
			// distance from s to t
			Map<Integer, Integer> distance = new HashMap<Integer, Integer>();
			// number of shortest paths from s to t
			Map<Integer, Integer> pathNum = new HashMap<Integer, Integer>();
			// all possible nodes ahead of t on the shortest path from s to t
			Map<Integer, List<Integer>> paths = new HashMap<Integer, List<Integer>>();
			for (int t : vertexes) {
				paths.put(t, new ArrayList<Integer>());
				if (t == s) {
					distance.put(t, 0);
					pathNum.put(t, 1);
				} else {
					distance.put(t, -1);
					pathNum.put(t, 0);
				}
			}
			queue.add(s);
			UndirectedGraphNode gn = null;
			// generate shortest path
			while (queue.size() > 0) {
				int v = queue.poll();
				stack.push(v);
				gn = nodes.get(v);
				for (Iterator<Integer> itr = gn.edgesIterator(); itr.hasNext();) {
					int w = itr.next();
					if (distance.get(w) < 0) {
						queue.add(w);
						distance.put(w, distance.get(v) + 1);
					}
					if (distance.get(w) == distance.get(v) + 1) {
						pathNum.put(w, pathNum.get(w) + pathNum.get(v));
						paths.get(w).add(v);
					}
				}
			}
			Map<Integer, Double> delta = new HashMap<Integer, Double>();
			for (int v : vertexes)
				delta.put(v, 0.0);
			// compute betweenness
			while (stack.size() > 0) {
				int w = stack.pop();
				for (int v : paths.get(w))
					delta.put(
							v,
							delta.get(v) + 1.0 * pathNum.get(v)
									/ pathNum.get(w) * (1 + delta.get(w)));
				if (w != s)
					betweenness.put(w, betweenness.get(w) + delta.get(w));
			}
			int sum = 0;
			for (int i : vertexes)
				if (distance.get(i) > 0)
					sum += distance.get(i);
			closeness.put(s, sum);
		}
	}

	/**
	 * extract sub graph according to the seeds
	 * 
	 * @param seeds
	 * @return a graph according to given seeds
	 */
	public UndirectedGraph subGraph(int[] seeds) {
		Set<Integer> nodeIds = new HashSet<Integer>();
		UndirectedGraphNode gn = null;

		// find all gene ids co-occur with the seeds
		for (Integer seedId : seeds) {
			gn = getNode(seedId);
			if (gn != null) {
				// add the ids of seeds
				nodeIds.add(seedId);
				// add the edge of seeds
				for (Iterator<Integer> itr = gn.edgesIterator(); itr.hasNext();) {
					int id = itr.next();
					if (getNode(id) != null)
						nodeIds.add(id);
				}
			}
		}

		// build the sub-graph
		UndirectedGraph res = new UndirectedGraph();
		for (Integer id : nodeIds) {
			UndirectedGraphNode newNode = new UndirectedGraphNode(id);
			UndirectedGraphNode oldNode = getNode(id);
			for (Iterator<Integer> itr = oldNode.edgesIterator(); itr.hasNext();) {
				int edgeId = itr.next();
				if (nodeIds.contains(edgeId)) {
					newNode.addOutEdge(edgeId, oldNode.getOutEdgeWeight(edgeId));
				}
			}
			res.addNode(newNode);
		}

		return res;
	}

	public static void main(String[] args) {
		// String inpath = "/home/dong/az/testgraph.txt";
		// Graph graph = new Graph();
		// try {
		// BufferedReader br = new BufferedReader(new FileReader(inpath));
		// String str = null, edges[] = null;
		// GraphNode gn = null;
		// int nodeId = 0;
		// while ((str = br.readLine()) != null) {
		// nodeId = Integer.parseInt(str);
		// gn = new GraphNode(nodeId);
		// edges = br.readLine().split(" ");
		// for (String edge : edges) {
		// gn.addEdge(Integer.parseInt(edge));
		// }
		// graph.addNode(gn);
		// }
		// br.close();
		// } catch (FileNotFoundException e) {
		// e.printStackTrace();
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
		//
		// int[] rank = new ClosenessRanker(graph).rank(true);
		// for( int i: rank)
		// System.out.println(i + "\t" +
		// graph.getClosenessValue(graph.getNode(i)));
		// rank = new BetweennessRanker(graph).rank(false);
		// for( int i: rank)
		// System.out.println(i + "\t" +
		// graph.getBetweennessValue(graph.getNode(i)));
	}
}
