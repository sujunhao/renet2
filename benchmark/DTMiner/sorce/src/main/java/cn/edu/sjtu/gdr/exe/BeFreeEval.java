package cn.edu.sjtu.gdr.exe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.evaluation.DisGeNet;
import cn.edu.sjtu.gdr.evaluation.Evaluator;
import cn.edu.sjtu.gdr.medline.rank.GeneScore;

public class BeFreeEval {

	public static String[] diseases = { "Kidney Failure, Chronic",
			"Kidney Neoplasms", "Acute Kidney Injury",
			"Cardiovascular Diseases", "Lung Neoplasms" };

	public static void main(String[] args) {
		DisGeNet befree = DisGeNet.getDisGeNet(ConfigManager.getConfig(
				"gene-disease-association", "BEFREE_ASSOCIATION"));
		for( String dis: diseases) {
			System.out.println("Start evaluate " + dis);
			Map<String, Double> geneList = befree.getGeneList(dis);
		
			List<GeneScore> genes = new ArrayList<GeneScore>();
			for(String gene: geneList.keySet()) {
				genes.add(new GeneScore(gene, geneList.get(gene)));
			}
			Collections.sort(genes, Collections.reverseOrder());
			Evaluator.evaluation(dis, genes);
		}
	}
}
