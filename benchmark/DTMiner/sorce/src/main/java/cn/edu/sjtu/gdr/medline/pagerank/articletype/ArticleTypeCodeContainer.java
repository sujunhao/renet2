package cn.edu.sjtu.gdr.medline.pagerank.articletype;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import cn.edu.sjtu.gdr.config.ConfigManager;

public class ArticleTypeCodeContainer {
	private static ArticleTypeCodeContainer instance;

	private static final ArticleTypeCode OTHERS = new ArticleTypeCode();

	private Map<Integer, ArticleTypeCode> map;

	public ArticleTypeCodeContainer(String path) {
		map = new HashMap<Integer, ArticleTypeCode>();
		readall(path);
	}

	public ArticleTypeCode getCode(int pmid) {
		ArticleTypeCode res = map.get(pmid);
		return res != null ? res : OTHERS;
	}

	public static ArticleTypeCodeContainer getInstance() {
		if (instance == null) {
			synchronized (ArticleTypeCodeContainer.class) {
				if (instance == null) {
					String path = ConfigManager.getConfig("artical-type",
							"ARTICLE_TYPE_PATH");
					instance = new ArticleTypeCodeContainer(path);
				}
			}
		}

		return instance;
	}

	private void readall(String path) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			String str = null, tmp[] = null;
			int pmid = 0;
			ArticleTypeCode code = null;

			while ((str = br.readLine()) != null) {
				tmp = str.split(" ");
				if (tmp.length == 4) {
					pmid = Integer.parseInt(tmp[0]);
					code = new ArticleTypeCode();
					if (tmp[1].equals("1")) {
						code.setClinicalTrial();
					}
					if (tmp[2].equals("1")) {
						code.setVivo();
					}
					if (tmp[3].equals("1")) {
						code.setVitro();
					}

					map.put(pmid, code);
				}
			}

			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
