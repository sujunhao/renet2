package cn.edu.sjtu.gdr.data.label;

import java.lang.ref.WeakReference;

import cn.edu.sjtu.gdr.ggr.NamePosition;

public class LabeledDiseaseData {

	public int getCount() {
		return count;
	}

	public WeakReference<LabeledData> getLabeledData() {
		return labeledData;
	}

	public NamePosition getDiseaseIndex() {
		return diseaseIndex;
	}

	public int getLabel() {
		return label;
	}

	public static final int IS_DISEASE = 1;
	public static final int NOT_DISEASE = 0;
	
	int count;
	WeakReference<LabeledData> labeledData;
	NamePosition diseaseIndex;
	int label;
	
	public LabeledDiseaseData(LabeledData data, int count, NamePosition diseaseIndex, int label) {
		this.labeledData = new WeakReference<LabeledData>(data);
		this.count = count;
		this.diseaseIndex = diseaseIndex;
		this.label = label;
	}
	
}
