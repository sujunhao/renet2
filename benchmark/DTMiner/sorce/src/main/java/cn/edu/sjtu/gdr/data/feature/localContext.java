package cn.edu.sjtu.gdr.data.feature;

import cn.edu.sjtu.gdr.data.label.LabeledData;
import cn.edu.sjtu.gdr.data.label.LabeledRelationData;
import cn.edu.sjtu.gdr.data.utils.NLPToolsForFeatures;
import cn.edu.sjtu.gdr.ggr.NamePosition;
import edu.stanford.nlp.ling.TaggedWord;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Xie Yanping on 2015/10/25.
 */
public class localContext {
    private static int w = 2;
    public static List<String> extract(LabeledRelationData data) {
        return extractFeature(data.getGeneIndex(), data.getDiseaseIndex(),
                data.getSentenceLabeledData());
    }

    public static List<String> extractFeature(NamePosition gene, NamePosition disease,
                                                                 LabeledData data) {
        List<String> res = new ArrayList<>();
        //int geneRoot = NLPToolsForFeatures.getRootOfWindow(gene, data.getDepTree());
        //int diseaseRoot = NLPToolsForFeatures.getRootOfWindow(disease, data.getDepTree());
        //List<TaggedWord> tagged = data.getTagged();
        List<String> lemma = data.getLemma();
        String geneLemma;
        /*for(int i=geneRoot-2;i<=geneRoot+2;++i){
            if(i == geneRoot)
                continue;
            if(i>=0 && i<lemma.size())
                geneLemma = lemma.get(i);
            else
                geneLemma = "";
            res.add(geneLemma);
        }*/
        int i;
        if((i = gene.getStart()-2) >= 0) {
            geneLemma = lemma.get(i);
            res.add(geneLemma);
        }else
            res.add("");
        if((i = gene.getStart()-1) >= 0) {
            geneLemma = lemma.get(i);
            res.add(geneLemma);
        }else
            res.add("");
        if((i = gene.getEnd()) < lemma.size()) {
            geneLemma = lemma.get(i);
            res.add(geneLemma);
        }else
            res.add("");
        if((i = gene.getEnd()+1) < lemma.size()) {
            geneLemma = lemma.get(i);
            res.add(geneLemma);
        }else
            res.add("");
        res.add("L|");
        String diseaseLemma;
        /*for(int i=diseaseRoot-2; i<=diseaseRoot+2; ++i){
            if(i == diseaseRoot)
                continue;
            if(i>=0 && i<lemma.size())
                diseaseLemma = lemma.get(i);
            else
                diseaseLemma = "";
            res.add(diseaseLemma);
        }*/
        if((i = disease.getStart()-2) >= 0) {
            diseaseLemma = lemma.get(i);
            res.add(diseaseLemma);
        }else
            res.add("");
        if((i = disease.getStart()-1) >= 0) {
            diseaseLemma = lemma.get(i);
            res.add(diseaseLemma);
        }else
            res.add("");
        if((i = disease.getEnd()) < lemma.size()) {
            diseaseLemma = lemma.get(i);
            res.add(diseaseLemma);
        }else
            res.add("");
        if((i = disease.getEnd()+1) < lemma.size()) {
            diseaseLemma = lemma.get(i);
            res.add(diseaseLemma);
        }else
            res.add("");
        res.add("R|");
        return res;
    }

}
