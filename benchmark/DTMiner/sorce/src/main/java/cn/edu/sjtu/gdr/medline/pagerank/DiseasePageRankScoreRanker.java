package cn.edu.sjtu.gdr.medline.pagerank;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BinaryOperator;
import java.util.function.Function;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.medline.pagerank.articletype.ArticleTypeCode;
import cn.edu.sjtu.gdr.medline.pagerank.articletype.ArticleTypeCodeContainer;
import cn.edu.sjtu.gdr.obo.EscapeSynonymNormalizer;
import cn.edu.sjtu.gdr.obo.Normalizer;
import cn.edu.sjtu.gdr.obo.SynonymNormalizer;
import cn.edu.sjtu.gdr.utils.GgrLogger;

public class DiseasePageRankScoreRanker {

	// disease names: disease -> a set of synonym
	private Map<String, Set<String>> diseaseNames;

	// disease -> gene -> a list of uuid
	private Map<String, Map<String, List<String>>> diseaseGeneMap;

	private List<PageRankScore> scores = new ArrayList<PageRankScore>();

	// pmid -> [author id list]
	private Map<Integer, List<Integer>> authorMeta;

	private Normalizer geneNorm;

	private ArticleTypeCodeContainer codes;

	public DiseasePageRankScoreRanker() {
		init();
	}

	private Map<String, List<String>> getGeneUuidMap(Set<String> diseaseNames) {
		return diseaseNames
				.parallelStream()
				.map(new Function<String, Map<String, List<String>>>() {

					/**
					 * For a disease name, return a map: gene -> a list uuid
					 */
					@Override
					public Map<String, List<String>> apply(String disease) {
						disease = disease.toLowerCase();
						Map<String, List<String>> map = null, res = null;
						if ((map = diseaseGeneMap.get(disease)) == null) {
							return new HashMap<String, List<String>>();
						}

						res = new HashMap<String, List<String>>();
						for (String gene : map.keySet()) {
							List<String> uuids = map.get(gene);
							List<String> norms = geneNorm.normalize(gene);
							for (String gg : norms) {
								res.put(gg, uuids);
							}
						}

						return res;
					}
				})
				.reduce(new HashMap<String, List<String>>(),
						new BinaryOperator<Map<String, List<String>>>() {
							/**
							 * reduce all same gene name together. result will
							 * be: [gene name] -> a list uuid
							 */
							@Override
							public Map<String, List<String>> apply(
									Map<String, List<String>> acc,
									Map<String, List<String>> element) {
								if (acc == null || element == null) {
									System.out.println("fuck");
									return new HashMap<String, List<String>>();
								}

								if (acc.size() == 0)
									return element;

								Map<String, List<String>> res = new HashMap<String, List<String>>();
								for (String key : acc.keySet())
									res.put(key,
											new ArrayList<String>(acc.get(key)));

								for (String key : element.keySet()) {
									if (res.containsKey(key)) {
										res.get(key).addAll(element.get(key));
									} else {
										res.put(key, new ArrayList<String>(
												element.get(key)));
									}
								}
								return res;

							}
						});

	}

	//remove duplication from the same abstract
	private Map<String, List<String>> getGeneUuidMap2nd(Set<String> diseaseNames) {
		return diseaseNames
				.parallelStream()
				.map(new Function<String, Map<String, List<String>>>() {

					/**
					 * For a disease name, return a map: gene -> a list uuid
					 */
					@Override
					public Map<String, List<String>> apply(String disease) {
						disease = disease.toLowerCase();
						Map<String, List<String>> map = null, res = null;
						if ((map = diseaseGeneMap.get(disease)) == null) {
							return new HashMap<String, List<String>>();
						}

						res = new HashMap<String, List<String>>();
						for (String gene : map.keySet()) {
							List<String> uuids = map.get(gene);
							List<String> norms = geneNorm.normalize(gene);
							for (String gg : norms) {
								res.put(gg, uuids);
							}
						}

						return res;
					}
				})
				.reduce(new HashMap<String, List<String>>(),
						new BinaryOperator<Map<String, List<String>>>() {
							/**
							 * reduce all same gene name together. result will
							 * be: [gene name] -> a list uuid
							 */
							@Override
							public Map<String, List<String>> apply(
									Map<String, List<String>> acc,
									Map<String, List<String>> element) {
								if (acc == null || element == null) {
									System.out.println("fuck");
									return new HashMap<String, List<String>>();
								}

								if (acc.size() == 0)
									return element;

								Map<String, List<String>> res = new HashMap<String, List<String>>();
								for (String key : acc.keySet())
									res.put(key,
											new ArrayList<String>(acc.get(key)));

								for (String key : element.keySet()) {
									if (res.containsKey(key)) {
										Set<Integer> pmids = new HashSet<Integer>();
										for( String s: res.get(key) ) {
											pmids.add(Integer.parseInt(s.substring(0, s.indexOf('-'))));
										}
										for( String s: element.get(key)) {
											int pmid = Integer.parseInt(s.substring(0, s.indexOf('-')));
											if( !pmids.contains(pmid)) {
												pmids.add(pmid);
												res.get(key).add(s);
											}
										}
									} else {
										res.put(key, new ArrayList<String>(
												element.get(key)));
									}
								}
								return res;

							}
						});

	}
	
	public List<RankedNode1st> rank1st(String disease) {
		disease = disease == null ? "" : disease.toLowerCase();
		Set<String> dnames = null;
		if ((dnames = diseaseNames.get(disease)) == null) {
			System.out.println("no disease found..");
			return new ArrayList<RankedNode1st>();
		}

		Map<String, List<String>> geneUuidMap = getGeneUuidMap(dnames);

		// linear composite all page rank scores
		Map<String, Double> geneScores = new HashMap<String, Double>(
				geneUuidMap.size());
		for (String gene : geneUuidMap.keySet()) {
			geneScores.put(gene, 0.0);
			List<Integer> pmids = uuids2pmids(geneUuidMap.get(gene));
			geneScores.put(gene, pmids2score(pmids));
		}

		// populate result
		List<RankedNode1st> res = new ArrayList<RankedNode1st>(
				geneScores.size());
		for (Map.Entry<String, Double> entry : geneScores.entrySet()) {
			res.add(new RankedNode1st(entry.getKey(), entry.getValue(),
					geneUuidMap.get(entry.getKey())));
		}
		Collections.sort(res);

		return res;
	}

	private static final int CLINICAL_TRIAL_INDEX = 0;
	private static final int VITRO_INDEX = 1;
	private static final int VIVO_INDEX = 2;
	private static final int OTHER_INDEX = 3;

	public List<RankNode2nd> rank2nd(String disease, RankNode2ndWeight weight) {
		disease = disease == null ? "" : disease.toLowerCase();
		Set<String> dnames = null;
		if ((dnames = diseaseNames.get(disease)) == null) {
			System.out.println("no disease found..");
			return new ArrayList<RankNode2nd>();
		}

		Map<String, List<String>> geneUuidMap = getGeneUuidMap(dnames);

		Map<String, List<List<Integer>>> genePmidDistMap = distributePmids(geneUuidMap);

		List<RankNode2nd> result = calculateRank2nd(genePmidDistMap, weight);
		Collections.sort(result);

		return result;
	}

	private List<RankNode2nd> calculateRank2nd(
			Map<String, List<List<Integer>>> genePmidDistMap,
			RankNode2ndWeight weight) {
		List<RankNode2nd> res = new ArrayList<RankNode2nd>();

		for (String gene : genePmidDistMap.keySet()) {
			List<List<Integer>> pmidDist = genePmidDistMap.get(gene);
			RankNode2nd rn2nd = new RankNode2nd(weight);

			rn2nd.name = gene;
			rn2nd.clinicalTrialScore = pmids2score(pmidDist
					.get(CLINICAL_TRIAL_INDEX));
			rn2nd.ctScoreDetail = pmids2scoreDetail(pmidDist
					.get(CLINICAL_TRIAL_INDEX));
			rn2nd.vivoScore = pmids2score(pmidDist.get(VIVO_INDEX));
			rn2nd.vivoDetail = pmids2scoreDetail(pmidDist.get(VIVO_INDEX));
			rn2nd.vitroScore = pmids2score(pmidDist.get(VITRO_INDEX));
			rn2nd.vitroDetail = pmids2scoreDetail(pmidDist.get(VITRO_INDEX));
			rn2nd.otherScore = pmids2score(pmidDist.get(OTHER_INDEX));
			rn2nd.otherDetail = pmids2scoreDetail(pmidDist.get(OTHER_INDEX));

			res.add(rn2nd);
		}

		return res;
	}

	private Map<String, List<List<Integer>>> distributePmids(
			Map<String, List<String>> geneUuidMap) {
		Map<String, List<List<Integer>>> genePmidDistMap = new HashMap<String, List<List<Integer>>>();

		for (Map.Entry<String, List<String>> entry : geneUuidMap.entrySet()) {
			List<List<Integer>> pmidDist = new ArrayList<List<Integer>>(4);
			for (int i = 0; i < 4; ++i)
				pmidDist.add(new ArrayList<Integer>());

			List<Integer> pmids = uuids2pmids(entry.getValue());
			// distribute pmids
			for (Integer pmid : pmids) {
				ArticleTypeCode code = codes.getCode(pmid);
				if (code.isOthers()) {
					pmidDist.get(OTHER_INDEX).add(pmid);
				} else {
					if (code.isClinicalTrial())
						pmidDist.get(CLINICAL_TRIAL_INDEX).add(pmid);
					if (code.isVitro())
						pmidDist.get(VITRO_INDEX).add(pmid);
					if (code.isVivo())
						pmidDist.get(VIVO_INDEX).add(pmid);
				}
			}

			genePmidDistMap.put(entry.getKey(), pmidDist);
		}

		return genePmidDistMap;
	}

	private double pmids2score(List<Integer> pmids) {
		double res = 0;
		for (PageRankScore score : scores) {
			res += score.score(pmids);
		}
		return res;
	}

	private Map<Class<? extends PageRankScore>, Double> pmids2scoreDist(
			List<Integer> pmids) {
		Map<Class<? extends PageRankScore>, Double> map = new HashMap<Class<? extends PageRankScore>, Double>();
		for (PageRankScore score : scores) {
			map.put(score.getClass(), score.score(pmids));
		}

		return map;
	}

	private Map<Integer, Double> pmids2scoreDetail(List<Integer> pmids) {
		Map<Integer, Double> res = new HashMap<Integer, Double>();

		for (Integer pmid : pmids) {
			double ss = 0.0;
			for (PageRankScore scorer : scores) {
				ss += scorer.score(pmid);
			}
			res.put(pmid, ss);
		}

		return res;
	}

	private List<Integer> uuids2pmids(List<String> uuids) {
		List<Integer> res = new ArrayList<Integer>(uuids.size());
		int pmid = 0;
		for (String uuid : uuids) {
			pmid = Integer.parseInt(uuid.substring(0, uuid.indexOf('-')));
			res.add(pmid);
		}
		return res;
	}

	private void init() {
		readDiseaseNames();
		readDiseaseGeneUuidMap();
		addScores();
		geneNorm = EscapeSynonymNormalizer.getGeneNormalizer();
		codes = ArticleTypeCodeContainer.getInstance();
	}

	private void addScores() {
		String paperCitePath = ConfigManager.getConfig("pagerank-results",
				"PAPER_CITE_PAGERANK");
		String authorCitePath = ConfigManager.getConfig("pagerank-results",
				"AUTHOR_CITING_PAGERANK");
		String coauthorPath = ConfigManager.getConfig("pagerank-results",
				"COAUTHOR_PAGERANK");
		String authorMetaPath = ConfigManager.getConfig("author",
				"AUTHOR_META_ID_PATH");
		readAuthorMeta(authorMetaPath);

		if ("true".equals(ConfigManager.getConfig("disease-pagerank-score",
				"PAPER_CITE_SCORE"))) {
			GgrLogger.log("add PaperCiteScore");
			scores.add(new PaperCiteScore(paperCitePath));
		}

		if ("true".equals(ConfigManager.getConfig("disease-pagerank-score",
				"AUTHOR_CITE_SCORE"))) {
			GgrLogger.log("add AuthorCiteScore");
			scores.add(new AuthorScore(authorCitePath, authorMeta));
		}

		if ("true".equals(ConfigManager.getConfig("disease-pagerank-score",
				"COAUTHOR_SCORE"))) {
			GgrLogger.log("add CoAuthorScore");
			scores.add(new AuthorScore(coauthorPath, authorMeta));
		}
	}

	private void readDiseaseNames() {
		GgrLogger.log("start reading disease names");
		this.diseaseNames = new HashMap<String, Set<String>>();
		String path = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG, "obo",
				"DISEASE_SYN");

		try {
			BufferedReader br = new BufferedReader(new FileReader(path));

			Set<String> set = null;
			String str = null, tmp[] = null;

			while ((str = br.readLine()) != null) {
				set = new HashSet<String>();
				tmp = str.split("###");
				for (String s : tmp) {
					set.add(s);
					diseaseNames.put(s, set);
				}
			}

			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		GgrLogger.log("finish reading disease names");
	}

	private void readDiseaseGeneUuidMap() {
		GgrLogger.log("start reading disease gene uuid map...");
		diseaseGeneMap = new HashMap<String, Map<String, List<String>>>();
		String path = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"evidence", "GENE_DISEASE_EVIDENCE_REDUCE");

		try {
			BufferedReader br = new BufferedReader(new FileReader(path));

			List<String> list = null;
			String str = null, tmp[] = null;

			while ((str = br.readLine()) != null) {
				tmp = str.split("\t");
				list = new ArrayList<String>();
				for (int i = 2; i < tmp.length; ++i) {
					list.add(tmp[i]);
				}
				addDiseaseGeneMap(tmp[0], tmp[1], list);
			}

			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		GgrLogger.log("finish reading disease gene uuid map...");
	}

	private void readAuthorMeta(String path) {
		GgrLogger.log("start reading author meta...");
		try {
			authorMeta = new HashMap<Integer, List<Integer>>();

			BufferedReader br = new BufferedReader(new FileReader(path));

			String str = null, authors[];
			List<Integer> authorList = null;

			br.readLine(); // skip empty line
			while ((str = br.readLine()) != null) {
				int pmid = 0;
				try {
					pmid = Integer.parseInt(str.substring("pmid: ".length()));
				} catch (NumberFormatException e) {
					br.readLine(); // skip next line
					continue;
				}

				str = br.readLine();
				if (str.equals(""))
					continue;

				authors = str.split("\t");
				authorList = new ArrayList<Integer>(authors.length);
				for (String authorId : authors)
					authorList.add(Integer.parseInt(authorId));
				authorMeta.put(pmid, authorList);
			}

			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		GgrLogger.log("finish reading author meta..");
	}

	private void addDiseaseGeneMap(String disease, String gene,
			List<String> uuids) {
		disease = disease.toLowerCase();
		gene = gene.toLowerCase();

		Map<String, List<String>> geneMap = null;
		if ((geneMap = diseaseGeneMap.get(disease)) == null) {
			geneMap = new HashMap<String, List<String>>();
			diseaseGeneMap.put(disease, geneMap);
		}

		geneMap.put(gene, uuids);
	}

	public Map<Class<? extends PageRankScore>, List<RankedNode1st>> rank3rd(
			String disease) {
		disease = disease == null ? "" : disease.toLowerCase();
		Set<String> dnames = null;
		if ((dnames = diseaseNames.get(disease)) == null) {
			System.out.println("no disease found..");
			return new HashMap<Class<? extends PageRankScore>, List<RankedNode1st>>();
		}

		Map<String, List<String>> geneUuidMap = getGeneUuidMap(dnames);

		Map<Class<? extends PageRankScore>, List<RankedNode1st>> res = new HashMap<Class<? extends PageRankScore>, List<RankedNode1st>>();
		for (PageRankScore s : scores)
			res.put(s.getClass(), new ArrayList<RankedNode1st>());

		for (String gene : geneUuidMap.keySet()) {
			List<Integer> pmids = uuids2pmids(geneUuidMap.get(gene));
			Map<Class<? extends PageRankScore>, Double> score = pmids2scoreDist(pmids);
			for (Class<? extends PageRankScore> clazz : score.keySet()) {
				res.get(clazz).add(
						new RankedNode1st(gene, score.get(clazz), geneUuidMap
								.get(gene)));
			}
		}
		
		for (Class<? extends PageRankScore> clazz : res.keySet()) {
			Collections.sort(res.get(clazz));
		}

		return res;
	}

	public Map<Class<? extends PageRankScore>, List<RankedNode1st>> rank4th(
			String disease) {
		disease = disease == null ? "" : disease.toLowerCase();
		Set<String> dnames = null;
		if ((dnames = diseaseNames.get(disease)) == null) {
			System.out.println("no disease found..");
			return new HashMap<Class<? extends PageRankScore>, List<RankedNode1st>>();
		}

		Map<String, List<String>> geneUuidMap = getGeneUuidMap2nd(dnames);

		Map<Class<? extends PageRankScore>, List<RankedNode1st>> res = new HashMap<Class<? extends PageRankScore>, List<RankedNode1st>>();
		for (PageRankScore s : scores)
			res.put(s.getClass(), new ArrayList<RankedNode1st>());

		for (String gene : geneUuidMap.keySet()) {
			List<Integer> pmids = uuids2pmids(geneUuidMap.get(gene));
			Map<Class<? extends PageRankScore>, Double> score = pmids2scoreDist(pmids);
			for (Class<? extends PageRankScore> clazz : score.keySet()) {
				res.get(clazz).add(
						new RankedNode1st(gene, score.get(clazz), geneUuidMap
								.get(gene)));
			}
		}
		
		for (Class<? extends PageRankScore> clazz : res.keySet()) {
			Collections.sort(res.get(clazz));
		}

		return res;
	}
}

class RankedNode1st implements Comparable<RankedNode1st> {
	public final String name;
	public final double score;
	public final List<String> uuids;

	public RankedNode1st(String name, double score, List<String> uuids) {
		this.name = name;
		this.score = score;
		this.uuids = uuids;
	}

	@Override
	public int compareTo(RankedNode1st o) {
		if (score < o.score) {
			return -1;
		} else if (score > o.score) {
			return 1;
		} else {
			return 0;
		}
	}
}

class RankNode2ndWeight {
	public double clinicalTrialWeight = 1.0;
	public double vitroWeight = 1.0;
	public double vivoWeight = 1.0;
	public double othersWeight = 1.0;
}

class RankNode2nd implements Comparable<RankNode2nd> {
	public String name;
	public double clinicalTrialScore;
	public Map<Integer, Double> ctScoreDetail;
	public double vitroScore;
	public Map<Integer, Double> vitroDetail;
	public double vivoScore;
	public Map<Integer, Double> vivoDetail;
	public double otherScore;
	public Map<Integer, Double> otherDetail;

	private boolean calc = false;
	private double score = 0;

	private final RankNode2ndWeight weight;

	public RankNode2nd(RankNode2ndWeight weight) {
		this.weight = weight;
	}

	public double score() {
		if (!calc) {
			score = clinicalTrialScore * weight.clinicalTrialWeight
					+ vitroScore * weight.vitroWeight + vivoScore
					* weight.vivoWeight + otherScore * weight.othersWeight;
			calc = true;
		}
		return score;
	}

	@Override
	public int compareTo(RankNode2nd o) {
		double res = score() - o.score();
		if (res < 0)
			return -1;
		else if (res > 0)
			return 1;
		else
			return 0;
	}

}