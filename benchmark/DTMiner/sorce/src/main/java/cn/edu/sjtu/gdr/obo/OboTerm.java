package cn.edu.sjtu.gdr.obo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class OboTerm {
	private String id;
	private String name;
	private Set<String> synonym;
	private List<String> xref;
	private String isA;

	public static final String SPLITTER = "###";

	public OboTerm() {
		synonym = new HashSet<String>();
		xref = new ArrayList<String>();
	}

	public String synString() {
		StringBuilder sb = new StringBuilder();
		// String escape = null;
		sb.append(id);

		if (name != null && !name.equals("")) {
			sb.append(SPLITTER).append(name.toLowerCase());
			// if (!(escape = StringEscapeUtils.escapeXml(name)).equals(name)) {
			// sb.append(SPLITTER).append(escape);
			// }
		}
		for (String str : synonym) {
			if (str != null && !str.equals("")) {
				sb.append(SPLITTER).append(str.toLowerCase());
				// if (!(escape = StringEscapeUtils.escapeXml(str)).equals(str))
				// {
				// sb.append(SPLITTER).append(escape);
				// }
			}
		}
		return sb.toString();
	}

	public void addSynonym(String syn) {
		syn = syn.toLowerCase();
		if (!name.equals(syn)) {
			synonym.add(syn);
		}
	}

	public void addXref(String ref) {
		xref.add(ref);
	}

	public Set<String> getSynonym() {
		return synonym;
	}

	public List<String> getXref() {
		return xref;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name.toLowerCase();
	}

	public String getIsA() {
		return isA;
	}

	public void setIsA(String isA) {
		this.isA = isA;
	}
}
