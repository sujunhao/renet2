/**
 * programs used to extract article type of medline abstract
 */
/**
 * @author Jack Sun(jacksunwei@gmail.com)
 *
 */
package cn.edu.sjtu.gdr.medline.articletype;