package cn.edu.sjtu.gdr.graph;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.utils.GgrLogger;

public class UndirectedGraph2DirectedGraph {

	private String inpath, outpath;

	public UndirectedGraph2DirectedGraph(String inpath, String outpath) {
		this.inpath = inpath;
		this.outpath = outpath;
	}
	
	public void run() {
		try {
			BufferedReader br = new BufferedReader(new FileReader(inpath));
			BufferedWriter bw = new BufferedWriter(new FileWriter(outpath));
			
			String str = null;
			
			while ((str = br.readLine()) != null) {
				bw.write(str); // node: pmid
				bw.newLine();
				bw.newLine(); // in edges
				str = br.readLine();
				bw.write(str);// out edges
				bw.newLine();
			}
			
			br.close();
			bw.flush();
			bw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		String inpath = ConfigManager.getConfig("graph-transform", "inpath");
		String outpath = ConfigManager.getConfig("graph-transform", "outpath");
		
		GgrLogger.log("start transforming..");
		new UndirectedGraph2DirectedGraph(inpath, outpath).run();
		GgrLogger.log("finish transforming... ");
	}

}
