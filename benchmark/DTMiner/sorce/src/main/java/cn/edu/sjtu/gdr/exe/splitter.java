package cn.edu.sjtu.gdr.exe;

import cn.edu.sjtu.gdr.async.netext.NetworkOutputData;
import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.ggr.NameHolder;
import cn.edu.sjtu.gdr.utils.GgrLogger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Xie Yanping on 2015/9/21.
 */
public class splitter {
    private String inpath;
    private String outpath;
    private Integer turn;
    private Integer threshold=4;

    public splitter(String inpath, String outpath) {
        this.inpath = inpath;
        this.outpath = outpath;
        turn = 0;
    }

    public void run() throws IOException {
        GgrLogger.log("start processing...");
        BufferedReader br = new BufferedReader(new FileReader(inpath+"0.txt"));
        BufferedWriter bw1 = new BufferedWriter(new FileWriter(outpath+"1.txt"));
        BufferedWriter bw2 = new BufferedWriter(new FileWriter(outpath+"2.txt"));
        BufferedWriter bw3 = new BufferedWriter(new FileWriter(outpath+"3.txt"));
        BufferedWriter bw4 = new BufferedWriter(new FileWriter(outpath+"4.txt"));

        String line = null, pmid = null, lineParse = null, lineTag = null, pmidTemp = null;
        String strs[];
        int num = 0, paperCount = 0;
        while ((line = br.readLine()).startsWith("pmid: ")) {

            pmid = line.substring(6, line.lastIndexOf(" "));
            NetworkOutputData data = new NetworkOutputData(pmid);
            num = Integer.valueOf(line.substring(line.lastIndexOf(" ") + 1));
            for (int i = 0; i < num; ++i) {
                List<NameHolder> genes = new ArrayList<NameHolder>();
                List<NameHolder> diseases = new ArrayList<NameHolder>();
                String sentence;
                line = br.readLine();
                strs = line.split("\\$\\$\\$");
                for (String s : strs)
                    if (s.length() > 0) {
                        NameHolder nh = new NameHolder();
                        nh.deserialize(s);
                        genes.add(nh);
                    }
                line = br.readLine();
                strs = line.split("$$$");
                for (String s : strs)
                    if (s.length() > 0) {
                        NameHolder nh = new NameHolder();
                        nh.deserialize(s);
                        diseases.add(nh);
                    }
                sentence = br.readLine();
                lineParse = br.readLine();
                lineTag = br.readLine();

                data.addDataParse(genes, diseases, sentence, lineParse, lineTag);
            }


            if(turn == 0) {
                bw1.write(data.serializeParse());
                bw1.newLine();
                turn ++;
            }else if (turn == 1){
                bw2.write(data.serializeParse());
                bw2.newLine();
                turn ++;
            }else if (turn == 2) {
                bw3.write(data.serializeParse());
                bw3.newLine();
                turn ++;
            }else if (turn == 3) {
                bw4.write(data.serializeParse());
                bw4.newLine();
                turn = 0;
            }


            ++paperCount;
            if ((paperCount & 0xfffff) == 0)
                GgrLogger.log((paperCount >> 20) + "M papers scanned...");
        }

        bw1.flush();
        bw1.close();
        bw2.flush();
        bw2.close();
        br.close();
        GgrLogger.log("finish processing...");
    }

    public static void main(String[] args) throws IOException {
        splitter erc = new splitter(ConfigManager.getConfig("exe",
                "GENE_NETWORK_FILE_CLEAN"), ConfigManager.getConfig("exe",
                "GENE_NETWORK_FILE_CLEAN"));
        erc.run();
    }
}
