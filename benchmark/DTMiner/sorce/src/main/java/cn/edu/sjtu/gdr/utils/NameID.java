package cn.edu.sjtu.gdr.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import cn.edu.sjtu.gdr.config.ConfigManager;

/**
 * all names go to lower case
 * 
 * @author Jack Sun(jacksunwei@gmail.com)
 *
 */
public class NameID {
	private static Object mutex = new Object();
	private static NameID geneId = null;

	public static NameID getGeneIdInstance() {
		if (geneId == null) {
			synchronized (mutex) {
				if (geneId == null) {
					geneId = new NameID(ConfigManager.getConfig(
							ConfigManager.MAIN_CONFIG, "exe", "GENE_ID"));
				}
			}
		}
		return geneId;
	}

	private static Object mDisease = new Object();
	private static NameID diseaseId = null;

	public static NameID getDiseaseIdInstance() {
		if (diseaseId == null) {
			synchronized (mDisease) {
				if (diseaseId == null) {
					diseaseId = new NameID(ConfigManager.getConfig(
							ConfigManager.MAIN_CONFIG, "exe", "DISEASE_ID"));
				}
			}
		}
		return diseaseId;
	}

	private Map<String, Integer> idMap;
	private String[] geneList;

	private NameID(String path) {
		idMap = new HashMap<String, Integer>();
		try {
			init(path);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void init(String path) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new FileReader(path));
		String str = null, gene = null;
		int pos = 0, id = 0, total = 0;
		str = br.readLine();
		total = Integer.valueOf(str.substring(7));
		geneList = new String[total + 1];
		while ((str = br.readLine()) != null) {
			pos = str.indexOf('\t');
			id = Integer.valueOf(str.substring(0, pos));
			// name should go for lower case
			gene = str.substring(pos + 1).toLowerCase();
			geneList[id] = gene;
			idMap.put(gene, id);
		}
		br.close();
	}

	/**
	 * get the gene of given id
	 * 
	 * @param id
	 * @return gene name; null if not found
	 */
	public String getName(int id) {
		if (id < 1 || id >= geneList.length) {
			return null;
		}
		return geneList[id];
	}

	/**
	 * get the id (start from 1) of given gene name
	 * 
	 * @param gene
	 * @return id of the gene; 0 if not found
	 */
	public int getId(String gene) {
		Integer i = idMap.get(gene);
		return i == null ? 0 : i;
	}
}
