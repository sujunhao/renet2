package cn.edu.sjtu.gdr.evaluation;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Xie Yanping on 2015/9/29.
 */
public class GroundTruthEvaluate {
    public static String[] diseases = {"lung neoplasms", "kidney neoplasms", "kidney failure, acute",
            "kidney failure, chronic", "cardiovascular system disease"};
    private static Map<String,List<String>> groundTruth = new HashMap<>();
    private static Map<String,List<String>> ranking = new HashMap<>();
    private static String rankingPath = "res/data/prog-output/score.PaperCiteScore";
    public static void readGroundTruth() throws IOException{
        String inpath = "res/groundtruth";
        File[] files = new File(inpath).listFiles();
        for(File file : files){
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line, disease, gene;
            List<String> list = new ArrayList<>();
            int num;
            line = br.readLine();
            disease = line.substring(0, line.lastIndexOf(" "));
            //disease = disease.replaceAll(" ","");
            num = Integer.valueOf(line.substring(line.lastIndexOf(" ") + 1));
            for(int i=0; i<num; i++){
                line = br.readLine();
                gene = line.substring(0,line.lastIndexOf("\t"));
                list.add(gene);
            }
            groundTruth.put(disease,list);
            br.close();
        }
    }
    public static void readRanking(int length, boolean isLength) throws  IOException{
        ranking.clear();
        BufferedReader br = new BufferedReader(new FileReader(rankingPath));
        String line,disease,gene;
        int num;
        while((line=br.readLine())!=null){
            List<String> list = new ArrayList<>();
            disease = line.substring(0,line.lastIndexOf(" "));
            num = Integer.valueOf(line.substring(line.lastIndexOf(" ")+1));
            double value, lastval = 0.0;
            int rank = 0, current = 1;
            for(int i=0; i<num; i++){
                line = br.readLine();
                ++rank;
                gene = line.substring(line.indexOf("\t") + 1, line.lastIndexOf("\t"));
                value = Double.parseDouble(line.substring(line.lastIndexOf("\t") + 1));
                if(isLength) {
                    if (value != lastval)
                        current = rank;
                    lastval = value;
                    //if (current <= length && value != 0.0)
                    if(rank<=length && value != 0.0)
                        list.add(gene);
                }else {
                    if(value>=length)
                        list.add(gene);
                }
            }
            ranking.put(disease, list);
        }
        br.close();
    }
    public static void readBeFree(int length, boolean isLength) throws  IOException{
        ranking.clear();
        String inDir = "res/data/befree";
        File[] files = new File(inDir).listFiles();
        String line,disease,gene;
        for(File file : files){
            int rank = 0, current = 1;
            double value, lastVal = 0.0;
            BufferedReader br = new BufferedReader(new FileReader(file));
            disease = file.getName();
            //disease = disease.replaceAll(" ","");
            List<String> list = new ArrayList<>();
            while((line=br.readLine())!=null){
                ++rank;
                gene = line.substring(0,line.lastIndexOf("\t"));
                value = Double.parseDouble(line.substring(line.lastIndexOf("\t") + 1));
                if(isLength) {
                    if (value != lastVal)
                        current = rank;
                    lastVal = value;
                    //if (current <= length)
                    if(rank<=length)
                        list.add(gene);
                }else{
                    if(value>=length)
                        list.add(gene);
                }
            }
            ranking.put(disease,list);
            br.close();
        }
    }
    public static Map<String,List<String>> evaluate(){
        Map<String,List<String>> res = new HashMap<>();
        int hitCount = 0;
        double fscoreCount = 0.0;
        for(String disease : ranking.keySet()){
            List<String> hits = new ArrayList<>();
            int count = 0;
            List<String> genes = ranking.get(disease);
            List<String> truth = groundTruth.get(disease);
            System.out.println(disease+":");
            for(String gene : genes){
                if(truth.contains(gene)){
                    //System.out.println(gene);
                    hits.add(gene);
                    count++;
                }
            }
            res.put(disease,hits);
            double precision = 1.0 * count / genes.size(),
                    recall = 1.0 * count / truth.size();
            double f1 = 2 * precision * recall / (precision + recall);
            System.out.println(String.format("Precision: %.3f\tRecall: %.3f", precision, recall));
            System.out.println(String.format("Hit: %d\tF-score: %.3f", count, f1));
            hitCount += count;
            fscoreCount += f1;
        }
        System.out.println(String.format("fscore: %.4f hit: %.2f",fscoreCount/5.0,hitCount/5.0));
        return res;
    }
    private static void common() throws IOException{
        Map<String, List<String>> ourList = new HashMap<>();
        Map<String, List<String>> befreeKList = new HashMap<>();
        BufferedReader br1 = new BufferedReader(new FileReader(rankingPath));
        String line,disease,gene;
        int num;
        while((line=br1.readLine())!=null){
            List<String> list = new ArrayList<>();
            disease = line.substring(0,line.lastIndexOf(" "));
            num = Integer.valueOf(line.substring(line.lastIndexOf(" ")+1));
            for(int i=0; i<num; i++){
                line = br1.readLine();
                gene = line.substring(line.indexOf("\t") + 1, line.lastIndexOf("\t"));
                list.add(gene);
            }
            ourList.put(disease, list);
        }
        br1.close();
        String inDir = "res/data/befree";
        File[] files = new File(inDir).listFiles();
        for(File file : files){
            BufferedReader br = new BufferedReader(new FileReader(file));
            disease = file.getName();
            //disease = disease.replaceAll(" ","");
            List<String> list = new ArrayList<>();
            while((line=br.readLine())!=null){
                gene = line.substring(0,line.lastIndexOf("\t"));
                list.add(gene);
            }
            befreeKList.put(disease,list);
            br.close();
        }
        BufferedWriter bw = new BufferedWriter(new FileWriter("res/data/prog-output/truth-befree.txt"));
        for(String dis : befreeKList.keySet()){
            bw.write(dis);
            List<String> tmp1 = befreeKList.get(dis);
            List<String> tmp2 = ourList.get(dis);
            List<String> tmp3 = groundTruth.get(dis);
            List<String> tmp = new ArrayList<>();
            for(String item : tmp2){
                //if(!tmp3.contains(item) && tmp2.contains(item)){
                if(tmp3.contains(item) && !tmp1.contains(item)){
                    tmp.add(item);
                }
            }
            bw.write(" "+tmp.size());
            bw.newLine();
            for(String item : tmp){
                bw.write(item);
                bw.newLine();
            }
        }
        bw.flush();
        bw.close();
    }
    private static void diff(Map<String,List<String>> our, Map<String,List<String>> beFree) throws IOException{
        String inPath = rankingPath;
        String outPath = "res/data/prog-output/diff.txt";
        BufferedReader br = new BufferedReader(new FileReader(inPath));
        String line;
        Map<String, List<String>> freqRank = new HashMap<>();
        while((line = br.readLine())!=null){
            String disease = line.substring(0,line.lastIndexOf(" "));
            //disease = disease.replaceAll(" ","");
            String gene;
            int num = Integer.parseInt(line.substring(line.lastIndexOf(" ")+1));
            List<String> temp = new ArrayList<>();
            for(int i=0; i<num; i++){
                line = br.readLine();
                gene = line.substring(line.indexOf("\t")+1, line.lastIndexOf("\t"));
                temp.add(gene);
            }
            freqRank.put(disease,temp);
        }
        br.close();
        BufferedWriter bw = new BufferedWriter(new FileWriter(outPath));
        for(String disease : diseases){
            //disease = disease.replaceAll(" ","");
            List<String> ourList = our.get(disease);
            List<String> ourFreq = freqRank.get(disease);
            List<String> beFreeList = beFree.get(disease);
            List<String> closeRank = new ArrayList<>();
            List<String> farRank = new ArrayList<>();
            List<String> noRank = new ArrayList<>();
            for(String tmp : beFreeList){
                if(ourList.contains(tmp))
                    continue;
                else if(ourFreq.contains(tmp)){
                    int num = ourFreq.indexOf(tmp);
                    if(num<200) closeRank.add(tmp+" "+Integer.toString(num+1));
                    else farRank.add(tmp+" "+Integer.toString(num+1));
                }else
                    noRank.add(tmp);
            }
            bw.write(disease);
            bw.newLine();
            bw.write("closeRank");
            bw.newLine();
            for(String tmp : closeRank){
                bw.write(tmp);
                bw.newLine();
            }
            bw.write("farRank");
            bw.newLine();
            for(String tmp : farRank){
                bw.write(tmp);
                bw.newLine();
            }
            bw.write("noRank");
            bw.newLine();
            for(String tmp : noRank){
                bw.write(tmp);
                bw.newLine();
            }
            bw.flush();
        }
        bw.close();
    }
    public static void main(String[] args) throws  IOException{
        readGroundTruth();
        //common();
        readRanking(300,true);
        Map<String,List<String>> ourRank = evaluate();
        //readBeFree(25,true);
        //Map<String,List<String>> beFreeRank = evaluate();
        //diff(ourRank,beFreeRank);
    }
}
