package cn.edu.sjtu.gdr.service;

import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import cn.edu.sjtu.gdr.service.dgfreq.DiseaseGeneFreqDetailThread;
import cn.edu.sjtu.gdr.service.dgfreq.DiseaseGeneFreqThread;
import cn.edu.sjtu.gdr.service.generank.GeneRankThread;
import cn.edu.sjtu.gdr.service.thread.CuratedAssoGeneThread;
import cn.edu.sjtu.gdr.service.thread.GeneDetailThread;

public class DispatchHandler {

	private Map<String, ServiceThread> threads;

	public DispatchHandler() {
		threads = new HashMap<String, ServiceThread>();
		addService();
		for (ServiceThread t : threads.values()) {
			t.start();
		}
	}

	public void handle(Socket client, String service, String body) {
		ServiceThread thread = threads.get(service);
		if (thread != null) {
			thread.getBuffer().push(new RequestWrapper(client, body));
			notice(thread);
		} else { // no handler thread, simple close the client socket
			try {
				client.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void addService() {
		threads.put(GeneRankThread.SERVICE_NAME, new GeneRankThread());
		threads.put(GeneDetailThread.SERVICE_NAME, new GeneDetailThread());
		threads.put(DiseaseGeneFreqThread.SERVICE_NAME, new DiseaseGeneFreqThread());
		threads.put(DiseaseGeneFreqDetailThread.SERVICE_NAME, new DiseaseGeneFreqDetailThread());
		threads.put(CuratedAssoGeneThread.SERVICE_NAME, new CuratedAssoGeneThread());
	}

	private void notice(Object o) {
		synchronized (o) {
			o.notify();
		}
	}
}
