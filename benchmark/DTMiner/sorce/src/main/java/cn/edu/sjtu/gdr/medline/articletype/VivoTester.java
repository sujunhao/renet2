package cn.edu.sjtu.gdr.medline.articletype;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.data.utils.NLPToolsForFeatures;
import cn.edu.sjtu.gdr.medline.json.MedlineCitationJson;
import cn.edu.sjtu.gdr.medline.json.MedlineCitationJsonUtils;
import cn.edu.sjtu.gdr.utils.GgrLogger;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.process.Morphology;

/**
 * Vivo evidence tester, not thread-safe
 * 
 * @author Jack Sun(jacksunwei@gmail.com)
 *
 */
public class VivoTester extends ArticleTypeTester {

	private static Set<String> animals;

	@Override
	public Boolean apply(MedlineCitationJson mc) {
		String title = MedlineCitationJsonUtils.findTitle(mc);
		if (sentenceScanner(title))
			return true;
		
		List<String> abstracts = MedlineCitationJsonUtils.findAbstract(mc);
		for (String sen : abstracts) {
			if (sentenceScanner(sen))
				return true;
		}
		
		return false;
	}

	private boolean sentenceScanner(String sentence) {
		DocumentPreprocessor dp = new DocumentPreprocessor(new StringReader(
				sentence));
		List<TaggedWord> tagged = null;
		Set<String> animals = getAnimals();

		for (List<HasWord> s : dp) {
			tagged = NLPToolsForFeatures.tagger().tagSentence(s);
			for (TaggedWord tw : tagged) {
				if (animals.contains(Morphology.stemStatic(tw.word(), tw.tag())
						.word())) {
					return true;
				}
			}
		}
		return false;
	}

	private static Set<String> getAnimals() {
		if (animals == null) {
			synchronized (VivoTester.class) {
				if (animals == null) {
					GgrLogger.log("start loading vivo animals...");
					animals = new HashSet<String>();
					String path = ConfigManager.getConfig("artical-type",
							"VIVO_ANIMAL_PATH");
					BufferedReader br = null;
					try {
						br = new BufferedReader(new FileReader(path));
						String str = null;

						while ((str = br.readLine()) != null) {
							animals.add(str);
						}

						br.close();
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
					GgrLogger.log("finish loading vivo animals...");
				}
			}
		}
		return animals;
	}

	@Override
	public String name() {
		return "Vivo";
	}

}
