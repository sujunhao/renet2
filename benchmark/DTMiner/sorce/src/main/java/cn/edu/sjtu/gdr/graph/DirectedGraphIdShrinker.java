package cn.edu.sjtu.gdr.graph;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.utils.GgrLogger;

public class DirectedGraphIdShrinker {

	private String graphPath, outPath;
	private Set<String> ids;

	public DirectedGraphIdShrinker(String graphPath, Set<String> ids,
			String outPath) {
		this.graphPath = graphPath;
		this.ids = ids;
		this.outPath = outPath;
	}

	public void shrink() {
		try {
			BufferedReader br = new BufferedReader(new FileReader(graphPath));
			BufferedWriter bw = new BufferedWriter(new FileWriter(outPath));

			String str = null, pmid = null, edges[] = null, tmp = null;
			int count = 0, p = 0;

			while ((str = br.readLine()) != null) {
				++count;
				if ((count & 0xfffff) == 0) {
					GgrLogger.log((count >> 20) + "M nodes processed..");
				}

				pmid = str.substring("node: ".length());
				if (!ids.contains(pmid)) {
					br.readLine(); // skip in edges
					br.readLine(); // skip out edges
					continue;
				}
				bw.write(str); // write node: /pmid/
				bw.newLine();

				// skip in edges to save time
				br.readLine();
				bw.newLine();

				// process out edges
				str = br.readLine();
				if (!str.equals("")) {
					edges = str.split("\t");
					for (String edge : edges) {
						p = edge.lastIndexOf(DirectedGraphOutputter.WEIGHT_SPLITTER);
						tmp = edge.substring(0, p);
						if (ids.contains(tmp)) {
							bw.write(edge);
							bw.write("\t");
						}
					}
				}
				bw.newLine();
			}

			br.close();
			bw.flush();
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		GgrLogger.log("finish shrinking graph...");
	}

	private static HashSet<String> readId(String idListPath) {
		HashSet<String> ids = new HashSet<String>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(idListPath));

			String str = null;

			GgrLogger.log("starting loading id...");
			while ((str = br.readLine()) != null) {
				ids.add(str.trim());
			}
			GgrLogger.log("finish loading ids...");

			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ids;
	}

	public static void main(String[] args) {
		String idListPath = ConfigManager.getConfig("cites",
				"PAPER_SHRINK_ID_LIST_PATH");
		Set<String> ids = readId(idListPath);
		String graphPath = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"cites", "PAPER_CITES_GRAPH_PATH");
		String outPath = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"cites", "PAPER_CITES_SHRINK_GRAPH_PATH");
		new DirectedGraphIdShrinker(graphPath, ids, outPath).shrink();
	}

}
