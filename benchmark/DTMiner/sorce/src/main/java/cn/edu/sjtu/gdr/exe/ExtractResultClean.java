package cn.edu.sjtu.gdr.exe;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cn.edu.sjtu.gdr.async.netext.NetworkOutputData;
import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.ggr.NameHolder;
import cn.edu.sjtu.gdr.medline.rank.Score;
import cn.edu.sjtu.gdr.utils.GgrLogger;

/**
 * remove some unexpected terms from the extracting result
 * 
 * @author dong
 *
 */
public class ExtractResultClean {

	private String inpath;
	private String outpath;
    private int turn;

	public ExtractResultClean(String inpath, String outpath) {
		this.inpath = inpath;
		this.outpath = outpath;
        turn = 0;
	}

	public void run() throws IOException {
		GgrLogger.log("start processing...");
		BufferedReader br = new BufferedReader(new FileReader(inpath));
		BufferedWriter bw1 = new BufferedWriter(new FileWriter(outpath+"1.txt"));
        BufferedWriter bw2 = new BufferedWriter(new FileWriter(outpath+"2.txt"));
        BufferedWriter bw3 = new BufferedWriter(new FileWriter(outpath+"3.txt"));
        BufferedWriter bw4 = new BufferedWriter(new FileWriter(outpath+"4.txt"));
		String line = null, pmid = null, lineParse = null, lineTag = null, pmidTemp = null;
		String strs[];
		int num = 0, paperCount = 0;
		while ((line = br.readLine()) != null) {
			pmid = line.substring(6, line.lastIndexOf(" "));
			NetworkOutputData data = new NetworkOutputData(pmid);
			num = Integer.valueOf(line.substring(line.lastIndexOf(" ") + 1));
			for (int i = 0; i < num; ++i) {
				List<NameHolder> genes = new ArrayList<NameHolder>();
				List<NameHolder> diseases = new ArrayList<NameHolder>();
                String sentence;
				line = br.readLine();
				strs = line.split("\\$\\$\\$");
				for (String s : strs)
					if (s.length() > 0) {
						NameHolder nh = new NameHolder();
						nh.deserialize(s);
						if( isValid(nh.name) ) {
							genes.add(nh);
						}
					}
				line = br.readLine();
				strs = line.split("$$$");
				for (String s : strs)
					if (s.length() > 0) {
						NameHolder nh = new NameHolder();
						nh.deserialize(s);
						if( isValid(nh.name) )
							diseases.add(nh);
					}
                sentence = br.readLine();
                lineParse = br.readLine();
                lineTag = br.readLine();

				data.addDataParse(genes, diseases, sentence, lineParse, lineTag);
			}

            //bw1.write(data.serializeParse());
            //bw1.newLine();

            if(turn == 0) {
                bw1.write(data.serializeParse());
                bw1.newLine();
                ++turn;
            }else if(turn == 1){
                bw2.write(data.serializeParse());
                bw2.newLine();
                ++turn;
            }else if(turn == 2){
                bw3.write(data.serializeParse());
                bw3.newLine();
                ++turn;
            }else{
                bw4.write(data.serializeParse());
                bw4.newLine();
                turn=0;
            }


			++paperCount;
			if ((paperCount & 0xfffff) == 0)
				GgrLogger.log((paperCount >> 20) + "M papers scanned...");
		}

		bw1.flush();
		bw1.close();
        bw2.flush();
        bw2.close();
        bw3.flush();
        bw3.close();
        bw4.flush();
        bw4.close();
		br.close();
		GgrLogger.log("finish processing...");
	}

	private boolean isValid(String s) {
		int i = 0;
		for( i = 0; i < s.length(); ++ i) {
			char c = s.charAt(i);
			if( (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') ) break;
		}
		if( i == s.length() ) return false;
		if( s.matches("v\\d+\\.\\d+")) return false;
		if( s.contains("(") && !s.contains(")")) return false;
		if( s.contains(")") && !s.contains("(")) return false;
		if( s.contains("\"") && !s.contains("")) return false;
		if( s.contains(")") && !s.contains("(")) return false;
		int pos = s.indexOf("\"");
		if( pos >= 0 && s.indexOf("\"", pos + 1) < 0) return false;
		return true;
	}
	
	public static void main(String[] args) throws IOException {
		ExtractResultClean erc = new ExtractResultClean(ConfigManager.getConfig("exe",
				"GENE_NETWORK_FILE"), ConfigManager.getConfig("exe",
				"GENE_NETWORK_FILE_CLEAN"));
		erc.run();
		System.out.println(erc.isValid("2.4 +"));
	}
}
