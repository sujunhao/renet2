package cn.edu.sjtu.gdr.obo.name;

import java.util.HashSet;
import java.util.Set;

public class NameEscaper {

	private static int MIN_LEN = 10;
	public static String escape(String name) {
		if( name.length() <= MIN_LEN ) return name;
		StringBuilder sb = new StringBuilder(name.length());
		char ch;
		for (int i = 0; i < name.length(); ++i) {
			ch = name.charAt(i);
			if (isMeaningful(ch)) {
				sb.append(ch);
			} else {
				sb.append(' ');
			}
		}

		return sb.toString();
	}
	
	public static Set<String> escapeSet(Set<String> names) {
		Set<String> res = new HashSet<String>();
		for (String name : names) {
			res.add(escape(name));
		}
		
		return res;
	}
	
	/**
	 * only character [0-9a-zA-Z] is meaningful for name
	 * 
	 * @param ch
	 * @return
	 */
	private static boolean isMeaningful(char ch) {
		return (ch >= '0' && ch <= '9') || (ch >= 'a' && ch <= 'z')
				|| (ch >= 'A' && ch <= 'Z');
	}
}
