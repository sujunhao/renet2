package cn.edu.sjtu.gdr.medline.author.misc;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;

import cn.edu.sjtu.gdr.utils.GgrLogger;

public class AuthorIdSelector {

	private String path;
	private Predicate<Integer> pred;
	private Set<String> authorIdSet = new HashSet<String>(10000000);

	public AuthorIdSelector(String path, Predicate<Integer> pred) {
		this.path = path;
		this.pred = pred;
		readAll();
	}

	public Set<String> getSelectedSet() {
		return authorIdSet;
	}

	private void readAll() {
		GgrLogger.log("start filter author id...");
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			String str = null, pair[] = null;

			while ((str = br.readLine()) != null) {
				pair = str.split("\t");
				if (pred.test(Integer.parseInt(pair[1]))) {
					authorIdSet.add(pair[0]);
				}
			}

			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		GgrLogger.log("finish filter author id, " + authorIdSet.size()
				+ " author ids left...");

	}
}
