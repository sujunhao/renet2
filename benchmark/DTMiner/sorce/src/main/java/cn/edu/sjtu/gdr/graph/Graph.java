package cn.edu.sjtu.gdr.graph;

public abstract class Graph<T extends GraphNode> implements Iterable<T> {

	public abstract void addNode(T gn);

	public abstract T getNode(int id);

	public abstract T ensureAndGetNode(int id);

	public void addEdge(int fromId, int toId, int weight) {
		addEdge(fromId, toId, weight, true);
	}

	public void addEdge(int fromId, int toId, int weight, boolean addInEdge) {
		T fromNode = ensureAndGetNode(fromId);
		T toNode = ensureAndGetNode(toId);
		fromNode.addOutEdge(toId, weight);
		if (addInEdge) {
			toNode.addInEdge(fromId, weight);
		}
	}

	public void addEdge(int fromId, int toId) {
		addEdge(fromId, toId, 1);
	}

	public abstract int size();
}
