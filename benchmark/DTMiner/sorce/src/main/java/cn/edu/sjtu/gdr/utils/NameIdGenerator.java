package cn.edu.sjtu.gdr.utils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class NameIdGenerator {

	private int curId;
	private Map<String, Integer> idMap;

	public NameIdGenerator(int startId) {
		curId = startId;
		idMap = new HashMap<String, Integer>();
	}

	public NameIdGenerator() {
		this(0);
	}

	public int getId(String name) {
		if (!idMap.containsKey(name)) {
			synchronized (this) {
				if (!idMap.containsKey(name)) {
					idMap.put(name, curId++);
				}
			}
		}
		return idMap.get(name);
	}

	public void output(String filename) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(filename));

		@SuppressWarnings("unchecked")
		Map.Entry<String, Integer>[] entries = new Map.Entry[0];
		entries = idMap.entrySet().toArray(entries);
		Arrays.sort(entries, new Comparator<Map.Entry<String, Integer>>() {
			@Override
			public int compare(Entry<String, Integer> o1,
					Entry<String, Integer> o2) {
				return o1.getValue() - o2.getValue();
			}
		});

		for (Map.Entry<String, Integer> entry : entries) {
			bw.write(entry.getValue() + "\t" + entry.getKey());
			bw.newLine();
		}

		bw.flush();
		bw.close();
	}
}
