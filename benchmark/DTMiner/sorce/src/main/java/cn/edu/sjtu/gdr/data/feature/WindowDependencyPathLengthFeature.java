package cn.edu.sjtu.gdr.data.feature;

import java.util.ArrayList;
import java.util.List;

import cn.edu.sjtu.gdr.data.label.LabeledData;
import cn.edu.sjtu.gdr.data.label.LabeledRelationData;
import cn.edu.sjtu.gdr.data.utils.NLPToolsForFeatures;
import cn.edu.sjtu.gdr.ggr.NamePosition;
import edu.stanford.nlp.ling.TaggedWord;

/**
 * Gene和Disease之间Dependency Path的长度
 * 
 * @author Jack Sun(jacksunwei@gmail.com)
 *
 */
public class WindowDependencyPathLengthFeature {

	public static double extractRelationGeneFeature(LabeledRelationData data) {
		NamePosition gene = data.getGeneIndex();
		NamePosition disease = data.getDiseaseIndex();
		LabeledData sentenceData = data.getSentenceLabeledData();
		int geneRoot = NLPToolsForFeatures.getRootOfWindow(gene,
				sentenceData.getDepTree());
		int diseaseRoot = NLPToolsForFeatures.getRootOfWindow(disease,
				sentenceData.getDepTree());
		return extractFeature(geneRoot, diseaseRoot, sentenceData);
	}

	public static double extractFeature(int fromIndex, int toIndex,
			LabeledData data) {
		List<TaggedWord> res = new ArrayList<>();
                //NLPToolsForFeatures.getDependencyPath(fromIndex, toIndex, data.getTagged(), data.getDepTree());
		return res.size();
	}
}
