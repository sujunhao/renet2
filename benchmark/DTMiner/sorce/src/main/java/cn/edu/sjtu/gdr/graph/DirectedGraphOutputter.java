package cn.edu.sjtu.gdr.graph;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class DirectedGraphOutputter {

	public static final String nodePrefix = "node: ";
	public static final String WEIGHT_SPLITTER = "$", EDGE_SPLITTER = "\t";

	private DirectedGraph graph;
	private File file;

	public DirectedGraphOutputter(DirectedGraph graph, File file) {
		this.graph = graph;
		this.file = file;
	}

	public DirectedGraphOutputter(DirectedGraph graph, String path) {
		this(graph, new File(path));
	}

	public DirectedGraphOutputter(DirectedGraph graph, String parent,
			String name) {
		this(graph, new File(parent, name));
	}

	public void output() {
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(file));

			for (DirectedGraphNode node : graph) {
				bw.write(nodePrefix + node.getId());
				bw.newLine();
				
				// output in edges
				for (Integer inEdgeId : node.getInEdges()) {
					bw.write(inEdgeId + WEIGHT_SPLITTER
							+ node.getInEdgeWeight(inEdgeId) + EDGE_SPLITTER);
				}
				bw.newLine();

				// output out edges
				for (Integer outEdgeId : node.getOutEdges()) {
					bw.write(outEdgeId + WEIGHT_SPLITTER
							+ node.getOutEdgeWeight(outEdgeId) + EDGE_SPLITTER);
				}
				bw.newLine();
			}

			bw.flush();
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
