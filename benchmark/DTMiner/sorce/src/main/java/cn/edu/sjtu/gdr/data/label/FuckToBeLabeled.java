package cn.edu.sjtu.gdr.data.label;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FuckToBeLabeled {

	public static void main(String[] args) throws IOException {
		String inpath = "res/experiment/labeled-more.txt";
		String outpath = "res/experiment/labeled-more-exp.txt";

		String gene = "gene", disease = "disease", relation = "relation";

		BufferedReader br = new BufferedReader(new FileReader(inpath));
		BufferedWriter bw = new BufferedWriter(new FileWriter(outpath));

		String str = null, last = "";
		int count = 0;

		while ((str = br.readLine()) != null) {
//			str = str.trim();
			if (str.startsWith(gene)) {
				count = last == gene ? count : 0;
				last = gene;
				bw.write(count++ + " " + str);
				bw.newLine();
			} else if (str.startsWith(disease)) {
				count = last == disease ? count : 0;
				last = disease;
				bw.write(count++ + " " + str);
				bw.newLine();
			} else if (str.startsWith(relation)) {
				count = last == relation ? count : 0;
				last = relation;
				bw.write(count++ + " " + str);
				bw.newLine();
			} else {
				count = 0;
				bw.write(str);
				bw.newLine();
			}
		}

		bw.flush();
		bw.close();
		br.close();
	}

}
