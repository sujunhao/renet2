package cn.edu.sjtu.gdr.data.feature;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RelationFeatureSubSet {

	private String inpath, outpath;
	private int[] features;

	public RelationFeatureSubSet(String inpath, int[] features) {
		this.inpath = inpath;
		this.outpath = this.inpath + ".sub";
		this.features = features;
	}

	public void run() {
		try {
			BufferedReader br = new BufferedReader(new FileReader(inpath));
			BufferedWriter bw = new BufferedWriter(new FileWriter(outpath));

			String str = null;
			SvmData data = null;

			while ((str = br.readLine()) != null) {
				data = SvmData.parse(str);
				bw.write(data.subData(features).toString());
				bw.newLine();
			}

			br.close();
			bw.flush();
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws IOException {
		String inpath = "res/experiment/svmdata";
		// int[] features = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
		// 13,
		// 14, 15, 16 };
		int[] features = new int[] { 1, 2, 5, 6, 7, 8, 9, 10, 11, 13, 14, 17, 18};
		new RelationFeatureSubSet(inpath, features).run();
		new TrainTestGenerate("res/experiment/svmdata.sub").run();
	}

}

class SvmData {
	int tag;
	List<Double> features;

	public SvmData() {
		this(0, new ArrayList<Double>());
	}

	public SvmData(int tag, List<Double> features) {
		this.tag = tag;
		this.features = features;
	}

	public SvmData subData(int[] selected) {
		List<Double> list = new ArrayList<Double>();

		for (Integer id : selected) {
			list.add(features.get(id - 1));
		}

		return new SvmData(tag, list);
	}

	public static SvmData parse(String point) {
		String strs[] = point.split(" "), t[] = null;
		int tag = Integer.parseInt(strs[0]);
		List<Double> list = new ArrayList<Double>();
		for (int i = 1; i < strs.length; ++i) {
			t = strs[i].split(":");
			list.add(Double.parseDouble(t[1]));
		}

		return new SvmData(tag, list);
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(tag);
		for (int i = 0; i < features.size(); ++i) {
			sb.append(" ").append(i + 1).append(":")
					.append(Double.toString(features.get(i)));
		}
		return sb.toString();
	}
}