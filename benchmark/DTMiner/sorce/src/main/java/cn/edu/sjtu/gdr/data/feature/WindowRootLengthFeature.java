package cn.edu.sjtu.gdr.data.feature;

import cn.edu.sjtu.gdr.data.label.LabeledData;
import cn.edu.sjtu.gdr.data.label.LabeledRelationData;
import cn.edu.sjtu.gdr.data.utils.NLPToolsForFeatures;
import cn.edu.sjtu.gdr.ggr.NamePosition;

/**
 * Feature 15-16:
 * <p>
 * Gene到root的距离 , Disease到root的距离
 * 
 * @author Jack Sun(jacksunwei@gmail.com)
 *
 */
public class WindowRootLengthFeature {

	public static double extractRelationGeneFeature(LabeledRelationData data) {
		return extractFeature(data.getGeneIndex(),
				data.getSentenceLabeledData());
	}

	public static double extractRelationDiseaseFeature(LabeledRelationData data) {
		return extractFeature(data.getDiseaseIndex(),
				data.getSentenceLabeledData());
	}

	public static double extractFeature(NamePosition np, LabeledData data) {
		int rootIndex = NLPToolsForFeatures.getRootOfWindow(np,
				data.getDepTree());
		return data.getDepTree().getNode(rootIndex + 1).getPathFromRoot()
				.size() - 1;
	}
}
