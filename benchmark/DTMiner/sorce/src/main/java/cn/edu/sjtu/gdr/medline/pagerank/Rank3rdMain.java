package cn.edu.sjtu.gdr.medline.pagerank;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;

public class Rank3rdMain {

	private static boolean dump = false;

	public static void main(String[] args) throws IOException {
		if (args.length >= 1 && args[0].equals("dump")) {
			dump = true;
		}

		String disease = null;
		DiseasePageRankScoreRanker worker = new DiseasePageRankScoreRanker();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Map<Class<? extends PageRankScore>, List<RankedNode1st>> res = null;

		while (true) {
			System.out.print("Enter Disease: ");
			disease = br.readLine().trim();

			res = worker.rank3rd(disease);
			for (Class<? extends PageRankScore> clazz : res.keySet()) {
				System.out.println(clazz.getSimpleName());
				List<RankedNode1st> list = res.get(clazz);
				for (RankedNode1st rn : list) {
					System.out.format("%s\t%s\n", rn.name, rn.score);
				}
			}

			if (dump) {
				for (Class<? extends PageRankScore> clazz : res.keySet()) {
					dump(disease, clazz.getSimpleName(), res.get(clazz));
				}
			}
		}
	}

	private static void dump(String disease, String tag,
			List<RankedNode1st> list) throws IOException {
		String path = String.format("rank3rd.%s.%s.%d.dump", disease, tag,
				System.currentTimeMillis());
		BufferedWriter bw = new BufferedWriter(new FileWriter(path));

		for (int i = list.size() - 1, cnt = 0; i >= 0; --i) {
			RankedNode1st rn = list.get(i);
			bw.write(String.format("%d\t%s\t%s", ++cnt, rn.name, rn.score));
			bw.newLine();
		}

		bw.flush();
		bw.close();
	}

}