package cn.edu.sjtu.gdr.obo;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class OboTermParser implements Iterable<OboTerm> {
	private String path;
	private List<OboTerm> terms;
	
	public OboTermParser(String path) {
		this.path = path;
		terms = new ArrayList<OboTerm>();
		readAll();
	}
	
	private void readAll() {
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			String str = null;
			OboTerm term = null;
			int pos = 0;
			
			while ((str = br.readLine()) != null) {
				if (str.equals("[Term]")) {
					if (term != null) {
						terms.add(term);
					}
					term = new OboTerm();
				} else {
					if ((pos = str.indexOf(":")) == -1) {
						continue;
					}
					String content = str.substring(0, pos);
					if (content.equals("id")){
						handleId(str, term);
					} else if (content.equals("name")) {
						handleName(str, term);
					} else if (content.equals("synonym")) {
						handleSynonym(str, term);
					}
				}
			}
			if (term != null) {
				terms.add(term);
			}
			
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void handleId(String line, OboTerm term) {
		term.setId(new String(line.substring(4)));
	}
	
	private void handleName(String line, OboTerm term) {
		term.setName(new String(line.substring(6)));
	}
	
	private void handleSynonym(String line, OboTerm term) {
		term.addSynonym(new String(line.substring(10, line.indexOf("\" EXACT "))));
	}
	
	@Override
	public Iterator<OboTerm> iterator() {
		return terms.iterator();
	}
}
