package cn.edu.sjtu.gdr.data.feature;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cn.edu.sjtu.gdr.data.label.LabeledData;
import cn.edu.sjtu.gdr.data.label.LabeledRelationData;
import cn.edu.sjtu.gdr.ggr.NamePosition;
import cn.edu.sjtu.gdr.utils.GgrLogger;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.process.Morphology;

/**
 * Disease Gene Window 之间的word bag的feature，检查中间时候有某个词
 * 
 * @author Jack Sun(jacksunwei@gmail.com)
 *
 */
public class WindowBetweenWordsFeature {

	public static final double CONTAIN_WORD = 1.0;
	public static final double NOT_CONTAIN_WORD = 0.0;

	private static final String bagPath = "res/feature/feature-path.txt";
	static Set<String> wordsBag = null;
	private static Morphology morph = new Morphology();

	public static double extractRelationFeature(LabeledRelationData data) {
		return extractFeature(data.getGeneIndex(), data.getDiseaseIndex(),
				data.getSentenceLabeledData());
	}

	public static double extractFeature(NamePosition np1, NamePosition np2,
			LabeledData data) {
		if (wordsBag == null)
			init();

		if (np1.getStart() > np2.getStart()) {
			NamePosition temp = np1;
			np1 = np2;
			np2 = temp;
		}

		List<TaggedWord> tagged = data.getTagged();
		for (int i = np1.getEnd(); i < np2.getStart(); ++i) {
			TaggedWord tw = tagged.get(i);
			String str = morph.lemma(tw.word(), tw.tag());
			if (wordsBag.contains(str)) {
				return CONTAIN_WORD;//contain word in wordbag between gene and disease
			}
		}

		return NOT_CONTAIN_WORD;
	}

	public static void init() {
		wordsBag = new HashSet<String>();
		GgrLogger.log("Loading path word bag...");
		try {
			BufferedReader br = new BufferedReader(new FileReader(bagPath));

			String str = null;
			while ((str = br.readLine()) != null) {
				wordsBag.add(str);
			}

			br.close();
			GgrLogger.log("Finish loading...");
			return;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		GgrLogger.warn("Error in loading...");
	}
}
