package cn.edu.sjtu.gdr.exe;

import cn.edu.sjtu.gdr.config.ConfigManager;
import edu.stanford.nlp.process.Morphology;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.zip.GZIPInputStream;

/**
 * Created by Xie Yanping on 2015/12/16.
 */
public class Lemmatize {
    private static Morphology morph = new Morphology();
    private static List<String> getLemma(String taggedIn) {
        List<String> lemmaList = new ArrayList<>();
        String taggedStr = taggedIn.substring(1, taggedIn.length() - 1);
        String tags[] = taggedStr.split(", ");
        for (int i = 0; i < tags.length; i++) {
            String tag = tags[i].substring(tags[i].lastIndexOf("/") + 1);
            String word = tags[i].substring(0, tags[i].lastIndexOf("/"));
            String lemma = morph.lemma(word,tag);
            lemmaList.add(lemma);
        }
        return lemmaList;
    }
    private static void run(String dirNum) throws IOException{
        String dirPar = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
                "exe", "MEDLINE_ABSTRACT_PAR_COMBINE")+dirNum;
        File dirFilePar = new File(dirPar);
        File[] filePar = dirFilePar.listFiles(new FileFilter() {
            public boolean accept(File pathname) {
                return pathname.getName().endsWith(".gz");
            }
        });
        Arrays.sort(filePar, new Comparator<File>() {
            public int compare(File o1, File o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        for(int i=0; i<filePar.length; ++i){
            processFile(filePar[i]);
        }
    }
    private static void processFile(File f) throws IOException{
        String outPath = "data/medline/abst-lemma-combine/"+f.getName().substring(0,14)+".txt";
        BufferedReader br = new BufferedReader(new InputStreamReader(new GZIPInputStream(
                new FileInputStream(f))));
        BufferedWriter bw = new BufferedWriter(new FileWriter(outPath));
        String line, tag;
        int num;
        while ((line = br.readLine())!=null){
            bw.write(line);
            bw.newLine();
            num = Integer.parseInt(line.substring(line.lastIndexOf(" ")+1));
            for(int i=0; i<num; ++i){
                bw.write(br.readLine());
                bw.newLine();
                tag = br.readLine();
                br.readLine();
                br.readLine();
                List<String> lemmaList = getLemma(tag);
                bw.write(lemmaList.toString());
                bw.newLine();
            }
            bw.flush();
        }
        br.close();
        bw.close();
    }
    public static void main(String[] args) throws IOException{
        run(args[0]);
    }
}
