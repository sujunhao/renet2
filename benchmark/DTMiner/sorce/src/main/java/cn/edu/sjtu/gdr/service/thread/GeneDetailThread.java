package cn.edu.sjtu.gdr.service.thread;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

import cn.edu.sjtu.gdr.obo.OboTerm;
import cn.edu.sjtu.gdr.obo.OboTermMap;
import cn.edu.sjtu.gdr.service.RequestWrapper;
import cn.edu.sjtu.gdr.service.ServiceThread;

public class GeneDetailThread extends ServiceThread {
	public static final String SERVICE_NAME = "geneDetail";

	public GeneDetailThread() {
		super();
	}

	public void run() {
		while (true) {
			try {
				RequestWrapper request = buffer.pop();
				if (request == null) {
					synchronized (this) {
						wait();
					}
				} else {
					BufferedWriter bw = new BufferedWriter(
							new OutputStreamWriter(
									request.client.getOutputStream()));
					
					OboTermMap geneMap = OboTermMap.getGeneMap();
					OboTerm term = geneMap.getTerm(request.body);
					if (term != null) {
						bw.write("found");
						bw.newLine();
						
						bw.write("id: ");
						bw.write(term.getId());
						bw.newLine();
						
						bw.write("name: ");
						bw.write(term.getName());
						bw.newLine();
						
						for (String syn : term.getSynonym()) {
							bw.write("synonym: ");
							bw.write(syn);
							bw.newLine();
						}
					} else {
						bw.write("not found");
						bw.newLine();
					}
					
					bw.flush();
					bw.close();
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
