package cn.edu.sjtu.gdr.medline.rank;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import bottomLevel.function.DirectedGraphReaderForPageRank;
import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.utils.GgrLogger;

public class CoAuthorSuppress extends CoAuthorScore {

	private Map<Integer, Map<Integer, Integer>> graph;
	
	public CoAuthorSuppress() {
		super();
		String path = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"graph", "COAUTHOR_GRAPH");
		GgrLogger.log("start reading graph...");
		graph = new DirectedGraphReaderForPageRank(
				path).read();
		GgrLogger.log("finish reading graph...");
	}
	
	public CoAuthorSuppress(Map<Integer, List<Integer>> authorMeta, Map<Integer, Double> pr) {
		super(authorMeta, pr);
		String path = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"graph", "COAUTHOR_GRAPH");
		GgrLogger.log("start reading graph from: " + path);
		graph = new DirectedGraphReaderForPageRank(
				path).read();
		GgrLogger.log("finish reading graph...");
	}
	
	public double score(Set<Integer> pmids) {
		double score = 0;
		Set<Integer> authors = new HashSet<Integer>();
 		for( Integer pmid: pmids ) {
 			authors.addAll(authorMeta.getOrDefault(pmid, new ArrayList<Integer>()));
		}
 		
 		for( Integer x: authors)
 			for( Integer y: authors) {
 				int co_num = graph.getOrDefault(x, new HashMap<Integer, Integer>())
 						.getOrDefault(y, 0);
 				if( x == y ) {
 					score += getPageRankScore(x);
 				}
 				else {
 					score += (getPageRankScore(x) + getPageRankScore(y)) / 
 							(2 * sigmoid(co_num));
 				}
 			}
		return score / authors.size();
	}
	
	private static double sigmoid(int x) {
		return 1.0 / (1 + Math.exp(-x));
	}
}
