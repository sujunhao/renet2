package cn.edu.sjtu.gdr.utils;

/**
 * An object that can be serialized to String for output.
 * 
 * @author Jack Sun(jacksunwei@gmail.com)
 *
 */
public interface IoObject {
	public String serialize();
}
