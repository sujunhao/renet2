package cn.edu.sjtu.gdr.medline.pagerank;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.BinaryOperator;
import java.util.function.Function;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.obo.EscapeSynonymNormalizer;
import cn.edu.sjtu.gdr.obo.Normalizer;
import cn.edu.sjtu.gdr.obo.SynonymNormalizer;
import cn.edu.sjtu.gdr.utils.GgrLogger;

public class PaperCitePageRank {

	// pagerank of paper: pmid -> pr
	private Map<Integer, Double> pagerank;

	// disease names: disease -> a set of synonym
	private Map<String, Set<String>> diseaseNames;

	// disease -> gene -> a list of uuid
	private Map<String, Map<String, List<String>>> diseaseGeneMap;

	private Normalizer geneNorm;

	public PaperCitePageRank() {
		init();
	}

	private void init() {
		geneNorm = EscapeSynonymNormalizer.getGeneNormalizer();
		readPagerank();
		readDiseaseNames();
		readDiseaseGeneUuidMap();
	}

	public Map<String, Double> pageRank(String disease) {
		Map<String, Double> res = new HashMap<String, Double>();

		disease = disease == null ? "" : disease.toLowerCase();
		Set<String> dnames = null; 
		if ((dnames = diseaseNames.get(disease)) == null) {
			System.out.println("no disease found..");
			return res;
		}
		
		Map<String, List<String>> geneUuidMap = null;
		geneUuidMap = dnames
				.stream()
				.map(new Function<String, Map<String, List<String>>>() {

					/**
					 * For a disease name, return a map: gene -> a list uuid
					 */
					@Override
					public Map<String, List<String>> apply(String disease) {
						disease = disease.toLowerCase();
						Map<String, List<String>> res = null;
						if ((res = diseaseGeneMap.get(disease)) == null) {
							res = new HashMap<String, List<String>>();
						}
						return res;
					}
				})
				.reduce(new HashMap<String, List<String>>(),
						new BinaryOperator<Map<String, List<String>>>() {
							/**
							 * reduce all same gene name together. result will
							 * be: [gene name] -> a list uuid
							 */
							@Override
							public Map<String, List<String>> apply(
									Map<String, List<String>> acc,
									Map<String, List<String>> element) {
								if (acc == null || element == null) {
									System.out.println("fuck");
								}
								for (Map.Entry<String, List<String>> entry : element
										.entrySet()) {
									List<String> norm = geneNorm.normalize(entry.getKey());
									for (String gene : norm) {
										if (acc.containsKey(gene)) {
											acc.get(gene).addAll(
													entry.getValue());
										} else {
											acc.put(gene, entry.getValue());
										}
									}
								}
								return acc;
							}
						});

		for (Map.Entry<String, List<String>> entry : geneUuidMap.entrySet()) {
			res.put(entry.getKey(), uuids2Double(entry.getValue()));
		}

		return res;
	}

	private double uuids2Double(List<String> uuids) {
		double res = 0;
		int pmid = 0;
		for (String uuid : uuids) {
			pmid = Integer.parseInt(uuid.substring(0, uuid.indexOf('-')));
			res += pagerank.getOrDefault(pmid, 0.0);
		}
		return res;
	}

	private void readPagerank() {
		GgrLogger.log("start reading pagerank...");
		this.pagerank = new HashMap<Integer, Double>();
		String path = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"cites", "PAPER_CITES_PAGERANK");

		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			int pmid = 0;
			double pr = 0.0;
			String str = null, pair[] = null;

			while ((str = br.readLine()) != null) {
				pair = str.split("\t");
				pmid = Integer.parseInt(pair[0]);
				pr = Double.parseDouble(pair[1]);
				this.pagerank.put(pmid, pr);
			}

			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		GgrLogger.log("finish reading pagerank...");
	}

	private void readDiseaseNames() {
		GgrLogger.log("start reading disease names");
		this.diseaseNames = new HashMap<String, Set<String>>();
		String path = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG, "obo",
				"DISEASE_SYN");

		try {
			BufferedReader br = new BufferedReader(new FileReader(path));

			Set<String> set = null;
			String str = null, tmp[] = null;

			while ((str = br.readLine()) != null) {
				set = new HashSet<String>();
				tmp = str.split("###");
				for (String s : tmp) {
					set.add(s);
					diseaseNames.put(s, set);
				}
			}

			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		GgrLogger.log("finish reading disease names");
	}

	private void readDiseaseGeneUuidMap() {
		GgrLogger.log("start reading disease gene uuid map...");
		diseaseGeneMap = new HashMap<String, Map<String, List<String>>>();
		String path = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"evidence", "GENE_DISEASE_EVIDENCE_REDUCE");

		try {
			BufferedReader br = new BufferedReader(new FileReader(path));

			List<String> list = null;
			String str = null, tmp[] = null;

			while ((str = br.readLine()) != null) {
				tmp = str.split("\t");
				list = new ArrayList<String>();
				for (int i = 2; i < tmp.length; ++i) {
					list.add(tmp[i]);
				}
				addDiseaseGeneMap(tmp[0], tmp[1], list);
			}

			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		GgrLogger.log("finish reading disease gene uuid map...");
	}

	private void addDiseaseGeneMap(String disease, String gene,
			List<String> uuids) {
		disease = disease.toLowerCase();
		gene = gene.toLowerCase();
		
		Map<String, List<String>> geneMap = null;
		if ((geneMap = diseaseGeneMap.get(disease)) == null) {
			geneMap = new HashMap<String, List<String>>();
			diseaseGeneMap.put(disease, geneMap);
		}

		geneMap.put(gene, uuids);
	}

	@SuppressWarnings("unchecked")
	public static void main(String[] argv) throws IOException {
		String disease = null;
		PaperCitePageRank worker = new PaperCitePageRank();
		Map<String, Double> res = null;
		Map.Entry<String, Double> entries[] = new Map.Entry[0];

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		while (true) {
			System.out.print("Enter Disease: ");
			disease = br.readLine().trim();
			res = worker.pageRank(disease);
			entries = new Map.Entry[0];
			entries = res.entrySet().toArray(entries);
			Arrays.sort(entries, new Comparator<Map.Entry<String, Double>>() {
				@Override
				public int compare(Entry<String, Double> o1,
						Entry<String, Double> o2) {
					double res = o1.getValue() - o2.getValue();
					if (res < 0) {
						return -1;
					} else if (res > 0) {
						return 1;
					} else {
						return 0;
					}
				}
			});
			
			for (Map.Entry<String, Double> entry : entries) {
				System.out.println(entry.getKey() + "\t" + entry.getValue());
			}
		}
	}
}
