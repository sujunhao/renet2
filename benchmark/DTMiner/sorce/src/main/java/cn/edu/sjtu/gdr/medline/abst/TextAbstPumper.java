package cn.edu.sjtu.gdr.medline.abst;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.apache.commons.lang3.StringEscapeUtils;

import rdi.cn.source.MedlineItem;

public class TextAbstPumper implements Iterator<MedlineItem> {

	private String fileName;
	private BufferedReader br;
	private List<MedlineItem> list;
	private Iterator<MedlineItem> inner;
	
	public TextAbstPumper(File file) throws FileNotFoundException, IOException {
		br = new BufferedReader(new InputStreamReader(new GZIPInputStream(
				new FileInputStream(file))));
		fileName = file.getName();
		readAll();
	}

	public TextAbstPumper(String path) throws FileNotFoundException, IOException {
		this(new File(path));
	}
	
	public String fileName() {
		return fileName;
	}

	public boolean hasNext() {
		return inner.hasNext();
	}

	public MedlineItem next() {
		return inner.next();
	}

	private void readAll() throws IOException {
		list = new ArrayList<MedlineItem>();
		String str = null, pmid = null, abst = null;
		MedlineItem item = null;
		while ((str = br.readLine()) != null) {
			pmid = str;
			// unescape from xml
			abst = StringEscapeUtils.unescapeXml(br.readLine().trim());
			if (abst != null && !abst.trim().equals("")) {
				item = new MedlineItem();
				item.setPmid(pmid);
				item.setText(abst);
				list.add(item);
			}
		}
		inner = list.iterator();
	}
}
