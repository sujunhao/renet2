package cn.edu.sjtu.gdr.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import rdi.cn.misc.XMLUtil;
import rdi.cn.source.MedlineItem;

public class MedlineParser implements Iterator<MedlineItem> {

	private static final String TITLE_XPATH = "Article/ArticleTitle/text()";
	private static final String TEXT_XPATH_DETAIL = "Article/Abstract/AbstractText";
	private static final String TEXT_XPATH = "Article/Abstract/text()";
	// private static final String AUT_XPATH = "Article/AuthorList/Author";
	// private static final String JOURNAL_XPATH = "Article/Journal";

	private static final String BACKGROUND = "BACKGROUND";
	private static final String METHODS = "METHODS";
	private static final String RESULTS = "RESULTS";
	private static final String CONCLUSIONS = "CONCLUSIONS";
	private static final String OBJECTIVE = "OBJECTIVE";

	private static final String CITATION_XPATH = "/MedlineCitationSet/MedlineCitation";

	private static final String CATEGORY_ATTR = "NlmCategory";

	private static XPath xPath;
	private static XPathExpression titleXPath;
	private static XPathExpression textXPathDetail;
	private static XPathExpression textXPath;
	// private static XPathExpression authXPath;
	// private static XPathExpression jounalXPath;

	private int index_;
	private NodeList citations_;
	private String filePath_;

	public MedlineParser(String filePath, InputStream is)
			throws ParserConfigurationException, SAXException, IOException,
			TransformerException {
		init();
		System.out.println("start reading: " + filePath);
		Document doc = XMLUtil.getDocument(is);
		System.out.println("start select node: " + filePath);
		citations_ = XPathAPI.selectNodeList(doc, CITATION_XPATH);
		System.out.println("finish init file: " + filePath);
		index_ = 0;
		filePath_ = filePath;
	}

	private void init() {
		if (xPath == null) {
			try {
				xPath = XPathFactory.newInstance().newXPath();
				titleXPath = xPath.compile(TITLE_XPATH);
				textXPathDetail = xPath.compile(TEXT_XPATH_DETAIL);
				textXPath = xPath.compile(TEXT_XPATH);
				// authXPath = xPath.compile(AUT_XPATH);
				// jounalXPath = xPath.compile(JOURNAL_XPATH);
			} catch (XPathExpressionException e) {
				e.printStackTrace();
			}
		}
	}

	public boolean hasNext() {
		return index_ != citations_.getLength();
	}

	public MedlineItem next() {
		// use clone to increase parsing speed
		Node node = citations_.item(index_++).cloneNode(true);
		MedlineItem item = parserMedlineItem(node);
		return item;
	}

	private MedlineItem parserMedlineItem(Node node) {
		MedlineItem item = new MedlineItem();

		try {
			item.setSourceFile(filePath_);
			parseAuthor(node, item);
			parsePmid(node, item);
			parseTitle(node, item);
			parseAbstract(node, item);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}

		return item;
	}

	private void parseAuthor(Node node, MedlineItem output) {
		// TODO
	}

	private void parsePmid(Node node, MedlineItem output)
			throws TransformerException {
		Node pmid = XPathAPI.selectSingleNode(node, "PMID");
		if (pmid != null)
			output.setPmid(pmid.getTextContent());
	}

	private void parseTitle(Node node, MedlineItem output)
			throws XPathExpressionException {
		String title = (String) titleXPath
				.evaluate(node, XPathConstants.STRING);
		output.setTitle(title);
	}

	private void parseAbstract(Node node, MedlineItem output)
			throws XPathExpressionException {
		NodeList texts = (NodeList) textXPathDetail.evaluate(node,
				XPathConstants.NODESET);
		if (texts.getLength() > 0) {
			StringBuilder text = new StringBuilder();
			StringBuilder background = new StringBuilder();
			StringBuilder method = new StringBuilder();
			StringBuilder result = new StringBuilder();
			StringBuilder objective = new StringBuilder();
			StringBuilder conclusion = new StringBuilder();

			for (int i = 0; i < texts.getLength(); ++i) {
				if (texts.item(i) instanceof Element) {
					Element elem = (Element) texts.item(i);
					String content = elem.getTextContent();
					String category = elem.getAttribute(CATEGORY_ATTR);
					if (BACKGROUND.equalsIgnoreCase(category)) {
						background.append(content).append("\n");
					} else if (METHODS.equalsIgnoreCase(category)) {
						method.append(content).append("\n");
					} else if (RESULTS.equalsIgnoreCase(category)) {
						result.append(content).append("\n");
					} else if (CONCLUSIONS.equalsIgnoreCase(category)) {
						conclusion.append(content).append("\n");
					} else if (OBJECTIVE.equalsIgnoreCase(category)) {
						objective.append(content).append("\n");
					}
					text.append(content).append("\n");
				}
				output.setText(text.toString());
				output.setBackground(background.toString());
				output.setMethods(method.toString());
				output.setResults(result.toString());
				output.setObjective(objective.toString());
				output.setConclusions(conclusion.toString());
			}
		} else {
			String abst = (String) textXPath.evaluate(node,
					XPathConstants.STRING);
			output.setText(abst);
		}
	}

}
