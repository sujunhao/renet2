package cn.edu.sjtu.gdr.medline.rank;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.utils.GgrLogger;

public class PaperCiteScore implements Score {

	// pmid -> pr
	protected Map<Integer, Double> pr = new HashMap<Integer, Double>();

	public PaperCiteScore() {
		GgrLogger.log("start reading paper citing page rank...");
		String path = ConfigManager.getConfig("cites",
				"PAPER_CITE_IN_EDGE_COUNT_PATH");
		readall(path);
		GgrLogger.log("finish reading paper citing page rank...");
	}
	
	public PaperCiteScore(Map<Integer, Double> pr) {
		this.pr = pr;
	}
	@Override
	public double score(Set<Integer> pmids) {
		double score = 0;
		for(Integer pmid: pmids) {
			score += getPageRankScore(pmid);
		}
		return score;
	}

	protected double getPageRankScore(Integer pmid) {
		return pr.getOrDefault(pmid, 0.0);
	}
	
	private void readall(String path) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
	
			String str = null, pair[] = new String[2];
			int pos = 0, count = 0;
	
			while ((str = br.readLine()) != null) {
				pos = str.indexOf(' ');
				pair[0] = str.substring(0, pos);
				pair[1] = str.substring(pos + 1);
				pr.put(Integer.parseInt(pair[0]), Double.parseDouble(pair[1]));
	
				++count;
				if ((count & 0xfffff) == 0) {
					GgrLogger.log((count >> 20) + "M line processed...");
				}
			}
	
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
