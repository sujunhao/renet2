package cn.edu.sjtu.gdr.medline.pagerank.articletype;

public class ArticleTypeCode {
	private static final byte CLINICAL_TRIAL = 0x1;
	private static final byte VITRO = 0x2;
	private static final byte VIVO = 0x4;
	
	private byte code;
	
	public ArticleTypeCode() {
		code = 0x0;
	}
	
	public void setClinicalTrial() {
		code |= CLINICAL_TRIAL;
	}
	
	public void setVitro() {
		code |= VITRO;
	}
	
	public void setVivo() {
		code |= VIVO;
	}
	
	public boolean isClinicalTrial() {
		return (code & CLINICAL_TRIAL) != 0;
	}
	
	public boolean isVitro() {
		return (code & VITRO) != 0;
	}
	
	public boolean isVivo() {
		return (code & VIVO) != 0;
	}
	
	public boolean isOthers() {
		return code == 0;
	}
}
