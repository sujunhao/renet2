package cn.edu.sjtu.gdr.exe;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FilterPmidViaYear {

	private int year;
	private String inpath, outpath;

	public FilterPmidViaYear(int year, String inpath) {
		this.year = year;
		this.inpath = inpath;
		this.outpath = inpath + "." + year + ".txt";
	}

	public void run() {
		try {
			BufferedReader br = new BufferedReader(new FileReader(inpath));
			BufferedWriter bw = new BufferedWriter(new FileWriter(outpath));
			
			String str = null, pmid = null;
			int year = 0;
			
			while ((str = br.readLine()) != null) {
				pmid = str;
				str = br.readLine();
				year = Integer.parseInt(str.substring(0, 4));
				
				if (year >= this.year) {
					bw.write(pmid);
					bw.newLine();
					bw.write(str);
					bw.newLine();
				}
			}
			
			
			br.close();
			bw.flush();
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		String inpath = "res/data/pubdate";
		int year = 2000;
		new FilterPmidViaYear(year, inpath).run();
	}

}
