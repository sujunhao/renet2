package cn.edu.sjtu.gdr.medline.rank;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.evaluation.Evaluator;
import cn.edu.sjtu.gdr.obo.EscapeSynonymNormalizer;
import cn.edu.sjtu.gdr.obo.Hierarchy;
import cn.edu.sjtu.gdr.obo.Normalizer;
import cn.edu.sjtu.gdr.obo.SynonymNormalizer;

public class HRank {

	public static String[] diseases = { "Kidney Failure, Chronic",
			"Kidney Neoplasms", "Acute Kidney Injury",
			"Cardiovascular Diseases", "Lung Neoplasms" };

	public static void main(String[] args) {
		String path = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"evidence", "GENE_RELATED_DISEASE_NUMBER");
		Map<String, Integer> DF = ComputeGeneDF.load(path);
		int total_num_of_disease = DF.get("");
		
		Normalizer disSyn = EscapeSynonymNormalizer.getDiseaseNormalizer();
		Hierarchy disHier = Hierarchy.getDisHierarchy();
		DiseaseGene disGene = new DiseaseGene(true);
		boolean comDF = true;
		
		for (String disease : diseases) {
			System.out.println(String.format("Start evaluating: %s", disease));
			List<String> dnames = disSyn.normalize(disease);
			Set<String> all = new HashSet<String>();
			for (String dis : dnames) {
				all.addAll(disHier.getAllDescendants(dis));
			}

			Map<String, Set<Integer>> genePmids = new HashMap<String, Set<Integer>>();
			for (String dis : all) {
				Map<String, Set<Integer>> tmp = disGene.getEvidencePmids(dis);
				for (String gene : tmp.keySet()) {
					if (!genePmids.containsKey(gene))
						genePmids.put(gene, new HashSet<Integer>());
					genePmids.get(gene).addAll(tmp.get(gene));
				}
			}

			Score scorer = new FreqGeneScore();
			List<GeneScore> genes = new ArrayList<GeneScore>();
			for (String gene : genePmids.keySet()) {
				double score = scorer.score(genePmids.get(gene));
				if( comDF )
					score *= Math.log(1.0 * total_num_of_disease / DF.get(gene));
				if( score > 0 )
					genes.add(new GeneScore(gene, score));
			}
			Collections.sort(genes, Collections.reverseOrder());
			Evaluator.evaluation(disease, genes);
		}
	}

}
