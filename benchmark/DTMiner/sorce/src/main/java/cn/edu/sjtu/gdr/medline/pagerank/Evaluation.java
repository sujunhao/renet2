package cn.edu.sjtu.gdr.medline.pagerank;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cn.edu.sjtu.gdr.evaluation.DisGeNet;
import cn.edu.sjtu.gdr.obo.EscapeSynonymNormalizer;
import cn.edu.sjtu.gdr.obo.Normalizer;
import cn.edu.sjtu.gdr.obo.SynonymNormalizer;

public class Evaluation {
	
	public static String[] diseases = {"Kidney Failure, Chronic", "Kidney Neoplasms",
		"Acute Kidney Injury", "Cardiovascular Diseases", "Lung Neoplasms"};
	
	public static boolean dump = false;
	private static int cutNum = 100;
	
	public static void main(String[] args ) throws IOException {
		Normalizer geneSyn = EscapeSynonymNormalizer.getGeneNormalizer();
		DisGeNet curatedAsso = DisGeNet.getCuratedAsso();
		
		if (args.length >= 1 && args[0].equals("dump")) {
			dump = true;
		}

		DiseasePageRankScoreRanker worker = new DiseasePageRankScoreRanker();
		Map<Class<? extends PageRankScore>, List<RankedNode1st>> res = null;

		for(String disease: diseases) {

			Map<String, Double> disGenesMap = curatedAsso.getGeneList(disease);
			Set<String> disGenes = new HashSet<String>();
			for( String key: disGenesMap.keySet())
				disGenes.addAll(geneSyn.normalize(key));
			
			List<String> rankedGenes = new ArrayList<String>();
			res = worker.rank4th(disease);
			for (Class<? extends PageRankScore> clazz : res.keySet()) {
				System.out.println(clazz.getSimpleName());
				List<RankedNode1st> list = res.get(clazz);
				for (RankedNode1st rn : list) {
					rankedGenes.add(rn.name);
				}
				int c = 0;
				for( int i = rankedGenes.size() - 1, cnt = 0; cnt < cutNum; ++ cnt, -- i)
					if( disGenes.contains(rankedGenes.get(i)))
						++ c;
				double precision = 1.0 * c / cutNum,
						recall = 1.0 * c / disGenes.size();
				double f1 = 2 * precision * recall / (precision + recall);
				System.out.println(String.format("Disease: %s\tHit: %d\tF-score: %.3f", disease, c, f1));
				rankedGenes.clear();
			}

			if( dump ) {
				dumpGroundTruth(disease, disGenes);
			}
			
			if (dump) {
				for (Class<? extends PageRankScore> clazz : res.keySet()) {
					dump(disease, clazz.getSimpleName(), res.get(clazz));
				}
			}
		}
	}
	
	private static void dump(String disease, String tag,
			List<RankedNode1st> list) throws IOException {
		String path = String.format("rank4th.%s.%s.dump", disease, tag);
		BufferedWriter bw = new BufferedWriter(new FileWriter(path));

		for (int i = list.size() - 1, cnt = 0; i >= 0; --i) {
			RankedNode1st rn = list.get(i);
			bw.write(String.format("%d\t%s\t%s", ++cnt, rn.name, rn.score));
			bw.newLine();
		}

		bw.flush();
		bw.close();
	}
	
	private static void dumpGroundTruth(String disease, Set<String> disGenes) throws IOException {
		String path = String.format("%s.Ground Truth.dump", disease);
		BufferedWriter bw = new BufferedWriter(new FileWriter(path));

		for ( String s: disGenes) {
			bw.write(s);
			bw.newLine();
		}

		bw.flush();
		bw.close();
	}
}
