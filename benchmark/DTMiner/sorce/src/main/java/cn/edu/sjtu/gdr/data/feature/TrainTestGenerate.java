package cn.edu.sjtu.gdr.data.feature;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TrainTestGenerate {

	private final String path;

	public TrainTestGenerate(String path) {
		this.path = path;
	}

	public void run() throws IOException {
		List<String> data;
		data = new ArrayList<String>();

		readAll(data);

		/*Random rand = new Random();
		for (int i = 0; i < data.size(); ++i) {
			int j = i + rand.nextInt(data.size() - i);
			swap(data, i, j);
		}*/


		output(data);
	}

	private void output(List<String> data) throws IOException {
		BufferedWriter bwTrain = new BufferedWriter(new FileWriter(path
				+ ".train"));

		BufferedWriter bwTest = new BufferedWriter(new FileWriter(path
				+ ".test"));

		int i = 0;
		for (; i < data.size() / 10; ++i) {
        //int i = data.size()-1;
        //for (; i>= data.size()*9/10;--i){
			String d = data.get(i);

			bwTest.write(d);
			bwTest.newLine();
		}

		for (; i < data.size(); ++i) {
        //for(;i>=0;--i){
			bwTrain.write(data.get(i));
			bwTrain.newLine();
		}

		bwTrain.flush();
		bwTrain.close();
		bwTest.flush();
		bwTest.close();
	}

	private <T> void swap(List<String> list, int i, int j) {
		if (i == j)
			return;
		String o1 = list.get(i);
		list.set(i, list.get(j));
		list.set(j, o1);
	}

	private void readAll(List<String> data)
			throws IOException {
		BufferedReader br1 = new BufferedReader(new FileReader(path));

		String str1 = null;

		while ((str1 = br1.readLine()) != null) {
			data.add(str1);
		}

		br1.close();
	}

	public static void main(String[] args) throws IOException {
		new TrainTestGenerate(args[0]).run();
	}
}
