package cn.edu.sjtu.gdr.medline.xml;

public class ParserResult {
	public String result;
	public int endPos;
	
	public ParserResult(int endPos, String result) {
		this.result = result;
		this.endPos = endPos;
	}
}
