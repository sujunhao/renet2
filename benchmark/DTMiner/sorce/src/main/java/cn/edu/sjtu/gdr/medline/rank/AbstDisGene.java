package cn.edu.sjtu.gdr.medline.rank;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.obo.EscapeSynonymNormalizer;
import cn.edu.sjtu.gdr.obo.Normalizer;
import cn.edu.sjtu.gdr.obo.SynonymNormalizer;

public class AbstDisGene {
	
	//disease -> gene -> set of pmids
	private static Map<String, Map<String, Set<Integer>>> abstDisGeneMap = null;
	private static Normalizer disSyn = null;
	
	public static Map<String, Set<Integer>> getAbstEvidence(String dis) {
		if( disSyn == null )
			disSyn = EscapeSynonymNormalizer.getDiseaseNormalizer();
		if( abstDisGeneMap == null )
			load();
		
		Map<String, Set<Integer>> ret = new HashMap<String, Set<Integer>>();
		List<String> dnames = disSyn.normalize(dis);
		for( String dname: dnames) {
			if( abstDisGeneMap.get(dname) != null ) {
				Map<String, Set<Integer>> tmp = abstDisGeneMap.get(dname);
				
				for( String gn: tmp.keySet()) {
					if( !ret.containsKey(gn))
						ret.put(gn, new HashSet<Integer>());
					ret.get(gn).addAll(tmp.get(gn));
				}
			}
		}
		return ret;
	}
	
	private static void load() {
		String filename = ConfigManager.getConfig("evidence", "ABST_EVIDENCE");
		abstDisGeneMap = new HashMap<String, Map<String, Set<Integer>>>();
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));
			String line;
			while( (line = br.readLine()) != null ) {
				String[] strs = line.split("\\$\\$\\$");
				if( !abstDisGeneMap.containsKey(strs[0]))
					abstDisGeneMap.put(strs[0], new HashMap<String, Set<Integer>>());
				abstDisGeneMap.get(strs[0]).put(strs[1], new HashSet<Integer>());
				for( int i = 2; i < strs.length; ++ i)
					abstDisGeneMap.get(strs[0]).get(strs[1]).add(Integer.parseInt(strs[i]));
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
