package cn.edu.sjtu.gdr.medline.pagerank;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Rank2ndMain {

	private static boolean dump = false;

	public static void main(String[] args) throws IOException {
		if (args.length >= 1 && args[0].equals("dump")) {
			dump = true;
		}

		String disease = null;
		DiseasePageRankScoreRanker worker = new DiseasePageRankScoreRanker();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		List<RankNode2nd> res = null;

		while (true) {
			System.out.print("Enter Disease: ");
			disease = br.readLine().trim();

			RankNode2ndWeight weight = new RankNode2ndWeight();
			System.out.print("Cliniclal Trial Weight (default 1.0): ");
			weight.clinicalTrialWeight = readNum(br, 1.0);
			System.out.print("Vitro Weight (default 1.0): ");
			weight.vitroWeight = readNum(br, 1.0);
			System.out.print("Vivo Weight (default 1.0): ");
			weight.vivoWeight = readNum(br, 1.0);
			System.out.print("Other Weight (default 1.0)");
			weight.othersWeight = readNum(br, 1.0);

			res = worker.rank2nd(disease, weight);

			for (RankNode2nd rn : res) {
				System.out.format("%s\t%.6f\t%.6f\t%.6f\t%.6f\t%.6f\n",
						rn.name, rn.score(), rn.clinicalTrialScore,
						rn.vitroScore, rn.vivoScore, rn.otherScore);
			}

			if (dump)
				dump(disease, weight, res);
		}

	}

	private static void dump(String disease, RankNode2ndWeight weight,
			List<RankNode2nd> res) throws IOException {
		String path = String.format("%s-%d.dump", disease,
				System.currentTimeMillis());
		BufferedWriter bw = new BufferedWriter(new FileWriter(path));

		bw.write(String.format("%s\t%s\t%s\t%s\t%s\t%s\t%s", "num", "gene",
				"score", "clinicalScore", "vitroScore", "vivoScore",
				"otherScore"));
		bw.newLine();
		bw.write(String.format("*\tweight\t%.1f\t%.1f\t%.1f\t%.1f",
				weight.clinicalTrialWeight, weight.vitroWeight,
				weight.vivoWeight, weight.othersWeight));
		bw.newLine();
		for (int i = 1, index = res.size() - 1; index >= 0; --index, ++i) {
			RankNode2nd rn = res.get(index);
			bw.write(String.format("%d\t%s\t%s\t%s\t%s\t%s\t%s", i, rn.name,
					rn.score(), rn.clinicalTrialScore, rn.vitroScore,
					rn.vivoScore, rn.otherScore));
			
			outputDetail("CT", rn.ctScoreDetail, bw);
			outputDetail("VITRO", rn.vitroDetail, bw);
			outputDetail("VIVO", rn.vivoDetail, bw);
			outputDetail("OTHER", rn.otherDetail, bw);
			
			bw.newLine();
		}

		bw.flush();
		bw.close();
	}

	private static void outputDetail(String tag, Map<Integer, Double> map,
			BufferedWriter bw) throws IOException {
		@SuppressWarnings("unchecked")
		Map.Entry<Integer, Double>[] entries = map.entrySet().toArray(
				new Map.Entry[0]);
		Arrays.sort(entries, new Comparator<Map.Entry<Integer, Double>>() {
			@Override
			public int compare(Entry<Integer, Double> o1,
					Entry<Integer, Double> o2) {
				double d = o1.getValue() - o2.getValue();
				if (d > 0)
					return -1;
				else if (d < 0)
					return 1;
				else
					return 0;
			}
		});

		bw.write("\t" + tag);
		for (Map.Entry<Integer, Double> entry : entries) {
			bw.write("\t" + entry.getKey() + "\t" + entry.getValue());
		}
	}

	private static double readNum(BufferedReader br, double defaultValue)
			throws IOException {
		String str = br.readLine().trim();
		if (str.equals(""))
			return defaultValue;
		return Double.parseDouble(str);
	}

}
