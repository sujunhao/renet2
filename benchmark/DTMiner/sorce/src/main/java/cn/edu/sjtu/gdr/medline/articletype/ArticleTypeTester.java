package cn.edu.sjtu.gdr.medline.articletype;

import java.util.function.Function;

import cn.edu.sjtu.gdr.medline.json.MedlineCitationJson;

public abstract class ArticleTypeTester implements
		Function<MedlineCitationJson, Boolean> {

	/**
	 * the name of this tester
	 * 
	 * @return
	 */
	public abstract String name();
}
