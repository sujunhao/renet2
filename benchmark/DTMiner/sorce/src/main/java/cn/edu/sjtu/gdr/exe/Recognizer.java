package cn.edu.sjtu.gdr.exe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.*;
import java.lang.*;
import java.io.*;

import cn.edu.sjtu.gdr.obo.EscapeSynonymNormalizer;
import cn.edu.sjtu.gdr.obo.Normalizer;
import cn.edu.sjtu.gdr.utils.CommonStopWord;
import edu.stanford.nlp.process.Morphology;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.process.DocumentPreprocessor;

import edu.stanford.nlp.util.Triple;

/**
 * Recognize gene name and disease name, and return the recognized names and the
 * responding postions in the given literature.
 * 
 * <p>
 * Notice: the instance of {@link NameRecognizer} is not thread safe, which
 * means each thread should have a thread-local instance
 * 
 * @author Jack Sun(jacksunwei@gmail.com)
 *
 */
public class Recognizer {
    private static final int MIN_ACRONYM_LENGTH = 3;//acronym length
    private static final int MIN_RECOGNIZE_LENGTH = 3;
    private List<String> nerG, nerD;
    private Morphology morph;
    private Normalizer geneNorm, diseaseNorm;
    private CommonStopWord csw;
    //private static String taggerPath = "edu/stanford/nlp/models/pos-tagger/english-left3words/english-left3words-distsim.tagger";
    //private static MaxentTagger tagger = new MaxentTagger(taggerPath);
    private String[] punctuation = {",", "-", ":", ".", "/"}; //no "(",")"
	private HashMap<String, String> bingMap;
    private static String taggerPath = "edu/stanford/nlp/models/pos-tagger/english-left3words/english-left3words-distsim.tagger";
    private static MaxentTagger tagger = new MaxentTagger(taggerPath);
    private static String spiderPath = "spider/spider.out";
    private List<String> wordList;
	private List<String> tagList;
	private String result;

    public Recognizer() {
        morph = new Morphology();
        csw = CommonStopWord.getInstance();
        geneNorm = EscapeSynonymNormalizer.getGeneNormalizer();
        diseaseNorm = EscapeSynonymNormalizer.getDiseaseNormalizer();
		bingMap = new HashMap<String, String>();
    }

    public String recognize(String sentence, boolean isGene) {
		result = null;
        if (sentence != null) {

            DocumentPreprocessor dp = new DocumentPreprocessor(new StringReader(sentence));
            List<List<HasWord>> sens = new ArrayList<List<HasWord>>();
            for (List<HasWord> sen : dp) {
                sens.add(sen);
            }
			List<TaggedWord> tagged;
			if (sens.size() > 0){
				tagged = tagger.tagSentence(sens.get(0));
			} else {
				tagged = new ArrayList<TaggedWord>();
			}
			String taggedIn = tagged.toString();

            tagList = new ArrayList<>();
            wordList = new ArrayList<>();
            parseTag(tagList, wordList, taggedIn);

            ArrayList<String> lemmas = new ArrayList<>();
            for (int i = 0; i < wordList.size(); i++) {
                lemmas.add(getLemma(tagList.get(i), i, wordList.get(i)));
            }

            for (int window = 5; window != 0; --window) {
                recognizeWindow(wordList, window, lemmas, isGene);
				if (result != null){
					return result;
				}
            }
        }
        return result;
    }

    private void recognizeWindow(List<String> wordList, int windowSize, List<String> lemmas, boolean isGene) {
        boolean checkFlag;
        int end = wordList.size() - windowSize + 1;
        for (int start = 0; start < end; ++start) {
            if (isPunc(wordList.get(start)) || isPunc(wordList.get(start + windowSize - 1)))
                continue;
            // scan window from left to right
                String phraseLiter = "";
                for (int i = start; i < start + windowSize; ++i) {
                    phraseLiter += wordList.get(i) + " ";
                }
                while (phraseLiter.endsWith(" "))
                    phraseLiter = phraseLiter.substring(0, phraseLiter.length() - 1);

                String lemma = lemmas.get(start + windowSize - 1);
                for (int i = start + windowSize - 2; i >= start; --i)
                    lemma = wordList.get(i) + " " + lemma;
                String recRes;
				if (!isGene){
	                List<String> disNorms;
			        if((disNorms=diseaseNorm.normalize(phraseLiter)).size()>1){
				        recRes = disNorms.get(0);
					    disNorms.remove(0);
	                    checkFlag = checkLongTermOrNot(windowSize, recRes, start, wordList);
		                if(!checkFlag){
							result = disNorms.get(0);
							return;
						}
			        }else if ((disNorms=diseaseNorm.normalize(lemma)).size()>1) {
					    recRes = disNorms.get(0);
	                    disNorms.remove(0);
			            checkFlag = checkLongTermOrNot(windowSize, recRes, start, wordList);
						if(!checkFlag){
							result = disNorms.get(0);
							return;
						}
	                }
				} else {
	                List<String> geneNorms;
		            if ((geneNorms = geneNorm.normalize(phraseLiter)).size()>1) {
			            recRes = geneNorms.get(0);
				        geneNorms.remove(0);
			            checkFlag = checkLongTermOrNot(windowSize, recRes, start, wordList);
						if (!checkFlag){
							result = geneNorms.get(0);
							return;
						}	
	                }else if ((geneNorms = geneNorm.normalize(lemma)).size()>1) {
	                    recRes = geneNorms.get(0);
		                geneNorms.remove(0);
			            checkFlag = checkLongTermOrNot(windowSize, recRes, start, wordList);
						if (!checkFlag){
							result = geneNorms.get(0);
							return;
						}	
                    }
                }
        }
    }


    private boolean isPunc(String input) {
        for (String tmp : punctuation) {
            if (input.equals(tmp))
                return true;
        }
        return false;
    }

    private boolean checkLongTermOrNot(int windowSize, String phraseLiter, int start,
                                       List<String> wordList) {
        if (windowSize == 1) {
            if ((csw.contains(morph
                    .stem(phraseLiter.toLowerCase()))) || csw.contains(phraseLiter.toLowerCase()))
                return true;
        }
        if (phraseLiter.length() <= MIN_ACRONYM_LENGTH) {
            return true;
        }
        return false;
    }


    private void parseTag(List<String> tagList, List<String> wordlist, String taggedIn) {
        String taggedStr = taggedIn.substring(1, taggedIn.length() - 1);
        String tags[] = taggedStr.split(", ");
        tagList.clear();
        wordlist.clear();
        for (int i = 0; i < tags.length; i++) {
            String temp = tags[i].substring(tags[i].lastIndexOf("/") + 1);
            String word = tags[i].substring(0, tags[i].lastIndexOf("/"));
            tagList.add(temp);
            wordlist.add(word);
        }
    }

    private String getLemma(String tag, int idx, String name) {
        String lemma = morph.lemma(name, tag);
        return lemma;
    }
}
