package cn.edu.sjtu.gdr.medline.json;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import com.fasterxml.jackson.databind.ObjectMapper;

public class MedlineJsonPumper implements Iterator<MedlineCitationJson> {

	private BufferedReader br;
	private MedlineCitationJson nextMedline;
	private ObjectMapper mapper;

	public MedlineJsonPumper(String path) {
		try {
			br = new BufferedReader(new FileReader(path));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			br = null;
		}
		mapper = new ObjectMapper();
		nextMedline = null;
	}

	@Override
	public boolean hasNext() {
		if (br == null) {
			return false;
		}
		if (nextMedline != null) {
			return true;
		}
		try {
			String str = br.readLine();
			if (str == null) {
				// no more
				br.close();
				br = null;
				return false;
			}

			nextMedline = new MedlineCitationJson(mapper.readTree(str));
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public MedlineCitationJson next() {
		if (hasNext()) {
			MedlineCitationJson res = nextMedline;
			nextMedline = null;
			return res;
		}
		throw new RuntimeException("no more data");
	}

}
