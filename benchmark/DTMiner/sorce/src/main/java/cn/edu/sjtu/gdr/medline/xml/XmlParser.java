package cn.edu.sjtu.gdr.medline.xml;

import java.util.ArrayList;
import java.util.List;

public class XmlParser {
	public static ParserResult getFirstInner(String xml, String tagName,
			int start) {
		String beginPattern = "<" + tagName;
		String endTag = "</" + tagName + ">";
		int beginPos = xml.indexOf(beginPattern + ">", start);
		if (beginPos == -1) {
			beginPos = xml.indexOf(beginPattern + " ", start);
			if (beginPos == -1) {
				return null;
			}
		}

		beginPos = xml.indexOf('>', beginPos);
		if (xml.charAt(beginPos - 1) == '/') {
			return new ParserResult(beginPos + 1, "");
		}

		++beginPos;
		int endPos = xml.indexOf(endTag, beginPos);
		return new ParserResult(endPos + endTag.length(), new String(
				xml.substring(beginPos, endPos)));
	}

	public static List<String> getAllInner(String xml, String tagName) {
		List<String> res = new ArrayList<String>();
		ParserResult pr = new ParserResult(0, "");
		
		while ((pr = getFirstInner(xml, tagName, pr.endPos)) != null) {
			res.add(pr.result);
		}
		
		return res;
	}

    public static List<String> getAuthors(String xml){
        List<String> authorList = getAllInner(xml,"Author");
        List<String> res = new ArrayList<>();
        String lastName;
        String initial;
        String collective;
        String suffix;
        ParserResult pr;
        for(int i=0; i<authorList.size(); i++){
            if((pr = getFirstInner(authorList.get(i),"LastName",0)) == null){
                lastName = "";
            }else{
                lastName = pr.result;
            }
            if((pr = getFirstInner(authorList.get(i),"Initials",0)) == null){
                initial = "";
            }else{
                initial = pr.result;
            }
            if((pr = getFirstInner(authorList.get(i),"CollectiveName",0)) == null){
                collective = "";
            }else{
                collective = pr.result;
            }
            if((pr = getFirstInner(authorList.get(i),"Suffix",0)) == null){
                suffix = "";
            }else{
                suffix = pr.result;
            }
            res.add(lastName+"+"+initial+"+"+collective+"+"+suffix);
        }
        return res;
    }
}
