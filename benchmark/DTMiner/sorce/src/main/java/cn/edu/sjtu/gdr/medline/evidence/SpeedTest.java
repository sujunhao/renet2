package cn.edu.sjtu.gdr.medline.evidence;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.data.feature.RelationFeatureSet1;
import cn.edu.sjtu.gdr.data.forsvm.RunSvmKernel;
import cn.edu.sjtu.gdr.data.label.LabeledData;
import cn.edu.sjtu.gdr.data.label.LabeledRelationData;
import cn.edu.sjtu.gdr.ggr.NameHolder;
import cn.edu.sjtu.gdr.ggr.NamePosition;
import cn.edu.sjtu.gdr.obo.EscapeSynonymNormalizer;
import cn.edu.sjtu.gdr.obo.Hierarchy;
import cn.edu.sjtu.gdr.obo.Normalizer;
import cn.edu.sjtu.gdr.utils.GgrLogger;
import edu.stanford.nlp.process.Morphology;
import svmJavaKernel.libsvm.ex.SVMPredictor;
import svmJavaKernel.libsvm.svm_model;

import java.io.*;
import java.util.*;

/**
 * Created by Xie Yanping on 2016/1/18.
 */
public class SpeedTest {
    public static final String SPLITTER = "\t";
    private static final String UUID_TEMPLATE = "%s-%d-%d-%d";
    private Hierarchy h = Hierarchy.getDisHierarchy();
    private String inDir;
    private Normalizer geneSyn = EscapeSynonymNormalizer.getGeneNormalizer();
    private Normalizer disSyn = EscapeSynonymNormalizer.getDiseaseNormalizer();
    private svm_model model;
    private RunSvmKernel kernel = new RunSvmKernel();
    private int generationSize = 1;
    private Morphology morph;

    public SpeedTest(String inpath, String m) throws IOException, ClassNotFoundException{
        this.inDir = inpath;
        model = SVMPredictor.loadModel("svm/hybrid.model"+m);
        morph = new Morphology();
    }

    public void extract() throws ClassNotFoundException{
        int testSize = 5000;
        GgrLogger.log("start extracting evidence...");
        try {
            File dirFilePar = new File(inDir);
            File[] filePar = dirFilePar.listFiles();
            Arrays.sort(filePar, new Comparator<File>() {
                public int compare(File o1, File o2) {
                    return o1.getName().compareTo(o2.getName());
                }
            });

            String str, pmid;
            int num, count = 0;
            long starttime = System.currentTimeMillis();
            for(File f : filePar) {
                System.out.println(f.getName());
                BufferedReader br = new BufferedReader(new FileReader(f));

                while ((str = br.readLine()) != null) {
                    pmid = str.substring("pmid: ".length(), str.lastIndexOf(' '));
                    GgrLogger.log(pmid);
                    num = Integer.parseInt(str.substring(str.lastIndexOf(' ') + 1));
                    Set<String> geneNormSet = new HashSet<>(), disNormSet = new HashSet<>();
                    for (int i = 0; i < num; ++i) {
                        List<NameHolder> genes = line2NameList(br.readLine());
                        List<NameHolder> diseases = line2NameList(br.readLine());
                        String sentence = br.readLine();
                        String parsed = br.readLine();
                        String tagged = br.readLine();
                        if(genes.size()>0 && diseases.size()>0)
                            count = record(count,pmid, i, genes, diseases, sentence, parsed, tagged, geneNormSet, disNormSet);
                    }
                    if ((count & 0xfffff) == 0) {
                        GgrLogger.log((count >> 20) + "M papers scanned..");
                    }
                    if(count == testSize){
                        System.out.println(count+" tests completed.");
                        break;
                    }
                }
                if(count == testSize) {
                    break;
                }
                br.close();
            }
            long endtime = System.currentTimeMillis();
            System.out.println("time elapsed: "+(endtime-starttime)/(1000));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        GgrLogger.log("finish extracting evidence...");
    }

    private int record(int count, String pmid, int senNum,
                        List<NameHolder> genes, List<NameHolder> diseases, String sentence, String parsed, String tagged,
                        Set<String> geneNormSet, Set<String> disNormSet)
            throws IOException, ClassNotFoundException {
        List<String> wordList = new ArrayList<>(), tagList = new ArrayList<>();
        parseTag(tagList,wordList,tagged);
        for (int i = 0; i < genes.size(); ++i) {
            for (int j = 0; j < diseases.size(); ++j) {
                String feature = null;
                LabeledData data = new LabeledData(pmid, senNum);
                data.set(sentence, parsed, tagList,wordList);
                data.addRelationLabel(new NamePosition(genes.get(i).beginIndex,
                        genes.get(i).endIndex), new NamePosition(diseases.get(j).beginIndex,
                        diseases.get(j).endIndex), 1);
                RelationFeatureSet1 rfs1 = new RelationFeatureSet1();
                for (LabeledRelationData relData : data.getRelationLabels()) {
                    feature = rfs1.getHybridFeature(relData,4);
                }
                double prediction = kernel.runKernel(feature, model);
                ++count;
            }
        }
        return count;
    }

    private void addNormSet(Set<String> normSet, List<String> norms){
        for(String norm : norms)
            normSet.add(norm);
    }
    private void normDisambiguation(Set<String> normSet, List<String> norms){
        List<String> tmp = new ArrayList<>();
        for (String norm : norms) {
            if (normSet.contains(norm)) {
                tmp.add(norm);
            }
        }
        if (tmp.size() != 0) {
            norms.clear();
            norms.addAll(tmp);
        } else {
            normSet.addAll(norms);
        }
    }
    private void parseTag(List<String> tagList, List<String> wordlist, String taggedIn) {
        String taggedStr = taggedIn.substring(1, taggedIn.length() - 1);
        String tags[] = taggedStr.split(", ");
        tagList.clear();
        wordlist.clear();
        for (int i = 0; i < tags.length; i++) {
            String temp = tags[i].substring(tags[i].lastIndexOf("/") + 1);
            String word = tags[i].substring(0, tags[i].lastIndexOf("/"));
            tagList.add(temp);
            wordlist.add(word);
        }
    }
    private String getLemma(String tag, String name) {
        String lemma = morph.lemma(name, tag);
        return lemma;
    }

    /*private boolean isInTest(String disNorm){
        for(String tmp1 : test){
            if(tmp1.equals(disNorm))
                return true;
            else{
                Set<String> ancestors = h.getAllAncestors(disNorm,generationSize);
                for(String tmp2 : ancestors){
                    if(tmp1.equals(tmp2))
                        return true;
                }
            }
        }
        return false;
    }*/

    private List<NameHolder> line2NameList(String line) {
        List<NameHolder> res = new ArrayList<NameHolder>();
        if (line.trim().equals("")) {
            return res;
        }

        String tmp[] = line.split("\\$\\$\\$");
        for (int i = 0; i < tmp.length; i += 1) {
            res.add(new NameHolder().deserialize(tmp[i]));
        }

        return res;
    }

    public static void main(String[] argv) throws IOException,ClassNotFoundException{
        String inDir = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
                "exe", "NER_DIR")+argv[0];
        String m = argv.length>1?argv[1]:"";
        new SpeedTest(inDir,m).extract();

    }
}
