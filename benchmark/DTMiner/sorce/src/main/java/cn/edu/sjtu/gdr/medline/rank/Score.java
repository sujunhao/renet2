package cn.edu.sjtu.gdr.medline.rank;

import java.util.Set;

public interface Score {
	
	public double score(Set<Integer> pmids);
}
