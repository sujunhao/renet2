package cn.edu.sjtu.gdr.service.thread;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import cn.edu.sjtu.gdr.evaluation.DisGeNet;
import cn.edu.sjtu.gdr.service.RequestWrapper;
import cn.edu.sjtu.gdr.service.ServiceThread;

public class CuratedAssoGeneThread extends ServiceThread {
	public static final String SERVICE_NAME = "diseaseCuratedAssoGene";
	
	public CuratedAssoGeneThread() {
		super();
	}
	@Override
	public void run() {
		while (true) {
			try {
				RequestWrapper request = buffer.pop();
				if (request == null) {
					synchronized (this) {
						wait();
					}
				} else {
					BufferedWriter bw = new BufferedWriter(
							new OutputStreamWriter(
									request.client.getOutputStream()));
					
					DisGeNet curatedAsso = DisGeNet.getCuratedAsso();
					Map<String, Double> map = curatedAsso.getGeneList(request.body);
					List<Map.Entry<String, Double>> list = new ArrayList<Map.Entry<String, Double>>(
							map.entrySet());
					Collections.sort(list, new Comparator<Map.Entry<String, Double>>() {

						@Override
						public int compare(Entry<String, Double> o1,
								Entry<String, Double> o2) {
							if (o1.getValue() > o2.getValue())
								return 1;
							else if (o1.getValue() < o2.getValue())
								return -1;
							else
								return 0;
						}

					});
					
					for( Map.Entry<String, Double> entry: list) {
						bw.write(entry.getKey());
						bw.newLine();
					}
					bw.flush();
					bw.close();
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
