package cn.edu.sjtu.gdr.medline.cites;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.graph.DirectedGraph;
import cn.edu.sjtu.gdr.graph.DirectedGraphOutputter;
import cn.edu.sjtu.gdr.utils.GgrLogger;

public class ConstructPaperCiteExe {

	private String outPath;
	private DirectedGraph graph;

	public ConstructPaperCiteExe(String outPath) {
		this.outPath = outPath;
		graph = new DirectedGraph();
	}

	public void reduce(File file) {
		GgrLogger.log("start reading file: " + file.getName());
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));

			String str = null, tos[] = null, fromIdStr = null;
			int fromId = 0;

			while ((str = br.readLine()) != null) {
				fromIdStr = str.substring("pmid: ".length());
				str = br.readLine().trim();
				if (str.equals(""))
					continue;
				tos = str.split("\t");
				fromId = Integer.parseInt(fromIdStr);
				for (String to : tos) {
					graph.addEdge(fromId, Integer.parseInt(to));
				}
			}

			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		GgrLogger.log("finish reading file: " + file.getName());

	}

	public void output() {
		new DirectedGraphOutputter(graph, outPath).output();;
	}

	public static void main(String[] args) {
		String citeDir = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"cites", "PAPER_CITES_DIR");
		String outPath = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"cites", "PAPER_CITES_GRAPH_PATH");

		ConstructPaperCiteExe executor = new ConstructPaperCiteExe(outPath);
		File dir = new File(citeDir);
		File[] files = dir.listFiles();

		for (File f : files) {
			executor.reduce(f);
		}

		executor.output();
	}
}
