package cn.edu.sjtu.gdr.exe;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.medline.abst.MedlineAbstPumper;
import rdi.cn.source.MedlineItem;

import java.io.*;
import java.util.*;

/**
 * Created by Xie Yanping on 2015/10/19.
 */
public class AuthorExtractor {
    private String indir;
    private String outPath;
    private File[] files;
    private Map<String,Integer> authorMap;
    private Integer authorNum;

    public AuthorExtractor(String indir, String outPath) {
        this.indir = indir;
        this.outPath = outPath;
        authorMap = new HashMap<>();
        authorNum = 0;
        init();
    }

    public void run() throws FileNotFoundException, IOException {
        MedlineAbstPumper pumper = null;
        MedlineItem item = null;
        BufferedWriter bw = new BufferedWriter(new FileWriter(outPath));
        for (File gzfile : files) {
            pumper = new MedlineAbstPumper(gzfile);
            while (pumper.hasNext()) {
                item = pumper.next();
                List<String> authors = item.getAuthors();
                if(authors.size()!=0) {
                    bw.write("pmid: ");
                    bw.write(item.getPmid());
                    bw.newLine();
                    //bw.write(item.getText());
                    Integer temp;
                    for (int i = 0; i < authors.size(); i++) {
                        if ((temp = authorMap.get(authors.get(i))) != null) {
                            bw.write(temp.toString() + "\t");
                        } else {
                            bw.write(authorNum.toString() + "\t");
                            authorMap.put(authors.get(i), authorNum++);
                        }
                    }
                    bw.newLine();
                }
            }
            System.out.println(gzfile.getName() + " finished");
            bw.flush();
        }
        bw.close();
    }

    private String filename(String infile) {
        return new String(infile.substring(0, infile.length() - 7) + ".gz");
    }

    private void init() {
        File dir = new File(indir);
        files = dir.listFiles(new FileFilter() {
            public boolean accept(File pathname) {
                return pathname.getName().endsWith(".gz");
            }
        });
        Arrays.sort(files, new Comparator<File>() {
            public int compare(File o1, File o2) {
                // fuck you, reverse order
                return -o1.getName().compareTo(o2.getName());
            }
        });
    }

    public static void main(String[] args) throws FileNotFoundException,
            IOException {
        String path = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
                "doc-dir", "MEDLINE_DIR");
        String outPath = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG, "author",
                "AUTHOR_META_ID_PATH");
        AuthorExtractor ae = new AuthorExtractor(path, outPath);
        ae.run();
    }
}
