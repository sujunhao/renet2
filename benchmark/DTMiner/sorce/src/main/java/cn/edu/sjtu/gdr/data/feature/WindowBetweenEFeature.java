package cn.edu.sjtu.gdr.data.feature;

import cn.edu.sjtu.gdr.data.label.LabeledData;
import cn.edu.sjtu.gdr.data.label.LabeledRelationData;
import cn.edu.sjtu.gdr.data.utils.NLPToolsForFeatures;
import cn.edu.sjtu.gdr.ggr.NamePosition;
import cn.edu.sjtu.gdr.utils.GgrLogger;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.process.Morphology;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Xie Yanping on 2015/7/28.
 */
public class WindowBetweenEFeature {
    public static final double CONTAIN = 1.0;
    public static final double NOT_CONTAIN = 0.0;
    private static final String featurePath = "res/feature/efeature.txt";

    private static Map<String, Double> res = null;
    private static Morphology morph = new Morphology();

    public static double extractRelationFeature(LabeledRelationData data) {
        return extractFeature(data.getGeneIndex(), data.getDiseaseIndex(),
                data.getSentenceLabeledData());
    }

    public static double extractFeature(NamePosition gene, NamePosition disease,
                                        LabeledData data) {
        double value = 0.0;
        if (res == null)
            init();

        int fromIndex = NLPToolsForFeatures.getRootOfWindow(gene, data.getDepTree());
        int toIndex = NLPToolsForFeatures.getRootOfWindow(disease, data.getDepTree());
        List<String> efeatures = NLPToolsForFeatures.getEfeature(fromIndex, toIndex, data.getTagged(), data.getDepTree());
        List<TaggedWord> tagged = data.getTagged();

        for (String feature : efeatures){
            if(res.containsKey(feature)){
                value += 1.0;
            }
        }
        return value;
        //return res;
    }

    public static void init() {
        res = new HashMap<String, Double>();
        GgrLogger.log("Loading efeatures...");
        try {
            BufferedReader br = new BufferedReader(new FileReader(featurePath));
            String str = null;
            while ((str = br.readLine()) != null) {
                res.put(str,NOT_CONTAIN);
            }
            br.close();
            GgrLogger.log("Finish loading...");
            return;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        GgrLogger.warn("Error in loading...");
    }
}
