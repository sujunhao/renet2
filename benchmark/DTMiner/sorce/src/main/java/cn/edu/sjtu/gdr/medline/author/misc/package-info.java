/**
 * Misc code used for shrink author citing graph. 
 */
/**
 * @author Jack Sun(jacksunwei@gmail.com)
 *
 */
package cn.edu.sjtu.gdr.medline.author.misc;