package cn.edu.sjtu.gdr.obo;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import cn.edu.sjtu.gdr.config.ConfigManager;

/**
 * SynonymNormalizer is used for synonym of gene
 * 
 * For a given gene, it will output the unified symbolic name, if a gene has two
 * symbolic name, the later-input one will show
 * 
 * @author Jack Sun(jacksunwei@gmail.com)
 *
 */
public class GeneSynonymNormalizer {
	private volatile static GeneSynonymNormalizer sn = null;

	private Set<String> outputter_;
	private Map<String, String> symbol_;

	private GeneSynonymNormalizer(Set<String> outputter,
			Map<String, String> symbol) {
		this.outputter_ = outputter;
		this.symbol_ = symbol;
	}

	public static GeneSynonymNormalizer getInstance() {
		if (sn == null) {
			synchronized (GeneSynonymNormalizer.class) {
				if (sn == null) {
					sn = newInstance(ConfigManager.getConfig(
							ConfigManager.MAIN_CONFIG, "obo",
							"GENE_SYN"));
				}
			}
		}
		return sn;
	}

	public static GeneSynonymNormalizer newInstance(String path) {
		return new Builder().build(path);
	}

	/**
	 * Normalize a {@link String} name
	 * 
	 * @param name
	 *            a name to be normalized
	 * @return String a normalized name; null if not found
	 */
	public String normalize(String name) {
		name = name.toLowerCase();
		return outputter_.contains(name) ? name : symbol_.get(name);
	}

	public boolean isOutput(String name) {
		name = name.toLowerCase();
		return outputter_.contains(name);
	}

	private static class Builder {
		private Set<String> outputter_;
		private Map<String, String> symbol_;

		public Builder() {
			outputter_ = new HashSet<String>();
			symbol_ = new HashMap<String, String>();
		}

		private void add(String output, String name) {
			if (!outputter_.contains(output)) {
				outputter_.add(output);
			}
			if (name != null) {
				symbol_.put(name, output);
			}
		}

		public GeneSynonymNormalizer build(String path) {
			try {
				BufferedReader br = new BufferedReader(new FileReader(path));
				String line, words[];

				while ((line = br.readLine()) != null) {
					words = line.split("\\^");
					add(words[0], null);
					for (int i = 1; i < words.length; ++i) {
						add(words[0], words[i]);
					}
				}

				br.close();
				return new GeneSynonymNormalizer(this.outputter_, this.symbol_);

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
	}

	public static void output(String gene, String norm) {
		System.out.format("norm of {%s}: %s\n", gene, norm);
	}

	public static void main(String[] argv) {
		GeneSynonymNormalizer norm = GeneSynonymNormalizer.getInstance();
		String test[] = { "h sapiens",
				"mitochondrially encoded cytochrome c oxidase i",
				"zinc finger protein 265" };
		for (int i = 0; i < test.length; ++i) {
			output(test[i], norm.normalize(test[i]));
		}
		while (null != System.console()) {
			System.out.print("gene? ");
			String gene = System.console().readLine();
			output(gene, norm.normalize(gene));
		}
	}
}
