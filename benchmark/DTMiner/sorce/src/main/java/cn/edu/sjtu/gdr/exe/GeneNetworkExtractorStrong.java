package cn.edu.sjtu.gdr.exe;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;

import cn.edu.sjtu.gdr.async.netext.ControlData;
import cn.edu.sjtu.gdr.async.netext.GeneNetworkExtractThread;
import cn.edu.sjtu.gdr.async.netext.GeneNetworkIoThread;
import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.medline.abst.AbstParPumper;
import cn.edu.sjtu.gdr.medline.abst.TextAbstPumper;
import edu.stanford.nlp.process.Morphology;

/**
 * a multi-thread version of gene network extractor
 * 
 * @author Jack Sun(jacksunwei@gmail.com)
 *
 */
public class GeneNetworkExtractorStrong {

//	static final int THREAD_COUNT = 2;
	static final int THREAD_COUNT = Runtime.getRuntime().availableProcessors() + 2;

	public static void main(String[] argv) throws InterruptedException {
		ControlData control = new ControlData(THREAD_COUNT);
		GeneNetworkIoThread ioThread = new GeneNetworkIoThread(control);
		GeneNetworkExtractThread[] workThreads = new GeneNetworkExtractThread[THREAD_COUNT];
		for (int i = 0; i < THREAD_COUNT; ++i) {
			workThreads[i] = new GeneNetworkExtractThread(control);
			workThreads[i].start();
		}
		ioThread.start();

		log("all thread init finished");
		log("start to populate data");
		populateData(control);
		log("finish populating data");

		ioThread.join();
	}

	static void log(String msg) {
		System.out.format("main thread: %s\n", msg);
	}
	
	static void populateData(ControlData control) {
        String dirPar = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
                "exe", "MEDLINE_ABSTRACT_PAR_COMBINE");
        File dirFilePar = new File(dirPar);
        File[] filePar = dirFilePar.listFiles(new FileFilter() {
            public boolean accept(File pathname) {
                return pathname.getName().endsWith(".gz");
            }
        });
        Arrays.sort(filePar, new Comparator<File>() {
            public int compare(File o1, File o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });

		final int THRESHOLD = 2 * THREAD_COUNT;
		for (int i = 0; i < filePar.length; ++i) {
			while (control.parseBuffer.length() > THRESHOLD) {
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			try {
                //TextAbstPumper tempb = new TextAbstPumper(file[i]);
				//control.inputBuffer.push(tempb);
                AbstParPumper tempa = new AbstParPumper(filePar[i]);
                control.parseBuffer.push(tempa);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		while(control.parseBuffer.length() > 0) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		control.setState(false);
	}
}
