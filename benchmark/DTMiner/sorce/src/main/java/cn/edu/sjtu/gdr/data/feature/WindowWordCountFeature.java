package cn.edu.sjtu.gdr.data.feature;

import cn.edu.sjtu.gdr.data.label.LabeledData;
import cn.edu.sjtu.gdr.data.label.LabeledRelationData;
import cn.edu.sjtu.gdr.ggr.NamePosition;

/**
 * Gene window的词数
 * 
 * Disease window词数
 * 
 * 句子的词数
 * 
 * @author Jack Sun(jacksunwei@gmail.com)
 *
 */
public class WindowWordCountFeature {
	/**
	 * Gene词的数量
	 * 
	 * @param data
	 * @return
	 */
	public static double extractRelationGeneFeature(LabeledRelationData data) {
		return extractFeature(data.getGeneIndex(),
				data.getSentenceLabeledData());
	}

	/**
	 * Disease词的数量
	 * 
	 * @param data
	 * @return
	 */
	public static double extractRelationDiseaseFeature(LabeledRelationData data) {
		return extractFeature(data.getDiseaseIndex(),
				data.getSentenceLabeledData());
	}

	public static double extractRelationSentenceFeature(LabeledRelationData data) {
		return data.getSentenceLabeledData().getTagged().size();
	}

	/**
	 * Disease和Gene之间词的数量
	 * 
	 * @param data
	 * @return
	 */
	public static double extractRelationGeneDiseaseDistanceFeature(
			LabeledRelationData data) {
		return Math.min(
				Math.abs(data.getGeneIndex().getStart()
						- data.getDiseaseIndex().getEnd()),
				Math.abs(data.getGeneIndex().getEnd()
						- data.getDiseaseIndex().getStart())) / 10.0;
	}

	public static double extractFeature(NamePosition np, LabeledData data) {
		return np.getEnd() - np.getStart();
	}
}
