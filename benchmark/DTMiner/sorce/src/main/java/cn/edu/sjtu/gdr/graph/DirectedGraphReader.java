package cn.edu.sjtu.gdr.graph;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Pattern;

public class DirectedGraphReader {
	private String inpath;
	private boolean readInEdge;
	public DirectedGraphReader(String path) {
		this(path, true);
	}
	
	public DirectedGraphReader(String path, boolean readInEdge) {
		this.inpath = path;
		this.readInEdge = readInEdge;
	}

	public DirectedGraph read() {
		DirectedGraph graph = null;

		try {
			BufferedReader br = new BufferedReader(new FileReader(inpath));

			graph = new DirectedGraph();
			String str = null, edges[] = null, pair[] = null;

			while ((str = br.readLine()) != null) {
				int id = Integer.parseInt(str
						.substring(DirectedGraphOutputter.nodePrefix.length()));
				// skip in edges, only add out edges is enough
				br.readLine(); 
				str = br.readLine().trim(); // read in out edges
				if (str.length() == 0) continue;
				edges = str.split(DirectedGraphOutputter.EDGE_SPLITTER);
				for (String edge : edges) {
					pair = edge.split(Pattern.quote(DirectedGraphOutputter.WEIGHT_SPLITTER));
					graph.addEdge(id, Integer.parseInt(pair[0]), Integer.parseInt(pair[1]), readInEdge);
				}
			}

			br.close();
			return graph;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}
}
