package cn.edu.sjtu.gdr.async.articletype;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.medline.articletype.ArticleTypeTester;
import cn.edu.sjtu.gdr.medline.articletype.ClinicalTrialTester;
import cn.edu.sjtu.gdr.medline.articletype.VitroTester;
import cn.edu.sjtu.gdr.medline.articletype.VivoTester;
import cn.edu.sjtu.gdr.medline.json.MedlineCitationJson;
import cn.edu.sjtu.gdr.medline.json.MedlineCitationJsonUtils;
import cn.edu.sjtu.gdr.medline.json.MedlineJsonPumper;
import cn.edu.sjtu.gdr.utils.GgrLogger;

public class FindArticleType {

	private static final int BUFFER_SIZE = 10000;

	private List<MedlineCitationJson> buffer;
	private List<ArticleTypeTester> testers;

	public FindArticleType() {
		buffer = new ArrayList<MedlineCitationJson>();
		testers = new ArrayList<ArticleTypeTester>();
		testers.add(new ClinicalTrialTester());
		testers.add(new VivoTester());
		testers.add(new VitroTester());
	}

	public void run() throws IOException {
		GgrLogger.log("start finding article types...");

		String medlinePath = ConfigManager.getConfig("python",
				"MEDLINE_JSON_FILE");
		String outpath = ConfigManager.getConfig("artical-type",
				"ARTICLE_TYPE_PATH");
		BufferedWriter bw = new BufferedWriter(new FileWriter(outpath));

		MedlineJsonPumper pumper = new MedlineJsonPumper(medlinePath);
		int count = 0;

		while (pumper.hasNext()) {
			buffer.clear();

			GgrLogger.log("start reading new buffer...");
			while (pumper.hasNext() && buffer.size() < BUFFER_SIZE) {
				buffer.add(pumper.next());
			}
			
			GgrLogger.log("finish reading buffer and start progcessing...");

			for (MedlineCitationJson mc : buffer) {
				ArticleTypeData data = findType(mc);
				if (!data.isOthers()) {
					bw.write(data.toString());
					bw.newLine();
				}
			}
			
			count += buffer.size();
			GgrLogger.log(count + " medline citation processed..");

		}

		bw.flush();
		bw.close();

		outputFormat(outpath + ".format");
		
		GgrLogger.log("finish finding article types..");
	}
	
	private void outputFormat(String path) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(path));
		
		bw.write("pmid");
		for (ArticleTypeTester tester : testers) {
			bw.write(" ");
			bw.write(tester.name());
		}
		bw.newLine();
		
		bw.flush();
		bw.close();
	}

	private ArticleTypeData findType(MedlineCitationJson mc) {
		ArticleTypeData data = new ArticleTypeData();
		data.pmid = MedlineCitationJsonUtils.findPmid(mc);

		for (ArticleTypeTester tester : testers) {
			data.tag.add(tester.apply(mc));
		}

		return data;
	}

	public static void main(String[] args) throws IOException {
		new FindArticleType().run();
		
	}

}
