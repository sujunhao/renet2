package cn.edu.sjtu.gdr.evaluation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.obo.EscapeSynonymNormalizer;
import cn.edu.sjtu.gdr.obo.Normalizer;
import cn.edu.sjtu.gdr.obo.SynonymNormalizer;
import cn.edu.sjtu.gdr.utils.GgrLogger;

public class DisGeNet {

	private static boolean dump = true;
	private static Normalizer geneSyn = null;
	private static Normalizer disSyn = null;
	private Map<String, Map<String, Double>> map;

	private DisGeNet(String path, int type) {
		map = new HashMap<String, Map<String, Double>>();
		if( type == 1)
			read(path);
		else if( type == 2)
			readBeFree(path);
	}

	public Map<String, Double> getGeneList(String dis) {
		List<String> diseases = disSyn.normalize(dis);
		Map<String, Double> genes = new HashMap<String, Double>();
		for (String d : diseases) {
			Map<String, Double> tmp = map.get(d);
			for (String gene : tmp.keySet())
				if (!genes.containsKey(gene) || genes.get(gene) < tmp.get(gene))
					genes.put(gene, tmp.get(gene));
		}
		return genes;
	}

	private void read(String path) {
		if (geneSyn == null)
			geneSyn = EscapeSynonymNormalizer.getGeneNormalizer();
		if (disSyn == null)
			disSyn = EscapeSynonymNormalizer.getDiseaseNormalizer();
		try {
			List<String> diseases = null, genes = null;
			double weight;
			BufferedReader br = new BufferedReader(new FileReader(path));
			String line = br.readLine();
			while ((line = br.readLine()) != null) {
				String[] strs = line.split("\t");
				diseases = disSyn.normalize(strs[4]);
				genes = geneSyn.normalize(strs[1]);
				weight = Double.valueOf(strs[5]);
				for (String dis : diseases) {
					if (!map.containsKey(dis))
						map.put(dis, new HashMap<String, Double>());
					for (String gene : genes) {
						if (!map.get(dis).containsKey(gene)
								|| map.get(dis).get(gene) < weight)
							map.get(dis).put(gene, weight);
					}
				}
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void readBeFree(String path) {
		if (geneSyn == null)
			geneSyn = EscapeSynonymNormalizer.getGeneNormalizer();
		if (disSyn == null)
			disSyn = EscapeSynonymNormalizer.getDiseaseNormalizer();
		
		GgrLogger.log("Start loading...");
		try {
			List<String> diseases = null, genes = null;
			double weight;
			int count = 0;
			BufferedReader br = new BufferedReader(new FileReader(path));
			String line = br.readLine();
			while ((line = br.readLine()) != null) {
				String[] strs = line.split("\t");
				diseases = disSyn.normalize(strs[15].replaceAll(" ",""));
				genes = geneSyn.normalize(strs[12].replaceAll(" ",""));
				weight = Double.valueOf(strs[5]);
				for (String dis : diseases) {
					if (!map.containsKey(dis))
						map.put(dis, new HashMap<String, Double>());
					for (String gene : genes) {
						if (!map.get(dis).containsKey(gene)
								|| map.get(dis).get(gene) < weight)
							map.get(dis).put(gene, weight);
					}
				}
				
				++ count;
				if( (count & 0xfffff) == 0)
					GgrLogger.log((count >> 20) + "M lines scanned...");
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		GgrLogger.log("End loading...");
	}

	public Set<String> getDiseases() {
		return map.keySet();
	}

	private static DisGeNet curatedAsso = null;

	public static DisGeNet getCuratedAsso() {
		if (curatedAsso == null)
			curatedAsso = new DisGeNet(ConfigManager.getConfig(
					ConfigManager.MAIN_CONFIG, "gene-disease-association",
					"BEFREE_ASSOCIATION"), 2);
		return curatedAsso;
	}
	
	public static DisGeNet getDisGeNet(String filename) {
		return new DisGeNet(filename, 2);
	}

	public static void main(String[] args) throws IOException {
        Normalizer geneSyn = EscapeSynonymNormalizer.getGeneNormalizer();

		DisGeNet net = DisGeNet.getCuratedAsso();
		/*String[] test = { "Kidney Failure, Chronic", "Kidney Neoplasms",
				"Acute Kidney Injury", "Cardiovascular Diseases",
				"Lung Neoplasms" };*/
        String [] test = {"kidney failure, chronic", "kidney neoplasms",
                "kidney failure, acute", "cardiovascular system disease", "lung neoplasms"};
		for (String s : test) {
            s = s.replaceAll(" ","");
			Map<String, Double> map = net.getGeneList(s);
			List<Map.Entry<String, Double>> list = new ArrayList<Map.Entry<String, Double>>(
					map.entrySet());
			Collections.sort(list, new Comparator<Map.Entry<String, Double>>() {

				@Override
				public int compare(Entry<String, Double> o1,
						Entry<String, Double> o2) {
					if (o1.getValue() > o2.getValue())
						return -1;
					else if (o1.getValue() < o2.getValue())
						return 1;
					else
						return 0;
				}

			});
            /*
			System.out.println(s);
			for (Map.Entry<String, Double> entry : list) {
				System.out.println(entry.getKey() + "\t" + entry.getValue());
			}*/

			if (dump) {
				BufferedWriter bw = new BufferedWriter(new FileWriter(
                        "res/data/befree/" + s ));
						//"res/groundtruth/" + s));
				for (Map.Entry<String, Double> entry : list) {
					List<String> genes = geneSyn.normalize(entry.getKey());
					for (String gene : genes) {
						bw.write(gene + "\t" + entry.getValue());
						bw.newLine();
					}
				}
				bw.flush();
				bw.close();
			}
		}
	}
	
}
