package cn.edu.sjtu.gdr.medline.cites;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileWriter;
import java.io.IOException;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.utils.GgrLogger;

public class ExtractCitesExe {

	private File gzFile, outFile;

	public ExtractCitesExe(File gzFile, String outDir) {
		this.gzFile = gzFile;
		String name = gzFile.getName();
		name = name.substring(0, name.length() - ".xml.gz".length()) + ".txt";
		this.outFile = new File(outDir, name);
	}

	public void run() throws IOException {
		MedlineCitesPumper pumper = new MedlineCitesPumper(gzFile);
		BufferedWriter bw = new BufferedWriter(new FileWriter(outFile));
		while (pumper.hasNext()) {
			bw.write(pumper.next().toString());
			bw.newLine();
		}

		bw.flush();
		bw.close();
	}

	public static void main(String[] args) throws IOException {
		String indir = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"doc-dir", "MEDLINE_DIR");
		String outdir = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"cites", "PAPER_CITES_DIR");
		
		File dir = new File(indir);
		File[] files = dir.listFiles(new FileFilter() {
			
			@Override
			public boolean accept(File pathname) {
				return pathname.getName().endsWith(".gz");
			}
		});
		
		for (File f : files) {
			GgrLogger.log("start to process file: " + f.getName());
			new ExtractCitesExe(f, outdir).run();
			GgrLogger.log("end to process file: " + f.getName());
		}
	}
}
