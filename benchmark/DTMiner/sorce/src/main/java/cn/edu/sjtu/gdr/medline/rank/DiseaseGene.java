package cn.edu.sjtu.gdr.medline.rank;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BinaryOperator;
import java.util.function.Function;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.obo.EscapeSynonymNormalizer;
import cn.edu.sjtu.gdr.obo.Normalizer;
import cn.edu.sjtu.gdr.obo.SynonymNormalizer;
import cn.edu.sjtu.gdr.utils.GgrLogger;

/**
 * Get a disease related genes and the evidence uuids
 * @author dong
 *
 */
public class DiseaseGene {

	// disease names: disease -> a set of synonym
	private Map<String, Set<String>> diseaseNames;

	// disease -> gene -> a list of uuid
	private Map<String, Map<String, List<String>>> diseaseGeneMap;

	private Normalizer geneNorm;
	
	/*private Map<String, List<String>> getGeneUuidMap(Set<String> diseaseNames) {
		return diseaseNames
				.parallelStream()
				.map(new Function<String, Map<String, List<String>>>() {

					*//**
					 * For a disease name, return a map: gene -> a list uuid
					 *//*
					@Override
					public Map<String, List<String>> apply(String disease) {
						disease = disease.toLowerCase();
						Map<String, List<String>> map = null, res = null;
						if ((map = diseaseGeneMap.get(disease)) == null) {
							return new HashMap<String, List<String>>();
						}

						res = new HashMap<String, List<String>>();
						for (String gene : map.keySet()) {
							List<String> uuids = map.get(gene);
							List<String> norms = geneNorm.normalize(gene);
							for (String gg : norms) {
								res.put(gg, uuids);
							}
						}

						return res;
					}
				})
				.reduce(new HashMap<String, List<String>>(),
						new BinaryOperator<Map<String, List<String>>>() {
							*//**
							 * reduce all same gene name together. result will
							 * be: [gene name] -> a list uuid
							 *//*
							@Override
							public Map<String, List<String>> apply(
									Map<String, List<String>> acc,
									Map<String, List<String>> element) {
								if (acc == null || element == null) {
									System.out.println("fuck");
									return new HashMap<String, List<String>>();
								}

								if (acc.size() == 0)
									return element;

								Map<String, List<String>> res = new HashMap<String, List<String>>();
								for (String key : acc.keySet())
									res.put(key,
											new ArrayList<String>(acc.get(key)));

								for (String key : element.keySet()) {
									if (res.containsKey(key)) {
										res.get(key).addAll(element.get(key));
									} else {
										res.put(key, new ArrayList<String>(
												element.get(key)));
									}
								}
								return res;

							}
						});

	}*/
	
	/*public Map<String, List<String>> getGeneUuidMap(String disease) {
		//disease = disease == null ? "" : disease.toLowerCase();
		Set<String> dnames = null;
		if ((dnames = diseaseNames.get(disease)) == null) {
			System.out.println("no disease found..");
			return new HashMap<String, List<String>>();
		}

		return getGeneUuidMap(dnames);	
	}
	*/
	public Map<String, Set<Integer>> getEvidencePmids(String disease) {
		Map<String, Set<Integer>> ret = new HashMap<String, Set<Integer>>();
		Map<String, List<String>> geneUuidMap = diseaseGeneMap.get(disease);
        Map<Integer, Double> scores = new HashMap<>();
        double score,tmp;
        Integer pmid;
        //Map<String, List<String>> geneUuidMap = getGeneUuidMap(disease);
        if(geneUuidMap==null)
            return ret;
        for( String gene: geneUuidMap.keySet() ) {
            scores.clear();
            for(String uuid: geneUuidMap.get(gene)){
                score = Double.parseDouble(uuid.split("/")[1]);
                pmid = Integer.parseInt(uuid.substring(0,uuid.indexOf("-")));
                if(scores.containsKey(pmid)){
                    scores.put(pmid,scores.get(pmid)+score);
                }else
                    scores.put(pmid,score);
            }
            Set<Integer> pmids = new HashSet<Integer>();
            for(Integer p : scores.keySet()){
                if(scores.get(p)>0)
                    pmids.add(p);
            }
            ret.put(gene, pmids);
        }
		return ret;
	}
	
	public DiseaseGene(boolean svmOrNot) {
		init(svmOrNot);
	}
	
	private void init(boolean svmOrNot) {
		//readDiseaseNames();
		readDiseaseGeneUuidMap(svmOrNot);
		geneNorm = EscapeSynonymNormalizer.getGeneNormalizer();
	}
	
	/*private void readDiseaseNames() {
		GgrLogger.log("start reading disease names");
		this.diseaseNames = new HashMap<String, Set<String>>();
		String path = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG, "obo",
				"DISEASE_SYN");

		try {
			BufferedReader br = new BufferedReader(new FileReader(path));

			Set<String> set = null;
			String str = null, tmp[] = null;

			while ((str = br.readLine()) != null) {
				set = new HashSet<String>();
				tmp = str.toLowerCase().split("###");
				for (String s : tmp) {
                    s = s.replaceAll(" ", "");
					set.add(s);
					diseaseNames.put(s, set);
				}
			}

			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		GgrLogger.log("finish reading disease names");
	}*/
	
	private void readDiseaseGeneUuidMap(boolean svmOrNot) {
		GgrLogger.log("start reading disease gene uuid map...");
		diseaseGeneMap = new HashMap<String, Map<String, List<String>>>();
		String path = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"evidence", "GENE_DISEASE_EVIDENCE_REDUCE");

		try {
			BufferedReader br = new BufferedReader(new FileReader(path));

			List<String> list = null;
			String str = null, tmp[] = null;

			while ((str = br.readLine()) != null) {
				tmp = str.split("\t");
				list = new ArrayList<String>();
				for (int i = 2; i < tmp.length; ++i) {
                    list.add(tmp[i]);
                    /*if(svmOrNot) {
                        if(tmp[i].endsWith("1"))
                            list.add(tmp[i]);
                    }else
                        list.add(tmp[i]);*/
				}
                //if(tmp[0].equals("doid:3683"))
                    addDiseaseGeneMap(tmp[0], tmp[1], list);
			}

			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		GgrLogger.log("finish reading disease gene uuid map...");
	}
	
	private void addDiseaseGeneMap(String disease, String gene,
			List<String> uuids) {
		//disease = disease.toLowerCase();
		//gene = gene.toLowerCase();
		Map<String, List<String>> geneMap = null;
		if ((geneMap = diseaseGeneMap.get(disease)) == null) {
			geneMap = new HashMap<String, List<String>>();
			diseaseGeneMap.put(disease, geneMap);
		}

		geneMap.put(gene, uuids);
	}
}
