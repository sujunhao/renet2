package cn.edu.sjtu.gdr.data.analysis;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.*;
import java.util.zip.GZIPInputStream;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.ggr.NameRecognizer;
import cn.edu.sjtu.gdr.ggr.NameRecognizerResult;
import cn.edu.sjtu.gdr.utils.GgrLogger;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.process.DocumentPreprocessor;

public class DataSetMark {

	private File file;

	// private MarkerString marker;

	/**
	 * 
	 * @param file
	 *            a gz file from MEDLINE_ABSTRACT_SEG to mark
	 */
	public DataSetMark(File file) {
		this.file = file;
		// marker = new MarkerString();
	}

	public void mark() {
		/*try {
			BufferedReader br = new BufferedReader(new InputStreamReader(
					new GZIPInputStream(new FileInputStream(file))));
			BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
					ConfigManager.getConfig(ConfigManager.MAIN_CONFIG, "exe",
							"MEDLINE_ABSTRACT_SEG_MARK"), file.getName()
							.substring(0, file.getName().length() - 3))));
			String str = null;

			while ((str = br.readLine()) != null) {
				int pos = str.lastIndexOf(' ');
				int num = Integer.parseInt(str.substring(pos + 1));
				String pmid = str.substring("pmid: ".length(), pos);

				for (int i = 0; i < num; ++i) {
					String line = br.readLine();
					DocumentPreprocessor dp = new DocumentPreprocessor(
							new StringReader(line));
					Iterator<List<HasWord>> itr = dp.iterator();
					if (itr.hasNext()) {
						List<HasWord> sentence = itr.next();
                        Set<String> longTerm = new HashSet<>();
						NameRecognizerResult res = new NameRecognizer()
								.recognize(sentence, longTerm, " ");
						if (res.genes.size() > 0 && res.diseases.size() > 0) {
							// bw.write("pmid: ");
							// bw.write(pmid);
							// bw.write(" " + i);
							// bw.newLine();
							// bw.write(line); // literature of the sentence
							// bw.newLine();
							// bw.write(res.underlineMark(line, 0)); // marker
							// line
							// bw.newLine();
							// bw.write(marker.markString(res));
							// bw.newLine();
							bw.write(new LabelData(pmid, i, line, res)
									.toString());
							bw.newLine();
						}
					}
				}
			}

			bw.flush();
			bw.close();
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}*/
	}

	public static void main(String[] args) {
		File dir = new File(ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"exe", "MEDLINE_ABSTRACT_SEG"));
		File[] files = dir.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				return pathname.getName().endsWith(".gz");
			}
		});
		Arrays.sort(files, new Comparator<File>() {

			@Override
			public int compare(File o1, File o2) {
				return o2.getName().compareTo(o1.getName());
			}
		});
		int count = 0;
		for (File f : files) {
			new DataSetMark(f).mark();
			GgrLogger.log(++count + " files procoded..");
		}
	}

}
