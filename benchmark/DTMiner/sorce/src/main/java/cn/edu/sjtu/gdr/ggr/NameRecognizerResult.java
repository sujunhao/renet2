package cn.edu.sjtu.gdr.ggr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * the result of {@link NameRecognizer}
 * 
 * @author Jack Sun(jacksunwei@gmail.com)
 *
 */
public class NameRecognizerResult {
	public static final char GENE_MARK = '-';
	public static final char DISEASE_MARK = '|';

	public List<String> genes, diseases;
	public List<NamePosition> genesPos, diseasesPos;
	// sentence index of genes & diseases
    //public List<List<String>> geneNorms, diseaseNorms;
	public List<NamePosition> genesSenIndex, diseasesSenIndex;

	public NameRecognizerResult() {
		genes = new ArrayList<String>();
		diseases = new ArrayList<String>();
		genesPos = new ArrayList<NamePosition>();
		diseasesPos = new ArrayList<NamePosition>();
        //geneNorms = new ArrayList<>();
        //diseaseNorms = new ArrayList<>();
		genesSenIndex = new ArrayList<NamePosition>();
		diseasesSenIndex = new ArrayList<NamePosition>();
	}

	/**
	 * given the literature of sentence, return the underline tag version of the
	 * literature
	 * 
	 * <p>
	 * Example: <br>
	 * the gene a is caused by gene b, which is related to disease c.<br>
	 *     ------              ------                      |||||||||
	 * 
	 * @param liter literature of the sentence
	 * @param offset the offset of the position in literature
	 * @return underline tag string
	 */
	public String underlineMark(String liter, int offset) {
		char[] mark = new char[liter.length()];
		Arrays.fill(mark, ' ');
		/*for (NamePosition np : genesPos) {
			Arrays.fill(mark, np.getStart() - offset, np.getEnd() - offset, GENE_MARK);
		}
		for (NamePosition np : diseasesPos) {
			Arrays.fill(mark, np.getStart() - offset, np.getEnd() - offset, DISEASE_MARK);
		}*/
		return new String(mark);
	}
}
