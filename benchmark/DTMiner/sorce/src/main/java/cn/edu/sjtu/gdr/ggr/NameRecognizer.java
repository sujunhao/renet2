package cn.edu.sjtu.gdr.ggr;

import java.util.*;

import cn.edu.sjtu.gdr.obo.EscapeSynonymNormalizer;
import cn.edu.sjtu.gdr.obo.Normalizer;
import cn.edu.sjtu.gdr.utils.CommonStopWord;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.pipeline.AnnotationSerializer;
import edu.stanford.nlp.process.Morphology;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import edu.stanford.nlp.util.IntTuple;
import edu.stanford.nlp.util.Triple;

/**
 * Recognize gene name and disease name, and return the recognized names and the
 * responding postions in the given literature.
 * 
 * <p>
 * Notice: the instance of {@link NameRecognizer} is not thread safe, which
 * means each thread should have a thread-local instance
 * 
 * @author Jack Sun(jacksunwei@gmail.com)
 *
 */
public class NameRecognizer {
    private static final int MIN_ACRONYM_LENGTH = 3;//acronym length
    private static final int MIN_RECOGNIZE_LENGTH = 3;
    //private List<String> nerG, nerD;
    private Morphology morph;
    private Normalizer geneNorm, diseaseNorm;
    private CommonStopWord csw;
    //private NER ner;
    //private static String taggerPath = "edu/stanford/nlp/models/pos-tagger/english-left3words/english-left3words-distsim.tagger";
    //private static MaxentTagger tagger = new MaxentTagger(taggerPath);
    private String[] stopPunc = {",", "-", ":", ".", "/"}; //no "(",")"
    private String[] punctuation = {",","(",")","-",":",".","/"};

    public NameRecognizer() {
        morph = new Morphology();
        csw = CommonStopWord.getInstance();
        geneNorm = EscapeSynonymNormalizer.getGeneNormalizer();
        diseaseNorm = EscapeSynonymNormalizer.getDiseaseNormalizer();
    }

    public NameRecognizerResult recognize(String sentence, //Set<String> longTerm,
                                          //String taggedIn) {
                                          List<TaggedWord> taggedWords){
        NameRecognizerResult result = new NameRecognizerResult();
        if (sentence != null) {
            boolean use[] = new boolean[taggedWords.size()];
            Arrays.fill(use, false);
            ArrayList<String> lemmas = new ArrayList<>();
            String cleanSen = sentence.replaceAll(",|\\(|\\)|-|:|\\.|/", " ");
            for (int i = 0; i < taggedWords.size(); i++) {
                lemmas.add(getLemma(taggedWords.get(i).tag(), i,taggedWords.get(i).word()));
            }
            Map<Integer,Map<String,List<Integer>>> splitMap = new HashMap<>();
            for (int i = 0; i < taggedWords.size(); i++) {
                Map<String,List<Integer>> splitters = new HashMap<>();
                String tmp;
                int idx, base;
                for(String punc : punctuation) {
                    List<Integer> index = new ArrayList<>();
                    tmp = sentence.substring(taggedWords.get(i).beginPosition(),
                            taggedWords.get(i).endPosition());
                    base = taggedWords.get(i).beginPosition();
                    while (tmp.contains(punc)){
                        idx = tmp.indexOf(punc);
                        if(idx != 0 && idx+1 != taggedWords.get(i).word().length())
                            index.add(base+idx);
                        base = base + idx+1;
                        tmp = tmp.substring(idx+1);
                    }
                    if(!index.isEmpty())
                        splitters.put(punc,index);
                }
                if(!splitters.isEmpty())
                    splitMap.put(i, splitters);
            }
            for (int window = taggedWords.size(); window != 0; --window) {
                recognizeWindow(result, taggedWords, use, window, splitMap, lemmas, sentence, cleanSen);
            }
        }
        return result;
    }

    private void recognizeWindow(NameRecognizerResult res, List<TaggedWord> taggedWords, boolean[] use, int windowSize,
                                 //Set<String> longTerm,
                                 Map<Integer,Map<String,List<Integer>>> splitMap,
                                 List<String> lemmas, String sen, String cleanSen) {
        //Boolean checkFlag, validFlag;
        int bound = taggedWords.size() - windowSize + 1;
        for (int start = 0; start < bound; ++start) {
            int end = start + windowSize - 1;
            /*if (isPunc(taggedWords.get(start).word()) || isPunc(taggedWords.get(end).word()))
                continue;*/
            // scan window from left to right
            if (!use[start] && !use[start + windowSize - 1]) {
                int beginPos = taggedWords.get(start).beginPosition();
                int endPos = taggedWords.get(end).endPosition();
                String phraseLiter = sen.substring(beginPos, endPos);
                String cleanPhraseLiter = cleanSen.substring(beginPos, endPos);
                Set<String> splitList;
                splitList = split(splitMap,start,end,sen,cleanSen,beginPos,endPos);
                String lemma = sen.substring(beginPos,taggedWords.get(end).beginPosition())+lemmas.get(end);
                String cleanLemma = cleanSen.substring(beginPos,taggedWords.get(end).beginPosition())+lemmas.get(end);;
                if(diseaseNorm.canNormalize(phraseLiter) || (diseaseNorm.canNormalize(cleanPhraseLiter)) ||
                        diseaseNorm.canNormalize(splitList) ||
                        diseaseNorm.canNormalize(lemma) || diseaseNorm.canNormalize(cleanLemma)){
                    addDisease(res, phraseLiter, start, windowSize, use, beginPos, endPos);
                }
                if (geneNorm.canNormalize(phraseLiter) || geneNorm.canNormalize(cleanPhraseLiter) ||
                        geneNorm.canNormalize(splitList) || geneNorm.canNormalize(lemma) ||
                        geneNorm.canNormalize(cleanLemma))
                    addGene(res, phraseLiter, start, windowSize, use, beginPos, endPos);
            }
        }
    }

    private Set<String> split(Map<Integer,Map<String,List<Integer>>> splitMap, int start, int end, String sen, String cleanSen,
                              int beginPos, int endPos){
        Set<String> ret = new HashSet<>();
        Map<String,List<Integer>> splitter = splitMap.get(start);
        if(splitter != null){
            for(String punc : splitter.keySet()){
                List<Integer> index = splitter.get(punc);
                for(int i=0; i<index.size(); ++i){
                    ret.add(sen.substring(index.get(i) + 1, endPos));
                    ret.add(cleanSen.substring(index.get(i)+1,endPos));
                    /*if(right.equals(""))
                        ret.add(startWord.substring(index.get(i)+1));
                    else
                        ret.add(startWord.substring(index.get(i)+1)+" "+right);*/
                }
            }
        }
        splitter = splitMap.get(end);
        if(splitter != null){
            for(String punc : splitter.keySet()){
                List<Integer> index = splitter.get(punc);
                for(int i=0; i<index.size(); ++i){
                    ret.add(sen.substring(beginPos,index.get(i)));
                    ret.add(cleanSen.substring(beginPos,index.get(i)));
                    /*if(left.equals(""))
                        ret.add(endWord.substring(0,index.get(i)));
                    else
                        ret.add(left+" "+endWord.substring(0,index.get(i)));*/
                }
            }
        }
        return ret;
    }

    private boolean isAllUpperCase(String input){
        for(int i=0; i<input.length(); i++){
            if(!(input.charAt(i)>='A' && input.charAt(i)<='Z'))
                return false;
        }
        return true;
    }

    private boolean isPunc(String input) {
        for (String tmp : stopPunc) {
            if (input.equals(tmp))
                return true;
        }
        return false;
    }

    /*private boolean checkLongTermOrNot(int windowSize, String phraseLiter, int start,
                                       List<String> wordList) {
        if (windowSize == 1) {
            *//*if (start > 1 && start + windowSize < wordList.size())
                if ((wordList.get(start - 1).endsWith("(")) && wordList.get(start + windowSize).startsWith(")"))
                    return true;
            if(isAllUpperCase(phraseLiter))// uppercase 2,3-letter word
                return false;*//*
            if ((csw.contains(morph
                    .stem(phraseLiter.toLowerCase()))) || csw.contains(phraseLiter.toLowerCase()))
                return true;
        }
        if (phraseLiter.length() <= MIN_ACRONYM_LENGTH) {
            return true;
        }
        return false;
    }*/

    /*private boolean hasLongTerm(List<String> norms, Set<String> longTerm) {
        List<String> tmp = new ArrayList<>();
        for (String norm : norms) {
            if (longTerm.contains(norm)) {
                tmp.add(norm);
            }
        }
        if (tmp.size() != 0) {
            norms.clear();
            norms.addAll(tmp);
            return true;
        } else
            return false;
    }*/

    private void addDisease(NameRecognizerResult res, String phraseLiter, int start, int windowSize,
                            boolean[] use, int beginIndex, int endIndex){
                            //Set<String> longTerm,
                            //List<String> disNorms) {
        res.diseases.add(phraseLiter);
        res.diseasesPos.add(new NamePosition(beginIndex, endIndex));
        //res.diseaseNorms.add(disNorms);
        res.diseasesSenIndex.add(new NamePosition(start, start
                + windowSize));
        Arrays.fill(use, start, start + windowSize, true);
        //longTerm.addAll(disNorms);
    }

    private void addGene(NameRecognizerResult res, String phraseLiter, int start, int windowSize,
                         boolean[] use, int beginIndex, int endIndex){
                         //Set<String> longTerm,
                         //List<String> geneNorms) {
        res.genes.add(phraseLiter);
        res.genesPos.add(new NamePosition(beginIndex, endIndex));
        //res.geneNorms.add(geneNorms);
        res.genesSenIndex.add(new NamePosition(start, start
                + windowSize));
        Arrays.fill(use, start, start + windowSize, true);
        //longTerm.addAll(geneNorms);
    }

    /*private void parseTag(List<String> tagList, List<String> wordlist, String taggedIn) {
        String taggedStr = taggedIn.substring(1, taggedIn.length() - 1);
        String tags[] = taggedStr.split(", ");
        tagList.clear();
        wordlist.clear();
        for (int i = 0; i < tags.length; i++) {
            String temp = tags[i].substring(tags[i].lastIndexOf("/") + 1);
            String word = tags[i].substring(0, tags[i].lastIndexOf("/"));
            tagList.add(temp);
            wordlist.add(word);
        }
    }*/

    private String getLemma(String tag, int idx, String name) {
        String lemma = morph.lemma(name, tag);
        return lemma;
    }
}
