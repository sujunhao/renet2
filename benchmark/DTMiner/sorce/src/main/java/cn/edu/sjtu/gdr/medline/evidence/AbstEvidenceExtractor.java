package cn.edu.sjtu.gdr.medline.evidence;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.ggr.NameHolder;
import cn.edu.sjtu.gdr.obo.EscapeSynonymNormalizer;
import cn.edu.sjtu.gdr.obo.Normalizer;
import cn.edu.sjtu.gdr.obo.SynonymNormalizer;
import cn.edu.sjtu.gdr.utils.GgrLogger;

public class AbstEvidenceExtractor {

	private static Normalizer geneSyn = null;
	private static Normalizer disSyn = null;
	private String inpath, outpath;

	public AbstEvidenceExtractor(String inpath, String outpath) {
		this.inpath = inpath;
		this.outpath = outpath;
		if (geneSyn == null)
			geneSyn = EscapeSynonymNormalizer.getGeneNormalizer();
		if (disSyn == null)
			disSyn = EscapeSynonymNormalizer.getDiseaseNormalizer();
	}

	public void extract() {
		GgrLogger.log("start extracting evidence...");

		try {
			BufferedReader br = new BufferedReader(new FileReader(inpath));
			Map<String, Map<String, Set<Integer>>> map = new HashMap<String, Map<String, Set<Integer>>>();
			Map<String, Set<Integer>> tmp = null;
			String str = null;
			int pmid = 0;
			int num = 0, count = 0;
			Set<String> gene = new HashSet<String>(), disease = new HashSet<String>();
			while ((str = br.readLine()) != null) {
				pmid = Integer.parseInt(str.substring("pmid: ".length(),
						str.lastIndexOf(' ')));
				num = Integer.parseInt(str.substring(str.lastIndexOf(' ') + 1));
				for (int i = 0; i < num; ++i) {
					List<NameHolder> genes = line2NameList(br.readLine());
					List<NameHolder> diseases = line2NameList(br.readLine());
					for (NameHolder nh : genes)
						gene.addAll(geneSyn.normalize(nh.name));
					for (NameHolder nh : diseases)
						disease.addAll(disSyn.normalize(nh.name));
                    br.readLine();
				}

				for (String dis : disease) {
					if (!map.containsKey(dis))
						map.put(dis, new HashMap<String, Set<Integer>>());
					tmp = map.get(dis);
					for (String gn : gene) {
						if (!tmp.containsKey(gn))
							tmp.put(gn, new HashSet<Integer>());
						tmp.get(gn).add(pmid);
					}
				}
				disease.clear();
				gene.clear();
				++count;
				if ((count & 0xfffff) == 0) {
					GgrLogger.log((count >> 20) + "M papers scanned..");
				}
			}
			br.close();

			BufferedWriter bw = new BufferedWriter(new FileWriter(outpath));
			for (String dis : map.keySet()) {
				for (String gn : map.get(dis).keySet()) {
					bw.write(dis + "$$$" + gn);
					Set<Integer> pmids = map.get(dis).get(gn);
					for( Integer p: pmids) {
						bw.write("$$$" + p);
					}
					bw.newLine();
				}
			}
			bw.flush();
			bw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		GgrLogger.log("finish extracting evidence...");
	}

	private List<NameHolder> line2NameList(String line) {
		List<NameHolder> res = new ArrayList<NameHolder>();
		if (line.trim().equals("")) {
			return res;
		}

		String tmp[] = line.split("\\$\\$\\$");
		for (int i = 0; i < tmp.length; i += 2) {
			res.add(new NameHolder().deserialize(tmp[i]));
		}

		return res;
	}

	public static void main(String[] argv) {
		String inpath = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"exe", "GENE_NETWORK_FILE_CLEAN");
		String outpath = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"evidence", "ABST_EVIDENCE");
		new AbstEvidenceExtractor(inpath, outpath).extract();
	}
}
