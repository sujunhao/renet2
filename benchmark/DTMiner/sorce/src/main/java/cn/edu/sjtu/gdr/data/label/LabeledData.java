package cn.edu.sjtu.gdr.data.label;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import cn.edu.sjtu.gdr.data.deptree.DepTree;
import cn.edu.sjtu.gdr.data.utils.NLPToolsForFeatures;
import cn.edu.sjtu.gdr.ggr.NamePosition;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.process.Morphology;
import edu.stanford.nlp.trees.GrammaticalRelation;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.TreeGraphNode;
import edu.stanford.nlp.trees.TypedDependency;

public class LabeledData {
	private static final boolean USE_PCFG_DEP_PARSER = false;
    private Morphology morph;
	
	String pmid;
	int num;

	String sentence;
	List<TaggedWord> tagged;
	GrammaticalStructure parse;
	DepTree depTree;
    List<String> lemma;

	List<LabeledGeneData> geneLabels;
	List<LabeledDiseaseData> diseaseLabels;
	List<LabeledRelationData> relationLabels;

	public LabeledData(String pmid, int num) {
		this.pmid = pmid;
		this.num = num;

		geneLabels = new ArrayList<LabeledGeneData>();
		diseaseLabels = new ArrayList<LabeledDiseaseData>();
		relationLabels = new ArrayList<LabeledRelationData>();
        morph = new Morphology();
	}

	public String getPmid() {
		return pmid;
	}

	public int getNum() {
		return num;
	}

	public String getSentence() {
		return sentence;
	}

	public List<TaggedWord> getTagged() {
		return tagged;
	}

    public String getTag(int index){return tagged.get(index).tag();}

	public GrammaticalStructure getParse() {
		return parse;
	}

    public List<String> getLemma(){return lemma;}

	public DepTree getDepTree() {
		return depTree;
	}

	public List<LabeledGeneData> getGeneLabels() {
		return geneLabels;
	}

	public List<LabeledDiseaseData> getDiseaseLabels() {
		return diseaseLabels;
	}

	public List<LabeledRelationData> getRelationLabels() {
		return relationLabels;
	}

	public void setSentence(String sentence) {
		this.sentence = sentence;
		parse();
	}

    public void set(String sentence, String parsedIn, List<String> tagList, List<String> wordList) {
        this.sentence = sentence;
        String parsedStr = parsedIn.substring(1,parsedIn.length()-2);
        String deps[] = parsedStr.split("\\), ");
        //String tags[] = taggedStr.split(", ");
        String keys[] = {"value", "word", "tag", "idx"};
        String values[] = {"","","",""};
        String keysR[] = {"value", "word", "idx"};
        String valuesR[] = {"ROOT", "ROOT", "0"};
        List<GrammaticalRelation> relns = new ArrayList<>();
        List<IndexedWord> indexedGovs = new ArrayList<>();
        List<IndexedWord> indexedDeps = new ArrayList<>();
        List<TypedDependency> tds = new ArrayList<>();
        tagged = new ArrayList<>();
        lemma = new ArrayList<>();

        for(int i=0; i<tagList.size(); i++){
            //String temp1 = tags[i].substring(0,tags[i].lastIndexOf("/"));
            //String temp2 = tags[i].substring(tags[i].lastIndexOf("/")+1,tags[i].length());
            tagged.add(new TaggedWord(wordList.get(i), tagList.get(i)));
            lemma.add(morph.lemma(wordList.get(i),tagList.get(i)));
        }

        for(int i=0; i<deps.length; i++){
            String reln = deps[i].substring(0,deps[i].indexOf("("));
            relns.add(GrammaticalRelation.valueOf((GrammaticalRelation.Language)GrammaticalRelation.Language.English, reln));
            String temp = deps[i].substring(deps[i].indexOf("(")+1, deps[i].indexOf(", "));
            values[0]=(temp.substring(0,temp.indexOf("-")));
            values[1]=values[0];
            values[3]=temp.substring(temp.lastIndexOf("-")+1,temp.length());
            if(!values[3].equals("0"))
                values[2]=tagged.get(Integer.parseInt(values[3])-1).tag();
            else values[2]=null;
            CoreLabel tempLabel = new CoreLabel(keys,values);
            indexedGovs.add(new IndexedWord(tempLabel));
            temp = deps[i].substring(deps[i].indexOf(", ")+2, deps[i].length());
            values[0]=(temp.substring(0,temp.indexOf("-")));
            values[1]=values[0];
            values[3]=temp.substring(temp.lastIndexOf("-")+1,temp.length());
            values[2]=tagged.get(Integer.parseInt(values[3])-1).tag();
            tempLabel = new CoreLabel(keys,values);
            indexedDeps.add(new IndexedWord(tempLabel));
        }
        for(int i=0; i<deps.length; i++){
            tds.add(new TypedDependency(relns.get(i), indexedGovs.get(i),indexedDeps.get(i)));
        }
        CoreLabel tempRoot = new CoreLabel(keysR,valuesR);
        TreeGraphNode root = new TreeGraphNode(tempRoot);
        parse = new GrammaticalStructure(tds, root){};

        depTree = new DepTree(tagged, parse);
    }

	private void parse() {
		DocumentPreprocessor dp = new DocumentPreprocessor(new StringReader(
				sentence));
		List<HasWord> sent = dp.iterator().next();
		tagged = NLPToolsForFeatures.tagger().tagSentence(sent);
        lemma = new ArrayList<>();
        for(int i=0; i<tagged.size(); ++i){
            lemma.add(morph.lemma(tagged.get(i).word(),tagged.get(i).tag()));
        }
		if (USE_PCFG_DEP_PARSER) {
			// pcfg parses
			parse = NLPToolsForFeatures.pcfgDepParse(sent);
		} else {
			// neural dependency parser
			parse = NLPToolsForFeatures.neuralDepParser().predict(tagged);
		}
		depTree = new DepTree(tagged, parse);
	}

	public void addGeneLabel(NamePosition geneIndex, int label) {
		geneLabels.add(new LabeledGeneData(this, geneLabels.size(), geneIndex,
				label));
	}

	public void addDiseaseLabel(NamePosition diseaseIndex, int label) {
		diseaseLabels.add(new LabeledDiseaseData(this, diseaseLabels.size(),
				diseaseIndex, label));
	}

	public void addRelationLabel(NamePosition geneIndex,
			NamePosition diseaseIndex, int label) {
		relationLabels.add(new LabeledRelationData(this, relationLabels.size(),
				geneIndex, diseaseIndex, label));
	}

    public void addGADRelationLabel(NamePosition geneIndex, // input is char offset
                                 NamePosition diseaseIndex, int label) {
        int startG=-1, endG=-1, startD=-1, endD=-1;
        int start, end, base =0;
        String gene = sentence.substring(geneIndex.getStart(),geneIndex.getEnd());
        String disease = sentence.substring(diseaseIndex.getStart(),diseaseIndex.getEnd());
        boolean getG = false, getD = false;
        cleanTagList();
        for(int i=0; i<tagged.size(); ++i){
            String startWord = tagged.get(i).word();
            start = sentence.indexOf(startWord,base);
            base = start+startWord.length();
            if(!getG) {
                start = recognizeStart(gene,startWord,start);
                if(start == geneIndex.getStart()) {
                    startG = i;
                    start = base - startWord.length();
                    for(int j=i; j<tagged.size(); ++j){
                        String endWord = tagged.get(j).word();
                        start = sentence.indexOf(endWord,start);
                        end = recognizeEnd(gene,endWord,start);
                        if(end == geneIndex.getEnd()){
                            endG = j+1;
                            getG = true;
                            break;
                        }
                        start += endWord.length();
                    }
                }
            }
            start = base-startWord.length();
            if(!getD) {
                start = recognizeStart(disease,startWord,start);
                if(start == diseaseIndex.getStart()) {
                    startD = i;
                    start = base-startWord.length();
                    for(int j=i; j<tagged.size(); ++j){
                        String endWord = tagged.get(j).word();
                        start = sentence.indexOf(endWord,start);
                        end = recognizeEnd(disease,endWord,start);
                        if(end == diseaseIndex.getEnd()){
                            endD = j+1;
                            getD= true;
                            break;
                        }
                        start += endWord.length();
                    }
                }
            }
            if(getD&&getG) break;
        }
        geneIndex.setStart(startG);
        geneIndex.setEnd(endG);
        diseaseIndex.setStart(startD);
        diseaseIndex.setEnd(endD);
        if(startD==-1||startG==-1||endD==-1||endG==-1)
            System.out.println("TAT");
        relationLabels.add(new LabeledRelationData(this, relationLabels.size(),
                geneIndex, diseaseIndex, label));
    }

    private int recognizeStart(String name, String startWord, int base){
        if(name.startsWith(startWord))
            return sentence.indexOf(startWord,base);
        if(startWord.endsWith(".")){
            String cleanStartWord = startWord.substring(0,startWord.length()-1);
            if(name.startsWith(cleanStartWord))
                return sentence.indexOf(cleanStartWord,base);
        }
        String[] splitters = {"-","/",Character.toString((char) 160), " ",".",",","+"};
        String s;
        for(String splitter : splitters) {
            if (startWord.contains(splitter)) {
                if(splitter.equals("."))
                    s = "\\.";
                else if(splitter.equals("+"))
                    s = "\\+";
                else
                    s = splitter;
                String[] tmp = startWord.split(s);
                String testWord;
                for (int i = 0; i < tmp.length; ++i) {
                    for (int j = i; j < tmp.length; ++j) {
                        testWord = "";
                        for (int k = i; k <= j; ++k) {
                            testWord += tmp[k] + splitter;
                        }
                        testWord = testWord.substring(0, testWord.length() - 1);
                        if (name.startsWith(testWord) && !testWord.equals(""))
                            return sentence.indexOf(testWord, base);
                    }
                }
            }
        }
        return -1;
    }

    private int recognizeEnd(String name, String endWord, int start){
        if(name.endsWith(endWord))
            return sentence.indexOf(endWord,start)+endWord.length();
        if(endWord.endsWith(".")) {
            String cleanEndWord = endWord.substring(0, endWord.length() - 1);
            if(name.endsWith(cleanEndWord))
                return sentence.indexOf(cleanEndWord,start)+cleanEndWord.length();
        }
        String[] splitters = {"-","/",Character.toString((char) 160), " ",".",",","+"};
        String s;
        for(String splitter : splitters) {
            if (endWord.contains(splitter)) {
                if(splitter.equals("."))
                    s = "\\.";
                else if(splitter.equals("+"))
                    s = "\\+";
                else
                    s = splitter;
                String[] tmp = endWord.split(s);
                String testWord;
                for (int i = 0; i < tmp.length; ++i) {
                    for (int j = i; j < tmp.length; ++j) {
                        testWord = "";
                        for (int k = i; k <= j; ++k) {
                            testWord += tmp[k] + splitter;
                        }
                        testWord = testWord.substring(0, testWord.length() - 1);
                        if (name.endsWith(testWord) && !testWord.equals(""))
                            return sentence.indexOf(testWord, start) + testWord.length();
                    }
                }
            }
        }
        return -1;
    }

    private void cleanTagList(){
        for(int i=0; i<tagged.size(); ++i){
            String word = tagged.get(i).word();
            if(word.contains(Character.toString((char) 160)))
                word = word.replaceAll(Character.toString((char) 160)," ");
            if(word.equals("-LRB-"))
                word = "(";
            if(word.equals("-RRB-"))
                word = ")";
            if(word.equals(Character.toString((char) 96)+Character.toString((char) 96)))
                word = Character.toString((char) 39)+Character.toString((char) 39);
            tagged.get(i).setWord(word);
        }
    }
	public String getIdentifier() {
		return pmid + "-" + num;
	}

}
