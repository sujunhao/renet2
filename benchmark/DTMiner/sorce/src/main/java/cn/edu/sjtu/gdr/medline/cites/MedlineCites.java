package cn.edu.sjtu.gdr.medline.cites;

import java.util.ArrayList;
import java.util.List;

import edu.stanford.nlp.util.StringUtils;

public class MedlineCites {
	private String pmid;
	private List<String> cites;

	public MedlineCites() {
		this("");
	}

	public MedlineCites(String pmid) {
		this.pmid = pmid;
		cites = new ArrayList<String>();
	}

	public void addCites(String pmid) {
		cites.add(pmid);
	}

	public String getPmid() {
		return pmid;
	}

	public void setPmid(String pmid) {
		this.pmid = pmid;
	}

	public List<String> getCites() {
		return cites;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("pmid: ").append(pmid).append(System.lineSeparator());
		sb.append(StringUtils.join(cites, "\t"));
		return sb.toString();
	}
}
