package cn.edu.sjtu.gdr.exe;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.stanford.nlp.ling.CoreLabel;
import rdi.cn.source.MedlineItem;
import cn.edu.sjtu.gdr.async.netext.NetworkOutputData;
import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.ggr.NameHolder;
import cn.edu.sjtu.gdr.ggr.NamePosition;
import cn.edu.sjtu.gdr.ggr.NameRecognizer;
import cn.edu.sjtu.gdr.ggr.NameRecognizerResult;
import cn.edu.sjtu.gdr.medline.abst.ArticleTitlePumper;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.process.DocumentPreprocessor;

/**
 * extract genes and diseases from title;
 * 
 * @author dong
 *
 */
public class GeneExtractorFromTitle {

	private ArticleTitlePumper pumper;
	private NameRecognizer recognizer;

	public GeneExtractorFromTitle() {
		pumper = new ArticleTitlePumper(ConfigManager.getConfig("exe",
				"MEDLINE_ARTICLE_TITLE"));
		recognizer = new NameRecognizer();
	}

	public void process(String path) throws IOException {
		/*DocumentPreprocessor dp = null;
		String pmid = null, title = null;
		MedlineItem item = null;

		BufferedWriter bw = new BufferedWriter(new FileWriter(path));
		int paperCount = 0;
		while (pumper.hasNext()) {
			item = pumper.next();
			pmid = item.getPmid();
			title = item.getTitle();
			dp = new DocumentPreprocessor(new StringReader(title));
			NetworkOutputData data = new NetworkOutputData(pmid);
			for (List<HasWord> sentence : dp) {
                int begin = ((CoreLabel) sentence.get(0)).beginPosition();
                int end = ((CoreLabel) sentence.get(sentence.size() - 1)).endPosition();
                Set<String> longTerm = new HashSet<>();
				NameRecognizerResult recogRes = recognizer.recognize(sentence, longTerm, " ");
				data.addData(
						translateResult(recogRes.genes, recogRes.genesPos,
								recogRes.genesSenIndex),
						translateResult(recogRes.diseases,
								recogRes.diseasesPos, recogRes.diseasesSenIndex),title.substring(begin,end));
			}

			bw.write(data.serialize());
			bw.newLine();

			++paperCount;
			if ((paperCount & 0xfffff) == 0) {
				System.out.println((paperCount >> 20) + "M papers processed");
			}
		}
		System.out.println((paperCount >> 20) + "M papers processed");
		bw.flush();
		bw.close();*/
	}

	private List<NameHolder> translateResult(List<String> names,
			List<NamePosition> namePos, List<NamePosition> senIndex) {
		List<NameHolder> res = new ArrayList<NameHolder>();
		for (int i = 0; i < names.size(); ++i) {
			NameHolder nh = new NameHolder();
			nh.name = names.get(i);
			//nh.beginPos = namePos.get(i).getStart();
			//nh.endPos = namePos.get(i).getEnd();
			nh.beginIndex = senIndex.get(i).getStart();
			nh.endIndex = senIndex.get(i).getEnd();
			res.add(nh);
		}
		return res;
	}

	public static void main(String[] args) throws IOException {
		new GeneExtractorFromTitle().process(ConfigManager.getConfig("exe",
				"ARTICLE_TITLE_RECOG"));
	}
}
