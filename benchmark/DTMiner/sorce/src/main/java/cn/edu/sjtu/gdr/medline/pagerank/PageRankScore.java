package cn.edu.sjtu.gdr.medline.pagerank;

import java.util.Collection;

public abstract class PageRankScore {

	public abstract double score(int pmid);

	public double score(Collection<Integer> pmids) {
		double res = 0;
		for (Integer pmid : pmids) {
			res += score(pmid);
		}
		return res;
	}
}
