package cn.edu.sjtu.gdr.obo;

import java.util.List;
import java.util.Set;

public interface Normalizer {

	boolean canNormalize(String name);
    boolean canNormalize(Set<String> name);
    List<String> normalize(String name);
	List<String> normalize(String name, String cleanName, Set<String> splitList);
}
