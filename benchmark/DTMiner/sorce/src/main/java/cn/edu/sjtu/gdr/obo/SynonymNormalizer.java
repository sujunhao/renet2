package cn.edu.sjtu.gdr.obo;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cn.edu.sjtu.gdr.config.ConfigManager;

/**
 * All name are lower case
 * 
 * @author Jack Sun(jacksunwei@gmail.com)
 *
 */
public class SynonymNormalizer implements Normalizer {

	private Set<String> norm;
	private Map<String, List<String>> map;
    private String pathIn = "";
    private String pathDic = "res/data/2of12inf.txt";
    private String pathOut = "res/data/prog-output/diseaseCommon.txt";

	public SynonymNormalizer(String path) {
		norm = new HashSet<String>();
		map = new HashMap<String, List<String>>();
        pathIn = path;
		readAll(path);
	}

	@Override
	public boolean canNormalize(String str) {
		return norm.contains(str) || map.containsKey(str);
	}

    @Override
    public boolean canNormalize(Set<String> str) {
        return norm.contains(str) || map.containsKey(str);
    }

	@Override
     public List<String> normalize(String str) {
        str = str.toLowerCase();
        List<String> res = new ArrayList<String>();
        if (norm.contains(str)) {
            res.add(str);
        }
        if (map.containsKey(str)) {
            res.addAll(map.get(str));
        }
        return res;
    }

    @Override
    public List<String> normalize(String str, String str1, Set<String> splitList) {
        str = str.toLowerCase();
        List<String> res = new ArrayList<String>();
        if (norm.contains(str)) {
            res.add(str);
        }
        if (map.containsKey(str)) {
            res.addAll(map.get(str));
        }
        return res;
    }

	private void readAll(String path) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			String str = null, names[] = null;

			while ((str = br.readLine()) != null) {
				names = str.split(OboTerm.SPLITTER);
				// names goes to lower case
				for (int i = 0; i < names.length; ++i) {
                    names[i] = names[i].toLowerCase();
                    names[i] = names[i].replaceAll(" ", "");
                }
				
				if (norm.contains(names[0])) {
					br.close();
					throw new RuntimeException(
							"fuck you! you give me two same name! " + names[0]);
				}
				norm.add(names[0]);
				for (int i = 1; i < names.length; ++i) {
					if (!map.containsKey(names[i])) {
						map.put(names[i], new ArrayList<String>());
					}
					map.get(names[i]).add(names[0]);
				}
			}

			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

    private void writeNorm(){
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(pathOut));
            for(String name : norm){
                bw.write(name);
                bw.write("\n");
            }
            bw.close();
            System.out.println("finish writing synonyms...");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeCommon() {
        try {
            BufferedReader br = new BufferedReader(new FileReader(pathIn));
            BufferedReader brDic = new BufferedReader(new FileReader(pathDic));
            BufferedWriter bw = new BufferedWriter(new FileWriter(pathOut));
            String str = null, words[] = null, phrases[] = null;
            Set<String> wordSet = new HashSet<>();

            while ((str = br.readLine()) != null) {
                phrases = str.split(OboTerm.SPLITTER);
                // names goes to lower case
                for (int i = 0; i < phrases.length; ++i) {
                    words = phrases[i].split("\\s+");
                    for(int j = 0; j < words.length; j++) {
                        words[j] = words[j].toLowerCase();
                        wordSet.add(words[j]);
                    }
                }
            }
            br.close();
            System.out.println("finish reading synonyms...");

            while ((str = brDic.readLine()) != null){
                if(wordSet.contains(str)){
                    bw.write(str);
                    bw.write("\n");
                }
            }
            brDic.close();
            bw.close();
            System.out.println("finish writing synonyms...");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void combineCommon(){
        try {
            BufferedReader brG = new BufferedReader(new FileReader("res/data/prog-output/geneCommon.txt"));
            BufferedReader brD = new BufferedReader(new FileReader("res/data/prog-output/diseaseCommon.txt"));
            BufferedWriter bw = new BufferedWriter(new FileWriter("res/data/prog-output/commonWord.txt"));
            String str = null, words[] = null, phrases[] = null;
            Set<String> wordSet = new HashSet<>();

            while ((str = brG.readLine()) != null) {
                wordSet.add(str);
            }
            while ((str = brD.readLine()) != null) {
                if(!wordSet.contains(str))
                    wordSet.add(str);
            }
            brG.close();
            brD.close();
            System.out.println("finish reading...");

            for(String word : wordSet){
                bw.write(word);
                bw.write("\n");
            }
            bw.close();
            System.out.println("finish writing...");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

	private static Object m1 = new Object(), m2 = new Object();
	private static SynonymNormalizer geneSyn = null, diseaseSyn = null;

	public static SynonymNormalizer getGeneSynNorm() {
		if (geneSyn == null) {
			synchronized (m1) {
				if (geneSyn == null) {
					geneSyn = new SynonymNormalizer(ConfigManager.getConfig(
							ConfigManager.MAIN_CONFIG, "obo", "GENE_SYN"));
				}
			}
		}
		return geneSyn;
	}

	public static SynonymNormalizer getDiseaseSynNorm() {
		if (diseaseSyn == null) {
			synchronized (m2) {
				if (diseaseSyn == null) {
					diseaseSyn = new SynonymNormalizer(ConfigManager.getConfig(
							ConfigManager.MAIN_CONFIG, "obo", "DISEASE_SYN"));
				}
			}
		}
		return diseaseSyn;
	}

	private static void output(String gene, List<String> norm) {
		System.out.format("norm of {%s}: %s\n", gene, norm);
	}

	public static void main(String[] argv) {
		SynonymNormalizer norm = new SynonymNormalizer(ConfigManager.getConfig(
				ConfigManager.MAIN_CONFIG, "obo", "DISEASE_SYN"));
		/*
        String test[] = { "msr1", "h sapiens",
				"mitochondrially encoded cytochrome c oxidase i",
				"zinc finger protein 265" };
		for (int i = 0; i < test.length; ++i) {
			output(test[i], norm.normalize(test[i]));
		}
		while (null != System.console()) {
			System.out.print("gene? ");
			String gene = System.console().readLine();
			output(gene, norm.normalize(gene));
		}
		
		norm = getDiseaseSynNorm();
		for( String s: norm.normalize("Lung Neoplasms"))
			System.out.println(s);
		*/
        //norm.writeCommon();
        norm.combineCommon();
        //norm.writeNorm();
	}
}
