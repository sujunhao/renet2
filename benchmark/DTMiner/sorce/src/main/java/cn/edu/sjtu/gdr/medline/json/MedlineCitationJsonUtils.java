 package cn.edu.sjtu.gdr.medline.json;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

public class MedlineCitationJsonUtils {

	private static final String[] titlePath = new String[] { "medline",
			"MedlineCitation", "Article", "ArticleTitle" };

	public static String findTitle(MedlineCitationJson mc) {
		return mc.getPathOrDefaultString(titlePath, "");
	}

	private static final String[] abstractPath = new String[] { "medline",
			"MedlineCitation", "Article", "Abstract", "AbstractText" };

	public static List<String> findAbstract(MedlineCitationJson mc) {
		List<String> res = new ArrayList<String>();

		JsonNode node = mc.getPath(abstractPath);
		if (node.isTextual()) {
			res.add(node.textValue());
		} else if (node.isArray()) {
			ArrayNode arr = (ArrayNode) node;
			for (int i = 0; i < arr.size(); ++i) {
				JsonNode tmp = arr.get(i).path("#text");
				if (tmp.isTextual())
					res.add(tmp.textValue());
			}
		}

		return res;
	}

	private static final String[] pmidPath = new String[] { "medline",
			"MedlineCitation", "PMID", "#text" };

	public static int findPmid(MedlineCitationJson mc) {
		return mc.getPathOrDefaultInt(pmidPath, 0);
	}
}
