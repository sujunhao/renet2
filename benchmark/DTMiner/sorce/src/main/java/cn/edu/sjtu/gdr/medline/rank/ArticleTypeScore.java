package cn.edu.sjtu.gdr.medline.rank;

import java.util.Set;

import cn.edu.sjtu.gdr.medline.pagerank.articletype.ArticleTypeCodeContainer;

public class ArticleTypeScore implements Score {

	private static ArticleTypeCodeContainer articleCode = null;
	public double score(Set<Integer> pmids) {
		if( articleCode == null )
			articleCode = ArticleTypeCodeContainer.getInstance();
		
		double score = 0;
		for( Integer pmid: pmids )
			if( articleCode.getCode(pmid).isClinicalTrial() )
				score += 1;
		return score;
	}
}
