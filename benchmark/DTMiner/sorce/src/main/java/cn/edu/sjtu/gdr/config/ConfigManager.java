package cn.edu.sjtu.gdr.config;

import java.util.HashMap;
import java.util.Map;

public class ConfigManager {
	public static final String MAIN_CONFIG = "gdr-config.ini";
	
	public static final String DEFAULT_CONFIG = MAIN_CONFIG;
	
	private static Map<String, IniParser> config = new HashMap<String, IniParser>();
	
	public static String getConfig(String file, String section, String key) {
		if (config.get(file) == null) {
			config.put(file, new IniParser(file));
		}
		return config.get(file).getConfig(section, key);
	}
	
	public static String getConfig(String section, String key) {
		return getConfig(DEFAULT_CONFIG, section, key);
	}
}
