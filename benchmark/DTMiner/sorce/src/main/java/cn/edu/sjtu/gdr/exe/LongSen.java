package cn.edu.sjtu.gdr.exe;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Xie Yanping on 2015/12/4.
 */
public class LongSen {
    static private int ratio = 1000;
    static private int recordCount = 0;
    static private boolean startOrEnd = true;
    static private Map<Integer, Integer> counts = new HashMap<>();
    static private String inPath = "res/data/prog-output/gene-network-clean1.txt";
    //static private String outPath = "res/data/prog-output/long-sen-count-start.txt";
    static private String outPath = "res/data/prog-output/long-sen.txt";
    static private void run() throws IOException{
        int count = 0, num, dist;
        BufferedReader br = new BufferedReader(new FileReader(inPath));
        BufferedWriter bw = new BufferedWriter(new FileWriter(outPath));
        String line, line1, line2, line3, line4, line5, pmid;
        while ((line = br.readLine())!=null){
            ++count;
            pmid = line.substring(6,line.lastIndexOf(" "));
            num = Integer.parseInt(line.substring(line.lastIndexOf(" ")+1));
            for(int i=0; i<num; i++){
                List<String> gene = new ArrayList<>(), disease = new ArrayList<>();
                List<Integer> geneS = new ArrayList<>(), diseaseS = new ArrayList<>();
                List<Integer> geneE = new ArrayList<>(), diseaseE = new ArrayList<>();
                //read genes
                if(!(line1 = br.readLine()).equals("")){
                    String[] tmps = line1.split("\\$\\$\\$");
                    for(String tmp : tmps){
                        //if(startOrEnd)
                        gene.add(tmp.substring(0,tmp.indexOf("##")));
                        geneS.add(Integer.parseInt(tmp.substring(tmp.indexOf("##")+2,tmp.lastIndexOf("##"))));
                        //else
                        geneE.add(Integer.parseInt(tmp.substring(tmp.lastIndexOf("##")+2)));
                    }
                }
                //read diseases
                if(!(line2 = br.readLine()).equals("")){
                    String[] tmps = line2.split("\\$\\$\\$");
                    for(String tmp : tmps){
                        //if(startOrEnd)
                        disease.add(tmp.substring(0,tmp.indexOf("##")));
                        diseaseS.add(Integer.parseInt(tmp.substring(tmp.indexOf("##") + 2, tmp.lastIndexOf("##"))));
                        //else
                        diseaseE.add(Integer.parseInt(tmp.substring(tmp.lastIndexOf("##")+2)));
                    }
                }
                //for(int j=0; j<3; j++)
                line3 = br.readLine();
                line4 = br.readLine();
                line5 = br.readLine();
                if(count%ratio==0 && geneS.size()>0 && diseaseS.size()>0){
                    System.out.println(pmid);
                    for(int m =0; m<gene.size(); ++m){
                        for(int n=0; n<disease.size();++n){
                            ++recordCount;
                            dist = geneS.get(m)>=diseaseS.get(n)?geneS.get(m)-diseaseS.get(n):diseaseS.get(n)-geneS.get(m);
                            if(dist>=20){
                                bw.write(gene.get(m)+"##"+geneS.get(m)+"##"+geneE.get(m));
                                bw.newLine();
                                bw.write(disease.get(n)+"##"+diseaseS.get(n)+"##"+diseaseE.get(n));
                                bw.newLine();
                                bw.write(line3);
                                bw.newLine();
                                bw.write(line4);
                                bw.newLine();
                                bw.write(line5);
                                bw.newLine();
                            }
                            /*if(counts.containsKey(dist)){
                                counts.put(dist,counts.get(dist)+1);
                            }else
                                counts.put(dist,1);*/
                        }
                    }
                }
            }
        }
        br.close();
        bw.flush();
        bw.close();
        //output();
    }

    static public void output() throws IOException{
        BufferedWriter bw = new BufferedWriter(new FileWriter(outPath));
        for(Integer num : counts.keySet()){
            bw.write(num+"\t"+counts.get(num));
            bw.newLine();
        }
        bw.write(recordCount);
        bw.flush();
        bw.close();
    }

    static public void main(String[] args) throws IOException{
        if(args.length>=1 && args[0].equals("0")) {
            startOrEnd = false;
            outPath = "res/data/prog-output/long-sen-count-end.txt";
        }
        if(args.length>=2)
            inPath = args[1];
        run();
    }
}
