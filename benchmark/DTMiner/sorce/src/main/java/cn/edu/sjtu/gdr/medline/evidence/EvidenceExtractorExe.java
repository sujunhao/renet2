package cn.edu.sjtu.gdr.medline.evidence;

import java.io.*;
import java.util.*;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.data.feature.RelationFeatureSet1;
import cn.edu.sjtu.gdr.data.forsvm.RunSvmKernel;
import cn.edu.sjtu.gdr.data.label.LabeledData;
import cn.edu.sjtu.gdr.data.label.LabeledRelationData;
import cn.edu.sjtu.gdr.ggr.NameHolder;
import cn.edu.sjtu.gdr.ggr.NamePosition;
import cn.edu.sjtu.gdr.obo.EscapeSynonymNormalizer;
import cn.edu.sjtu.gdr.obo.Hierarchy;
//import cn.edu.sjtu.gdr.obo.name.NameEscaper;
import cn.edu.sjtu.gdr.obo.Normalizer;
import cn.edu.sjtu.gdr.obo.SynonymNormalizer;
import cn.edu.sjtu.gdr.utils.GgrLogger;
//import cn.edu.sjtu.gdr.obo.SynonymNormalizer;
import edu.stanford.nlp.process.Morphology;
import svmJavaKernel.libsvm.ex.SVMPredictor;
import svmJavaKernel.libsvm.svm_model;

public class EvidenceExtractorExe {

	public static final String SPLITTER = "\t";
	private static final String UUID_TEMPLATE = "%s-%d-%d-%d";
    /*public static String[] test = {"doid:784", "doid:263",
            "doid:3021", "doid:1287", "doid:3683"};*/
    private Hierarchy h = Hierarchy.getDisHierarchy();
	private String inDir, outDir;
    private Normalizer geneSyn = EscapeSynonymNormalizer.getGeneNormalizer();
    private Normalizer disSyn = EscapeSynonymNormalizer.getDiseaseNormalizer();
    private svm_model model;
    private RunSvmKernel kernel = new RunSvmKernel();
    private int generationSize = 1;
    private Morphology morph;

	public EvidenceExtractorExe(String inpath, String outpath) throws IOException, ClassNotFoundException{
		this.inDir = inpath;
		this.outDir = outpath;
        model = SVMPredictor.loadModel("svm/hybrid.model");
        morph = new Morphology();
	}

	public void extract(String input) throws ClassNotFoundException{
		GgrLogger.log("start extracting evidence...");
        Boolean flag = input.equals("")? true : false;
		try {
            File dirFilePar = new File(inDir);
            File[] filePar = dirFilePar.listFiles();
            Arrays.sort(filePar, new Comparator<File>() {
                public int compare(File o1, File o2) {
                    return o1.getName().compareTo(o2.getName());
                }
            });

            String str, pmid;
            int num, count = 0;

            for(File f : filePar) {
                System.out.println(f.getName());
                BufferedReader br = new BufferedReader(new FileReader(f));
                BufferedWriter bw = new BufferedWriter(new FileWriter(outDir + f.getName()));

                while ((str = br.readLine()) != null) {
                    pmid = str.substring("pmid: ".length(), str.lastIndexOf(' '));
                    if (pmid.equals(input))
                        flag = true;
                    if(flag)
                        GgrLogger.log(pmid);
                    num = Integer.parseInt(str.substring(str.lastIndexOf(' ') + 1));
                    Set<String> geneNormSet = new HashSet<>(), disNormSet = new HashSet<>();
                    for (int i = 0; i < num; ++i) {
                        List<NameHolder> genes = line2NameList(br.readLine());
                        List<NameHolder> diseases = line2NameList(br.readLine());
                        String sentence = br.readLine();
                        String parsed = br.readLine();
                        String tagged = br.readLine();
                        if (flag) {
                            if(genes.size()>0 && diseases.size()>0)
                                record(bw, pmid, i, genes, diseases, sentence, parsed, tagged, geneNormSet, disNormSet);
                        }
                    }
                    bw.flush();
                    ++count;
                    if ((count & 0xfffff) == 0) {
                        GgrLogger.log((count >> 20) + "M papers scanned..");
                    }
                }

                br.close();
                bw.flush();
                bw.close();
            }
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
            e.printStackTrace();
        }

		GgrLogger.log("finish extracting evidence...");
	}

	private void record(BufferedWriter bw, String pmid, int senNum,
			List<NameHolder> genes, List<NameHolder> diseases, String sentence, String parsed, String tagged,
            Set<String> geneNormSet, Set<String> disNormSet)
			throws IOException, ClassNotFoundException {
		String uuid;
        List<String> wordList = new ArrayList<>(), tagList = new ArrayList<>();
        parseTag(tagList,wordList,tagged);
		for (int i = 0; i < genes.size(); ++i) {
			for (int j = 0; j < diseases.size(); ++j) {
                String disLiter = diseases.get(j).name;
                List<String> disNorms = disSyn.normalize(disLiter);
                if(disNorms.size() == 0){
                    String lastWord = wordList.get(diseases.get(j).endIndex-1);
                    String lemma = getLemma(tagList.get(diseases.get(j).endIndex-1),lastWord);
                    disLiter = disLiter.replace(lastWord, lemma);
                    disNorms = disSyn.normalize(disLiter);
                }
                if(disNorms.size() == 0)
                    System.out.println(diseases.get(j).name+" not found in dict");
                else if(disNorms.size() == 1)
                    addNormSet(disNormSet,disNorms);
                else
                    normDisambiguation(disNormSet,disNorms);
                String geneLiter = genes.get(i).name;
                List<String> geneNorms = geneSyn.normalize(geneLiter);
                if(geneNorms.size() == 0){
                    String lastWord = wordList.get(genes.get(i).endIndex-1);
                    String lemma = getLemma(tagList.get(genes.get(i).endIndex-1),lastWord);
                    geneLiter = geneLiter.replace(lastWord,lemma);
                    geneNorms = geneSyn.normalize(geneLiter);
                }
                if(geneNorms.size() == 0)
                    System.out.println(genes.get(i).name+" not found in dict");
                else if(geneNorms.size() == 1)
                    addNormSet(geneNormSet,geneNorms);
                else
                    normDisambiguation(geneNormSet,geneNorms);
                for (String disNorm : disNorms) {
                    /*if(!isInTest(disNorm))
                        continue;*/
                    for (String geneNorm : geneNorms) {
                        String feature = null;
                        LabeledData data = new LabeledData(pmid,
                                senNum);
                        data.set(sentence, parsed, tagList,wordList);
                        data.addRelationLabel(new NamePosition(genes.get(i).beginIndex,
                                genes.get(i).endIndex), new NamePosition(diseases.get(j).beginIndex,
                                diseases.get(j).endIndex), 1);
                        RelationFeatureSet1 rfs1 = new RelationFeatureSet1();
                        for (LabeledRelationData relData : data.getRelationLabels()) {
                            feature = rfs1.getHybridFeature(relData,4);
                        }
                        double prediction = kernel.runKernel(feature, model);
                        //double prediction = 1.0;
                        uuid = String.format(UUID_TEMPLATE, pmid, senNum, i, j);
                        bw.write(uuid);
                        bw.write(SPLITTER);
                        bw.write(geneNorm);
                        bw.write(SPLITTER);
                        bw.write(disNorm);
                        bw.write(SPLITTER);
                        bw.write(genes.get(i).name);
                        bw.write(SPLITTER);
                        bw.write(diseases.get(j).name);
                        bw.write(SPLITTER);
                        bw.write(Double.toString(prediction));
                        bw.newLine();
                    }
                }
			}
		}
	}

    private void addNormSet(Set<String> normSet, List<String> norms){
        for(String norm : norms)
            normSet.add(norm);
    }
    private void normDisambiguation(Set<String> normSet, List<String> norms){
        List<String> tmp = new ArrayList<>();
        for (String norm : norms) {
            if (normSet.contains(norm)) {
                tmp.add(norm);
            }
        }
        if (tmp.size() != 0) {
            norms.clear();
            norms.addAll(tmp);
        } else {
            normSet.addAll(norms);
        }
    }
    private void parseTag(List<String> tagList, List<String> wordlist, String taggedIn) {
        String taggedStr = taggedIn.substring(1, taggedIn.length() - 1);
        String tags[] = taggedStr.split(", ");
        tagList.clear();
        wordlist.clear();
        for (int i = 0; i < tags.length; i++) {
            String temp = tags[i].substring(tags[i].lastIndexOf("/") + 1);
            String word = tags[i].substring(0, tags[i].lastIndexOf("/"));
            tagList.add(temp);
            wordlist.add(word);
        }
    }
    private String getLemma(String tag, String name) {
        String lemma = morph.lemma(name, tag);
        return lemma;
    }

    /*private boolean isInTest(String disNorm){
        for(String tmp1 : test){
            if(tmp1.equals(disNorm))
                return true;
            else{
                Set<String> ancestors = h.getAllAncestors(disNorm,generationSize);
                for(String tmp2 : ancestors){
                    if(tmp1.equals(tmp2))
                        return true;
                }
            }
        }
        return false;
    }*/

	private List<NameHolder> line2NameList(String line) {
		List<NameHolder> res = new ArrayList<NameHolder>();
		if (line.trim().equals("")) {
			return res;
		}
		
		String tmp[] = line.split("\\$\\$\\$");
		for (int i = 0; i < tmp.length; i += 1) {
			res.add(new NameHolder().deserialize(tmp[i]));
		}
		
		return res;
	}

	public static void main(String[] argv) throws IOException,ClassNotFoundException{
		String inDir = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"exe", "NER_DIR")+argv[0];
        System.out.println("inpath: "+inDir);
		String outDir = "res/data/prog-output/gd-evidence/";
        if(argv.length == 2 )
            new EvidenceExtractorExe(inDir, outDir).extract(argv[1]);
        else
            new EvidenceExtractorExe(inDir, outDir).extract("");
	}
}
