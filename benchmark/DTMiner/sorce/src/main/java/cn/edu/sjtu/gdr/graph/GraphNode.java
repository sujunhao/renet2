package cn.edu.sjtu.gdr.graph;

import java.util.Collection;

public abstract class GraphNode {

	protected int id;

	public GraphNode(int id) {
		this.id = id;
	}

	public void addOutEdge(int id) {
		addOutEdge(id, 1);
	}

	public void addInEdge(int id) {
		addInEdge(id, 1);
	}

	public abstract void addOutEdge(int id, int weight);

	public abstract void addInEdge(int id, int weight);

	public abstract void removeOutEdge(int id);

	public void removeOutEdges(Collection<Integer> ids) {
		for (int id : ids) {
			removeOutEdge(id);
		}
	}
	
	public abstract void removeInEdge(int id);
	
	public void removeInEdges(Collection<Integer> ids) {
		for (int id : ids) {
			removeInEdge(id);
		}
	}

	public abstract int getOutEdgeWeight(int nodeId);

	public abstract int getInEdgeWeight(int nodeId);

	public abstract Collection<Integer> getOutEdges();

	public abstract Collection<Integer> getInEdges();

	public abstract int getOutDegree();

	public abstract int getInDegree();

	public int getId() {
		return this.id;
	}
}
