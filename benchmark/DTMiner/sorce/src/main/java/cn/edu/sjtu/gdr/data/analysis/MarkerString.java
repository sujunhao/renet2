package cn.edu.sjtu.gdr.data.analysis;

import java.util.ArrayList;
import java.util.List;

import cn.edu.sjtu.gdr.ggr.NameRecognizerResult;
import edu.stanford.nlp.util.StringUtils;

public class MarkerString {
	private static final String GENE_RECOGNIZE_FORMAT = "gene \"%s\" recognize: ";
	private static final String DISEASE_RECOGNIZE_FORMAT = "disease \"%s\" recognize: ";
	private static final String RELATION_FORMAT = "gene \"%s\" and disease \"%s\" relation: ";

	public MarkerString() {

	}

	public String markString(NameRecognizerResult res) {
		List<String> buffer = new ArrayList<String>(res.genes.size()
				+ res.diseases.size() + res.genes.size() * res.diseases.size());

		for (String gene : res.genes) {
			buffer.add(String.format(GENE_RECOGNIZE_FORMAT, gene));
		}
		
		for (String disease : res.diseases) {
			buffer.add(String.format(DISEASE_RECOGNIZE_FORMAT, disease));
		}
		
		for (String gene : res.genes) {
			for (String disease : res.diseases) {
				buffer.add(String.format(RELATION_FORMAT, gene, disease));
			}
		}

		return StringUtils.join(buffer, "\n");
	}
	
}
