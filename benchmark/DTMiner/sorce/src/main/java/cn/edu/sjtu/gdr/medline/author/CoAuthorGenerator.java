package cn.edu.sjtu.gdr.medline.author;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.graph.UndirectedGraph;
import cn.edu.sjtu.gdr.graph.UndirectedGraphOutputter;
import cn.edu.sjtu.gdr.utils.GgrLogger;

public class CoAuthorGenerator {

	private String authorMetaPath, graphPath;

	public CoAuthorGenerator(String authorMetaPath, String graphPath) {
		this.authorMetaPath = authorMetaPath;
		this.graphPath = graphPath;
	}

	public void run() throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(authorMetaPath));
		UndirectedGraph graph = new UndirectedGraph();

		String str = null, idstr[] = null;
		List<Integer> ids = new ArrayList<Integer>();
		br.readLine(); // skip empty line
		int count = 0;

		while ((str = br.readLine()) != null) {
			str = br.readLine().trim(); // read the name id line
			if (str.length() > 0) {
				idstr = str.split("\t");
				ids.clear();
				for (String s : idstr) {
					ids.add(Integer.parseInt(s));
				}
				
				for (int i = 0; i < ids.size(); ++i) {
					for (int j = i + 1; j < ids.size(); ++j) {
						if (i != j) {
							graph.ensureAndGetNode(ids.get(i)).addOutEdge(ids.get(j));
							graph.ensureAndGetNode(ids.get(j)).addOutEdge(ids.get(i));
						}
					}
				}
			}
			
			++count;
			if ((count & 0xfffff) == 0) {
				GgrLogger.log((count >> 20) + "M papers processed...");
			}
		}

		br.close();
		
		GgrLogger.log("start outputing graph... ");
		new UndirectedGraphOutputter(graph, graphPath).output();;
		GgrLogger.log("finish outputing graph...");
	}

	public static void main(String[] args) throws IOException {
		String authorPath = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"author", "AUTHOR_META_ID_PATH");
		String graphPath = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"author", "COAUTHOR_GRAPH_PATH");
		new CoAuthorGenerator(authorPath, graphPath).run();
	}

}
