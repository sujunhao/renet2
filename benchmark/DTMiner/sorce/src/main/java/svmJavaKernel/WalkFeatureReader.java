package svmJavaKernel;

import svmJavaKernel.libsvm.ex.Instance;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Xie Yanping on 2015/7/29.
 */
public class WalkFeatureReader {
    public static Instance[] readWalkFeature(String fileName) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(fileName));

        ArrayList<Double> labels = new ArrayList<Double>();
        ArrayList<Map<Integer,String>> mems = new ArrayList<Map<Integer,String>>();

        String line;
        int lineCount = 0;
        while ((line = reader.readLine()) != null) {
            lineCount++;
            String[] tokens = line.split("\\s+");
            if (tokens.length < 2) {
                System.err.println("Inappropriate file format: " + fileName);
                System.err.println("Error in line " + lineCount);
                System.exit(-1);
            }

            labels.add(Double.parseDouble(tokens[0]));
            //SparseVector vector = new SparseVector(tokens.length - 1);
            Map<Integer, String> mem = new HashMap<>();

            for (int i = 1; i < tokens.length; i++) {
                String[] fields = tokens[i].split(":");
                if (fields.length < 2) {
                    System.err.println("Inappropriate file format: " + fileName);
                    System.err.println("Error in line " + lineCount);
                    System.exit(-1);
                }
                int index = Integer.parseInt(fields[0]);
                mem.put(index, fields[1]);
            }

            mems.add(mem);
        }

        Instance[] instances = new Instance[labels.size()];
        for (int i = 0; i < instances.length; i++) {
            instances[i] = new Instance(labels.get(i), mems.get(i));
        }

        return instances;
    }

    public static void main () throws IOException{
        String path = "res\\experiment\\svmdata.walk.train";
        Instance[] res;
        res = readWalkFeature(path);
    }
}
