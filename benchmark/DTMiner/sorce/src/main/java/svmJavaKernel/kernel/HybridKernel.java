package svmJavaKernel.kernel;

import cn.edu.sjtu.gdr.utils.CollectionUtils;
import svmJavaKernel.libsvm.ex.Instance;
import svmJavaKernel.libsvm.svm_node;

import javax.print.DocFlavor;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Xie Yanping on 2015/8/6.
 */
public class HybridKernel implements CustomKernel, Serializable {
    @Override
    public double evaluate(svm_node x, svm_node y) {
        Object datax = x.data;
        Object datay = y.data;
        double score;
        //double score_vec = 0.0;
        //double score_amod = 0.0;
        double score_FB, score_B, score_BA, score_gc;
        double score_L, score_R, score_lc = 0.0;
        double score_walk = 0.0;
        double score_S = 0.0, score_Root = 0.0;
        double scorex = 0.0;
        double scorey = 0.0;

        if (!(datax instanceof ArrayList) || !(datay instanceof ArrayList)) {
            throw new IllegalArgumentException("svm_node does not contain walk data.");
        }

        ArrayList<String> hybridx = (ArrayList<String>) datax;
        ArrayList<String> hybridy = (ArrayList<String>) datay;
        ArrayList<String> temp;
        /*ArrayList<String> walkx = new ArrayList<>();
        ArrayList<String> walky = new ArrayList<>();
        Map<String, Integer> gcx = new HashMap<>();
        Map<String, Integer> gcy = new HashMap<>();*/
        Map<String, Integer> sx = new HashMap<>();
        Map<String, Integer> sy = new HashMap<>();
        Map<String, Integer> rootx = new HashMap<>();
        Map<String, Integer> rooty = new HashMap<>();
        int gcF,gcB,gcA;
        int lcL, lcR;
        int split;
        String tempString, key;
        int tempInt, value;

        //shortest dependency tree and root to LCA
        lcR = hybridx.indexOf("R|");
        split = hybridx.indexOf("S|");
        for(int m = lcR+1; m<split; ++m){
            tempString = hybridx.get(m);
            key = tempString.substring(0,tempString.indexOf("\t"));
            value = Integer.parseInt(tempString.substring(tempString.indexOf("\t") + 1));
            if(sx.containsKey(key))
                sx.put(key,sx.get(key)+value);
            else
                sx.put(key,value);
        }
        for(int m = split+1; m<hybridx.size(); ++m){
            tempString = hybridx.get(m);
            key = tempString.substring(0,tempString.indexOf("\t"));
            value = Integer.parseInt(tempString.substring(tempString.indexOf("\t") + 1));
            if(rootx.containsKey(key))
                rootx.put(key,sx.get(key)+value);
            else
                rootx.put(key,value);
        }
        lcR = hybridy.indexOf("R|");
        split = hybridy.indexOf("S|");
        for(int m = lcR+1; m<split; ++m){
            tempString = hybridy.get(m);
            key = tempString.substring(0,tempString.indexOf("\t"));
            value = Integer.parseInt(tempString.substring(tempString.indexOf("\t") + 1));
            if(sy.containsKey(key))
                sy.put(key,sy.get(key)+value);
            else
                sy.put(key,value);
        }
        for(int m = split+1; m<hybridy.size(); ++m){
            tempString = hybridy.get(m);
            key = tempString.substring(0,tempString.indexOf("\t"));
            value = Integer.parseInt(tempString.substring(tempString.indexOf("\t") + 1));
            if(rooty.containsKey(key))
                rooty.put(key,sy.get(key)+value);
            else
                rooty.put(key,value);
        }
        score_S = score(sx,sy);
        score_Root = score(rootx,rooty);

        //global feature
        /*//FBx
        gcF = hybridx.indexOf("F|");
        gcB = hybridx.indexOf("B|");
        for(int m=0; m<gcF; ++m){
            tempString = hybridx.get(m);
            gcx.put(tempString.substring(0,tempString.indexOf("\t")),
                    Integer.parseInt(tempString.substring(tempString.indexOf("\t") + 1)));
        }
        for(int m=gcF+1; m<gcB; ++m){
            tempString = hybridx.get(m);
            gcx.put(tempString.substring(0,tempString.indexOf("\t")),
                    Integer.parseInt(tempString.substring(tempString.indexOf("\t")+1)));
        }
        //FBy
        gcF = hybridy.indexOf("F|");
        gcB = hybridy.indexOf("B|");
        for(int m=0; m<gcF; ++m){
            tempString = hybridy.get(m);
            gcy.put(tempString.substring(0,tempString.indexOf("\t")),
                    Integer.parseInt(tempString.substring(tempString.indexOf("\t") + 1)));
        }
        for(int m=gcF+1; m<gcB; ++m){
            tempString = hybridy.get(m);
            gcy.put(tempString.substring(0,tempString.indexOf("\t")),
                    Integer.parseInt(tempString.substring(tempString.indexOf("\t")+1)));
        }
        score_FB = score(gcx,gcy);

        //Bx
        gcx.clear();
        gcy.clear();
        gcF = hybridx.indexOf("F|");
        gcB = hybridx.indexOf("B|");
        for(int m=gcF+1; m<gcB; ++m){
            tempString = hybridx.get(m);
            gcx.put(tempString.substring(0,tempString.indexOf("\t")),
                    Integer.parseInt(tempString.substring(tempString.indexOf("\t")+1)));
        }
        //By
        gcF = hybridy.indexOf("F|");
        gcB = hybridy.indexOf("B|");
        for(int m=gcF+1; m<gcB; ++m){
            tempString = hybridy.get(m);
            gcy.put(tempString.substring(0,tempString.indexOf("\t")),
                    Integer.parseInt(tempString.substring(tempString.indexOf("\t")+1)));
        }
        score_B = score(gcx,gcy);

        //BAx
        gcx.clear();
        gcy.clear();
        gcF = hybridx.indexOf("F|");
        gcB = hybridx.indexOf("B|");
        gcA = hybridx.indexOf("A|");
        for(int m=gcF+1; m<gcB; ++m){
            tempString = hybridx.get(m);
            gcx.put(tempString.substring(0,tempString.indexOf("\t")),
                    Integer.parseInt(tempString.substring(tempString.indexOf("\t") + 1)));
        }
        for(int m=gcB+1; m<gcA; ++m){
            tempString = hybridx.get(m);
            gcx.put(tempString.substring(0,tempString.indexOf("\t")),
                    Integer.parseInt(tempString.substring(tempString.indexOf("\t")+1)));
        }
        //BAy
        gcF = hybridy.indexOf("F|");
        gcB = hybridy.indexOf("B|");
        gcA = hybridy.indexOf("A|");
        for(int m=gcF+1; m<gcB; ++m){
            tempString = hybridy.get(m);
            gcy.put(tempString.substring(0,tempString.indexOf("\t")),
                    Integer.parseInt(tempString.substring(tempString.indexOf("\t") + 1)));
        }
        for(int m=gcB+1; m<gcA; ++m){
            tempString = hybridy.get(m);
            gcy.put(tempString.substring(0,tempString.indexOf("\t")),
                    Integer.parseInt(tempString.substring(tempString.indexOf("\t")+1)));
        }
        score_BA = score(gcx,gcy);
        score_gc = score_FB + score_B + score_BA;*/

        Map<String, Integer> lx = new HashMap<>();
        Map<String, Integer> ly = new HashMap<>();
        Map<String, Integer> rx = new HashMap<>();
        Map<String, Integer> ry = new HashMap<>();
        gcA = hybridx.indexOf("A|");
        lcL = hybridx.indexOf("L|");
        lcR = hybridx.indexOf("R|");
        for(int i=gcA+1; i<lcL; i++){
        //for(int i=0; i<lcL; i++){
            tempString = hybridx.get(i);
            if(lx.containsKey(tempString))
                lx.put(tempString,lx.get(tempString)+1);
            else
                lx.put(tempString,1);
        }
        for(int i=lcL+1; i<lcR; i++){
            tempString = hybridx.get(i);
            if(rx.containsKey(tempString))
                rx.put(tempString,rx.get(tempString)+1);
            else
                rx.put(tempString,1);
        }
        //lcy
        gcA = hybridy.indexOf("A|");
        lcL = hybridy.indexOf("L|");
        lcR = hybridy.indexOf("R|");
        for(int i=gcA+1; i<lcL; i++){
        //for(int i=0; i<lcL; i++){
            tempString = hybridy.get(i);
            if(ly.containsKey(tempString))
                ly.put(tempString,ly.get(tempString)+1);
            else
                ly.put(tempString,1);
        }
        for(int i=lcL+1; i<lcR; i++){
            tempString = hybridy.get(i);
            if(ry.containsKey(tempString))
                ry.put(tempString,ry.get(tempString)+1);
            else
                ry.put(tempString,1);
        }


        score_L = score(lx,ly);
        score_R = score(rx,ry);
        score_lc = score_L+score_R;

        //score = 1.0*score_lc + 1.0*score_gc;
        score = 1.0*score_S+1.0*score_Root+1.0*score_lc;

        //walk feature
        /*for(int i = hybridx.indexOf("R|")+1; i < hybridx.size(); i++){
            //if(hybridx.get(i).equals("|")) break;
            walkx.add(hybridx.get(i));
        }
        for(int j = hybridy.indexOf("R|")+1; j< hybridy.size(); j++){
            //if(hybridy.get(j).equals("|")) break;
            walky.add(hybridy.get(j));
        }
        if(walkx.size() > walky.size()){
            temp = walky;
            walky = walkx;
            walkx = temp;
        }
        // get u from x
        int xsize = walkx.size();
        int ysize = walky.size();
        Map<String,Double> phiux = new HashMap<>();
        Map<String,Double> phiuy = new HashMap<>();
        for(int first=0; first<xsize-2; first++){ // p is set to 3
            for(int second=first+1; second<xsize-1; second=second+2){
                for(int third=second+1; third<xsize; third=third+2){
                    String u = walkx.get(first)+walkx.get(second)+walkx.get(third);
                    if (phiux.get(u) == null) {
                        phiux.put(u,0.0);
                        phiuy.put(u,0.0);
                    }
                    if((third-first+1)==3){
                        if(first%2==0)
                            phiux.put(u,phiux.get(u)+3.0);
                        else
                            phiux.put(u,phiux.get(u)+2.0);
                    }else{
                        phiux.put(u,phiux.get(u)+1.0);
                    }
                }
            }
        }

        // find u from y
        for(int first=0; first<ysize-2; first++){
            for(int second=first+1; second<ysize-1; second=second+2){
                for(int third=second+1; third<ysize; third=third+2){
                    String s = walky.get(first)+walky.get(second)+walky.get(third);
                    if (!phiuy.containsKey(s)) {
                        phiuy.put(s,0.0);
                    }
                    if((third-first+1)==3){
                        if(first%2==0)
                            phiuy.put(s,phiuy.get(s)+3.0);
                        else
                            phiuy.put(s,phiuy.get(s)+2.0);
                    }else{
                        phiuy.put(s,phiuy.get(s)+1.0);
                    }
                }
            }
        }

        Map.Entry<String, Double>[] entries = CollectionUtils.sortMap(phiux);
        for (Map.Entry<String, Double> entry : entries) {
            scorex += entry.getValue()*entry.getValue();
            //scorey += phiuy.get(entry.getKey())*phiuy.get(entry.getKey());
            score_walk += entry.getValue()*phiuy.get(entry.getKey());
        }
        entries = CollectionUtils.sortMap(phiuy);
        for(Map.Entry<String, Double> entry : entries){
            scorey += entry.getValue()*entry.getValue();
        }

        if(scorex !=0 && scorey !=0) {
            score_walk = score_walk / (Math.sqrt(scorex * scorey));
        }*/

        /*for(int m=0; m<amodx.size(); m=m+2){
            for(int n=0; n<amody.size(); n=n+2){
                if(amodx.get(m).equals(amody.get(n))){
                    if(amodx.get(m+1).equals(amody.get(n+1)))
                        score_amod += 4.0;
                    else score_amod += 2.0;
                }
            }
        }*/

        /*int k = 0;
        scorex = 0.0;
        scorey = 0.0;
        while (k < 12) {
            double e1 = Double.parseDouble(hybridx.get(k));
            double e2 = Double.parseDouble(hybridy.get(k));

            scorex += e1*e1;
            scorey += e2*e2;
            score_vec+= e1 * e2;
            k++;
        }
        if(scorex !=0 && scorey !=0) {
            //score_vec = score_vec / (Math.sqrt(scorex * scorey));
        }*/

        //score = 1.0*score_walk + 1.0*score_gc + 1.0*score_lc;
        return score;
    }

    private double score(Map<String,Integer> mapx, Map<String,Integer> mapy ){
        double score = 0.0;
        double scorex = 0.0;
        double scorey = 0.0;
        for(String str : mapx.keySet()){
            //scorex += mapx.get(str)*mapx.get(str);
            scorex += 1;
            if(mapy.containsKey(str))
                //score += mapx.get(str)*mapy.get(str);
                score += 1;
        }
        for(String str : mapy.keySet()){
            scorey += 1;
            //scorey += mapy.get(str)*mapy.get(str);
        }

        if(scorex!=0 && scorey!=0)
            score /= Math.sqrt(scorex*scorey);
        return score;
    }

    private double score(List<String> listx, List<String> listy){
        double score = 0.0;
        double scorex = 0.0;
        double scorey = 0.0;
        String x,y;
        for(int i=0; i<listx.size(); ++i){
            x = listx.get(i);
            y = listy.get(i);
            if(!x.equals("")){
                scorex += 1;
                if(x.equals(y))
                    score += 1;
            }
            if(!y.equals("")){
                scorey += 1;
            }
        }
        if(scorex != 0 && scorey != 0)
            score /= Math.sqrt(scorex*scorey);
        return score;
    }
}
