package svmJavaKernel.kernel;

import cn.edu.sjtu.gdr.utils.CollectionUtils;
import svmJavaKernel.libsvm.svm_node;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Xie Yanping on 2015/7/29.
 */
public class WalkKernel implements CustomKernel, Serializable {
    @Override
    public double evaluate(svm_node x, svm_node y) {
        Object pathx = x.data;
        Object pathy = y.data;
        double score = 0.0;
        double scorex = 0.0;
        double scorey = 0.0;

        if (!(pathx instanceof Map) || !(pathy instanceof Map)) {
            throw new IllegalArgumentException("svm_node does not contain walk data.");
        }

        Map<Integer,String> walkx = (Map<Integer,String>) pathx;
        Map<Integer,String> walky = (Map<Integer,String>) pathy;
        Map<Integer,String> temp;
        if(walkx.size() > walky.size()){
            temp = walky;
            walky = walkx;
            walkx = temp;
        }
        int xsize = walkx.size();
        int ysize = walky.size();

        // get u from x
        Map<String,Double> phiux = new HashMap<>();
        Map<String,Double> phiuy = new HashMap<>();
        for(int first=1; first<=xsize-2; first++){ // p is set to 3
            for(int second=first+1; second<=xsize-1; second=second+2){
                for(int third=second+1; third<=xsize; third=third+2){
                    String u = walkx.get(first)+walkx.get(second)+walkx.get(third);
                    if (phiux.get(u) == null) {
                        phiuy.put(u,0.0);
                        if((third-first+1)==3){
                            if(first%2==0)
                                phiux.put(u,3.0);
                            else
                                phiux.put(u,2.0);
                        }else{
                            phiux.put(u,1.0);
                        }
                    }else{
                        if((third-first+1)==3){
                            if(first%2==0)
                                phiux.put(u,phiux.get(u)+3.0);
                            else
                                phiux.put(u,phiux.get(u)+2.0);
                        }else{
                            phiux.put(u,phiux.get(u)+1.0);
                        }
                    }
                }
            }
        }

        // find u from y
        for(int first=1; first<=ysize-2; first++){
            for(int second=first+1; second<=ysize-1; second=second+2){
                for(int third=second+1; third<=ysize; third=third+2){
                    String s = walky.get(first)+walky.get(second)+walky.get(third);
                    if (phiuy.containsKey(s)) {
                        if((third-first+1)==3){
                            if(first%2==0)
                                phiuy.put(s,phiuy.get(s)+3.0);
                            else
                                phiuy.put(s,phiuy.get(s)+2.0);
                        }else{
                            phiuy.put(s,phiuy.get(s)+1.0);
                        }
                    }
                }
            }
        }

        Map.Entry<String, Double>[] entries = CollectionUtils.sortMap(phiux);
        for (Map.Entry<String, Double> entry : entries) {
            scorex += entry.getValue()*entry.getValue();
            scorey += phiuy.get(entry.getKey())*phiuy.get(entry.getKey());
            score += entry.getValue()*phiuy.get(entry.getKey());
        }
        if(scorex !=0 && scorey !=0) {
            //score = score / (Math.sqrt(scorex * scorey));
        }
        return score;
    }
}
