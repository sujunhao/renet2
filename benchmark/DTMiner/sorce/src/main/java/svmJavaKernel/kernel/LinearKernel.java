package svmJavaKernel.kernel;

import svmJavaKernel.common.SparseVector;
import svmJavaKernel.libsvm.svm_node;
import java.io.Serializable;

/**
 *  <code>LinearKernel</code> implements a linear kernel function.
 * @author Syeed Ibn Faiz
 */
public class LinearKernel implements CustomKernel, Serializable {

    @Override
    public double evaluate(svm_node x, svm_node y) {                        
        if (!(x.data instanceof SparseVector) || !(y.data instanceof SparseVector)) {
            throw new RuntimeException("Could not find sparse vectors in svm_nodes");
        }        
        SparseVector v1 = (SparseVector) x.data;
        SparseVector v2 = (SparseVector) y.data;

        double score = v1.dot(v2);
        score = score / Math.sqrt(v1.dot(v1)*v2.dot(v2));
        return score;
    }    
}
