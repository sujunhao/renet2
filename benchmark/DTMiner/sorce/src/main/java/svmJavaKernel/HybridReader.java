package svmJavaKernel;

import svmJavaKernel.libsvm.ex.Instance;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Xie Yanping on 2015/8/6.
 */
public class HybridReader {
    public static Instance[] readFeature(String hybridFileName, boolean hasLabel) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(hybridFileName));

        ArrayList<Double> labels = new ArrayList<Double>();
        //ArrayList<Map<Integer,String>> mems = new ArrayList<Map<Integer,String>>();
        ArrayList<ArrayList<String>> features = new ArrayList<>();

        String line;
        int lineCount = 0;
        while ((line = reader.readLine()) != null) {
            lineCount++;
            String[] tokens = line.split("\\t\\t");
            if (tokens.length < 2) {
                System.err.println("Inappropriate file format: " + hybridFileName);
                System.err.println("Error in line " + lineCount);
                System.exit(-1);
            }

            if(hasLabel)
                labels.add(Double.parseDouble(tokens[0]));
            else labels.add(0.0);
            ArrayList<String> feature = new ArrayList<>();

            int i = hasLabel? 1:0;
            for (; i < tokens.length; i++) {
                String f = tokens[i].substring(tokens[i].indexOf(":")+1);
                //String[] fields = tokens[i].split(":\\$:");
                /*if (f.length() < 1) {
                    System.err.println("Inappropriate file format: " + hybridFileName);
                    System.err.println("Error in line " + lineCount);
                    System.exit(-1);
                }*/
                feature.add(f);
            }
            features.add(feature);
        }
        Instance[] instances = new Instance[labels.size()];
        for (int i = 0; i < instances.length; i++) {
            instances[i] = new Instance(labels.get(i), features.get(i));
        }
        return instances;
    }

    public static Instance readFeature(String featureStr) throws IOException {

        String[] tokens = featureStr.split("\\t\\t");
        if (tokens.length < 2) {
            System.err.println("token<2 "+featureStr);
            System.exit(-1);
        }

        ArrayList<String> feature = new ArrayList<>();
        int i = 1;
        for (; i < tokens.length; i++) {
            String f = tokens[i].substring(tokens[i].indexOf(":")+1);
            //String[] fields = tokens[i].split(":\\$:");
            feature.add(f);
        }
        return new Instance(0.0, feature);
    }

    public static void main () throws IOException{
        String path = "res\\experiment\\svmdata.hybrid.test";
        Instance[] res;
        res = readFeature(path,true);
    }
}
