package bottomLevel.function;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.utils.GgrLogger;

public class DirectedGraphReaderForPageRank {

	private String path;

	public DirectedGraphReaderForPageRank(String path) {
		this.path = path;
	}

	public Map<Integer, Map<Integer, Integer>> read() {
		Map<Integer, Map<Integer, Integer>> graph = new TreeMap<Integer, Map<Integer, Integer>>();

		Function<Integer, Integer> ensureNode = new Function<Integer, Integer>() {
			@Override
			public Integer apply(Integer nodeId) {
				if (!graph.containsKey(nodeId)) {
					graph.put(nodeId, new TreeMap<Integer, Integer>());
				}
				return nodeId;
			}
		};

		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			String str = null, edges[] = null;
			int fromId = 0, toId = 0, count = 0, pos = 0;

			while ((str = br.readLine()) != null) {
				fromId = Integer.parseInt(str.substring("node: ".length()));
				ensureNode.apply(fromId);
				br.readLine(); // skip in edges
				str = br.readLine(); // read out edges
				if (!str.equals("")) {
					edges = str.split("\t");
					for (String edge : edges) {
						pos = edge.lastIndexOf('$');
						toId = Integer.parseInt(edge.substring(0, pos));
						ensureNode.apply(toId);
						graph.get(fromId).put(toId,
								Integer.parseInt(edge.substring(pos + 1)));
					}
				}

				++count;
				if ((count & 0xffff) == 0) {
					GgrLogger.log((count >> 10) + "K node processed...");
				}
			}

			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return graph;
	}

	public static void main(String[] argv) {
		@SuppressWarnings("unused")
		Map<Integer, Map<Integer, Integer>> graph = null;
		GgrLogger.log("start to read..");
		String path = ConfigManager
				.getConfig("graph-memory", "TEST_GRAPH_PATH");
		graph = new DirectedGraphReaderForPageRank(path).read();
		GgrLogger.log("finish to read...");

		System.out.println("totalMemory: the total amount of memory in the "
				+ "Java virtual machine.\n"
				+ (Runtime.getRuntime().totalMemory() >> 20) + "MB");

		System.out.println("maxMemory: the maximum amount of memory that the "
				+ "Java virtual machine will attempt to use.\n"
				+ (Runtime.getRuntime().maxMemory() >> 20) + "MB");

		System.out.println("freeMemory: the amount of free memory in the Java "
				+ "Virtual Machine.\n"
				+ (Runtime.getRuntime().freeMemory() >> 20) + "MB");

		System.out.println();
	}

}
