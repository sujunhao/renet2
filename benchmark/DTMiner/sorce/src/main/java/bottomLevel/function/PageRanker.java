package bottomLevel.function;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import cn.edu.sjtu.gdr.utils.GgrLogger;

public class PageRanker<T> {
	final double teleportFactor = 0.2;
	double convergenceError;

	private Map<T, Map<T, Double>> linkGraph;
	private Map<T, Double> pagesRank = new HashMap<T, Double>(1000000);

	public PageRanker(Map<T, Map<T, Integer>> linkGraph, double convergenceError) {
		this.linkGraph = normalizeGraph(linkGraph);
		this.convergenceError = convergenceError;
		calcuPageRank();
	}

	public Map<T, Double> getPagesRank() {
		return pagesRank;
	}

	private Map<T, Map<T, Double>> normalizeGraph(
			Map<T, Map<T, Integer>> linkGraph2) {
		Map<T, Map<T, Double>> normalizedGraph = new TreeMap<T, Map<T, Double>>();
		for (T index1 : linkGraph2.keySet()) {
			normalizedGraph.put(index1, new TreeMap<T, Double>());
			int totalWeight = 0;
			for (T index2 : linkGraph2.get(index1).keySet()) {
				totalWeight += linkGraph2.get(index1).get(index2);
			}
			for (T index2 : linkGraph2.get(index1).keySet()) {
				normalizedGraph.get(index1).put(
						index2,
						(double) linkGraph2.get(index1).get(index2)
								/ totalWeight);
			}
		}
		return normalizedGraph;
	}

	private void calcuPageRank() {
		for (T index1 : linkGraph.keySet()) {
			pagesRank.put(index1, (double) 0);
			for (T index2 : linkGraph.get(index1).keySet()) {
				pagesRank.put(index2, (double) 0);
			}
		}
		for (T word : pagesRank.keySet()) {
			pagesRank.put(word, 1.0 / pagesRank.keySet().size());
		}

		Map<T, Double> newPagesRank = new HashMap<T, Double>(pagesRank.size());
		Map<T, Double> temp = null;
		for (int iteration = 1; iteration <= 1000; ++iteration) {
			GgrLogger.log("iteration " + iteration + " start!");

			for (T page : pagesRank.keySet()) {
				newPagesRank.put(page, 0.0);
			}
			double commonIncre = 0;
			for (T thisPage : linkGraph.keySet()) {
				Map<T, Double> nextPages = linkGraph.get(thisPage);
				if (nextPages.isEmpty()) {
					commonIncre += pagesRank.get(thisPage);
					continue;
				}
				for (T nextPage : nextPages.keySet()) {
					double increment = nextPages.get(nextPage)
							* pagesRank.get(thisPage);
					newPagesRank.put(nextPage, newPagesRank.get(nextPage)
							+ increment);
				}
			}
			for (T page : newPagesRank.keySet()) {
				newPagesRank.put(page, newPagesRank.get(page) + commonIncre
						/ pagesRank.size());
			}
			for (T page : newPagesRank.keySet()) {
				newPagesRank.put(page,
						newPagesRank.get(page) * (1 - teleportFactor)
								+ (teleportFactor / newPagesRank.size()));
			}
			GgrLogger.log("see whether converge! ");
			boolean endFlag = true;
			for (T page : pagesRank.keySet()) {
				double diff = Math.abs(newPagesRank.get(page)
						- pagesRank.get(page));
				if (diff > convergenceError / pagesRank.size()) {
					endFlag = false;
					break;
				}
			}
			GgrLogger.log("iteration " + iteration + " is done!");

			temp = pagesRank;
			pagesRank = newPagesRank;
			newPagesRank = temp;
			newPagesRank.clear();

			if (endFlag) {
				break;
			}
		}
	}
}