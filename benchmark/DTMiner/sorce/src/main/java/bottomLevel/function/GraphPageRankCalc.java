package bottomLevel.function;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

import cn.edu.sjtu.gdr.config.ConfigManager;
import cn.edu.sjtu.gdr.utils.GgrLogger;

public class GraphPageRankCalc {

	public static void main(String[] args) throws IOException {
		String inpath = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"pagerank", "inpath");
		String outpath = ConfigManager.getConfig(ConfigManager.MAIN_CONFIG,
				"pagerank", "outpath");
		GgrLogger.log("start reading graph...");
		Map<Integer, Map<Integer, Integer>> graph = new DirectedGraphReaderForPageRank(
				inpath).read();
		GgrLogger.log("finish reading graph...");
		
		GgrLogger.log("start calculating page rank");
		Map<Integer, Double> pr = new PageRanker<Integer>(graph, 1.0).getPagesRank();
		GgrLogger.log("finish calculating page rank");
		
		GgrLogger.log("start outputing..");
		BufferedWriter bw = new BufferedWriter(new FileWriter(outpath));
		
		for (Map.Entry<Integer, Double> entry : pr.entrySet()) {
			bw.write(entry.getKey() + "\t" + entry.getValue());
			bw.newLine();
		}
		
		bw.flush();
		bw.close();
		GgrLogger.log("finish outputing");
	}

}
